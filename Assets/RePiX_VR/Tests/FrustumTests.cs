﻿using OmiLAXR.Adapters.SteamVR;
using NUnit.Framework;
using UnityEngine;

using OmiLAXR.Interaction;

using Valve.VR;

namespace RePiX_VR.Tests
{
    public class FrustumTests
    {
        private Frustum _frustum;
        private FrustumPlaneInteraction _farPlane;
        private FrustumPlaneInteraction _nearPlane;
        private SteamVR_ImprovedLaserPointer _laserPointer;
        private LaserPointerHandler _laserPointerHandler;
        [SetUp]
        public void Setup() 
        {
            // init interaction handler
            _laserPointerHandler = new GameObject("InteractionHandler").AddComponent<LaserPointerHandler>();

            // init right hand
            var rightHand = new GameObject("RightHand");
            var pose = rightHand.AddComponent<SteamVR_Behaviour_Pose>();
            pose.poseAction = SteamVR_Input.GetAction<SteamVR_Action_Pose>("Pose");
            pose.inputSource = SteamVR_Input_Sources.RightHand;

            _laserPointer = rightHand.AddComponent<SteamVR_ImprovedLaserPointer>();
            _laserPointer.pose = pose;
           

            // Initialize light frustum
            var frustumGo = new GameObject("Frustum");
            var farPlaneGo = new GameObject("Far Plane");
            var nearPlaneGo = new GameObject("Near Plane");

            frustumGo.transform.position = Vector3.zero;

            farPlaneGo.transform.SetParent(frustumGo.transform);
            nearPlaneGo.transform.SetParent(frustumGo.transform);

            
            _frustum = frustumGo.AddComponent<Frustum>();
            _farPlane = farPlaneGo.AddComponent<FrustumPlaneInteraction>();
            _nearPlane = nearPlaneGo.AddComponent<FrustumPlaneInteraction>();
        }
        // A Test behaves as an ordinary method
        public void PointableInteractionTest()
        {
            //planeInteraction.GetPlaneTouchPoint()
            
        }
    }
}
