﻿using NUnit.Framework;
using RePiX_VR.GeometryStage;
using UnityEngine;

namespace RePiX_VR.Tests
{
    public class VertexManagerTestScript
    {
        private VertexManager vertexManager;

        [SetUp]
        public void Setup()
        {
            // Create and configure VM
            var vmGO = new GameObject("VertexManager");
            vertexManager = vmGO.AddComponent<VertexManager>();
        }

        [Test]
        public void CreateVertexObjectTest()
        {
            // Create vertex
            var vertex = vertexManager.CreateVertexObject(Vector3.forward, Color.red);
            var mr = vertex.GetComponent<MeshRenderer>();

            Assert.AreEqual(vertex.transform.position, Vector3.forward);
            Assert.AreEqual("IgnorePointer", vertex.tag);
            Assert.AreEqual(mr.material.GetColor("_Color"), Color.red);
        }
    }
}
