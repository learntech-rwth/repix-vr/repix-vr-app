﻿using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Class collecting multiple clickable Objects, making sure only one of them can be selected at a given time.
    /// </summary>
    public class RadioGroup : MonoBehaviour
    {
        [SerializeField]
        private List<RadioButton> buttons = new List<RadioButton>();
        private RadioButton current;

        public Material selectedMat, unselectedMat;

        private void Awake()
        {
            var first = true;
            
            foreach (var b in buttons)
            {
                b.AddClickListener(() =>
                {
                    if (b == current) return;

                    if (current)
                    {
                        current.Deselect();
                        current.GetComponent<Renderer>().material = unselectedMat;
                    }
                    
                    b.Select();

                    b.GetComponent<Renderer>().material = selectedMat;
                    current = b;
                });

                b.GetComponent<Renderer>().material = first ? selectedMat : unselectedMat;

                if (!first) continue;
                
                b.Select();
                current = b;
                first = false;
            }
        }
    }
}
