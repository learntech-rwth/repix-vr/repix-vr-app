﻿using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RePiX_VR
{
    [RequireComponent(typeof(Slider))]
    public class AspectSlider : MonoBehaviour
    {
        [Tooltip("Frustum instance to apply on.")]
        public Frustum frustum;

        public float maxAspect = Frustum.Aspect_Max;
        public float minAspect = Frustum.Aspect_Min;

        public UnityEvent onValueChanged = new UnityEvent();


        private Slider slider;
        private TMP_Text valueText;
        private TMP_Text minText;
        private TMP_Text maxText;

        private void Awake()
        {
            slider = GetComponent<Slider>();
            slider.minValue = minAspect;
            slider.maxValue = maxAspect;
            slider.value = frustum.aspect;
            slider.onValueChanged.AddListener(SliderChanged);

            var texts = GetComponentsInChildren<TMP_Text>();
            valueText = texts.FirstOrDefault(t => t.name.Contains("value"));
            minText = texts.FirstOrDefault(t => t.name.Contains("min"));
            maxText = texts.FirstOrDefault(t => t.name.Contains("max"));

            if (minText)
                minText.text = minAspect.ToString(CultureInfo.CurrentCulture);
            if (maxText)
                maxText.text = maxAspect.ToString(CultureInfo.CurrentCulture);
            if (valueText)
                valueText.text = "Aspect: " + slider.value.ToString(CultureInfo.CurrentCulture);
        }

        private void SliderChanged(float value)
        {
            // calc the actual aspect value
            var aspect = value;

            if (valueText)
                valueText.text = "Aspect: " + aspect;
            frustum.aspect = aspect;
            frustum.FireChange(this);
            onValueChanged.Invoke();
        }

        public void SetValue(float value, bool withoutNotify = false)
        {
            if (withoutNotify)
                slider.SetValueWithoutNotify(value);
            else
                slider.value = value;
        }
    }
}