﻿using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Helper struct to hold frustum values.
    /// </summary>
    public struct FrustumValues
    {
        public float top;
        public float bottom;
        public float left;
        public float right;

        public FrustumValues(float z, float fovInDeg, float aspect)
        {
            top = z * Mathf.Tan((fovInDeg * Mathf.Deg2Rad) / 2.0f);
            bottom = -top;
            right = aspect * top;
            left = -right;
        }
    }
}