﻿using OmiLAXR.Adapters.SteamVR;
using UnityEngine;

using OmiLAXR.Extensions;
using OmiLAXR.Interaction;
using OmiLAXR.xAPI.Tracking;

namespace RePiX_VR
{
    [RequireComponent(typeof(FrustumPlaneInteraction))]
    public class FovGripInteraction : MonoBehaviour
    {
        private const string FovGripsName = "FOV Grips";
        private Frustum frustum;
        private GameObject fovGrips;

        private FrustumPlaneInteraction fpInteraction;

        public float scale = 0.02f;

        private Vector3 startDragPosition;
        private float startFov;
        private Vector3 normalDirection;
        private GameObject touchPoint;

        private void Start()
        {
            frustum = GetComponentInParent<Frustum>();
            fpInteraction = GetComponent<FrustumPlaneInteraction>();

            frustum.OnChanges += OnFrustumChanges;

            // Create if not exists FovGrips
            fovGrips = gameObject.Find(FovGripsName);
            if (!fovGrips)
            {
                // Create grips holder
                fovGrips = new GameObject(FovGripsName);
                fovGrips.transform.SetParent(transform);
            }

            // Create only if has no childs
            if (fovGrips.transform.childCount < 1)
            {
                // Create grips
                var qc = fpInteraction.GetCorners();
                // Top right
                CreateGrip(qc.topRight, fovGrips.transform);
                // Top left
                CreateGrip(qc.topLeft, fovGrips.transform);
                // Bottom right
                CreateGrip(qc.bottomRight, fovGrips.transform);
                // Bottom left
                CreateGrip(qc.bottomLeft, fovGrips.transform);
            }

            fovGrips.transform.localPosition = Vector3.zero;

            // Init Touchpoint
            touchPoint = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            touchPoint.SetActive(false);
            touchPoint.transform.localScale = Vector3.one * scale;
            touchPoint.transform.SetParent(transform);
        }

        private void OnFrustumChanges(object sender)
        {
            // Update grip positions
            var qc = fpInteraction.GetCorners();
            var positions = new[] {qc.topLeft, qc.topRight, qc.bottomRight, qc.bottomLeft};
            for (var i = 0; i < fovGrips.transform.childCount; i++)
            {
                var grip = fovGrips.transform.GetChild(i);
                grip.localPosition = positions[i];
            }
        }

        private void CreateGrip(Vector3 localPosition, Transform parent)
        {
            var grip = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            grip.name = "FOV Grip";

            // Attach to position
            grip.transform.SetParent(parent);
            grip.transform.localScale = Vector3.one * scale;
            grip.transform.localPosition = localPosition;

            grip.AddComponent<Trackable>();

            // Enable interaction
            var pointable = grip.AddComponent<Pointable>();
            pointable.allowClick = fpInteraction.allowClick;
            pointable.allowHover = fpInteraction.allowHover;
            pointable.allowSelection = fpInteraction.allowSelection;
            pointable.reactsOnLeftPointer = fpInteraction.reactsOnLeftPointer;
            pointable.reactsOnRightPointer = fpInteraction.reactsOnRightPointer;

            // Set hover and select material
            pointable.selectMaterial = fpInteraction.selectMaterial;
            pointable.hoverMaterial = fpInteraction.hoverMaterial;

            pointable.onPressed.AddListener(OnBeginDrag);
            pointable.onIsHolding.AddListener(OnDragging);
            pointable.onReleased.AddListener(OnEndDrag);
        }

        private void OnEndDrag(LaserPointerEventArgs e)
        {
            touchPoint.SetActive(false);
        }

        private void OnBeginDrag(LaserPointerEventArgs e)
        {
            var position = e.Target.position;
            // Save start values
            startDragPosition = position;
            startFov = frustum.fov;

            // Create normalized direction
            var planeCenter = new Vector3(0, 0, fpInteraction.GetZ());
            var direction = position - planeCenter;
            normalDirection = direction.normalized;

            // Enable touchpoint
            touchPoint.SetActive(true);
        }

        private void OnDragging(LaserPointerEventArgs e)
        {
            // Update touchpoint
            if (!fpInteraction.GetPlaneTouchPoint((SteamVR_ImprovedLaserPointer) e.Sender, out var position))
                return;

            // Get dragging direction
            var dir = position - startDragPosition;

            // get scaling factor
            var s = Vector3.Dot(dir, normalDirection);

            touchPoint.transform.position = startDragPosition + fpInteraction.position + normalDirection * s;

            // Update FOV
            frustum.fov = startFov + s;
            frustum.FireChange(this);
        }
    }

}