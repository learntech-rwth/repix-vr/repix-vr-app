﻿using OmiLAXR.Adapters.SteamVR;
using OmiLAXR.Interaction;
using UnityEngine;
using UnityEngine.Events;

using TMPro;

using OmiLAXR.Utils;

using xAPI.Registry;
using OmiLAXR.xAPI.Tracking;

namespace RePiX_VR
{
    public class FrustumPlaneInteraction : Pointable
    {
        [Tooltip("This event get released if the plane get released.")]
        public UnityEvent OnReleased;

        // Frustum reference
        private Frustum frustum;
        // Flag to store if it is a far or near plane
        private bool isFarPlane = false;
        // Label game object reference
        public GameObject label;
        // TextMesh reference to change label immediatly.
        private TMP_Text textMeshRef;

        /// <summary>
        /// 3D position of plane.
        /// </summary>
        public Vector3 position => transform.position + new Vector3(0, 0, GetZ());
        public QuadCorners GetCorners() => isFarPlane ? frustum.GetFarPlaneCorners() : frustum.GetNearPlaneCorners();
        public float GetZ() => isFarPlane ? frustum.farZ : frustum.nearZ;

        protected new void Start()
        {
            base.Start();
            // Get frustum information
            frustum = GetComponentInParent<Frustum>();
            isFarPlane = this.name == "Far Plane";
            // Set custom xAPI Statement Provider
            var trackable = GetComponent<Trackable>();
            if (trackable)
            {
                trackable.OnLaserInteraction += OnLaserInteraction;
            }
            // Init plane events
            //onPointerIn.AddListener(_ => label.SetActive(true));
            //onPointerOut.AddListener(_ => label.SetActive(false));
            onIsHolding.AddListener(PointerIsHolding);
            onReleased.AddListener(_ => OnReleased?.Invoke());
            // Init label game object
            InitLabel();
        }

        private void OnLaserInteraction(object controller, Trackable trackable, LaserPointerEventArgs e)
        {
            LrsController.Instance.SendStatement(
                verb: xAPI_Definitions.virtualReality.verbs.moved,
                activity: trackable.ObjectsActivity,
                extensions: new xAPI_Extensions
                {
                    xAPI_Definitions.virtualReality.extensions.activity.vrObjectName(trackable.GetTrackingName()),
                    xAPI_Definitions.seriousGames.extensions.result.position("{Z: " + GetZ() + "}")
                }
            );
        }

        private void InitLabel()
        {
            textMeshRef = label.transform.GetComponentInChildren<TMP_Text>();
            UpdateLabelText();
        }
        
        private void SetZ(float z)
        {
            if (z > -0.1f) z = -0.1f;
            
            var oldZ = isFarPlane ? frustum.farZ : frustum.nearZ;
            var oldZInt = (int)(oldZ * 10);

            var zInt = (int)(z * 10.0f);

            if (isFarPlane)
                frustum.farZ = frustum.nearZ < z ? frustum.nearZ : zInt / 10.0f;
            else
                frustum.nearZ = zInt / 10.0f;

            frustum.FireChange(zInt != oldZInt);
        }
        public void UpdateLabelText()
        {
            var z = GetZ();
            textMeshRef.text = (isFarPlane ? "Far" : "Near") + " Z: " + string.Format("{0:0.0}", z);
            var pos = label.transform.localPosition;
            pos.z = z;
            label.transform.localPosition = pos;
        }

        private void PointerIsHolding(LaserPointerEventArgs e)
        {
            if (!allowClick)
                return;

            var laser = (SteamVR_ImprovedLaserPointer) e.Sender;
            if (!GetForwardTouchPoint(laser, out var touchPoint))
                return;
            
            // Transform Touch Point of Plane into local space
            var localPoint = frustum.transform.InverseTransformPoint(touchPoint);

            // Adjust z
            var z = localPoint.z;
           
            // Send information
            SetZ(z);
            UpdateLabelText();
        }

        public bool GetPlaneTouchPoint(SteamVR_ImprovedLaserPointer laser, out Vector3 touchPoint)
            => laser.ProjectOnPlane(frustum.right, frustum.up, planePosition: position, out touchPoint);
        public bool GetForwardTouchPoint(SteamVR_ImprovedLaserPointer laser, out Vector3 touchPoint)
            => laser.ProjectOnPlane(frustum.direction, frustum.up, planePosition: position, out touchPoint);

        public void SetAllowFlags(bool flag) => allowSelection = allowClick = allowHover = flag;
    }
}