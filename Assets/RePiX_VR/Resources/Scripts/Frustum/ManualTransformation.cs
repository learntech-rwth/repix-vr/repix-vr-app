﻿using System.Linq;
using UnityEngine;

namespace RePiX_VR
{
    public class ManualTransformation : MonoBehaviour
    {
        public Matrix4x4 TransformationMatrix = Matrix4x4.identity;

        private Mesh meshReference;
        private Vector4[] meshVertices = null;

        public bool runInUpdate = false;

        private MeshCollider meshCollider;

        protected virtual void Start()
        {
            InitTransformation();
        }

        private void InitTransformation()
        {
            // Save all mesh components
            var meshFilter = GetComponentInChildren<MeshFilter>();
            meshReference = meshFilter.mesh;

            var vertices = meshReference.vertices;
            meshVertices = vertices.Select(v => new Vector4(v.x, v.y, v.z, 1.0f)).ToArray();

            meshCollider = GetComponent<MeshCollider>();
        }


        public virtual Vector3 TransformationFunction(Matrix4x4 T, Vector4 v) => T * v;

        public void UpdateVertices()
        {
            if (meshVertices == null)
                return;

            var vertices = new Vector3[meshVertices.Length];
            for (var i = 0; i < meshVertices.Length; i++)
            {
                vertices[i] = TransformationFunction(TransformationMatrix, meshVertices[i]);
            }

            meshReference.vertices = vertices;
        }

        public void RecalculateMeshProperties()
        {
            // no effect
            //meshReference.RecalculateBounds();
            // meshReference.RecalculateNormals();
            if (!meshCollider)
                return;
            meshCollider.sharedMesh = meshReference;
        }

        private void Update()
        {
            if (runInUpdate)
                UpdateVertices();
        }
    }

}