﻿using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RePiX_VR
{
    [RequireComponent(typeof(Slider))]
    public class FovSlider : MonoBehaviour
    {
        public Frustum frustum;
        public float maxFov = Frustum.Fov_Max;
        public float minFov = Frustum.Fov_Min;

        public UnityEvent onValueChanged = new UnityEvent();

        private Slider slider;

        private TMP_Text valueText;
        private TMP_Text minText;
        private TMP_Text maxText;

        // Start is called before the first frame update
        private void Awake()
        {
            slider = GetComponent<Slider>();
            slider.maxValue = maxFov;
            slider.minValue = minFov;
            slider.value = frustum.fov;
            slider.onValueChanged.AddListener(SliderChanged);

            var texts = GetComponentsInChildren<TMP_Text>();
            valueText = texts.FirstOrDefault(t => t.name.Contains("value"));
            minText = texts.FirstOrDefault(t => t.name.Contains("min"));
            maxText = texts.FirstOrDefault(t => t.name.Contains("max"));

            if (minText)
                minText.text = minFov.ToString(CultureInfo.CurrentCulture);
            if (maxText)
                maxText.text = maxFov.ToString(CultureInfo.CurrentCulture);
            if (valueText)
                valueText.text = "FOV: " + slider.value.ToString(CultureInfo.CurrentCulture);
        }

        private void SliderChanged(float value)
        {
            // Calc the actual fov value
            var fov = value;

            slider.value = fov;
            if (valueText)
                valueText.text = "FOV: " + fov;
            frustum.fov = fov;
            frustum.FireChange(this);
            onValueChanged.Invoke();
        }

        public void SetValue(float value, bool withoutNotify = false)
        {
            if (withoutNotify)
                slider.SetValueWithoutNotify(value);
            else
                slider.value = value;
        }
    }
}