﻿using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Represents a quad holding four corners (top,left,right,right).
    /// </summary>
    public struct QuadCorners
    {
        public Vector3 topLeft;
        public Vector3 topRight;
        public Vector3 bottomLeft;
        public Vector3 bottomRight;

        /// <summary>
        /// Get corners in clockwise order.
        /// </summary>
        /// <returns>[topLeft,topRight,bottomRight,bottomLeft]</returns>
        public Vector3[] GetVerticesClockwise()
        {
            return new [] {
                topLeft,
                topRight,
                bottomRight,
                bottomLeft
            };
        }

        /// <summary>
        /// Get corners in counter clockwise order.
        /// </summary>
        /// <returns>[bottomLeft,bottomRight,topRight,topLeft]</returns>
        public Vector3[] GetVerticesCounterClockwise()
        {
            return new [] {
                bottomLeft,
                bottomRight,
                topRight,
                topLeft
            };
        }
    }
}