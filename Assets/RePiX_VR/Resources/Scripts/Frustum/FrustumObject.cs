﻿using UnityEngine;

namespace RePiX_VR
{
    [DisallowMultipleComponent]
    public class FrustumObject : MonoBehaviour
    {
        [Tooltip("Material which will be placed for clipping objects.")]
        public Material clippingMaterial;

        public Vector3 unitCubeCenter = Vector3.zero;
        public bool doClipping = false;

        private Material originalMaterial;

        private void Awake()
        {
            clippingMaterial = Instantiate(Resources.Load<Material>(@"Materials/FrustumObject"));
            clippingMaterial.name = "FrustumObject";
        }

        /// <summary>
        /// Add clipping material to all mesh renderes.
        /// </summary>
        void OnEnable()
        {
            var mr = GetComponent<MeshRenderer>();
            originalMaterial = mr.material;

            clippingMaterial.color = originalMaterial.color;

            mr.material = clippingMaterial;
        }

        /// <summary>
        /// Remove clipping material from all mesh renderers.
        /// </summary>
        void OnDisable()
        {
            var mr = GetComponent<MeshRenderer>();
            mr.material = originalMaterial;
        }

        private void OnDestroy()
        {
            Destroy(clippingMaterial);
            originalMaterial = null;
        }

        private void Update()
        {
            clippingMaterial.SetVector("_Center", unitCubeCenter);
            clippingMaterial.SetFloat("_DoClipping", doClipping ? 1.0f : 0.0f);
        }
    }
}