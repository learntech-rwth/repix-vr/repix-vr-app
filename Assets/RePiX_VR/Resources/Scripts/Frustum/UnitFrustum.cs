﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using OmiLAXR;
using OmiLAXR.Extensions;
using OmiLAXR.UI;
using OmiLAXR.Utils;
using OmiLAXR.xAPI.Tracking;
using xAPI.Registry;
using Valve.VR;

namespace RePiX_VR
{
    //[ExecuteInEditMode]
    public class UnitFrustum : MonoBehaviour
    {
        public const string UnitFrustumName = "Unit Frustum";
        public SteamVR_Action_Boolean clippingOnOffButton = SteamVR_Input.GetBooleanAction("Menu");

        public bool enableClipping = false;
        public Frustum frustum;

        public GameObject labelPrefab;

        [Tooltip("The subobjects will be used as scene which get transformed.")]
        public GameObject sceneToTransform;

        /// <summary>
        /// This is a copy (instance) of field "sceneToTransform".
        /// </summary>
        private GameObject _sceneCopy;
        private GameObject _unitFrustum;
        private GameObject _labels;
        private GameObject _lines;

        private PerspectiveTransformation[] _perspectiveTransformations;
        private FrustumObject[] _frustumObjects;

        private Coroutine _transformationCoroutine;
        private Coroutine _recalculationCoroutine;

        private bool _isTransformationDirty;
        private bool _isRecalculationDirty;

        private void Start()
        {
            if (!Learner.Instance.IsVR) return;
            
            var primaryHand = Learner.Instance.GetPrimaryHand();
            var secondaryHand = Learner.Instance.GetSecondaryHand();
            
            var primaryPose = primaryHand.GameObject.GetComponent<SteamVR_Behaviour_Pose>();
            var secondaryPose = secondaryHand.GameObject.GetComponent<SteamVR_Behaviour_Pose>();
            
            clippingOnOffButton?.AddOnStateUpListener(OnToggleClipping, primaryPose.inputSource);
            clippingOnOffButton?.AddOnStateUpListener(OnToggleClipping, secondaryPose.inputSource);
        }

        private void OnEnable()
        {
            InitUnitFrustum();

            _transformationCoroutine = StartCoroutine(UpdateTransformation());
            _recalculationCoroutine = StartCoroutine(RecalculateBoundsAndNormals());

            MakeDirty();
        }

        private void OnDisable()
        {
            StopCoroutine(_transformationCoroutine);
            StopCoroutine(_recalculationCoroutine);
            
            Destroy(_unitFrustum);
        }

        private void OnToggleClipping(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource)
            => FireToggleClipping();

        public void FireToggleClipping()
        {
            enableClipping = !enableClipping;
            var textIterator = GameObject.Find("Button_ToggleClipping")?.GetComponentInChildren<ButtonTextIterator>();
            if (textIterator)
            {
                textIterator.textMesh.text = enableClipping ? "Hide clipping" : "Show clipping";
                textIterator.UpdateText();
            }

            LrsController.Instance.SendStatement(
                verb: xAPI_Definitions.generic.verbs.changed,
                activity: xAPI_Definitions.virtualReality.activities.vrObject,
                extensions: new xAPI_Extensions
                {
                    xAPI_Definitions.virtualReality.extensions.activity.vrObjectName("clipping"),
                    xAPI_Definitions.generic.extensions.result.value(enableClipping)
                }
            );
            
            MakeDirty();
        }

        private void OnDestroy()
        {
            if (_transformationCoroutine != null)
                StopCoroutine(_transformationCoroutine);
            
            if (_recalculationCoroutine != null)
                StopCoroutine(_recalculationCoroutine);

            Destroy(_unitFrustum);
        }

        private IEnumerator RecalculateBoundsAndNormals()
        {
            while (true)
            {
                RecalculateBoundsAndNormalsOfScene();
                yield return new WaitForSeconds(1);
            }
        }

        private IEnumerator UpdateTransformation()
        {
            while (true)
            {
                TransformScene();
                yield return new WaitForSeconds(0.005f); 
            }
        }

        /// <summary>
        /// Initializes Holder game object which contains all created unit frustum game objects.
        /// </summary>
        private void InitHolder()
        {
            // Pick Frustum Instance
            if (!frustum)
                frustum = GetComponent<Frustum>();

            // apply change event
            frustum.OnChanges += Frustum_OnChanges;

            // Destroy old holder, if one exist
            Destroy(transform.parent.FindGameObject(UnitFrustumName));

            if (_unitFrustum)
                Destroy(_unitFrustum);

            // Create Unit Frustum Holder
            _unitFrustum = new GameObject(UnitFrustumName, typeof(MeshRenderer), typeof(MeshFilter));
            _unitFrustum.transform.SetParent(transform.parent);

            // Ensure default values
            _unitFrustum.transform.localScale = Vector3.one;
            _unitFrustum.transform.localEulerAngles = Vector3.zero;

            // Shift unit frustum a bit
            _unitFrustum.transform.position = frustum.imagePreviewTransform.position + frustum.transform.forward * -1.0f;

            // Provide frustum material to Unit Frustum
            _unitFrustum.GetComponent<MeshRenderer>().sharedMaterial = GetComponent<MeshRenderer>().sharedMaterial;
        }
        private void Frustum_OnChanges(object sender) => MakeDirty();
        private QuadCorners GetNearPlaneCorners()
        {
            var tl = new Vector3(1f, 1f, -1f);
            var tr = new Vector3(-1f, 1f, -1f);
            var br = new Vector3(-1f, -1f, -1f);
            var bl = new Vector3(1f, -1f, -1f);

            return new QuadCorners()
            {
                topLeft = tl,
                topRight = tr,
                bottomLeft = bl,
                bottomRight = br
            };
        }

        private QuadCorners GetFarPlaneCorners()
        {
            var tl = new Vector3(1f, 1f, 1f);
            var tr = new Vector3(-1f, 1f, 1f);
            var br = new Vector3(-1f, -1f, 1f);
            var bl = new Vector3(1f, -1f, 1f);

            return new QuadCorners()
            {
                topLeft = tl,
                topRight = tr,
                bottomLeft = bl,
                bottomRight = br
            };
        }

        private void UpdateModel()
        {
            var far = GetFarPlaneCorners();
            var near = GetNearPlaneCorners();

            // get near and far plane vertices
            var nearVertices = near.GetVerticesClockwise();
            var farVertices = far.GetVerticesClockwise();

            // create all 8 vertices
            var corners = nearVertices.Concat(farVertices).ToArray();

            // merge left, right, top and bottom triangles
            var triangles = FrustumTriangles.all;

            // Create sides mesh
            var mesh = Mesh_Ext.CreateFrom(corners, triangles);
            _unitFrustum.GetComponent<MeshFilter>().mesh = mesh;

            Destroy(_lines);
            _lines = new GameObject("Lines");
            _lines.transform.SetParent(_unitFrustum.transform);
            _lines.transform.localPosition = Vector3.zero;

            // Create new lines
            Frustum.InsertLines(_lines.transform);

            // Create border lines for near plane
            SetLinesForPlane(0, near);
            // Create border lines between for sides near and far plane
            SetBridgeLines(4, near, far);
            // Create border lines for far plane
            SetLinesForPlane(8, far);

            // Create Frustum labels
            InitLabels(near, far);
        }

        /// <summary>
        /// Set lines between far and near plane.
        /// </summary>
        private void SetBridgeLines(int offset, QuadCorners from, QuadCorners to)
        {
            SetLine(offset + 0, from.topLeft, to.topLeft);
            SetLine(offset + 1, from.topRight, to.topRight);
            SetLine(offset + 2, from.bottomLeft, to.bottomLeft);
            SetLine(offset + 3, from.bottomRight, to.bottomRight);
        }
        /// <summary>
        /// Create wire for mesh.
        /// </summary>
        private void SetLine(int index, Vector3 from, Vector3 to)
        {
            var transform = _lines.transform;
            if (index < 0 || index >= transform.childCount) return;

            var line = transform.GetChild(index);
            Transform_Utils.PlaceLine(line, from, to);
        }

        private void SetLinesForPlane(int offset, QuadCorners plane)
        {
            SetLine(offset + 0, from: plane.topLeft, to: plane.topRight);
            SetLine(offset + 1, from: plane.topRight, to: plane.bottomRight);
            SetLine(offset + 2, from: plane.bottomRight, to: plane.bottomLeft);
            SetLine(offset + 3, from: plane.bottomLeft, to: plane.topLeft);
        }

        /// <summary>
        /// Instantiate (copies) the GameObject, which is put inside of "sceneToTransform" and use it for Unit Frustum.
        /// </summary>
        private void CopyScene()
        {
            // destroy the copy instance if there exists already one
            if (_sceneCopy)
                Destroy(_sceneCopy);

            // create a scene copy
            _sceneCopy = Instantiate(sceneToTransform);
            _sceneCopy.transform.SetParent(_unitFrustum.transform);
            // default values
            _sceneCopy.transform.localPosition = Vector3.zero;
            _sceneCopy.transform.localScale = sceneToTransform.transform.localScale;
            _sceneCopy.transform.localEulerAngles = sceneToTransform.transform.localEulerAngles;
            // Collection of transformations
            var perspectiveTransformations = new List<PerspectiveTransformation>();
            var frustumObjects = new List<FrustumObject>();
            _sceneCopy.ForEachChild(child =>
            {
                child.name += " Transformed";

                var transformation = child.AddComponent<PerspectiveTransformation>();
                perspectiveTransformations.Add(transformation);

                var mr = child.GetComponentInChildren<MeshRenderer>();

                var fo = mr.gameObject.AddComponent<FrustumObject>();
                fo.enabled = true;

                frustumObjects.Add(fo);
            });
            this._frustumObjects = frustumObjects.ToArray();
            this._perspectiveTransformations = perspectiveTransformations.ToArray();
        }
        /// <summary>
        /// Force a recomputation of the transformations.
        /// </summary>
        public void MakeDirty()
        {
            _isTransformationDirty = true;
            _isRecalculationDirty = true;
        }

        private void RecalculateBoundsAndNormalsOfScene()
        {
            if (!_isRecalculationDirty)
                return;

            foreach (var t in _perspectiveTransformations)
            {
                t.RecalculateMeshProperties();
            }

            _isRecalculationDirty = false;
        }

        private void TransformScene()
        {
            if (!_isTransformationDirty)
                return;
            // prepare matrices for the transformation
            var P = frustum.GetFrustumMatrix();
            var S = Matrix4x4.Scale(new Vector3(1, 1, -1));
            var T = S * P;
            // provide properties
            for (var i = 0; i < _frustumObjects.Length; i++)
            {
                var fo = _frustumObjects[i];
                fo.unitCubeCenter = _unitFrustum.transform.position;
                fo.doClipping = enableClipping;
            }
            // provide matrices
            for (var i = 0; i < _perspectiveTransformations.Length; i++)
            {
                var p = _perspectiveTransformations[i];
                p.TransformationMatrix = T;
                p.UpdateVertices();
            }
            // update finished
            _isTransformationDirty = false;
        }


        /// <summary>
        /// Instantiate unit frustum and its content.
        /// </summary>
        private void InitUnitFrustum()
        {
            InitHolder();
            UpdateModel();
            CopyScene();
            TransformScene();
        }

        private void DestroyUnitFrustum()
        {
            Destroy(_sceneCopy);
            Destroy(_unitFrustum);
        }
        #region Label Generation
        private void InitLabels(QuadCorners near, QuadCorners far)
        {
            if (!_unitFrustum)
                return;

            // Destroy Holder if exists, to clean it fast
            var labelHolder = _unitFrustum.Find("Labels");
            Destroy(labelHolder);

            if (!labelPrefab)
                return;

            // Create labels holder
            _labels = new GameObject("Labels");
            _labels.transform.SetParent(_unitFrustum.transform);
            _labels.transform.localPosition = Vector3.zero;

            // Create Unit Cube Title label
            CreateLabel("Normalized Device Coordinates (NDC)", new Vector3(0f, 2f, 0f), parent: _labels.transform);

            // Create near plane corner labels
            CreateLabelsByQuadCorners(_labels, near);

            // Create far plane corner labels
            CreateLabelsByQuadCorners(_labels, far);
        }

        private void CreateLabelsByQuadCorners(GameObject holder, QuadCorners corners)
        {
            var offset = new Vector3(0, 0.15f, 0);
            CreateCornerLabel(holder, corners.bottomLeft, offset);
            CreateCornerLabel(holder, corners.bottomRight, offset);

            CreateCornerLabel(holder, corners.topLeft, offset);
            CreateCornerLabel(holder, corners.topRight, offset);
        }

        private GameObject CreateLabel(string text, Vector3 localPosition, float fontSize = 8.0f, Transform parent = null)
        {
            if (!labelPrefab)
                return null;

            var label = Instantiate(labelPrefab, parent: parent);
            label.transform.localPosition = localPosition;

            var textMesh = TextMeshPro_Utils.GetTextMesh(label);
            textMesh.text = text;
            textMesh.fontSize = fontSize;
            label.name = text;

            return label;
        }

        private void CreateCornerLabel(GameObject holder, Vector3 pos, Vector3 offset)
        {
            var posTextVector = new Vector3(pos.x, pos.y, pos.z * -1.0f);
            var text = posTextVector.ToString();

            CreateLabel(text, pos, parent: holder.transform, fontSize: 4.0f);
        }
        #endregion
    }
}