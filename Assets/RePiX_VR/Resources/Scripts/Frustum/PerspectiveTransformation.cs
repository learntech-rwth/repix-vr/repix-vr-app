﻿using UnityEngine;

namespace RePiX_VR
{
    public class PerspectiveTransformation : ManualTransformation
    {
        // model matrix
        Matrix4x4 M; 
        // inverted model matrix
        Matrix4x4 Mi;

        protected override void Start()
        {
            base.Start();

            M = Matrix4x4.TRS(transform.localPosition, transform.localRotation, transform.localScale);
            Mi = M.inverse;
        }
        public override Vector3 TransformationFunction(Matrix4x4 P, Vector4 v)
        {
            // Transform to world position
            var vModel = M * v;
            // Transform into perspective space
            var vPerspective = (P * vModel);
            // Normalize into Normal Device Coordinates
            var vNDC = vPerspective / vPerspective.w;
            // Transform back to local position
            return Mi * vNDC;
        }
    }
}