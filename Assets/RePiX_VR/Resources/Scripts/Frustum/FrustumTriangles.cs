﻿using System.Linq;

namespace RePiX_VR
{
    /// <summary>
    /// Indices definitions of the frustum mesh.
    /// </summary>
    public class FrustumTriangles
    {
        public static int[] near => new int[] {
            0, 1, 2, 0, 2, 3,
            0, 2, 1, 0, 3, 2,
        };
        public static int[] far => new int[] {
            4, 5, 6, 4, 6, 7,
            4, 6, 5, 4, 7, 6,
        };
        public static int[] left => new int[] {
            0, 4, 7, 0, 7, 3,
            0, 7, 4, 0, 3, 7,
        };
        public static int[] right => new int[] {
            1, 5, 6, 1, 6, 2,
            1, 6, 5, 1, 2, 6,
        };
        public static int[] top => new int[] {
            0, 4, 5, 0, 5, 1,
            0, 5, 4, 0, 1, 5,
        };
        public static int[] bottom => new int[] {
            2, 3, 7, 2, 7, 6,
            2, 7, 3, 2, 6, 7,
        };

        public static int[] sides => left.Concat(right).Concat(top).Concat(bottom).ToArray();
        public static int[] all => sides.Concat(near).Concat(far).ToArray();
    }
}