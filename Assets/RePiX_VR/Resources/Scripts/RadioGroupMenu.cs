using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RadioGroupMenu : MonoBehaviour
{
    public List<RadioButton> buttons = new List<RadioButton>();

    private void Start()
    {
        if (buttons.Count == 0)
            buttons = gameObject.GetComponentsInChildren<RadioButton>().ToList();

        foreach (var button in buttons)
        {
            button.selected.AddListener(() => DeselectOtherButtons(button));
        }
    }

    private void DeselectOtherButtons(RadioButton button)
    {
        foreach (var b in buttons)
        {
            if (b != button)
                b.Deselect();
        }
    }
}
