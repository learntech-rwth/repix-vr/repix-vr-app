﻿using OmiLAXR.Interaction;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Pointable))]
public class Checkbox : MonoBehaviour
{
    public UnityEvent OnSelectOn = new UnityEvent();
    public UnityEvent OnSelectOff = new UnityEvent();
    
    public Material onMat, offMat;

    [SerializeField] private bool state;

    public bool State => state;
    
    private void Awake()
    {
        var p = GetComponent<Pointable>();
        
        var rend = p.GetComponent<Renderer>();
        rend.material = state ? onMat : offMat;
        
        p.onClick.AddListener(_ =>
        {
            state = !state;
            rend.material = state ? onMat : offMat;
            (state ? OnSelectOn : OnSelectOff).Invoke();
        });
    }
}
