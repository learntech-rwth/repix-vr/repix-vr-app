using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    public class MatrixMenuManager : MonoBehaviour
    {
        public GameObject translationMatrixPanel;
        public GameObject rotationMatrixPanel;
        public GameObject scalingMatrixPanel;
        
        public Button translationButton;
        public Button rotationButton;
        public Button scalingButton;
        
        private enum ActiveMatrix
        {
            Translation,
            Rotation,
            Scaling
        }

        private void Start()
        {
            if (!translationButton || !scalingButton || !rotationButton)
            {
                Button[] buttons = gameObject.GetComponentsInChildren<Button>();
                translationButton = buttons[0];
                rotationButton = buttons[1];
                scalingButton = buttons[2];
            }
            ChangeMatrix(ActiveMatrix.Translation);
            translationButton.onClick.AddListener(() => ChangeMatrix(ActiveMatrix.Translation));
            rotationButton.onClick.AddListener(() => ChangeMatrix(ActiveMatrix.Rotation));
            scalingButton.onClick.AddListener(() => ChangeMatrix(ActiveMatrix.Scaling));
        }

        private void ChangeMatrix(ActiveMatrix activeMatrix)
        {
            DeactivateAllPanels();
            switch (activeMatrix)
            {
                case ActiveMatrix.Translation:
                    translationMatrixPanel.SetActive(true);
                    translationButton.interactable = false;
                    break;
                case ActiveMatrix.Rotation:
                    rotationMatrixPanel.SetActive(true);
                    rotationButton.interactable = false;
                    break;
                case ActiveMatrix.Scaling:
                    scalingMatrixPanel.SetActive(true);
                    scalingButton.interactable = false;
                    break;
            }
        }

        private void DeactivateAllPanels()
        {
            translationMatrixPanel.SetActive(false);
            rotationMatrixPanel.SetActive(false);
            scalingMatrixPanel.SetActive(false);
            translationButton.interactable = true;
            rotationButton.interactable = true;
            scalingButton.interactable = true;
        }
    }
}
