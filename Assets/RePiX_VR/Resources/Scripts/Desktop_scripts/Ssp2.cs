
using UnityEngine.UI;
using UnityEngine;


namespace RePiX_VR.NonVrCanvas
{
    public class Ssp2 : MonoBehaviour
    {
        public Button buttonApplication;
        public Button button3DGeometry;
        public Button button3DTransformation;
        public Button buttonClipping;
        public Button buttonRasterization;
        public Button buttonLighting;
        public Button buttonTexturing;
        public Button buttonVisibilityTest;

        public Button GetButton(string name) => transform.Find(name).GetComponent<Button>();

        public void GetButtons()
        {
            buttonApplication = GetButton("Button_Application");
            button3DGeometry = GetButton("Button_3DGeometry");
            button3DTransformation = GetButton("Button_3DTransformation");
            buttonClipping = GetButton("Button_Clipping");
            buttonRasterization = GetButton("Button_Rasterization");
            buttonLighting = GetButton("Button_(Local)Lighting");
            buttonTexturing = GetButton("Button_Texturing");
            buttonVisibilityTest = GetButton("Button_VisibilityTest");


        }


        /*private void Start () {
    
    
           
            
            buttonApplication.SetDisabled(false);
            button3DGeometry.SetDisabled(false);
            button3DTransformation.SetDisabled(false);
            buttonClipping.SetDisabled(false);
            buttonRasterization.SetDisabled(false);
            buttonLighting.SetDisabled(false);
            buttonTexturing.SetDisabled(false);
            buttonVisibilityTest.SetDisabled(false);
            buttonTexturing.SetDisabled(false);
    
    
    
        }*/


    }
}
