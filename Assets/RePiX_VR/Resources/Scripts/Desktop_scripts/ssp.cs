
using UnityEngine.UI;


using UnityEngine;

namespace RePiX_VR.NonVrCanvas
{
    public class Ssp : MonoBehaviour
    {

        public Button buttonApplication => GetButton("Button_Application");
        public Button button3DGeometry => GetButton("Button_3DGeometry");
        public Button button3DTransformation => GetButton("Button_3DTransformation");
        public Button buttonClipping => GetButton("Button_Clipping");
        public Button buttonRasterization => GetButton("Button_Rasterization");
        public Button buttonLighting => GetButton("Button_(Local)Lighting");
        public Button buttonTexturing => GetButton("Button_Texturing_2DUI");

        public Button buttonVisibilityTest => GetButton("Button_VisibilityTest");

        //public Button buttonImage => GetButton("Button_Image");
        public Button GetButton(string name) => transform.Find(name).GetComponent<Button>();
        // Start is called before the first frame update
        /*public void OnOpenPanel()
        {
            buttonApplication.SetDisabled(false);
            button3DGeometry.SetDisabled(false);
            button3DTransformation.SetDisabled(false);
            buttonClipping.SetDisabled(false);
            buttonRasterization.SetDisabled(false);
            buttonLighting.SetDisabled(false);
            buttonTexturing.SetDisabled(false);
            buttonVisibilityTest.SetDisabled(false);
            //buttonImage.SetDisabled(false);        
    
    
    
    
        }*/
    }
}


