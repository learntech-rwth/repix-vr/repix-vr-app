﻿using OmiLAXR.Interaction;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[RequireComponent(typeof(Pointable))]
public class RadioButton : MonoBehaviour
{
    [FormerlySerializedAs("select")] public UnityEvent selected = new UnityEvent();
    [FormerlySerializedAs("deselect")] public UnityEvent deselected = new UnityEvent();
    
    public void Select() => selected.Invoke();
    public void Deselect() => deselected.Invoke();
    public void AddClickListener(UnityAction action) => GetComponent<Pointable>().onClick.AddListener(_ => action());
}
