﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Resets a childs position if the parent component was disabled.
    /// </summary>
    public class ResetChild : MonoBehaviour
    {
        [SerializeField, Tooltip("Child Component to track")]
        private Transform child;

        private Vector3 originPos;
        private Quaternion originRot;

        private void Awake()
        {
            originPos = child.localPosition;
            originRot = child.localRotation;
        }

        private void OnDisable()
        {
            child.parent = transform;
            child.localPosition = originPos;
            child.localRotation = originRot;
        }
    }
}