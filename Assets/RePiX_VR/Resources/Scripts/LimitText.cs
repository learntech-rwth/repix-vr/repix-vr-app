﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RePiX_VR
{

    /// <summary>
    /// Updates a text label with the state of a limit queue.
    /// </summary>
    public class LimitText : MonoBehaviour
    {
        /// <summary>
        /// Reference to the associated text label.
        /// </summary>
        public TextMeshProUGUI tmp;

        /// <summary>
        /// Color for non-critical counts.
        /// </summary>
        public Color neutralColor = Color.white;

        /// <summary>
        /// Color for critical count (e.g. object deleted for every new object).
        /// </summary>
        public Color warnColor = Color.red;

        public int limit = 8;
        
        public Placer placer;

        private void Update()
        {
            var c = placer.transform.childCount;

            if (c > limit)
            {
                DestroyImmediate(placer.transform.GetChild(0).gameObject);
                c--;
            }

            tmp.text = $"{c} / {limit}";
            tmp.color = c == limit ? warnColor : neutralColor;
        }
    }
}