using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using OmiLAXR.Effects;
using OmiLAXR.SceneManagement.Runtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StageButtons : MonoBehaviour
{
    public GameObject buttonPrefab;
    public Transform buttonParent;
    public SceneGroup sceneGroup;

    public List<OmiLAXRScene> ignoredScenes = new List<OmiLAXRScene>();

    [Tooltip("Remove this pattern from the original scene name, if no custom name specified.")]
    public string regexRemovePattern = @"[\d.]+[\d.]*";

    private void Start()
    {
        var referenceCheck = true;
        if (!buttonPrefab)
        {
            Debug.LogError("No button prefab provided!", this);
            referenceCheck = false;
        }

        sceneGroup ??= GetComponent<SceneGroup>();
        if (!sceneGroup)
        {
            Debug.LogError("No scene group found. Set it manually!", this);
            referenceCheck = false;
        }

        var text = GetComponentInChildren<TMP_Text>();
        if (text && sceneGroup)
            text.text = sceneGroup.groupName;
        
        if (!referenceCheck) return;

        if (!buttonParent)
            buttonParent = transform;

        foreach (var scene in sceneGroup.scenes.Where(namedScene => !ignoredScenes.Contains(namedScene.scene)))
            CreateButtonForScene(Instantiate(buttonPrefab, buttonParent), scene, regexRemovePattern);
    }

    private static void CreateButtonForScene(GameObject buttonObject, NamedOmiLAXRScene namedScene, string regexPattern)
    {
        var scene = namedScene.scene;
        var name = namedScene.name;
        if (string.IsNullOrEmpty(namedScene.name))
        {
            name = Path.GetFileNameWithoutExtension(namedScene.scene.Path());
            name = Regex.Replace(name, regexPattern, string.Empty).Trim();
        }
        
        buttonObject.name = $"Button: {name}";
        buttonObject.GetComponentInChildren<TMP_Text>().text = name;

        var sceneQueue = SceneQueue.Instance;
        buttonObject.GetComponentInChildren<Button>().onClick.AddListener(() =>
        {
            if (sceneQueue.NextScene != scene)
                sceneQueue.EnqueueAtFront(scene);
            sceneQueue.MoveToNextScene();
        });

        sceneQueue.finishedInitializing.AddListener(() =>
        {
            if (sceneQueue.NextScene != scene) return;
            buttonObject.GetComponent<BlinkImageController>().enabled = true;
        });

        if (sceneQueue.NextScene != scene) return;
        buttonObject.GetComponent<BlinkImageController>().enabled = true;
    }
}