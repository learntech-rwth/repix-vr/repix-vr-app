using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadioButtonMenuVR : MonoBehaviour
{
    private Button _button;
    private RadioButton _radioButton;
    private Image _image;
    private void Start()
    {
        _button = gameObject.GetComponent<Button>();
        _radioButton = gameObject.GetComponent<RadioButton>();
        _image = gameObject.GetComponent<Image>();
        _button.onClick.AddListener(() => _radioButton.Select());
        _radioButton.selected.AddListener(() => changeColor(Color.green));
        _radioButton.deselected.AddListener(() => changeColor(Color.white));
    }

    private void changeColor(Color color)
    {
        if (!_image)
            return;
        _image.color = color;
    }
}
