using UnityEngine;
using UnityEngine.Timeline;

namespace RePiX_VR
{
    public class SignalEmitter : MonoBehaviour
    {
        /// <summary>
        /// The target signal receiver.
        /// If none is specified, the component looks for a signal receiver on its gameobject on start.
        /// </summary>
        public SignalReceiver signalReceiver;

        private void Start()
        {
            signalReceiver ??= GetComponent<SignalReceiver>();
            if (signalReceiver) return;
            Debug.LogWarning("No Signal Receiver specified and none could be found. Disabling Signal Emitter!", this);
            enabled = false;
        }

        /// <summary>
        /// Emit a signal for the target signal receiver specified by the component.
        /// </summary>
        /// <param name="signal">The signal to emit.</param>
        public void Emit(SignalAsset signal) => Emit(signal, signalReceiver);

        /// <summary>
        /// Emit a signal to a given target signal receiver.
        /// </summary>
        /// <param name="signal">The signal to emit.</param>
        /// <param name="receiver">The target signal receiver.</param>
        public static void Emit(SignalAsset signal, SignalReceiver receiver)
        {
            var reaction = receiver.GetReaction(signal);
            reaction?.Invoke();
        }
    }
}