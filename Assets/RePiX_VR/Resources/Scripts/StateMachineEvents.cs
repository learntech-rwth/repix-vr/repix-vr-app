﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR
{ 
    [Serializable]
    internal class StateEvent
    {
        public string eventName;
        public UnityEvent unityEvent = new UnityEvent();
    }

    [Serializable]
    internal class StateEvents
    {
        /// <summary>
        /// 
        /// </summary>
        public string state;
        public List<StateEvent> events;
    }

    public class StateMachineEvents : MonoBehaviour
    {
        /// <summary>
        /// Allows setting up events to be called for any combination of state name and
        /// event name.
        /// Please note that event names are given as a path string, i.e. "[layer].[state]".
        /// </summary>

        [SerializeField]
        private List<StateEvents> events;

        private void Awake()
        {
            foreach (var e in events)
            {
                Debug.Log(e.state + ": " + Animator.StringToHash(e.state));
            }
        }

        /// <summary>
        /// Calls event from registered events.
        /// </summary>
        /// <param name="stateHash">hash value of the state path</param>
        /// <param name="eventName">name of the event</param>
        public void Call(int stateHash, string eventName)
        {
            Debug.Log("Called " + stateHash + ", " + eventName);
            var s = events.Find(e => Animator.StringToHash(e.state) == stateHash);
            if (s != null)
            {
                var ev = s.events.Find(e => e.eventName == eventName);
                ev.unityEvent.Invoke();
            }
        }

        /// <summary>
        /// Calls event from registered events.
        /// </summary>
        /// <param name="statePath">state path (format: [layer].[state name])</param>
        /// <param name="eventName">name of the event</param>
        public void Call(string statePath, string eventName) => Call(Animator.StringToHash(statePath), eventName);
    }
}