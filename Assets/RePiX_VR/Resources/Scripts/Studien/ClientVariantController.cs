﻿using System.Collections.Generic;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;

namespace RePiX_VR.Studien
{

    [DisallowMultipleComponent]
    public class ClientVariantController : MonoBehaviour
    {
        public InteractiveNonInteractiveToggleController variantToggler;
        public ClientVariant variant = ClientVariant.Simple;
        
        [Header("Desktop variant settings")]
        public bool isForDesktop;

        public string lrs;
        public string uri;
        public string clientKey;
        public string clientSecret;
        public string emailPostfix;

        private Dictionary<ClientVariant, ClientVariantInformation> Variants => new Dictionary<ClientVariant, ClientVariantInformation>()
        {
            [ClientVariant.A] = new ClientVariantInformation("A", true, true, false, false),
            [ClientVariant.B] = new ClientVariantInformation("B",false, true, true, false),
            [ClientVariant.C] = new ClientVariantInformation("C",false, false, true, true),
            [ClientVariant.D] = new ClientVariantInformation("D",true, false, true, false),
            [ClientVariant.E] = new ClientVariantInformation("E",true, false, false, true),
            [ClientVariant.F] = new ClientVariantInformation("F",false, true, false, true),
            [ClientVariant.Full] = new ClientVariantInformation("full",true, true, true, true),
            [ClientVariant.Simple] = new ClientVariantInformation("simple",false, false, false, false)
        };

        private void Awake()
        {
            var variantInformation = Variants[variant];
            SetVariant(variantInformation);
        }

        private void Start()
        {
            if (isForDesktop)
            {
                LoadDesktopVariant(); 
            }
        }

        private void LoadDesktopVariant()
        {
            var trackingSystem = MainTrackingSystem.Instance;

            Destroy(trackingSystem.GetComponent<LocalStorageController>());

            var scenario = trackingSystem.GetScenario();
            var trackingConfig = trackingSystem.GetTrackingConfig(scenario);

            var pseudonym = PseudonymInputField.PseudonymName;

            if (string.IsNullOrEmpty(pseudonym))
                pseudonym = "Anonymous";
            
            emailPostfix = emailPostfix.Replace("@", "").ToLower();

            var email = pseudonym.ToLower() + "@" + emailPostfix;

            trackingConfig.identity.email = email;
            trackingConfig.identity.name = pseudonym;
            trackingConfig.uri = uri;
            trackingConfig.lrs = lrs;
            trackingConfig.auth.key = clientKey;
            trackingConfig.auth.secret = clientSecret;

            trackingSystem.Setup(trackingConfig);
            trackingSystem.StartTracking();
        }
        
        protected void SetVariant(ClientVariantInformation v)
            => SetVariant(v.VariantName, v.Application, v.Texturing, v.Lighting, v.Rasterization);

        protected void SetVariant(string variantName, bool application, bool texturing, bool lighting, bool rasterization)
        {
            LrsController.GameVariant = variantName;

            var c = variantToggler;
            c.enableInteractiveLighting = lighting;
            c.enableInteractiveRasterization = rasterization;
            c.enableInteractiveApplicationStage = application;
            c.enableInteractiveTexturing = texturing;
            c.gameObject.SetActive(true);
        }
    }

}