﻿using System.Linq;
using OmiLAXR.xAPI.Extensions;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace RePiX_VR.Studien
{
    [RequireComponent(typeof(TMP_InputField))]
    public class PseudonymInputField : MonoBehaviour
    {
        public static string PseudonymName { get; private set; }
        private TMP_InputField _inputField;
        public Button submitButton;
        // Start is called before the first frame update
        private void Start()
        {
            var inputField = _inputField = GetComponent<TMP_InputField>();
            inputField.onValueChanged.AddListener(OnTextChanged);
            OnTextChanged("");
            
            submitButton.onClick.AddListener(OnSubmit);
        }

        private string RemoveChars(string text)
        {
            var forbiddenChars = "();:?*'#äüöß!\"§%&/=`´+/ ".ToCharArray();
            return text.Where(t => !forbiddenChars.Contains(t)).Aggregate("", (current, t) => current + t);
        }

        private void OnSubmit()
        { 
            submitButton.SetDisabled(false);
            _inputField.readOnly = true;
            PseudonymName = _inputField.text = RemoveChars(_inputField.text).ToUpper();
            SceneManager.LoadScene("Guided Tour");
        }

        private void OnTextChanged(string newValue)
        {
            if (submitButton)
            {
                submitButton.SetDisabled(newValue == string.Empty);
            }
        }
    }

}