﻿namespace RePiX_VR.Studien
{
    public struct ClientVariantInformation
    {
        public string VariantName;
        public bool Application; 
        public bool Texturing;
        public bool Lighting;
        public bool Rasterization;

        public ClientVariantInformation(string variantName, bool application, bool texturing, bool lighting, bool rasterization)
        {
            VariantName = variantName;
            Application = application;
            Texturing = texturing;
            Lighting = lighting;
            Rasterization = rasterization;
        }
    }
}