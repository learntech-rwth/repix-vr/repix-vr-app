﻿using System;

namespace RePiX_VR.Studien
{
    [Serializable]
    public enum ClientVariant
    {
        A, B, C, D, E, F, Full, Simple
    }
}