﻿using OmiLAXR.InstructionalDesign;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR.Studien
{

    public class InteractiveNonInteractiveToggleController : MonoBehaviour
    {
        [Header("Guided Tour Management")]
        public GuidedTourManager guidedTourManager;

        public bool enableInteractiveRasterization;
        public bool enableInteractiveTexturing;
        public bool enableInteractiveLighting;
        public bool enableInteractiveApplicationStage;

        public GameObject interactiveRasterizationStage;
        public GameObject nonInteractiveRasterizationStage;

        public GameObject interactiveTexturing;
        public GameObject nonInteractiveTexturing;
    
        public GameObject interactiveLighting;
        public GameObject nonInteractiveLighting;
    
        public GameObject interactiveApplicationStage;
        public GameObject nonInteractiveApplicationStage;

        [Header("Learning Scenario Context")] 
        public LearningUnit interactiveApplicationStageUnit;
        public LearningUnit nonInteractiveApplicationStageUnit;

        public LearningUnit interactiveRasterizationStageUnit;
        public LearningUnit nonInteractiveRasterizationStageUnit;
        
        public LearningUnit interactiveLightingStageUnit;
        public LearningUnit nonInteractiveLightingStageUnit;

        public LearningUnit interactiveTexturingStageUnit;
        public LearningUnit nonInteractiveTexturingStageUnit;
        
        public UnityEvent onPrepared;
        
        private void Awake()
        {
            interactiveApplicationStageUnit.gameObject.SetActive(enableInteractiveApplicationStage);
            nonInteractiveApplicationStageUnit.gameObject.SetActive(!enableInteractiveApplicationStage);
            
            interactiveRasterizationStageUnit.gameObject.SetActive(enableInteractiveRasterization);
            nonInteractiveRasterizationStageUnit.gameObject.SetActive(!enableInteractiveRasterization);
            
            interactiveLightingStageUnit.gameObject.SetActive(enableInteractiveLighting);
            nonInteractiveLightingStageUnit.gameObject.SetActive(!enableInteractiveLighting);
            
            interactiveTexturingStageUnit.gameObject.SetActive(enableInteractiveTexturing);
            nonInteractiveTexturingStageUnit.gameObject.SetActive(!enableInteractiveTexturing);

            guidedTourManager.applicationStage = GetApplicationStage();
            guidedTourManager.rasterizationStage = GetRasterizationStage();
            guidedTourManager.texturingStage = GetTexturingStage();
            guidedTourManager.lightingStage = GetLightingStage();
            
            onPrepared?.Invoke();
        }

        public GameObject GetApplicationStage()
            => enableInteractiveApplicationStage ? interactiveApplicationStage : nonInteractiveApplicationStage;

        public GameObject GetRasterizationStage()
            => enableInteractiveRasterization ? interactiveRasterizationStage : nonInteractiveRasterizationStage;

        public GameObject GetTexturingStage()
            => enableInteractiveTexturing ? interactiveTexturing : nonInteractiveTexturing;

        public GameObject GetLightingStage()
            => enableInteractiveLighting ? interactiveLighting : nonInteractiveLighting;
    }
}