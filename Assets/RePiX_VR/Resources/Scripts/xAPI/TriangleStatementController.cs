﻿using UnityEngine;

using Valve.Newtonsoft.Json.Linq;
using OmiLAXR.xAPI.Tracking;
using RePiX_VR.GeometryStage;
using xAPI.Registry;

namespace RePiX_VR.xAPI
{
    [RequireComponent(typeof(TriangleBuilder))]
    public class TriangleStatementController : MonoBehaviour
    {
        private TriangleBuilder _triangleBuilder;
        private LrsController _lrsController;
        // Use this for initialization
        private void Start()
        {
            _lrsController = MainTrackingSystem.Instance.GetComponent<LrsController>();
            _lrsController.Register("Vertex");

            _triangleBuilder = GetComponent<TriangleBuilder>();
            _triangleBuilder.OnCreatedVertex += OnCreatedVertex;
            _triangleBuilder.OnMovedVertex += OnMovedVertex;
            _triangleBuilder.OnRemovedVertex += OnRemovedVertex;
            _triangleBuilder.OnCreatedTriangle += OnCreatedTriangle;
            _triangleBuilder.OnRemovedTriangle += TriangleBuilderOnOnRemovedTriangle;
        }

        private void TriangleBuilderOnOnRemovedTriangle(TriangleComponent triangle, VertexComponent[] vertices)
        {
            var triangleData = GetTriangleData(vertices);

            _lrsController.SendStatement(
                verb: xAPI_Definitions.virtualReality.verbs.removed,
                activity: xAPI_Definitions.virtualReality.activities.vrObject,
                extensions: new xAPI_Extensions(
                    xAPI_Definitions.virtualReality.extensions.activity
                        .vrObjectName("Triangle")
                        .triangle(triangleData.ToString())
                )
            );
        }

        private JArray GetTriangleData(VertexComponent[] vertices)
        {
            var triangleData = new JArray();

            foreach (var v in vertices)
            {
                // Get vertex color and position
                var vertex = Vertex.FromGameObject(v.gameObject);
                // Get vertex index
                var index = v.Index;
                // Transform into json
                var jVertex = new JObject
                {
                    { "index", index },
                    { "color", vertex.color.ToString() },
                    { "position", vertex.position.ToString() }
                };
                // Collect
                triangleData.Add(jVertex);
            }

            return triangleData;
        }

        private void OnCreatedTriangle(TriangleComponent triangle, VertexComponent[] vertices)
        {
            if (!MainTrackingSystem.Instance.IsTracking(Gestures.Interacting))
                return;

            var triangleData = GetTriangleData(vertices);

            _lrsController.SendStatement(
                verb: xAPI_Definitions.virtualReality.verbs.created,
                activity: xAPI_Definitions.virtualReality.activities.vrObject,
                extensions: new xAPI_Extensions(
                    xAPI_Definitions.virtualReality.extensions.activity
                    .vrObjectName("Triangle")
                    .triangle(triangleData.ToString())
                )
            );
        }

        private void SendVertexStatement(VertexComponent vertex, xAPI_Verb verb)
        {
            if (!MainTrackingSystem.Instance.IsTracking(Gestures.Interacting))
                return;

            // Get vertex local and global position
            var position = vertex.transform.position;
            // Get Vertex color and index
            var vertexStruct = Vertex.FromGameObject(vertex.gameObject);
            var index = vertex.Index;
            // Get trackable state
            var trackable = vertex.GetComponent<Trackable>();

            _lrsController.SendStatement(
                verb,
                activity: xAPI_Definitions.virtualReality.activities.vrObject,
                extensions: new xAPI_Extensions
                {
                    xAPI_Definitions.virtualReality.extensions.activity.vrObjectName(trackable.GetTrackingName()),
                    xAPI_Definitions.generic.extensions.activity.color(vertexStruct.color),
                    xAPI_Definitions.generic.extensions.result.index(index),
                    xAPI_Definitions.seriousGames.extensions.result.position(position)
                }
            );
        }

        private void OnRemovedVertex(VertexComponent vertex)
            =>  SendVertexStatement(vertex, verb: xAPI_Definitions.virtualReality.verbs.removed);

        private void OnCreatedVertex(VertexComponent vertex) 
            => SendVertexStatement(vertex, verb: xAPI_Definitions.virtualReality.verbs.placed);

        private void OnMovedVertex(VertexComponent vertex)
        => SendVertexStatement(vertex, verb: xAPI_Definitions.virtualReality.verbs.moved);
    }
}