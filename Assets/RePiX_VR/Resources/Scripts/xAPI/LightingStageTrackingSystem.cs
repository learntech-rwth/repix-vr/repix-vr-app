﻿using OmiLAXR;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;
using xAPI.Registry;

namespace RePiX_VR.xAPI
{
    public class LightingStageTrackingSystem : TrackingSystemBase
    {
        //-------------------------------------------------
        // Singleton instance of the GeometryTrackingSystem. Only one can exist at a time.
        //-------------------------------------------------
        private static LightingStageTrackingSystem _instance;
        public static LightingStageTrackingSystem Instance
        {
            get
            {
                if ( _instance == null )
                {
                    _instance = FindObjectOfType<LightingStageTrackingSystem>();
                }
                return _instance;
            }
        }

        public override MonoBehaviour GetInstance() => Instance;
        public override TrackingMeta GetMeta(string metaContext) => TrackingMeta.Empty;
        
        public void SendLampCreationStatement(GameObject newLamp)
        {
            LrsController.Instance.SendStatement(
                verb: xAPI_Definitions.virtualReality.verbs.created,
                activity: xAPI_Definitions.virtualReality.activities.vrObject,
                extensions: xAPI_Definitions.virtualReality.extensions.activity.vrObjectName(newLamp.name));
        }
    }
}