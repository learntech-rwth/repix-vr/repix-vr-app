﻿using OmiLAXR.xAPI.Tracking;
using xAPI.Registry;
using UnityEngine;

namespace RePiX_VR.xAPI
{
    public class ColorPickerStatementsController : TrackingControllerBase
    {
        private static ColorPickerStatementsController _instance;
        /// <summary>
        /// Singleton instance of the ColorPickerStatementsController. Only one can exist at a time.
        /// </summary>
        public static ColorPickerStatementsController Instance
            => _instance ??= FindObjectOfType<ColorPickerStatementsController>();

        public override void StartTracking(MainTrackingSystem mainTrackingSystem, ITrackingSystem trackingSystem, TrackingContext context)
        {
            
        }

        public override void StopTracking(MainTrackingSystem mainTrackingSystem, ITrackingSystem trackingSystem, TrackingContext context)
        {
        }

        public override MonoBehaviour GetInstance() => Instance;
        public override void Consider(GameObject go)
        {
        }

        public void PickColor(Color color)
        {
            SendStatement(
                verb: xAPI_Definitions.generic.verbs.changed,
                activity: xAPI_Definitions.generic.activities.colorPicker,
                activityExtensions: xAPI_Definitions.generic.extensions.activity.color(color)
            );
        }
        
        public void SendColorChangeStatement(Color color)
        {
            SendStatement(
                verb: xAPI_Definitions.generic.verbs.changed,
                activity: xAPI_Definitions.generic.activities.colorPicker,
                activityExtensions: xAPI_Definitions.generic.extensions.activity.color(color)
            );
        }
    }
}