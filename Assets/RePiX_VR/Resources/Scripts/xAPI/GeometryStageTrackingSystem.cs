﻿using OmiLAXR;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;

namespace RePiX_VR.xAPI
{
    public class GeometryTrackingSystem : TrackingSystemBase
    {
        //-------------------------------------------------
        // Singleton instance of the GeometryTrackingSystem. Only one can exist at a time.
        //-------------------------------------------------
        private static GeometryTrackingSystem _instance;
        public static GeometryTrackingSystem Instance
        {
            get
            {
                if ( _instance == null )
                {
                    _instance = FindObjectOfType<GeometryTrackingSystem>();
                }
                return _instance;
            }
        }

        public override MonoBehaviour GetInstance() => Instance;

        public override TrackingMeta GetMeta(string metaContext) => TrackingMeta.Empty;
    }
}