﻿using System.Linq;
using OmiLAXR.Extensions;
using OmiLAXR.Utils;
using RePiX_VR.GeometryStage;
using UnityEngine;

namespace RePiX_VR
{
    public class GameObject_Utils
    {
        public static GameObject CreateTriangle(Vertex[] vertices, GameObject holder = null, bool collider = false)
        {
            var points = vertices.Select(v => v.position).ToArray();
            var colors = vertices.Select(v => v.color).ToArray();

            var triangleMesh = Mesh_Ext.CreateTriangle(points, colors);
            triangleMesh.name = "TriangleMesh";
            var triangle = new GameObject("Triangle", typeof(MeshRenderer), typeof(MeshFilter));
            triangle.GetComponent<MeshFilter>().mesh = triangleMesh;
            triangle.tag = "IgnorePointer";

            var col = triangle.AddComponent<SphereCollider>();
            col.isTrigger = true;
            var cog = Vector3_Utils.CenterOfGravity(points);
            col.center = cog;

            var minMidDistance = Vector3_Utils.SmallestMid(cog, points);
            col.radius = minMidDistance;
            col.enabled = collider;

            if (holder)
                triangle.transform.parent = holder.transform;

            return triangle;
        }
        public static GameObject CreateLine(Vector3 from, Vector3 to, float? thickness = null, GameObject holder = null)
        {
            var line = CreateLine(holder ? holder.transform : null);
            var distance = Vector3.Distance(from, to);
            line.transform.position = from;
            line.transform.LookAt(to);
            var s = thickness.HasValue ? line.transform.localScale : thickness.Value * Vector3.one;
            line.transform.localScale = new Vector3(s.x, s.y, distance);
            return line;
        }
        public static GameObject CreateLine(Transform parent = null)
        {

            var line = new GameObject("Line");
            var lineMeshObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
            lineMeshObject.name = "Line Mesh";
            lineMeshObject.GetComponent<BoxCollider>()?.Destroy();

            lineMeshObject.transform.localPosition = new Vector3(0, 0, 0.5f);
            lineMeshObject.transform.localScale = new Vector3(0.0025f, 0.0025f, 1f);

            lineMeshObject.transform.SetParent(line.transform);
            line.transform.SetParent(parent);
            return line;
        }

    }
}