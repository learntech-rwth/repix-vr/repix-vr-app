﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR
{

    /// <summary>
    /// Simple state machine behaviour that calls events from an objects StateMachineEvents instance if it exists.
    /// </summary>

    public class StateLifeCycleEventEmitter : StateMachineBehaviour
    {
        // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var listener = animator.GetComponent<StateMachineEvents>();
            if (listener != null) listener.Call(stateInfo.fullPathHash, "OnStateEnter");
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            var listener = animator.GetComponent<StateMachineEvents>();
            if (listener != null) listener.Call(stateInfo.fullPathHash, "OnStateExit");
        }
    }
}