﻿using System.Collections;
using UnityEngine;

namespace RePiX_VR
{

    /// <summary>
    /// Makes a object check if it should delete itself.
    /// Deletion requires contact with a deletion zone
    /// and not being in a locked state.
    /// </summary>

    public class Deletable : MonoBehaviour
    {
        [SerializeField, Tooltip("Determines whether object is protected from deletion currently.")]
        private bool _isProtected = false;

        [SerializeField, Tooltip("Time in seconds until object is actually deleted.")]
        private float deletionTimer = 0f;

        /// <summary>
        /// Determines whether the attached object is currently barred from being
        /// deleted.
        /// </summary>
        public bool IsProtected { get => _isProtected; set => _isProtected = value; }

        private bool inDeletionZone = false;

        /// <summary>
        /// Tries to delete the object, if it is in a deletion zone and unprotected.
        /// </summary>
        /// <param name="unprotect">set to true if instance should be unprotected</param>
        /// </summary>
        public void TryDelete(bool unprotect = false)
        {
            // override protection if needed
            if (unprotect) _isProtected = false;

            if (!_isProtected && inDeletionZone)
            {
                StartCoroutine(Delete());
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<DeletionZone>())
            {
                inDeletionZone = true;
                TryDelete(false);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<DeletionZone>())
            {
                inDeletionZone = false;
            }
        }

        private IEnumerator Delete()
        {
            // deletion may be deferred, e.g. to allow a physics object
            // to move a slight amount inside of the trash can or in case
            // users should be able to rescue their object.
            yield return new WaitForSeconds(deletionTimer);
            if (!_isProtected) Destroy(gameObject);
        }
    }
}