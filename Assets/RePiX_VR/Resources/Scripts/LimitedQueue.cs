﻿using System;
using System.Collections.Generic;
using OmiLAXR;
using UnityEngine;

namespace RePiX_VR
{

    /// <summary>
    /// Ensures that older objects in the queue are destroyed if new ones are
    /// added beyond a given limit.
    /// </summary>
    public class LimitedQueue : MonoBehaviour
    {
        /// <summary>
        /// Called with new count when some change occurs.
        /// </summary>
        public event Action<int> updatingCount;

        /// <summary>
        /// Maximum allowed number of game objects in the queue.
        /// </summary>
        public int limit;

        private List<GameObject> objects = new List<GameObject>();

        public void AddToQueue(GameObject go)
        {
            if (objects.Count >= limit)
            {
                DestroyImmediate(objects[0]);
            }

            objects.Add(go);
            go.AddComponent<DestructionListener>().Destroyed += Remove;
            updatingCount?.Invoke(objects.Count);
        }

        private void Remove(GameObject go)
        {
            objects.Remove(go);
            updatingCount?.Invoke(objects.Count);
        }

        private void OnDisable()
        {
            foreach (var obj in objects) Destroy(obj);
            objects = new List<GameObject>();
        }
    }
}