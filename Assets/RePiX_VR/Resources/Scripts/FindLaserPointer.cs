﻿using OmiLAXR;
using UnityEngine;
using Valve.VR;

namespace RePiX_VR
{
    /// <summary>
    /// This is a hacky way to determine which hand currently has a working laser pointer attached.
    /// Replace this with a sensible way of finding this at a later point!
    /// </summary>
    public static class FindLaserPointer
    {
        public static Transform Left => Learner.Instance.leftHand.transform;
        public static Transform Right => Learner.Instance.rightHand.transform;

        public static SteamVR_Input_Sources Primary =>
            IsActive(Left) ? SteamVR_Input_Sources.LeftHand : SteamVR_Input_Sources.RightHand;

        public static SteamVR_Input_Sources Secondary =>
            IsActive(Left) ? SteamVR_Input_Sources.RightHand : SteamVR_Input_Sources.LeftHand;
        
        public static Transform Find(bool primary = true) => IsActive(Left) == primary ? Left : Right;

        private static bool IsActive(Transform hand)
        {
            var holder = hand.Find("New Game Object");
            return holder && holder.GetChild(0).gameObject.activeSelf;
        }

        public static Ray CastFrom(bool primary = true)
        {
            var hand = Find(primary);
            return new Ray(hand.position, hand.forward);
        }
    }
}