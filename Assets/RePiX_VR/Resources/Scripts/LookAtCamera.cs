﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    [SerializeField, Tooltip("Look-at target, leave blank for main camera.")]
    private Transform target;
    
    [SerializeField, Tooltip("Up vector, leave blank to use this objects transform.up instead.")]
    private Transform upRef;

    private Vector3 upVect;

    public Vector3 offset;
    
    private void Awake()
    {
        if (!target)
        {
            var main = Camera.main;

            if (!main) throw new NullReferenceException("No main camera in this scene?");
            target = main.transform;
        }

        upVect = upRef ? upRef.position : transform.up;
    }

    private void Update()
    {
        transform.LookAt(target, upVect);
        transform.Rotate(offset);
    }
}
