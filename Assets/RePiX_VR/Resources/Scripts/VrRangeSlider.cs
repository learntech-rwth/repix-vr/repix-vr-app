﻿using System;
using System.Collections;
using OmiLAXR;
using OmiLAXR.Interaction;
using RePiX_VR;
using OmiLAXR.xAPI.Tracking;
using OmiLAXR.xAPI.Tracking.Interaction;
using xAPI.Registry;
using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(MouseInteractionEvents))]
public class VrRangeSlider : MonoBehaviour
{
    public Transform knob;
    
    public SteamVR_Action_Boolean vrSelectAction;
    
    [Header("Range Start")]
    public Transform startPoint;
    public float startValue;

    [Header("Range End")]
    public Transform endPoint;
    public float endValue = 1f;

    private float _value;
    public float Value => Mathf.Lerp(startValue, endValue, _value);

    private bool _down;

    public event Action<float> OnValueChange;

    private Camera _main;

    private void Awake()
    {
        if (endPoint == null || startPoint == null) throw new NullReferenceException();
        if (!Camera.main) throw new NullReferenceException();
        _main = Camera.main;
    }

    private void Start()
    {
        _trackable = GetComponent<Trackable>();
        
    }

    #region xAPI Tracking
    private Coroutine _timeout;
    private Trackable _trackable;
    private void StartTracking(float newValue)
    {
        if (!_trackable)
            return;
            
        if (_timeout != null)
            StopCoroutine(_timeout);
            
        _timeout = StartCoroutine(TriggerTracking(0.3f, newValue));
    }
    private IEnumerator TriggerTracking(float waitTime, float newValue)
    {
        yield return new WaitForSeconds(waitTime);
        var name = _trackable.GetTrackingName();
            
        LrsController.Instance.SendStatement(
            verb: xAPI_Definitions.generic.verbs.changed,
            activity: xAPI_Definitions.virtualReality.activities.uiElement,
            activityExtensions:  xAPI_Definitions.virtualReality.extensions.activity
                .vrObjectName(name).uiElementValue(newValue).uiElementType(UiInteractionController.UiInteractionTypes.slider.ToString())
                .uiElementMinValue(startValue).uiElementMaxValue(endValue)
        );
    }
    

    #endregion
    
#region Input Event Handling
    private void OnEnable()
    {
        var mouse = GetComponent<MouseInteractionEvents>();
        mouse.onMouseDown.AddListener(_ => _down = true);
        mouse.onMouseUp.AddListener(_ => _down = false);
        vrSelectAction.onStateDown += OnVrDown;
        vrSelectAction.onStateUp += OnVrUp;
    }

    private void OnDisable()
    {
        var mouse = GetComponent<MouseInteractionEvents>();
        vrSelectAction.onStateDown -= OnVrDown;
        vrSelectAction.onStateUp -= OnVrUp;
    }

    private void OnVrDown(SteamVR_Action_Boolean action, SteamVR_Input_Sources source)
    {
        var ray = FindLaserPointer.CastFrom();
        if (!Physics.Raycast(ray, out var hit) || hit.collider.gameObject.transform != transform) return;
        _down = true;
    }

    private void OnVrUp(SteamVR_Action_Boolean action, SteamVR_Input_Sources source) => _down = false;
#endregion

    private void Update()
    {
        if (!_down) return;

        var ray = Learner.Instance.IsVR ? FindLaserPointer.CastFrom() : _main.ScreenPointToRay(Input.mousePosition);

        var point = IntersectionWithPlane(ray);
        if (point == Vector3.positiveInfinity) return;

        _value = ProjectOnSlider(point);
        OnValueChange?.Invoke(Value);
        StartTracking(_value);
        knob.position = Vector3.Lerp(startPoint.position, endPoint.position, _value);
    }

    private Vector3 IntersectionWithPlane(Ray ray)
    {
        var plane = new Plane(transform.forward, startPoint.position);
        if (!plane.Raycast(ray, out var enter)) return Vector3.positiveInfinity;
        return ray.origin + enter * ray.direction;
    }
    
    private float ProjectOnSlider(Vector3 hit)
    {
        var s = startPoint.position;
        var e = endPoint.position;
        var v = e - s;

        return Mathf.Clamp01(-Vector3.Dot(s - hit, v) / v.sqrMagnitude);
    }
}
