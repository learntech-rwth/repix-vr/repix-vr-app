﻿using System;
using OmiLAXR;
using OmiLAXR.Helpers.Togglers;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR
{
    public enum DeviceMode
    {
        VR, Desktop
    }

    public class DeviceStatus : MonoBehaviour
    {
        public static event Action<DeviceMode> OnDeviceModeChange;

        private OnEnableDisableController edController;

        public DeviceMode Current { get; private set; }
        
        private void OnEnable()
        {
            edController = Learner.Instance.Player.GetTransform().gameObject.AddComponent<OnEnableDisableController>();
            
            edController.onEnabled.AddListener(SetDeviceMode(DeviceMode.Desktop));
            edController.onDisabled.AddListener(SetDeviceMode(DeviceMode.VR));
        }

        private void OnDisable()
        {
            Destroy(edController);
        }

        private void Start()
        {
            SetDeviceMode(Learner.Instance.IsVR ? DeviceMode.VR : DeviceMode.Desktop)();
        }

        private UnityAction SetDeviceMode(DeviceMode mode)
        {
            return delegate
            {
                Current = mode;
                OnDeviceModeChange?.Invoke(mode);
            };
        }

    }
}