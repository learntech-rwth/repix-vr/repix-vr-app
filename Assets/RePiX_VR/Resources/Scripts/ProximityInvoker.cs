using UnityEngine;
using UnityEngine.Events;

public class ProximityInvoker : MonoBehaviour
{
    public Transform proximityLocation;
    public float proximityRadius = 1f;
    public bool onlyXZPlane = true;

    public UnityEvent onEnter = new UnityEvent();
    public UnityEvent onLeave = new UnityEvent();

    private Camera _playerCamera;
    private bool _entered;

    private void FixedUpdate()
    {
        if (!_playerCamera)
            _playerCamera = Camera.main;
        
        if (!_playerCamera) return;

        if (!proximityLocation)
            proximityLocation = transform;

        var cameraPosition = _playerCamera.transform.position;
        var locationPosition = proximityLocation.position;

        if (onlyXZPlane)
        {
            cameraPosition = new Vector3(cameraPosition.x, 0, cameraPosition.z);
            locationPosition = new Vector3(locationPosition.x, 0, locationPosition.z);
        }

        var distance = Vector3.Distance(cameraPosition, locationPosition);

        if (!_entered)
        {
            if (distance > proximityRadius) return;
            onEnter.Invoke();
            _entered = true;
        }
        else
        {
            if (distance < proximityRadius) return;
            onLeave.Invoke();
            _entered = false;
        }
    }
}