using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Explanation : MonoBehaviour {
    public Text Textfield ;

    public void select(string selection){
        switch (selection)
        {
        case "Translation":
            setText ("Die Veränderung der Position eines 3D Modells wird Translation genannt.");
            break;
        case "Rotation":
            setText ("Der Rotationswert bestimmt die Ausrichtung eines 3D Modells.");
            break;
        case "Scale":
            setText ("Die Größe eines 3D Modells wird Scale genannt.");
            break;
        case "TRS Matrix":
            setText ("Bei der TRS Matrix handelt es sich um das Produkt der Translations-, Rotations- und Scalematrix.");
            break;
        case "Mesh":
            setText ("Bei einem Mesh handelt es sich um eine Art Netz aus vielen Dreiecken.");
            break;
        case "Vertices":
            setText ("Vertices werden die Knotenpunkte eines Meshes genannt.");
            break;
        case "Interpolation":
            setText ("Interpolation nennt man den Vorgang, dass die Fläche eines Dreiecks eine bestimmte Farbmixtur annimmt. Diese Farbmixtur entsteht wenn man die Farben der Knotenpunkte zusammen mischt.");
            break;
        case "Indizes":
            setText ("Die Knotenpunkte des Meshes werden mit Indizes versehen, damit man sie eindeutig wiedererkennen kann. Sie werden also durchgezählt.");
            break;
        case "View Transform":
            setText ("View Transform beschreibt den Vorgang, der die Kamera zum Mittelpunkt der Szene macht.");
            break;
        case "Vertex Shader":
            setText ("Der Vertex Shader nutzt die TRS Matrix und rechnet sie in die Weltkoordinaten um.");
            break;
        case "Eye Space":
            setText ("Das die Weltkoordinaten relativ zur Kamera übersetzt werden nennt man Eye Space.");
            break;
        case "Projektionsmatrix":
            setText ("Die Projektionsmatrix transformiert die Koordinaten relativ zur Kamera.");
            break;
        case "Near and Far Plane":
            setText ("Far und Near Plane sind die beiden parallelen Flächen des Frustums.");
            break;
        case "Field of View":
            setText ("Field of View gibt an wie weit die Kamera in die Szene rein schaut.");
            break;
        case "Frustum":
            setText ("Ein Frustum ist eine abgeschnittene Pyramide. Sie wird verwendet um die Perspektive der Kamera darzustellen.");
            break;
        case "Aspect Ratio":
            setText ("Aspect Ratio ist das Seitenverhältnis des Frustums.");
            break;
        case "NDC":
            setText ("Normalized Device Coordinates sind die Koordinaten die die Meshes im normalisierten Raum haben.");
            break;
        case "Primitiven":
            setText ("Der Begriff Primitiv bezeichnet 2D oder 3D geometrische Figuren.");
            break;
        case "Fragments":
            setText ("Fragmente sind die Pixel, die bei der Rasterization als potentiell anzuzeigend ermittelt werden.");
            break;
        case "DDA":
            setText ("Digital Differential Analysis Algorithmus ist ein möglicher Algorithmus zur Rasterization.");
            break;
        case "BMA":
            setText ("Bresenham Midpoint Algorithmus ist ein möglicher Algorithmus zur Rasterization.");
            break;
        case "Pineda":
            setText ("Pineda Algorithmus ist ein möglicher Algorithmus zur Rasterization.");
            break;
        case "Fragment Shader":
            setText ("Der Fragment Shader generiert die Farben der einzelnen Fragmente.");
            break;
        case "UV Mapping":
            setText ("Mit dem UV Mapping bekommt ein 3D Objekt seine Textur.");
            break;
        case "z Buffer":
            setText ("Der Z-Buffer wird verwendet um die Pixel zu ermitteln, die endgültig am Monitor angezeigt werden.");
            break;
        }
    }

    public void setText(string text){
        Textfield.text = text;
    }

}
