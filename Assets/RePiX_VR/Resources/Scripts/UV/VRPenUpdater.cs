﻿using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR
{
    public class VRPenUpdater : MonoBehaviour
    {
        public Slider hardness, opacity, size;
        public PointableColorPicker color;

        private void Update()
        {
            var pen = TexturePen.Instance;
            pen.color = color.PickedColor;
            pen.hardness = hardness.value;
            pen.opacity = opacity.value;
            pen.size = size.value;
        }
    }
}