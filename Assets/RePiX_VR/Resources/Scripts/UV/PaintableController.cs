﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Object = UnityEngine.Object;

namespace RePiX_VR
{
    [Serializable]
    internal class RenderPair
    {
        /// <summary>
        /// A mesh/canvas pair. The canvas is optional and a missing mesh does not impede
        /// the mesh painting functionality.
        /// The target mesh needs to be properly UV mapped to be paintable.
        /// </summary>

        public GameObject targetMesh, canvasMesh;

        private PaintableMesh paintable;
        private UVCanvas canvas;

        public void Init()
        {
            if (!targetMesh) throw new MissingFieldException("Paintable requires a target mesh!");

            // The canvas is considered to be optional. Initialize it, if it exists.
            if (!canvasMesh) return;
            
            canvas = canvasMesh.AddComponent<UVCanvas>();
            canvas.MakeCanvas(paintable);
        }

        public void Clean()
        {
            Object.Destroy(paintable);
            if (canvas) Object.Destroy(canvas);
        }

        public void Clear()
        {
            paintable.ClearTexture();
        }
    }

    public class PaintableController : MonoBehaviour
    {
        /// <summary>
        /// Controls and cleans up a set of rendering components that make meshes
        /// paintable by the user.
        /// </summary>

        [SerializeField] private List<RenderPair> paintables;

        public void Init()
        {
            foreach (var p in paintables) p.Init();
        }

        public void ShutDown()
        {
            foreach (var p in paintables) p.Clean();
        }

        public void ClearTextures()
        {
            foreach (var p in paintables) p.Clear();
        }
    }
}