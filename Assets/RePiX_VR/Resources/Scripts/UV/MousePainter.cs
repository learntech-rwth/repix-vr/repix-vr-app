﻿using UnityEngine;

namespace RePiX_VR
{
    public class MousePainter : MonoBehaviour
    {
        private Camera mainC; // main camera for screen space ray casting 

        [SerializeField]
        private TexturePen pen;

        private void Start()
        {
            // determine main camera in scene
            mainC = Camera.main;
            if (mainC == null) mainC = GetComponent<Camera>();
            if (mainC == null) mainC = FindObjectOfType<Camera>();
        }

        private void Update()
        {
            var ray = mainC.ScreenPointToRay(Input.mousePosition);
            var mwp = Vector3.positiveInfinity;

            if (Physics.Raycast(ray, out var hit))
            {
                if (hit.collider.gameObject.CompareTag("PaintObject"))
                {
                    mwp = hit.point;
                }
            }

            pen.UpdatePen(mwp, Input.GetMouseButton(0));
        }
    }
}