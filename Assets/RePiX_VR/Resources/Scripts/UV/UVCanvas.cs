﻿using UnityEngine;
using UnityEngine.Rendering;

namespace RePiX_VR
{
    public class UVCanvas : MonoBehaviour
    {
        private Shader uvShader;
        private Material meshMaterial;

        private RenderTexture targetTexture;
        private Texture runTimeTexture;
        private CommandBuffer cbTexturePainting;

        private Mesh meshToDraw;
        private Material uvMaterial;

        private string originalTag;
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");
        private static readonly int MeshObject2World = Shader.PropertyToID("mesh_Object2World");

        public void Awake()
        {
            uvShader = Shader.Find("Texture Painter/TexturePaint");
            gameObject.tag = "PaintObject";
        }

        public void MakeCanvas(PaintableMesh painter)
        {
            if (!painter)
                return;
            
            var rend = GetComponent<Renderer>();

            if (!rend)
                return;

            rend.material = painter.Material;
            meshMaterial = rend.material;

            targetTexture = painter.paintedTexture;

            // setup shader to render into
            uvMaterial = new Material(uvShader);
            if (!uvMaterial.SetPass(0))
            {
                Debug.LogError("Invalid Shader Pass: " + name);
            }

            uvMaterial.SetTexture(MainTex, targetTexture);

            var baseTexture = meshMaterial.GetTexture(MainTex);
            runTimeTexture = new RenderTexture(baseTexture.width, baseTexture.height, 0)
            {
                anisoLevel = 0,
                useMipMap = false,
                filterMode = FilterMode.Bilinear
            };

            meshMaterial.SetTexture(MainTex, runTimeTexture);

            meshToDraw = GetComponent<MeshFilter>().mesh;
            cbTexturePainting = new CommandBuffer();
            cbTexturePainting.Blit(targetTexture, runTimeTexture);
            cbTexturePainting.name = "UVTexturePainting";
            cbTexturePainting.SetRenderTarget(runTimeTexture);
            cbTexturePainting.DrawMesh(meshToDraw, Matrix4x4.identity, uvMaterial);
            cbTexturePainting.Blit(runTimeTexture, targetTexture);

            painter.MainC.AddCommandBuffer(CameraEvent.AfterDepthTexture, cbTexturePainting);

            painter.OnUpdate += () => uvMaterial.SetMatrix(MeshObject2World, transform.localToWorldMatrix);
        }
    }
}