﻿using UnityEngine;

namespace RePiX_VR
{
    public class TexturePen : MonoBehaviour
    {
        public static TexturePen Instance { get; private set; } // singleton reference

        [Tooltip("Color of the pen.")]
        public Color color = Color.white;

        [Range(0.0f, 1.0f), Tooltip("Pen opacity")]
        public float opacity = 1.0f;

        [Tooltip("Size of the painted area.")]
        public float size = 1f;

        [Tooltip("Hardness of the pen.")]
        public float hardness = 1f;

        [SerializeField]
        private float minSize = 0.01f, maxSize = 0.1f;

        private static readonly int Hidden = Shader.PropertyToID("_Hidden");
        private static readonly int BrushColor = Shader.PropertyToID("_BrushColor");
        private static readonly int BrushOpacity = Shader.PropertyToID("_BrushOpacity");
        private static readonly int BrushSize = Shader.PropertyToID("_BrushSize");
        private static readonly int BrushHardness = Shader.PropertyToID("_BrushHardness");
        private static readonly int Mouse = Shader.PropertyToID("_Mouse");

        /// <summary>
        /// Size in the unity coordinate system.
        /// </summary>
        public float UnitSize => Mathf.Lerp(minSize, maxSize, size);

        private void Start()
        {
            if (Instance != null) Destroy(this);
            else Instance = this;
        }

        /// <summary>
        /// Updates the pen position in space.
        /// </summary>
        /// <param name="position">new position of the pen</param>
        /// <param name="down">the pen should currently be drawing</param>
        public void UpdatePen(Vector3 position, bool down)
        {
            var hideCursor = float.IsInfinity(position.x);

            transform.position = hideCursor ? Vector3.one * 1000f : position;

            Shader.SetGlobalFloat(Hidden, hideCursor ? 1f : 0f);

            Shader.SetGlobalColor(BrushColor, color);
            Shader.SetGlobalFloat(BrushOpacity, opacity);
            Shader.SetGlobalFloat(BrushSize, UnitSize);
            Shader.SetGlobalFloat(BrushHardness, hardness);
            Shader.SetGlobalVector(Mouse, (Vector4)position + new Vector4(0f, 0f, 0f, down ? 1f : 0f));
        }
    }
}