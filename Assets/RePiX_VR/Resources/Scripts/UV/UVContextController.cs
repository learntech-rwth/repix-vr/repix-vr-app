﻿using OmiLAXR;
using UnityEngine;
using OmiLAXR.xAPI;

namespace RePiX_VR
{
    [RequireComponent(typeof(PaintableController))]
    public class UVContextController : MonoBehaviour
    {
        public GameObject clearTextureButton;

        private PaintableController pc;

        private void Awake()
        {
            pc = GetComponent<PaintableController>();
        }

        private void OnEnable()
        {
            // this component will not work if enabled at the exact game start as
            // the below animator will not be accessible.

            pc.Init();
            clearTextureButton.SetActive(true);
        }

        private void Start()
        {
            transform.Find(Learner.Instance.IsVR ? "VR" : "NonVR").gameObject.SetActive(true);
        }


        private void OnDisable()
        {
            pc.ShutDown();
            clearTextureButton.SetActive(false);
        }
    }
}