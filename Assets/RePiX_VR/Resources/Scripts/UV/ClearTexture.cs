﻿using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Provides access to a mesh painter's clear method if such a component is attached.
    /// </summary>
    public class ClearTexture : MonoBehaviour
    {
        /// <summary>
        /// Clear texture if mesh painter exists.
        /// </summary>
        public void Try()
        {
            var pc = GetComponent<PaintableMesh>();
            if (pc) pc.ClearTexture();
            else Debug.LogWarning("Tried accessing paintable mesh where none exists.");
        }
    }
}