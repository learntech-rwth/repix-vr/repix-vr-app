﻿using OmiLAXR.UI;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR
{
    public class PenUpdater : MonoBehaviour
    {
        public ColorPicker2D colorPicker;
        public Slider opacity, hardness, size;

        private void Start()
        {
            colorPicker.OnChanged += (_, color) => TexturePen.Instance.color = color;
        }

        private void Update()
        {
            TexturePen.Instance.hardness = hardness.value;
            TexturePen.Instance.size = size.value;
            TexturePen.Instance.opacity = opacity.value;
        }
    }
}