﻿using OmiLAXR.Helpers;
using OmiLAXR.Interaction;
using UnityEngine;
using Valve.VR;

namespace RePiX_VR
{
    [RequireComponent(typeof(Attacher))]
    public class VRPainter : MonoBehaviour
    {
        public SteamVR_Action_Boolean action;

        private Transform handTransform;
        private SteamVR_Input_Sources handSource;

        [SerializeField]
        private TexturePen pen;
        public bool state;

        public float minSize = .1f, maxSize = 1f;
        public float range = 1f;

        private TrackedColorPicker tColorPicker;

        private Attacher attacher;

        private void RegisterColorPicker()
        {
            var colorPicker = attacher.AttachTo(handTransform);
            colorPicker.GetComponent<TrackedColorPicker>().ColorPicked += (_, c) => SetColor(c);
        }

        private void SetColor(Color c) => pen.color = c;

        private void Awake()
        {
            action.onChange += HandleAction;
            attacher = GetComponent<Attacher>();
        }

        private void OnEnable()
        {
            handTransform = FindLaserPointer.Find();
            
            RegisterColorPicker();
        }

        private void OnDisable() => GetComponent<Attacher>().Detach();

        private void HandleAction(SteamVR_Action_Boolean steamVrAction, SteamVR_Input_Sources source, bool newState)
        {
            state = newState;
        }

        private void Update()
        {
            var mwp = Vector3.positiveInfinity;

            if (Physics.Raycast(handTransform.position, handTransform.forward, out RaycastHit hit))
            {
                if (hit.collider.gameObject.CompareTag("PaintObject"))
                {
                    mwp = hit.point;

                    // Resize pen depending on distance to the surface
                    var dist = Mathf.Clamp01(Vector3.Distance(hit.point, handTransform.position) / range);
                    pen.size = Mathf.Lerp(minSize, maxSize, dist);
                }
                else
                {
                    hit.collider.GetComponent<IPointable>()?.PointAtWorldPosition(hit.point);
                }
            }

            pen.UpdatePen(mwp, state);
        }
    }
}