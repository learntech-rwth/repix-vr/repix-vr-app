﻿using RePiX_VR;
using UnityEngine;

namespace RePiX_VR
{
    public class Brush : MonoBehaviour
    {
        private TexturePen pen;
        private Renderer rend;

        private Transform opacityBrush;
        private Renderer opacityRend;
        private static readonly int Color1 = Shader.PropertyToID("_Color");

        private void Start()
        {
            pen = GetComponentInParent<TexturePen>();
            rend = GetComponent<Renderer>();

            opacityBrush = transform.GetChild(0);
        }

        private void Update()
        {
            rend.material.SetColor(Color1, pen.color);
            transform.localScale = Vector3.one * pen.UnitSize * 2f;
            opacityBrush.localScale = Vector3.one * pen.opacity;
        }
    }
}