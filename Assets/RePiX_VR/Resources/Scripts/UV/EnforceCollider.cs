﻿using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Enforces the existence of a collider of some form on a mesh
    /// by either activating a existing one or making a new mesh collider.
    /// </summary>
    public class EnforceCollider : MonoBehaviour
    {
        private bool isActive, exists;

        public void Init()
        {
            var col = GetComponent<Collider>();

            if (col)
            {
                exists = true;
                isActive = col.enabled;
                col.enabled = true;
            }
            else
            {
                gameObject.AddComponent<MeshCollider>();
            }
        }

        private void OnDestroy()
        {
            // restore collider to previous state
            var col = GetComponent<Collider>();

            if (exists)
            {
                col.enabled = isActive;
            }
            else
            {
                Destroy(col);
            }
        }
    }
}