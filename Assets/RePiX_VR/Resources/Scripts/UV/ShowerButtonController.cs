﻿using RePiX_VR;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR
{
    [RequireComponent(typeof(Button))]
    public class ShowerButtonController : MonoBehaviour
    {
        private Button btn;
        public ShowerController shower;

        private void Start()
        {
            btn = GetComponent<Button>();
            btn.onClick.AddListener(PlayAnimation);
            shower.showerEnd.AddListener(() => btn.interactable = true);
        }

        private void PlayAnimation()
        {
            shower.Shower();
            btn.interactable = false;
        }
    }
}