﻿using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace RePiX_VR
{
    // Adapted from Shahriar Shahrabi (2019)
    // https://github.com/IRCSS/TexturePaint

    public class PaintableMesh : MonoBehaviour
    {
        [HideInInspector]
        public Texture baseTexture; // used to determine the dimensions of the runtime texture
        
        public Mesh meshToDraw;
        
        public RenderTexture paintedTexture;

        public UVCanvas canvas;
        
        // Properties
        public Camera MainC { get; private set; }

        public Material Material { get; private set; }
        
        private Shader sUV, sIslandMarker, sFixIslands;
        
        private RenderTexture runTimeTexture; // the actual shader the stuff are going to be drawn to
        
        private Material mUV; // material which the UV shader is bound to
        private CommandBuffer cbTexturePainting, cbMarkingIslands;
        private RenderTexture markedIslands, fixedIslands;
        
        private int numberOfFrames;
        private Material mFixEdges;
        private static readonly int MainTex = Shader.PropertyToID("_MainTex");
        private static readonly int IslandMap = Shader.PropertyToID("_IslandMap");
        private static readonly int MeshObject2World = Shader.PropertyToID("mesh_Object2World");

        public event Action OnUpdate;

        private void Awake()
        {
            var rend = GetComponent<Renderer>();
            gameObject.tag = "PaintObject";
            
            var material = rend.material;
            baseTexture = material.mainTexture;
            
            // Copy renderer material
            Material = new Material(material);
            material = Material;
            rend.material = material;
            
            meshToDraw = GetComponent<MeshCollider>().sharedMesh;
            
            sUV = Shader.Find("Texture Painter/TexturePaint");
            sIslandMarker = Shader.Find("Texture Painter/MarkIslands");
            sFixIslands = Shader.Find("Texture Painter/FixIslandEdges");
            
            // Initialize main camera
            MainC = Camera.main;
            if (MainC == null) MainC = GetComponent<Camera>();
            if (MainC == null) MainC = FindObjectOfType<Camera>();
            
            var w = baseTexture ? baseTexture.width : 1024;
            var h = baseTexture ? baseTexture.height : 1024;

            // Initialize texture that is going to be rendered on the mesh (post-processing)
            runTimeTexture = new RenderTexture(w, h, 0)
            {
                anisoLevel = 0,
                useMipMap = false,
                filterMode = FilterMode.Bilinear
            };

            // Initialize texture that will be painted on
            paintedTexture = new RenderTexture(w, h, 0)
            {
                anisoLevel = 0,
                useMipMap = false,
                filterMode = FilterMode.Bilinear
            };

            // Render texture to use in island fixing shader
            fixedIslands = new RenderTexture(paintedTexture.descriptor);

            // Render texture that will be used in island marking shader
            markedIslands = new RenderTexture(w, h, 0, RenderTextureFormat.R8);

            // Set up material for the texture to be painted on, based on UVShader
            mUV = new Material(sUV);
            if (!mUV.SetPass(0)) Debug.LogError("Invalid Shader Pass: " + name);
            mUV.SetTexture(MainTex, paintedTexture);

            // Use runtime texture for the mesh material
            Material.SetTexture(MainTex, runTimeTexture);

            ClearTexture();

            // Prepare shader pass that removes seams between UV islands
            mFixEdges = new Material(sFixIslands);
            mFixEdges.SetTexture(IslandMap, markedIslands);
            mFixEdges.SetTexture(MainTex, paintedTexture);

            SetupCommandBuffer();
            
            if (canvas) canvas.MakeCanvas(this);
        }

        private void Update()
        {
            if (numberOfFrames > 2) MainC.RemoveCommandBuffer(CameraEvent.AfterDepthTexture, cbMarkingIslands);
            mUV.SetMatrix(MeshObject2World, gameObject.transform.localToWorldMatrix);
            numberOfFrames++;

            OnUpdate?.Invoke();
        }

        public void ClearTexture()
        {
            Graphics.SetRenderTarget(runTimeTexture);
            GL.Clear(false, true, Color.white);

            // clear with base texture
            if (baseTexture) Graphics.Blit(baseTexture, paintedTexture);
        }

        private void SetupCommandBuffer()
        {
            // Add command buffer that will fix island edges
            cbMarkingIslands = new CommandBuffer();
            cbMarkingIslands.name = "markingIslands";

            cbMarkingIslands.SetRenderTarget(markedIslands);
            Material mIslandMarker = new Material(sIslandMarker);

            // Add a command to draw the object's mesh into the command buffer, add the command buffer to rendering pipeline
            cbMarkingIslands.DrawMesh(meshToDraw, Matrix4x4.identity, mIslandMarker);
            MainC.AddCommandBuffer(CameraEvent.AfterDepthTexture, cbMarkingIslands);

            // Add command buffer to be painted on by player
            cbTexturePainting = new CommandBuffer();
            cbTexturePainting.name = "TexturePainting";

            // Set target texture, add mesh draw command to buffer
            cbTexturePainting.SetRenderTarget(runTimeTexture);
            cbTexturePainting.DrawMesh(meshToDraw, Matrix4x4.identity, mUV);

            // Copy textures for next processing frame
            cbTexturePainting.Blit(runTimeTexture, fixedIslands, mFixEdges);
            cbTexturePainting.Blit(fixedIslands, runTimeTexture);
            cbTexturePainting.Blit(runTimeTexture, paintedTexture);

            // Add command buffer to the main cameras rendering pipeline
            MainC.AddCommandBuffer(CameraEvent.AfterDepthTexture, cbTexturePainting);
        }
    }
}