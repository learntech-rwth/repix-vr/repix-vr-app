﻿using OmiLAXR.Interaction;
using System;
using OmiLAXR.Adapters.SteamVR;
using UnityEngine;

namespace RePiX_VR
{
    [RequireComponent(typeof(AreaInteraction))]
    public class PointableColorPicker : MonoBehaviour
    {
        [Tooltip("Gradient texture to use for color picker.")]
        public Texture2D gradient;

        public Color PickedColor { get; private set; } = Color.red;

        public event Action<Color> OnColorChange;

        private AreaInteraction area;
        private RectTransform picker;

        private RectTransform self;

        private void Start()
        {
            GetComponent<AreaInteraction>().OnSelect += PositionToColor;

            picker = transform.Find("PickerCircle").GetComponent<RectTransform>();
            self = GetComponent<RectTransform>();
        }

        private void PickColor(Color c)
        {
            PickedColor = c;
            OnColorChange?.Invoke(c);
        }

        private void PositionToColor(Vector2 pos)
        {
            // map into [-1, 1]^2 domain (unclamped)
            var vec = (pos - 0.5f * Vector2.one) * 2f;
            if (vec.magnitude > 1f) vec.Normalize();

            // Leave some space on the circle rim
            vec *= 0.98f;

            // back into UV space
            vec = (vec + Vector2.one) / 2f;

            picker.localPosition = new Vector2((vec.x - self.pivot.x) * self.rect.width, (vec.y - self.pivot.y) * self.rect.width);
            
            PickColor(gradient.GetPixelBilinear(vec.x, vec.y));
        }
    }
}