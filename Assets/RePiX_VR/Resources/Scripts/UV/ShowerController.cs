﻿using System;
using System.Collections;
using System.Threading.Tasks;
using RePiX_VR;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR
{
    [RequireComponent(typeof(Animator))]
    public class ShowerController : MonoBehaviour
    {
        [Header("References")]
        private Animator anim;
        private Vector3 startPoint, endPoint;
        
        public ParticleSystem[] particleSystems;

        [Header("Properties")]
        [Tooltip("Total time spent in shower animation (in s).")]
        public float showerTime = 2f;

        [Tooltip("Distance the shower sinks into the floor.")]
        public float sinkDepth = 3.5f;

        /// <summary>
        /// Gets called once the player model is fully hidden by the shower.
        /// </summary>
        [Header("Events")]
        public UnityEvent showerStart = new UnityEvent();

        /// <summary>
        /// Gets called once the shower cycle ended and the curtain starts to open.
        /// </summary>
        public UnityEvent showerEnd = new UnityEvent();
        
        /// <summary>
        /// Gets called once the shower emerged and is about to open.
        /// </summary>
        public UnityEvent emerge = new UnityEvent();

        /// <summary>
        /// Gets called once the shower is fully closed before submerging back into the ground.
        /// </summary>
        public UnityEvent submerge = new UnityEvent();

        private void Awake()
        {
            endPoint = transform.position;
            startPoint = endPoint - Vector3.up * sinkDepth;
            anim = GetComponent<Animator>();
        }

        private void Open() => anim.Play("Open");

        // Called as AnimationEvent at the end of Close animation
        public async void Shower()
        {
            anim.Play("Close");

            var state = Animator.StringToHash("Base Layer.Closed");
            while (anim.GetCurrentAnimatorStateInfo(0).fullPathHash != state) await Task.Yield();
            showerStart.Invoke();

            foreach (var p in particleSystems) p.Play();
            await Task.Delay((int) (showerTime * 1000));

            foreach (var p in particleSystems) p.Stop();
            await Task.Delay(1000);

            Open();
            showerEnd.Invoke();
        }

        private void OnEnable() => StartCoroutine(Emerge());
        
        private IEnumerator Emerge()
        {
            var t = 0f;

            while (t < 1f)
            {
                transform.position = Vector3.Slerp(startPoint, endPoint, t);
                t = Mathf.Clamp01(t + Time.deltaTime);
                yield return null;
            }
            
            emerge.Invoke();
            anim.Play("Emerge");
        }
        
        public async void HideShower()
        {
            anim.Play("Submerge");

            var state = Animator.StringToHash("Base Layer.Submerged");
            while (anim.GetCurrentAnimatorStateInfo(0).fullPathHash != state) await Task.Yield();

            submerge.Invoke();
            
            var t = 0f;

            while (t < 1f)
            {
                transform.position = Vector3.Lerp(endPoint, startPoint, t);
                t = Mathf.Clamp01(t + Time.deltaTime);
                await Task.Yield();
            }

            transform.parent.gameObject.SetActive(false);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ShowerController))]
public class ShowerControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var shower = target as ShowerController;
        
        if (GUILayout.Button("Hide shower"))
        {
            if (shower != null) shower.HideShower();
        }
    }
}
#endif