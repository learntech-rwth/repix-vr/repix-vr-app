﻿using UnityEngine;

namespace RePiX_VR
{
    public class DispenseObject : MonoBehaviour
    {
        [Tooltip("Object to be dispensed.")]
        public GameObject prefab;

        [Tooltip("Place at where object will be dispensed.")]
        public Transform origin;

        [Tooltip("Force vector to apply on ejection.")]
        public Vector3 force = new Vector3(0f, 0f, 0f);


        public void Dispense()
        {
            var obj = Instantiate(prefab);
            obj.transform.position = origin.position;

            var rb = obj.GetComponent<Rigidbody>();

            if (rb)
            {
                rb.AddForce(force);
            }
        }
    }
}