﻿using UnityEngine.UI;

using OmiLAXR.UI;
using OmiLAXR.xAPI.Extensions;

namespace RePiX_VR
{
    public class RPPanel : MenuPanel
    {
        public Button buttonApplication => GetButton("Button_Application");
        public Button button3DGeometry => GetButton("Button_3DGeometry");
        public Button button3DTransformation => GetButton("Button_3DTransformation");
        public Button buttonClipping => GetButton("Button_Clipping");
        public Button buttonRasterization => GetButton("Button_Rasterization");
        public Button buttonLighting => GetButton("Button_(Local)Lighting");
        public Button buttonTexturing => GetButton("Button_Texturing");
        public Button buttonVisibilityTest => GetButton("Button_VisibilityTest");
        public Button buttonImage => GetButton("Button_Image");
        public Button GetButton(string name) => transform.Find(name).GetComponent<Button>();

        public void ResetState()
        {
            buttonApplication.SetDisabled(false);
            button3DGeometry.SetDisabled(true);
            button3DTransformation.SetDisabled(true);
            buttonClipping.SetDisabled(true);
            buttonRasterization.SetDisabled(true);
            buttonLighting.SetDisabled(true);
            buttonTexturing.SetDisabled(true);
            buttonVisibilityTest.SetDisabled(true);
            buttonImage.SetDisabled(true);
        }
    }

}