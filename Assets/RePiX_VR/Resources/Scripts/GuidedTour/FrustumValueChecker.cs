﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR
{
    public class FrustumValueChecker : MonoBehaviour
    {
        [Tooltip("The frustum you want to check.")]
        public Frustum frustum;

        public bool considerNearZ = true;
        public float nearZ = 0.0f;
        
        public bool considerFarZ = true;
        public float farZ = 0.0f;

        public bool considerFov = true;
        public float fov = 0.0f;

        public bool considerAspect = true;
        public float aspect = 0.0f;

        [Tooltip("How accurate should the values be comared? Comparison is frustum.value * accurationLevel. Greater means more accuracy")]
        public uint accurationLevel = 1000;

        public bool useRound = false;
        public bool checkMatchOnUpdate = false;

        private bool _isMatching = false;

        public UnityEvent ValuesMatched;

        private void Update()
        {
            if (!frustum)
                return;
            
            if (checkMatchOnUpdate)
                CheckMatch();
        }

        public void CheckMatch()
        {
            // If it is considering a value, but not matching the values, then do not match
            if (considerFov && !AreMatching(fov, frustum.fov) ||
                considerAspect && !AreMatching(aspect, frustum.aspect) ||
                considerFarZ && !AreMatching(farZ, frustum.farZ) ||
                considerNearZ && !AreMatching(nearZ, frustum.nearZ))
            {
                _isMatching = false;
                return;
            }

            Match();
        }

        public bool AreMatching(float a, float b)
        {
            if (useRound)
                return (int)Math.Round(a * accurationLevel) == (int)Math.Round(b * accurationLevel);
            return (int) a * accurationLevel == (int) b * accurationLevel;
        }

        private void Match()
        {
            if (_isMatching)
                return;
            ValuesMatched.Invoke();
            _isMatching = true;
        }

        #region Manipulation Functions
        public void SetConsiderAspect(bool value) => considerAspect = value;
        public void SetConsiderFov(bool value) => considerFov = value;
        public void SetConsiderFarZ(bool value) => considerFarZ = value;
        public void SetConsiderNearZ(bool value) => considerNearZ = value;
        public void SetFarZ(float z) => farZ = z;
        public void SetNearZ(float z) => nearZ = z;
        public void SetFov(float fov) => this.fov = fov;
        public void SetAspect(float aspect) => this.aspect = aspect;
        public void SetAccurationLevel(int level) => accurationLevel = (uint)level;
        public void SetUseRound(bool yes) => useRound = yes;

        #endregion
    }
}