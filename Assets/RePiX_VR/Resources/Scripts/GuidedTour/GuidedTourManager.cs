﻿using System;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR;
using UnityEngine;
using UnityEngine.Playables;

using OmiLAXR.Extensions;
using OmiLAXR.Effects;
using OmiLAXR.Helpers.Sticky;
using OmiLAXR.Interaction;
using OmiLAXR.InstructionalDesign;
using OmiLAXR.Timeline;
using OmiLAXR.UI;
using RePiX_VR.GeometryStage;
using TMPro;
using Valve.VR.InteractionSystem;

namespace RePiX_VR
{
    [Obsolete("Will be removed soon.")]
    public class GuidedTourManager : MonoBehaviour
    {
        private struct TourEntry
        {
            public PlayableDirector Director { get; }
            public Action StartFunction { get; }
            public PointableUI Button { get; }

            public TourEntry(PlayableDirector director, Action startFunction, PointableUI button)
            {
                Director = director;
                StartFunction = startFunction;
                Button = button;
            }
        }
        private PlayableDirector DirectorApplication
            => applicationStage?.GetComponentInChildren<PlayableDirector>(true);

        private PlayableDirector Director3DGeometry => GetDirector("3D Geometry Sequence");
        private PlayableDirector DirectorImage => GetDirector("Image Sequence");
        private PlayableDirector DirectorVisibilityTest => GetDirector("VisibilityTest Sequence");

        private PlayableDirector DirectorTexturing
            => texturingStage.GetComponentInChildren<PlayableDirector>();

        private PlayableDirector DirectorLighting
            => lightingStage.GetComponentInChildren<PlayableDirector>();

        private PlayableDirector DirectorRasterization
            => rasterizationStage.GetComponentInChildren<PlayableDirector>();

        private PlayableDirector DirectorClipping => GetDirector("Clipping Sequence");
        private PlayableDirector Director3DTransformation => GetDirector("3D Transformation Sequence");

        public Progressbar progressbar;
        public PlayPauseDirector playPauseDirector;

        private PlayableDirector GetDirector(string directorName) =>
            directorRenderingPipeline.transform.Find(directorName).GetComponent<PlayableDirector>();

        private string _activeStage = "";

        private IEnumerable<PlayableDirector> AllDirectors => NamedAllDirectors.Values;

        public TextMeshProUGUI titleTextObject;

        public LearningScenario learningScenario;

        private Dictionary<string, TourEntry> TourEntries => new Dictionary<string, TourEntry>()
        {
            ["Tutorial"] = new TourEntry(directorTutorial, StartTutorial, null),
            ["Introduction"] = new TourEntry(directorRenderingPipeline, StartIntroduction, null),
            ["Application"] = new TourEntry(DirectorApplication, StartApplicationStage, GameObject.Find("Button_Application")?.GetComponent<PointableUI>()),
            ["3D Geometry"] = new TourEntry(Director3DGeometry, Start3DGeometry, GameObject.Find("Button_3DGeometry")?.GetComponent<PointableUI>()),
            ["3D Transformation"] = new TourEntry(Director3DTransformation, Start3DTransformation, GameObject.Find("Button_3DTransformation")?.GetComponent<PointableUI>()),
            ["Clipping"] = new TourEntry(DirectorClipping, StartClipping, GameObject.Find("Button_Clipping")?.GetComponent<PointableUI>()),
            ["Lighting"] = new TourEntry(DirectorLighting, StartLighting, GameObject.Find("Button_(Local)Lighting")?.GetComponent<PointableUI>()),
            ["Texturing"] = new TourEntry(DirectorTexturing, StartTexturing, GameObject.Find("Button_Texturing")?.GetComponent<PointableUI>()),
            ["Rasterization"] = new TourEntry(DirectorRasterization, StartRasterization, GameObject.Find("Button_Rasterization")?.GetComponent<PointableUI>()),
            ["Visibility Test"] = new TourEntry(DirectorVisibilityTest, StartVisibilityTest,  GameObject.Find("Button_VisibilityTest")?.GetComponent<PointableUI>()),
            ["Image"] = new TourEntry(DirectorImage, StartImage,  GameObject.Find("Button_Image")?.GetComponent<PointableUI>()),
        };

        private Dictionary<string, PlayableDirector> NamedAllDirectors
            => TourEntries.Select(te => new KeyValuePair<string, PlayableDirector>(te.Key, te.Value.Director))
                .ToDictionary(t => t.Key, t => t.Value);

        private Dictionary<string, PointableUI> AllButtons
            => TourEntries.Select(te => new KeyValuePair<string, PointableUI>(te.Key, te.Value.Button))
                .ToDictionary(t => t.Key, t => t.Value);

        public PlayableDirector directorRenderingPipeline;
        public PlayableDirector directorTutorial;
        public GameObject scenes;

        public GameObject stageOverviewPanel;
        
        [Header("Stages")]
        public GameObject applicationStage;
        public GameObject rasterizationStage;
        public GameObject texturingStage;
        public GameObject lightingStage;

        private GameObject TutorialScene => scenes.transform.Find("Tutorial").Find("Tutorial Scene").gameObject;
        private GameObject RPScene => scenes.transform.Find("Rendering Pipeline").gameObject;
        private GameObject GbScene => scenes.transform.Find("Geometry Basics").gameObject;
        private GameObject FrustumContext => RPScene.transform.Find("Frustum Context").gameObject;
        private GameObject FrustumPanel => FrustumContext.transform.Find("Frustum Panel").gameObject;
        private Frustum Frustum => FrustumContext.GetComponentInChildren<Frustum>();
        private UnitFrustum UnitFrustum => FrustumContext.GetComponentInChildren<UnitFrustum>();
        private GameObject Image2D => FrustumContext.transform.Find("2D Preview Image").gameObject;

        public void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        private void RebindEvents()
        {
            if (DirectorApplication)
            {
                DirectorApplication.stopped -= DirectorApplicationOnStopped;
                DirectorApplication.stopped += DirectorApplicationOnStopped;
            }
            
            if (Director3DGeometry)
            {
                Director3DGeometry.stopped -= Director3DGeometryOnStopped;
                Director3DGeometry.stopped += Director3DGeometryOnStopped;
            }
            
            if (Director3DTransformation)
            {
                Director3DTransformation.stopped -= Director3DTransformationOnStopped;
                Director3DTransformation.stopped += Director3DTransformationOnStopped;
            }
            
            if (DirectorClipping)
            {
                DirectorClipping.stopped -= DirectorClippingOnStopped;
                DirectorClipping.stopped += DirectorClippingOnStopped;
            }
            
            if (DirectorRasterization)
            {
                DirectorRasterization.stopped -= DirectorRasterizationOnStopped;
                DirectorRasterization.stopped += DirectorRasterizationOnStopped;
            }

            if (DirectorTexturing)
            {
                DirectorTexturing.stopped -= DirectorTexturingOnStopped;
                DirectorTexturing.stopped += DirectorTexturingOnStopped;
            }

            if (DirectorLighting)
            {
                DirectorLighting.stopped -= DirectorLightingOnStopped;
                DirectorLighting.stopped += DirectorLightingOnStopped;
            }

            if (DirectorImage)
            {
                DirectorImage.stopped -= DirectorImageOnStopped;
                DirectorImage.stopped += DirectorImageOnStopped;
            }

            if (directorTutorial)
            {
                directorTutorial.stopped -= DirectorTutorialOnStopped;
                directorTutorial.stopped += DirectorTutorialOnStopped;
            }

            if (directorRenderingPipeline)
            {
                directorRenderingPipeline.stopped -= DirectorRenderingPipelineOnStopped;
                directorRenderingPipeline.stopped += DirectorRenderingPipelineOnStopped;
            }

            if (DirectorVisibilityTest)
            {
                DirectorVisibilityTest.stopped -= DirectorVisibilityTestOnStopped;
                DirectorVisibilityTest.stopped += DirectorVisibilityTestOnStopped;
            }

            AllDirectors.ForEach(director =>
            {
                if (director == null)
                    return;
                
                director.played -= SetDirectorToProgressBar;
                director.played += SetDirectorToProgressBar;
            });
            
            if (learningScenario)
                learningScenario.GetActiveStep().End();
        }

        private void DirectorVisibilityTestOnStopped(PlayableDirector obj)
        {
            Image2D.SetActive(false);
            FrustumContext.SetActive(false);
        }

        private void DirectorRenderingPipelineOnStopped(PlayableDirector obj)
            => RPScene.Find("Mini Scene Context").SetActive(false);

        private void SetDirectorToProgressBar(PlayableDirector director)
        {
            if (progressbar)
                progressbar.SetDirector(director);
            if (playPauseDirector)
                playPauseDirector.SetDirector(director);
        }

        private void DirectorLightingOnStopped(PlayableDirector obj)
        {
            lightingStage.transform.Find("Lighting Stage Scene")?.gameObject.SetActive(false);
            lightingStage.transform.Find("Mini Scene")?.gameObject.SetActive(false);
        }

        private void DirectorTutorialOnStopped(PlayableDirector obj)
        {
            
        }

        private void DirectorImageOnStopped(PlayableDirector obj)
        {
            Frustum.enabled = false;
            UnitFrustum.enabled = false;
            Image2D.SetActive(false);
            FrustumContext.SetActive(false);
            FrustumPanel.SetActive(false);

            if (Learner.Instance.IsVR) return;
            GameObject.Find("Slider_Aspect_2D")?.SetActive(false);
            GameObject.Find("Slider_FOV_2D")?.SetActive(false);
            GameObject.Find("Button_ToggleClipping")?.SetActive(false);
            GameObject.Find("Button_ToggleClippingColor_2D")?.SetActive(false);
            GameObject.Find("NonVR_Controls")?.SetActive(false);
        }

        private void DirectorTexturingOnStopped(PlayableDirector obj)
        {
            texturingStage.transform.Find("Texturing Stage Scene")?.gameObject.SetActive(false);
            texturingStage.transform.Find("Simple Texturing")?.gameObject.SetActive(false);
        }

        private void DirectorRasterizationOnStopped(PlayableDirector obj)
        {
            rasterizationStage.GetComponentInChildren<TriangleBuilder>()?.SetInteractionState(TriangleInteractionState.None);
            var scene = rasterizationStage.transform.Find("Rasterization Stage Scene");
            if (!scene)
                return;
            scene.Find("Rasterization Panel")?.gameObject.SetActive(false);
            scene.Find("3D Scene Interaction")?.gameObject.SetActive(false);
            scene.Find("InputMappingCalculation")?.gameObject.SetActive(false);
            scene.Find("InputMappingSketch")?.gameObject.SetActive(false);
            scene.Find("FragmentsSketch")?.gameObject.SetActive(false);
            scene.Find("Bresenham Image Canvas")?.gameObject.SetActive(false);
            
        }

        private void DirectorApplicationOnStopped(PlayableDirector obj)
        {
            var stage = applicationStage.transform.Find("Application Stage Scene");
            if (!stage)
                return;
            stage.Find("Solar System")?.gameObject.SetActive(false);
            stage.gameObject.SetActive(false);
            
            var skipButton = GameObject.Find("Button_SkipApplicationStep");
            if (skipButton && skipButton.gameObject)
                skipButton.gameObject.SetActive(false);
        }

        private void Director3DTransformationOnStopped(PlayableDirector obj)
        {
            Frustum.GetComponentsInChildren<BlinkController>().ForEach(b =>
            {
                if (!b) return;
                b.Stop();
                b.enabled = false;
            });
            Frustum.transform.Find("Camera Position Label")?.gameObject.SetActive(false);

            HideFrustumElements();
        }

        private void DirectorClippingOnStopped(PlayableDirector obj) => HideFrustumElements();

        private void HideFrustumElements()
        {
            if (Frustum)
            {
                var videoRenderCanvasGo = Frustum.transform.Find("VideoRenderCanvas")?.gameObject;
                if (videoRenderCanvasGo)
                    videoRenderCanvasGo.SetActive(false);

                var videoPlayerGo = Frustum.transform.Find("Video Player")?.gameObject;
                if (videoPlayerGo)
                    videoPlayerGo.SetActive(false);

                Frustum.enabled = false;
            }

            if (UnitFrustum)
            {
                UnitFrustum.enableClipping = false;
                UnitFrustum.enabled = false;
            }

            if (FrustumPanel)
                FrustumPanel.SetActive(false);

            if (FrustumContext)
                FrustumContext.SetActive(false);

            if (Learner.Instance.IsVR) return;
            
            HideGameObjects("Slider_Aspect_2D", "Slider_FOV_2D", "Button_ToggleClipping", "Button_ToggleClippingColor_2D", "NonVR_Controls");
        }

        private static void HideGameObjects(params string[] goNames)
        {
            foreach (var goName in goNames)
            {
                var go = GameObject.Find(goName);
                if (!go)
                    continue;
                go.SetActive(false);
            }
        }

        private void Director3DGeometryOnStopped(PlayableDirector obj)
        {
            var exampleMesh = GbScene.Find("ExampleMesh");
            var exampleTriangle = GbScene.Find("ExampleTriangle");

            // Reset rotating bunny
            var rotatingBunny = GbScene.Find("Rotating Bunny");
            if (rotatingBunny)
            {
                rotatingBunny.Find("White Bunny")?.SetActive(true);
                rotatingBunny.Find("Wireframe Bunny")?.SetActive(false);
                rotatingBunny.SetActive(false);
            }

            // Recover sticky manager state
            var stickyManager = GbScene.GetComponentInChildren<StickyManager>();
            if (stickyManager != null)
                stickyManager.RecoverStartList();

            // Disable example mesh
            if (exampleMesh)
            {
                exampleMesh.ForEachChild(c => c.SetActive(false));
                exampleMesh.SetActive(false);
            }

            // Disable example triangle
            if (exampleTriangle)
            {
                exampleTriangle.ForEachChild(c => c.SetActive(false));
                exampleTriangle.SetActive(false);
            }
            
            GbScene.GetComponentInChildren<TriangleBuilder>()?.SetInteractionState(TriangleInteractionState.None);
            GbScene.GetComponentInChildren<TourVertexManager>()?.HideElements();
        }

        private void StopAll() => AllDirectors.ForEach(director =>
        {
            if (director)
                director.Stop();
        });

        private void HideStartStage()
        {
            if (playPauseDirector.enabled)
                return;
            // hide start buttons
            GameObject.Find("Button_StartTutorialEnglish")?.SetActive(false);
            GameObject.Find("Button_StartTutorialDeutsch")?.SetActive(false);
            playPauseDirector.enabled = true;
            playPauseDirector.gameObject.SetActive(true);
            playPauseDirector.director.Play();
        }

        public void StartStage(PlayableDirector director, string title, string learningUnit)
        {
            if (titleTextObject)
                titleTextObject.text = title;
            
            RebindEvents();
            HideStartStage();
            StopAll();

            if (learningScenario)
                learningScenario.BeginStep(learningUnit);
            
            progressbar.SetDirector(director);
            director.Play();
        }

        public void StartApplicationStage() => StartStage(DirectorApplication, "Application", "application");
        public void Start3DGeometry() => StartStage(Director3DGeometry, "3D Geometry", "3d_geometry");
        public void Start3DTransformation() => StartStage(Director3DTransformation, "3D Transformation", "3d_transformation");
        public void StartClipping() => StartStage(DirectorClipping, "Clipping", "clipping");
        public void StartRasterization() => StartStage(DirectorRasterization, "Rasterization", "rasterization");
        public void StartLighting() => StartStage(DirectorLighting, "Lighting", "lighting");
        public void StartTexturing() => StartStage(DirectorTexturing, "Texturing", "texturing");
        public void StartVisibilityTest() => StartStage(DirectorVisibilityTest, "Visibility Test", "visibility_test");
        public void StartImage() => StartStage(DirectorImage, "Image", "image");
        public void StartIntroduction() => StartStage(directorRenderingPipeline, "Introduction", "rp_introduction");

        public void JumpToStage(string stage)
        {
            var action = TourEntries[stage].StartFunction;
            action();
            EnableButtonsTo(stage);
        }

        public void EnableButtonsTo(string stage = "")
        {
            if (stageOverviewPanel)
                stageOverviewPanel.SetActive(true);
            var allButtons = AllButtons;
            foreach (var item in allButtons)
            {
                var button = item.Value;
                if (!button)
                    continue;
                Destroy(button.GetComponent<BlinkController>());
                button.SetDisabled(false);
                if (item.Key != stage) continue;
                button.FireSelect();
                return;
            }
        }
        
        public void StartTutorial()
        {
            HideStartStage();
            
            if (learningScenario)
                learningScenario.BeginStep("tutorial");
            
            directorTutorial.Play();
        }

        private void SetStage(string stage)
        {
            if (_activeStage == stage || stage == string.Empty)
                return;
            _activeStage = stage;
            print("[GuidedTourManager]: Started stage " + stage);
        }

        private void Start()
        {
            foreach (var item in NamedAllDirectors)
            {
                var director = item.Value;
                var stageName = item.Key;
                director.played += pd => SetStage(stageName);
            }
        }
    }

}