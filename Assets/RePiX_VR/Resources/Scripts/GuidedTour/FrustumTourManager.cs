﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR
{
    public class FrustumTourManager : MonoBehaviour
    {
        public FrustumPlaneInteraction farPlane;
        public FrustumPlaneInteraction nearPlane;
        public Frustum frustum;
        public UnitFrustum unitFrustum;
        public GameObject cameraPositionLabel;
        public GameObject cameraGizmoCanvas;
        public GameObject cameraModel;
        public GameObject preview2DImage;
        public GameObject frustumPanel;
        public void ResetTourFrustum()
        {
            // Reset Frustum Value Checker
            var fvc = GetComponent<FrustumValueChecker>();
            fvc.considerNearZ = true;
            fvc.considerFarZ = true;
            fvc.considerFov = false;
            fvc.considerAspect = false;
            fvc.useRound = false;
            fvc.enabled = false;

            // Reset values
            farPlane.SetAllowFlags(false);
            nearPlane.SetAllowFlags(false);

            // Disable objects
            cameraGizmoCanvas.SetActive(false);
            cameraModel.SetActive(false);
            cameraPositionLabel.SetActive(false);
            unitFrustum.enableClipping = false;
            unitFrustum.enabled = false;
            frustum.enabled = false;
            preview2DImage.SetActive(false);
            frustumPanel.SetActive(false);
        }
    }
}
