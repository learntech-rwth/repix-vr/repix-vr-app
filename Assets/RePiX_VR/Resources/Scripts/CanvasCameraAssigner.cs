using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class CanvasCameraAssigner : MonoBehaviour
{
    private void Awake()
    {
        var canvas = GetComponent<Canvas>();
        var camera = Camera.main;
        if (!camera) return;
        canvas.worldCamera = camera;
    }

}
