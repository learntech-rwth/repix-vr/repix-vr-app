﻿using System;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;

namespace RePiX_VR
{ 
    [Serializable]
    public class TrackpadPositionAction : UnityEvent<Vector2> { }

    [Serializable]
    public class TrackpadTouchAction : UnityEvent<bool> { }

    public class TrackpadListener : MonoBehaviour
    {
        [Tooltip("Gradient selection action. (E.g. trackpad touch)")]
        public SteamVR_Action_Vector2 touchPositionAction = SteamVR_Input.GetVector2Action("Move");

        [Tooltip("Determines whether the trackpad is currently being touched.")]
        public SteamVR_Action_Boolean touchAction = SteamVR_Input.GetBooleanAction("Touch");

        public TrackpadPositionAction change = new TrackpadPositionAction();
        public TrackpadTouchAction touchStateChange = new TrackpadTouchAction();
        
        private bool _freeze;

        private Vector2 _axis;

        private void Awake()
        {
            var pose = transform.parent.GetComponent<SteamVR_Behaviour_Pose>();
            touchPositionAction.AddOnChangeListener(ChangeListener, pose.inputSource);
            touchAction.AddOnUpdateListener(TouchListener, pose.inputSource);
        }

        private void ChangeListener(SteamVR_Action_Vector2 fromAction, SteamVR_Input_Sources fromSource, Vector2 axis,
            Vector2 delta)
        {
            this._axis = axis;
        }

        private void TouchListener(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool newState)
        {
            if (_freeze == newState)
            {
                _freeze = !newState;
                touchStateChange.Invoke(newState);
            }
            else if (!_freeze)
            {
                change.Invoke(_axis);
            }
        }
    }
}