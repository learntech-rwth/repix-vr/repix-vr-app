﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class controls the animations shown on the screen in the substages "Pineda" and "Bresenham"
// The animations are done by setting individual pixels of an image and showing that (upscaled) image on the screen
namespace RePiX_VR
{
    public class Ra_FragmentsAnimation : MonoBehaviour
    {
        Ra_ImageController imageCtrl;
        private int pinedaScaler = 5;
        private int bresenhamScaler = 10;
        public GameObject bresenhamImage;

        // Start is called before the first frame update
        void Start()
        {
            imageCtrl = GetComponent<Ra_ImageController>();
        }


        public void PinedaAnimationSetup()
        {
            imageCtrl.AnimationSetup(20 * pinedaScaler);
        }

        public void BresenhamAnimationSetup()
        {
            imageCtrl.AnimationSetup(10 * bresenhamScaler);
        }

        // This region defines the Pineda animations
        #region Pineda Animation

        public void PinedaAnimationVertices()
        {
            Vector2Int[] image = new Vector2Int[]
                        {
                new Vector2Int (4,4),
                new Vector2Int (14,4),
                new Vector2Int (4,14)
            };

            imageCtrl.UpdateAnimation(imageCtrl.ScaleImageUp(image, pinedaScaler), new Color(0.7f, 0.07f, 0.17f, 1.0f));
        }

        public void PinedaAnimationEquations()
        {
            PinedaAnimationEq1(false);
            PinedaAnimationEq2(false);
            PinedaAnimationEq3(false);
        }

        public void PinedaAnimationEq1(bool highlighted)
        {
            Vector2Int[] image = new Vector2Int[]
                        {
                new Vector2Int (0,4),
                new Vector2Int (1,4),
                new Vector2Int (2,4),
                new Vector2Int (3,4),
                new Vector2Int (5,4),
                new Vector2Int (6,4),
                new Vector2Int (7,4),
                new Vector2Int (8,4),
                new Vector2Int (9,4),
                new Vector2Int (10,4),
                new Vector2Int (11,4),
                new Vector2Int (12,4),
                new Vector2Int (13,4),
                new Vector2Int (15,4),
                new Vector2Int (16,4),
                new Vector2Int (17,4),
                new Vector2Int (18,4),
                new Vector2Int (19,4)
            };
            Color col = highlighted ? new Color(0.99f, 0.39f, 0.13f, 1.0f) : new Color(0.86f, 0.51f, 0.13f, 0.7f);
            imageCtrl.UpdateAnimation(imageCtrl.ScaleImageUp(image, pinedaScaler), col);
        }

        public void PinedaAnimationEq2(bool highlighted)
        {
            Vector2Int[] image = new Vector2Int[]
                        {
                new Vector2Int (4,0),
                new Vector2Int (4,1),
                new Vector2Int (4,2),
                new Vector2Int (4,3),
                new Vector2Int (4,5),
                new Vector2Int (4,6),
                new Vector2Int (4,7),
                new Vector2Int (4,8),
                new Vector2Int (4,9),
                new Vector2Int (4,10),
                new Vector2Int (4,11),
                new Vector2Int (4,12),
                new Vector2Int (4,13),
                new Vector2Int (4,15),
                new Vector2Int (4,16),
                new Vector2Int (4,17),
                new Vector2Int (4,18),
                new Vector2Int (4,19)
            };
            Color col = highlighted ? new Color(0.99f, 0.39f, 0.13f, 1.0f) : new Color(0.86f, 0.51f, 0.13f, 0.7f);
            imageCtrl.UpdateAnimation(imageCtrl.ScaleImageUp(image, pinedaScaler), col);
        }


        public void PinedaAnimationEq3(bool highlighted)
        {
            Vector2Int[] image = new Vector2Int[]
                        {
                new Vector2Int (0,18),
                new Vector2Int (1,17),
                new Vector2Int (2,16),
                new Vector2Int (3,15),
                new Vector2Int (5,13),
                new Vector2Int (6,12),
                new Vector2Int (7,11),
                new Vector2Int (8,10),
                new Vector2Int (9,9),
                new Vector2Int (10,8),
                new Vector2Int (11,7),
                new Vector2Int (12,6),
                new Vector2Int (13,5),
                new Vector2Int (15,3),
                new Vector2Int (16,2),
                new Vector2Int (17,1),
                new Vector2Int (18,0)
            };
            Color col = highlighted ? new Color(0.99f, 0.39f, 0.13f, 1.0f) : new Color(0.86f, 0.51f, 0.13f, 0.7f);
            imageCtrl.UpdateAnimation(imageCtrl.ScaleImageUp(image, pinedaScaler), col);
        }

        public void PinedaAnimationBorders()
        {
            Vector2Int[] image = new Vector2Int[]
                        {
                new Vector2Int (5,4),
                new Vector2Int (6,4),
                new Vector2Int (7,4),
                new Vector2Int (8,4),
                new Vector2Int (9,4),
                new Vector2Int (10,4),
                new Vector2Int (11,4),
                new Vector2Int (12,4),
                new Vector2Int (13,4),
                new Vector2Int (4,5),
                new Vector2Int (4,6),
                new Vector2Int (4,7),
                new Vector2Int (4,8),
                new Vector2Int (4,9),
                new Vector2Int (4,10),
                new Vector2Int (4,11),
                new Vector2Int (4,12),
                new Vector2Int (4,13),
                new Vector2Int (5,14),
                new Vector2Int (6,14),
                new Vector2Int (7,14),
                new Vector2Int (8,14),
                new Vector2Int (9,14),
                new Vector2Int (10,14),
                new Vector2Int (11,14),
                new Vector2Int (12,14),
                new Vector2Int (13,14),
                new Vector2Int (14,5),
                new Vector2Int (14,6),
                new Vector2Int (14,7),
                new Vector2Int (14,8),
                new Vector2Int (14,9),
                new Vector2Int (14,10),
                new Vector2Int (14,11),
                new Vector2Int (14,12),
                new Vector2Int (14,13),
                new Vector2Int (14,14)
            };
            imageCtrl.UpdateAnimation(imageCtrl.ScaleImageUp(image, pinedaScaler), new Color(0.14f, 0.81f, 0.39f, 0.7f));
        }

        // One method to generically create the Pixels on the screen
        private void PinedaAnimationPixels(int x, int y, int state)
        {
            // Only for the 9 possible pixels and 3 possible states
            if (x <= 2 && x >= 0 && y <= 2 && y >= 0 && state <= 2 && state >= 0)
            {
                Vector2Int[] image = new Vector2Int[]
                            {
                new Vector2Int (5 + (x * 3), 5 + (y * 3)),
                new Vector2Int (5 + (x * 3), 6 + (y * 3)),
                new Vector2Int (5 + (x * 3), 7 + (y * 3)),
                new Vector2Int (6 + (x * 3), 5 + (y * 3)),
                new Vector2Int (6 + (x * 3), 6 + (y * 3)),
                new Vector2Int (6 + (x * 3), 7 + (y * 3)),
                new Vector2Int (7 + (x * 3), 5 + (y * 3)),
                new Vector2Int (7 + (x * 3), 6 + (y * 3)),
                new Vector2Int (7 + (x * 3), 7 + (y * 3))
                };
                Color col;
                switch (state)
                {
                    case 0:
                        col = new Color(0.70f, 0.70f, 0.70f, 1f);
                        break; // Light color for "in testing" mode
                    case 1:
                        col = new Color(0.50f, 0.50f, 0.50f, 1.0f);
                        break; // Dark color for "tested correctly" mode
                    default:
                        col = new Color(1.00f, 1.00f, 1.00f, 1.0f);
                        break; // white for "tested incorrectly and removed" mode
                }
                imageCtrl.UpdateAnimation(imageCtrl.ScaleImageUp(image, pinedaScaler), col);
            }
        }

        // Methods for setting the pixels of the Pineda animation by calling the generic method from above
        public void PinedaAnimationPixel11N()
        {
            PinedaAnimationPixels(0, 0, 0);
        }
        public void PinedaAnimationPixel12N()
        {
            PinedaAnimationPixels(0, 1, 0);
        }
        public void PinedaAnimationPixel13N()
        {
            PinedaAnimationPixels(0, 2, 0);
        }
        public void PinedaAnimationPixel21N()
        {
            PinedaAnimationPixels(1, 0, 0);
        }
        public void PinedaAnimationPixel22N()
        {
            PinedaAnimationPixels(1, 1, 0);
        }
        public void PinedaAnimationPixel23N()
        {
            PinedaAnimationPixels(1, 2, 0);
        }
        public void PinedaAnimationPixel31N()
        {
            PinedaAnimationPixels(2, 0, 0);
        }
        public void PinedaAnimationPixel32N()
        {
            PinedaAnimationPixels(2, 1, 0);
        }
        public void PinedaAnimationPixel33N()
        {
            PinedaAnimationPixels(2, 2, 0);
        }
        public void PinedaAnimationPixel11H()
        {
            PinedaAnimationPixels(0, 0, 1);
        }
        public void PinedaAnimationPixel12H()
        {
            PinedaAnimationPixels(0, 1, 1);
        }
        public void PinedaAnimationPixel21H()
        {
            PinedaAnimationPixels(1, 0, 1);
        }
        public void PinedaAnimationPixel22H()
        {
            PinedaAnimationPixels(1, 1, 1);
        }
        public void PinedaAnimationPixel13H()
        {
            PinedaAnimationPixels(0, 2, 1);
        }
        public void PinedaAnimationPixel31H()
        {
            PinedaAnimationPixels(2, 0, 1);
        }
        public void PinedaAnimationPixel23H()
        {
            PinedaAnimationPixels(1, 2, 2);
        }
        public void PinedaAnimationPixel32H()
        {
            PinedaAnimationPixels(2, 1, 2);
        }
        public void PinedaAnimationPixel33H()
        {
            PinedaAnimationPixels(2, 2, 2);
        }

        #endregion

        // This region defines the Bresenham animations
        #region Bresenham Animation

        public void BresenhamAnimationCoordinateSystem()
        {
            // Vertices:
            Vector2Int[] image = new Vector2Int[]
            {
                new Vector2Int (1,8),
                new Vector2Int (8,5),
                new Vector2Int (3,2)
            };
            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, bresenhamScaler), 100), new Color(0.07f, 0.7f, 0.17f, 1.0f));

            // Grid:
            image = new Vector2Int[2000];
            int counter = 0;
            for(int i = 0; i < 100; i++)
            {
                if((i % 10) == 9)
                {
                    for(int j = 0; j < 100 && counter < 2000; j++)
                    {
                        image[counter] = new Vector2Int(j + 1, i);
                        counter++;
                    }
                }
                else
                {
                    for (int j = 9; j < 100 && counter < 2000; j += 10)
                    {
                        image[counter] = new Vector2Int(j, i);
                        counter++;
                    }
                }
            }
            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, 1), 100), new Color(0.05f, 0.05f, 0.05f, 1.0f));
        }

        public void BresenhamAnimationLineSlopeMapped()
        {
            BresenhamAnimationSetup();

            // Grid:
            Vector2Int[] image = new Vector2Int[2000];
            int counter = 0;
            for (int i = 0; i < 100; i++)
            {
                if ((i % 10) == 9)
                {
                    for (int j = 0; j < 100 && counter < 2000; j++)
                    {
                        image[counter] = new Vector2Int(j, i);
                        counter++;
                    }
                }
                else
                {
                    for (int j = 9; j < 100 && counter < 2000; j += 10)
                    {
                        image[counter] = new Vector2Int(j, i);
                        counter++;
                    }
                }
            }

            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, 1), 100), new Color(0.05f, 0.05f, 0.05f, 1.0f));

            // m = 1:
            image = new Vector2Int[90];
            counter = 0;
            for (int i = 0; i < 90; i++)
            {
                    image[counter] = new Vector2Int(i+3, i + 13);
                    counter++;
            }

            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, 1), 100), new Color(0.25f, 0.25f, 0.25f, 1.0f));


            // Slope:
            image = new Vector2Int[99];
            counter = 0;
            for (int i = 0; i < 33; i++)
            {
                image[counter] = new Vector2Int((i * 3) + 1, i + 10);
                counter++;
                image[counter] = new Vector2Int((i * 3) + 2, i + 10);
                counter++;
                image[counter] = new Vector2Int((i * 3) + 3, i + 10);
                counter++;
            }
            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, 1), 100), new Color(0.51f, 0.86f, 0.13f, 0.7f));

            // 2 Vertices:
            image = new Vector2Int[]
            {
                new Vector2Int (1,11),
                new Vector2Int (1,9),
                new Vector2Int (1,10),
                new Vector2Int (2,11),
                new Vector2Int (2,9),
                new Vector2Int (2,10),
                new Vector2Int (59,30),
                new Vector2Int (60,30),
                new Vector2Int (61,30),
                new Vector2Int (59,31),
                new Vector2Int (60,31),
                new Vector2Int (61,31),
                new Vector2Int (59,29),
                new Vector2Int (60,29),
                new Vector2Int (61,29)
            };

            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, 1), 100), new Color(0.39f, 0.99f, 0.13f, 1.0f));
        }

        // One method to generically create the Pixels on the screen
        private void BresenhamAnimationPixels(int x, int y)
        {
            // Vertix:
            Vector2Int[] image = new Vector2Int[]
            {
                new Vector2Int (x, y + 1)
            };

            Color col = new Color(0.51f, 0.86f, 0.13f, 0.7f);
            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, bresenhamScaler), 100), col);
        }

        // Methods for setting the Pixels of the Bresenham animation by using the generic method from above
        public void BresenhamAnimationPixel0()
        {
            BresenhamAnimationPixels(0, 0);
        }
        public void BresenhamAnimationPixel1()
        {
            BresenhamAnimationPixels(1, 0);
        }
        public void BresenhamAnimationPixel2()
        {
            BresenhamAnimationPixels(2, 0);
        }
        public void BresenhamAnimationPixel3()
        {
            BresenhamAnimationPixels(3, 1);
        }
        public void BresenhamAnimationPixel4()
        {
            BresenhamAnimationPixels(4, 1);
        }
        public void BresenhamAnimationPixel5()
        {
            BresenhamAnimationPixels(5, 1);
        }

        public void BresenhamAnimationTriangleLines()
        {
            BresenhamAnimationSetup();
            BresenhamAnimationCoordinateSystem();
            // Vertix:
            Vector2Int[] image = new Vector2Int[]
            {
                new Vector2Int (1, 7),
                new Vector2Int (1, 6),
                new Vector2Int (2, 5),
                new Vector2Int (2, 3),
                new Vector2Int (2, 4),
                new Vector2Int (4, 3),
                new Vector2Int (5, 3),
                new Vector2Int (6, 4),
                new Vector2Int (7, 4),
                new Vector2Int (5, 6),
                new Vector2Int (6, 6),
                new Vector2Int (7, 6),
                new Vector2Int (4, 7),
                new Vector2Int (3, 7),
                new Vector2Int (2, 7)
            };

            Color col = new Color(0.39f, 0.99f, 0.13f, 1.0f);
            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, bresenhamScaler), 100), col);
        }

        // One method to generically create the Pixels on the screen for the triangle
        private void BresenhamAnimationTrianglePixels(int x, int y)
        {
            // Vertix:
            Vector2Int[] image = new Vector2Int[]
            {
                new Vector2Int (x, y)
            };

            Color col = new Color(0.39f, 0.99f, 0.13f, 1.0f);
            imageCtrl.UpdateAnimation(imageCtrl.FlipAnimationX(imageCtrl.ScaleImageUp(image, bresenhamScaler), 100), col);
        }

        // Methods for setting the Pixels of the Bresenham animation by using the generic method from above
        public void BresenhamAnimationPixelRow1()
        {
            BresenhamAnimationTrianglePixels(2, 6);
            BresenhamAnimationTrianglePixels(3, 6);
            BresenhamAnimationTrianglePixels(4, 6);
        }
        public void BresenhamAnimationPixelRow2()
        {
            BresenhamAnimationTrianglePixels(3, 5);
            BresenhamAnimationTrianglePixels(4, 5);
            BresenhamAnimationTrianglePixels(5, 5);
            BresenhamAnimationTrianglePixels(6, 5);
            BresenhamAnimationTrianglePixels(7, 5);
        }
        public void BresenhamAnimationPixelRow3()
        {
            BresenhamAnimationTrianglePixels(3, 4);
            BresenhamAnimationTrianglePixels(4, 4);
            BresenhamAnimationTrianglePixels(5, 4);
        }
        public void BresenhamAnimationPixelRow4()
        {
            BresenhamAnimationTrianglePixels(3, 3);
        }

        // Resets the world position of the BresenhamImage gameObject
        public void BresenhamAnimationImageReset()
        {
            bresenhamImage.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        }

        // Updates the world position of the BresenhamImage gameObject to the East direction for the animation
        public void BresenhamAnimationImageE()
        {
            bresenhamImage.transform.localPosition += new Vector3(0.1925f, 0.0f, 0.0f);
        }

        // Updates the world position of the BresenhamImage gameObject to the North-East direction for the animation
        public void BresenhamAnimationImageNE()
        {
            bresenhamImage.transform.localPosition += new Vector3(0.1925f, 0.1925f, 0.0f);
        }

        #endregion
    }
}