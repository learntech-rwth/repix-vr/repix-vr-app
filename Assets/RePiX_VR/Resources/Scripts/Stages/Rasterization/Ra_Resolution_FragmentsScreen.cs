﻿using System.Collections;
using System.Collections.Generic;
using OmiLAXR;
using OmiLAXR.Adapters.SteamVR;
using UnityEngine;
using OmiLAXR.xAPI;
using UnityEngine.UI;

// This class is controlling the communication for the resolution between the screen (receiver) and the resolution panels
// Since there are two different panels (VR and nonVR)

namespace RePiX_VR
{
    public class Ra_Resolution_FragmentsScreen : MonoBehaviour
    {
        public Ra_Fragment_Calculation receiver;
        public float resolution;
        public static float min_Resolution = 10;
        public static float max_Resolution = 200;
        public GameObject ResPanel_VR;
        public GameObject ResPanel_NonVR;

        void Start()
        {

            if (Learner.Instance.IsVR)
            {
                ResPanel_VR.SetActive(true);
                ResPanel_NonVR.SetActive(false);

                return;
            }
            ResPanel_VR.SetActive(false);
            ResPanel_NonVR.SetActive(true);
        }

        // If invokeUpdate is called from the resolution panels
        // The value is read from the specific resolution slider component, the resolution is updated
        // And the receiver update method is invoked
        public void invokeUpdate()
        {
            if (Learner.Instance.IsVR)
            {
                this.resolution = ResPanel_VR.GetComponentInChildren<Slider>().value;
                this.resolution = Mathf.Round(this.resolution);
            }
            else
            {
                this.resolution = ResPanel_NonVR.GetComponentInChildren<Slider>().value;
            }
            receiver.InvokeUpdate();
        }
    }

}