﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

// This class is used for the calculations of the fragments
// It uses the concept of rasterization to create 2D images from given 3D points of the triangles
// And send these images to the screen
namespace RePiX_VR
{
    public class Ra_Fragment_Calculation : MonoBehaviour
    {
        [FormerlySerializedAs("Canvas")] public Ra_ImageController canvas;

        [FormerlySerializedAs("ResolutionPanel")]
        public GameObject resolutionPanel;

        [FormerlySerializedAs("CameraObject")] public GameObject cameraObject;
        private Ra_Resolution_FragmentsScreen _resScript;
        [FormerlySerializedAs("Resolution")] public int resolution;

        // Needed for resolution update
        private Ra_Triangle _curTriangle;

        // Constants
        // Distance to near and far plane in camera space
        private const float XNearPlane = 0.75f;
        private const float XFarPlane = 4.65f;
        private const float MaxWidthAndHeight = 3.455f;
        private const float MinWidthAndHeight = 0.46f;

        // This struct is used for the pineda algorithm line equations
        // The check method returns true or false if a given point is on the left or right side of the line equation
        private readonly struct Equation
        {
            private readonly Vector2 _normalVector;
            private readonly float _offset;

            public Equation(Vector2 point1, Vector2 point2)
            {
                var distanceVector = point2 - point1;
                this._normalVector.x = -distanceVector.y;
                this._normalVector.y = distanceVector.x;
                this._offset = -1 * (_normalVector.x * point1.x + _normalVector.y * point1.y);
            }

            public bool Check(int x, int y)
                => (_normalVector.x * x + _normalVector.y * y + _offset) >= 0;
        }

        // Start is called before the first frame update
        private void Start()
        {
            _resScript = resolutionPanel.GetComponent<Ra_Resolution_FragmentsScreen>();
            cameraObject.GetComponent<Camera>();
            resolution = 50;
        }

        // Gets the current resolution from the resolution component
        private void InvokedResolutionUpdate()
        {
            resolution = (int)_resScript.resolution;
        }

        // If a primitve update is invoked (changed primitive) this method is called
        // The method calculates and return the 2D image of the curTriangle
        private Vector2Int[] InvokedPrimitiveUpdate()
        {
            var mappedInput = InputMapping(_curTriangle.vertex1.transform.position,
                _curTriangle.vertex2.transform.position, _curTriangle.vertex3.transform.position);
            return mappedInput.Length == 0
                ? new Vector2Int[] { }
                : Pineda(mappedInput[0], mappedInput[1], mappedInput[2]);
        }

        // If an update is invoked this method is called to recalculate the fragments on the screen
        public void InvokeUpdate()
        {
            // If null then no current Triangle is available
            if (_curTriangle == null) return;
            // Get current Resolution from the slider
            InvokedResolutionUpdate();
            // Call image calculating functions
            var image = InvokedPrimitiveUpdate();
            if (image.Length == 0) return;
            UpdateImage(image, this.resolution);
        }

        // Resets the screen to a white screen
        public void ScreenSetup()
        {
            canvas.Scene3DSetup(resolution);
        }

        // Updates the image and scales it up if it has a low resolution to reduce the blurriness of the image
        private void UpdateImage(Vector2Int[] image, int res)
        {
            if (res < 50) canvas.UpdateImage(canvas.ScaleImageUp(image, 10), res * 10);
            else if (res < 100) canvas.UpdateImage(canvas.ScaleImageUp(image, 5), res * 5);
            else canvas.UpdateImage(image, res);
        }

        // Called when a new triangle is inputed and calculates the image of the new triangle
        public void TriangleInput(Ra_Triangle triangle)
        {
            _curTriangle = triangle;
            InvokedResolutionUpdate();
            var mappedInput = InputMapping(triangle.vertex1.transform.position, triangle.vertex2.transform.position,
                triangle.vertex3.transform.position);
            if (mappedInput.Length == 0) return;
            var image = Pineda(mappedInput[0], mappedInput[1], mappedInput[2]);
            UpdateImage(image, resolution);
        }

        // This method maps the 3D coordinates of the vertices to 2D coordinates on the screen
        private Vector2[] InputMapping(Vector3 vertex1, Vector3 vertex2, Vector3 vertex3)
        {
            Vector2 resVertex1 = new Vector2(), resVertex2 = new Vector2(), resVertex3 = new Vector2();
            // Content of method:
            // Calculate the height and width at the current x-layer in the frustum.
            // Calculate the z and y positions in respect to the current width => value between -0.5 and 0.5
            // Add 0.5 for value between 0 and 1. Multiply with Resolution to get the pixel value.
            // (x is negative because the canvas starts in the bottom right corner instead of bottom left)
            var t = cameraObject.transform;
            var position = t.position;
            var heightAndWidthCur1 =
                ((-((vertex1 - position).x) - XNearPlane) / (XFarPlane - XNearPlane)) *
                (MaxWidthAndHeight - MinWidthAndHeight) + MinWidthAndHeight;
            resVertex1.x = (((vertex1 - position).z / heightAndWidthCur1) + 0.5f) * resolution;
            resVertex1.y = (((vertex1 - position).y / heightAndWidthCur1) + 0.5f) * resolution;
            var heightAndWidthCur2 =
                ((-((vertex2 - cameraObject.transform.position).x) - XNearPlane) / (XFarPlane - XNearPlane)) *
                (MaxWidthAndHeight - MinWidthAndHeight) + MinWidthAndHeight;
            resVertex2.x = (((vertex2 - position).z / heightAndWidthCur2) + 0.5f) * resolution;
            resVertex2.y = (((vertex2 - position).y / heightAndWidthCur2) + 0.5f) * resolution;
            var heightAndWidthCur3 =
                ((-((vertex3 - position).x) - XNearPlane) / (XFarPlane - XNearPlane)) *
                (MaxWidthAndHeight - MinWidthAndHeight) + MinWidthAndHeight;
            resVertex3.x = (((vertex3 - position).z / heightAndWidthCur3) + 0.5f) * resolution;
            resVertex3.y = (((vertex3 - position).y / heightAndWidthCur3) + 0.5f) * resolution;

            // If the triangle is outside of the frustum (behind the camera)
            if (heightAndWidthCur1 <= 0 || heightAndWidthCur1 <= 0 || heightAndWidthCur1 <= 0)
                return new Vector2[] { };

            return new [] { resVertex1, resVertex2, resVertex3 };
        }

        // Implementation of the pineda algorithm
        // Uses the Equation struct to calculate all pixels within the 2D triangle from the given 2D vectors
        private static Vector2Int[] Pineda(Vector2 vertex1, Vector2 vertex2, Vector2 vertex3)
        {
            var res = new List<Vector2Int>();
            // 1. Create the three equations (No Backface-Culling -> Both sides get rendered -> 6 equations)
            var eq1 = new Equation(vertex1, vertex3);
            var eq2 = new Equation(vertex3, vertex2);
            var eq3 = new Equation(vertex2, vertex1);
            var eq4 = new Equation(vertex3, vertex1);
            var eq5 = new Equation(vertex2, vertex3);
            var eq6 = new Equation(vertex1, vertex2);

            // 2. Get the min and max x and y values
            float xMin, xMax, yMin, yMax;
            if (vertex1.x <= vertex2.x && vertex1.x <= vertex3.x) xMin = vertex1.x;
            else if (vertex2.x <= vertex1.x && vertex2.x <= vertex3.x) xMin = vertex2.x;
            else xMin = vertex3.x;
            if (vertex1.y <= vertex2.y && vertex1.y <= vertex3.y) yMin = vertex1.y;
            else if (vertex2.y <= vertex1.y && vertex2.y <= vertex3.y) yMin = vertex2.y;
            else yMin = vertex3.y;
            if (vertex1.x >= vertex2.x && vertex1.x >= vertex3.x) xMax = vertex1.x;
            else if (vertex2.x >= vertex1.x && vertex2.x >= vertex3.x) xMax = vertex2.x;
            else xMax = vertex3.x;
            if (vertex1.y >= vertex2.y && vertex1.y >= vertex3.y) yMax = vertex1.y;
            else if (vertex2.y >= vertex1.y && vertex2.y >= vertex3.y) yMax = vertex2.y;
            else yMax = vertex3.y;

            // Loop:
            // Check -> Add vector to array if one of the 2 equation-triples are correct
            for (var x = Mathf.RoundToInt(xMin); x <= Mathf.RoundToInt(xMax); x++)
            {
                for (var y = Mathf.RoundToInt(yMin); y <= Mathf.RoundToInt(yMax); y++)
                {
                    if ((eq1.Check(x, y) && eq2.Check(x, y) && eq3.Check(x, y)) ||
                        (eq4.Check(x, y) && eq5.Check(x, y) && eq6.Check(x, y)))
                        res.Add(new Vector2Int(x, y));
                }
            }

            // Return an array instead of a List
            return res.ToArray();
        }

        // Resets the current triangle to null
        public void ResetCurrentTriangle()
        {
            _curTriangle = null;
        }

        // Resets the position of the screen to the position of the fragments screen
        public void UpdatePositionReset()
        {
            var t = transform;
            t.localEulerAngles = new Vector3(0.0f, 270.0f, 0.0f);
            t.localPosition = new Vector3(-6f, 0.8f, 2.7f);
            t.localScale = new Vector3(1.5f, 1.5f, 1f);
        }

        // Updates the position of the screen to the position of the animation screen
        public void UpdatePositionAnimation()
        {
            var t = transform;
            t.localEulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
            t.localPosition = new Vector3(-1.615f, 0.77f, 2.0f);
            t.localScale = new Vector3(1.0f, 1.0f, 1f);
        }
    }
}