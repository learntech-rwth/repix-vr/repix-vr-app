﻿using OmiLAXR.Adapters.SteamVR;
using OmiLAXR.Interaction;
using UnityEngine;

using OmiLAXR.xAPI.Tracking;
using RePiX_VR.GeometryStage;

// This class is the same as the original class ("TriangleBuilder")
// Some adjustments are made for creating and handling the triangles
// The triangles need a "Ra_Triangle" component for the interaction with the fragments-screen
// Also some other components are added when creating the triangles
// Main adjustments can be found in CreateTriangle()
namespace RePiX_VR
{
    /// <summary>
    /// Component to realize 3D Geometry generation with vertices and triangles.
    /// It regulates all interaction states (place vertices, remove vertices, move vertices, build triangles)
    /// and creates their visualizations.
    /// </summary>
    [RequireComponent(typeof(VertexManager))]
    public class Ra_TriangleBuilder : TriangleBuilder
    {
        #region Rasterization additions

        [Tooltip("Material the object gets if laser pointer points on it.")]
        public Material Ra_hoverMaterial;

        [Tooltip("Material the object gets if user selects the pointed object.")]
        public Material Ra_selectMaterial;

        [Tooltip("The fragments Image GameObject.")]
        public GameObject FragmentsImage;

        [Tooltip("The fragments Image GameObject.")]
        public Ra_TriangleCollection TriangleCollection;
        #endregion

        protected override void Start()
        {
            base.Start();
            OnCreatedTriangle += CreatedTriangle;
            OnCreatingTriangle += CreatingTriangle;
        }

        private static void CreatedTriangle(TriangleComponent triangle, VertexComponent[] vertices)
        {
            triangle.GetComponent<Ra_Triangle>().vertex1 = triangle.GetComponent<TriangleHelper>().vertices[0];
            triangle.GetComponent<Ra_Triangle>().vertex2 = triangle.GetComponent<TriangleHelper>().vertices[1];
            triangle.GetComponent<Ra_Triangle>().vertex3 = triangle.GetComponent<TriangleHelper>().vertices[2];
        }

        private void CreatingTriangle(TriangleComponent triangle, VertexComponent[] vertices)
        {
            // Additions made for Rasterization:
            var raTriangle = triangle.gameObject.AddComponent<Ra_Triangle>();
            raTriangle.userTriangle = true;
            raTriangle.useBackfaceCulling = true;
            
            // Create and configure Pointable Script:
            triangle.gameObject.AddComponent<Pointable>();
            var pointable = triangle.gameObject.GetComponent<Pointable>();
            pointable.hoverMaterial = Ra_hoverMaterial;
            pointable.selectMaterial = Ra_selectMaterial;
            pointable.allowClick = true;
            pointable.allowHover = true;
            pointable.allowSelection = true;
            
            // Add interactable Script:
            triangle.gameObject.AddComponent<SteamVR_ImprovedInteractable>();
            // Create and configure Mouse Interaction Events Script:
            triangle.gameObject.AddComponent<MouseInteractionEvents> ();
            //triangle.GetComponent<MouseInteractionEvents>().fireToPointable = true;
            //triangle.GetComponent<MouseInteractionEvents>().pointable = triangle.GetComponent<Pointable>();
            
            // Create and configure Trackable Script:
            triangle.gameObject.AddComponent<Trackable>();
            triangle.GetComponent<Trackable>().customTrackingName = "Ra_OwnTriangles";
            //triangle.GetComponent<Trackable>().useCustomTrackingName = true;
            // End additions for Rasterization
        }
    }
}