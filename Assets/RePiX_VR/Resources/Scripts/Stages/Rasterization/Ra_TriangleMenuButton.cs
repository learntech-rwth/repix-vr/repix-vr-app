﻿using System;
using OmiLAXR;
using UnityEngine;
using UnityEngine.UI;
using OmiLAXR.xAPI;
using OmiLAXR.xAPI.Extensions;
using RePiX_VR.GeometryStage;
using Valve.VR.InteractionSystem;

// This class is the same as the original class ("TriangleMenuButton")
// It just works with a Ra_TriangleBuilder component instead of a TriangleBuilder component
namespace RePiX_VR
{
    [RequireComponent(typeof(Button)), Obsolete("Use TriangleMenuButton instead.")]
    public class Ra_TriangleMenuButton : MonoBehaviour
    {
        public TriangleInteractionState interactionState = TriangleInteractionState.None;


        private void Start()
        {
            // Bind click event
            var button = GetComponent<Button>();
            button.onClick.AddListener(UpdateInteractionState);
            if (Learner.Instance.IsVR) return;
            var triangleBuilder = transform.GetComponentInParent<Ra_TriangleBuilder>();
            triangleBuilder.OnChangedState += TriangleBuilder_OnChangedState;
            button.SetDisabled(interactionState == triangleBuilder.GetInteractionState());

        }

        private void TriangleBuilder_OnChangedState(TriangleInteractionState from, TriangleInteractionState to)
        {
            var button = GetComponent<Button>();
            // disable if selected
            button.SetDisabled(to == interactionState);
        }

        private void UpdateInteractionState()
        {
            var triangleBuilder = transform.GetComponentInParent<Ra_TriangleBuilder>();
            if (Learner.Instance.IsVR)
                triangleBuilder.SetNextInteractionState(interactionState);
            else
                triangleBuilder.SetInteractionState(interactionState);
        }

    }
}