﻿using System.Linq;
using OmiLAXR;
using OmiLAXR.Extensions;
using OmiLAXR.Utils;

using UnityEngine;
using UnityEngine.Events;

using OmiLAXR.xAPI;
using OmiLAXR.xAPI.Tracking;
using xAPI.Registry;


// This class is the same as the original class ("Frustum")
// It just has some adjustments and deletions made to work with the frustum used for the rasterization stage
namespace RePiX_VR
{
    [ExecuteInEditMode, RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
    public class Ra_Frustum : MonoBehaviour
    {
        // todo: maybe split in more components. e.g. some setter functions in own component etc.
        public delegate void FrustumChangesHandler(object sender);

        #region Constants
        public const float NearZ_Default = -0.7f;
        public const float FarZ_Default = -4.7f;

        public const float Aspect_Min = 0.1f;
        public const float Aspect_Max = 100.0f;
        public const float Aspect_Default = 1.0f;

        public const float Fov_Default = 42;
        public const float Fov_Min = 5.0f;
        public const float Fov_Max = 179.0f;
        #endregion

        public float
            fov = Fov_Default,
            aspect = Aspect_Default,
            nearZ = NearZ_Default,
            farZ = FarZ_Default;

        private GameObject lines;

        [Tooltip("Camera which gets the values from this frustum.")]
        private Camera appliedCamera;

        public event FrustumChangesHandler OnChanges;

        public UnityEvent OnFrustumChanged;

        // Representations of direction
        public Vector3 direction => transform.forward;
        public Vector3 right => transform.right;
        public Vector3 up => transform.up;

        [Tooltip("(Optional) Label prefab to create labels for frustum context.")]
        public GameObject labelPrefab;

        public Transform imagePreviewTransform;
        public Material frustumMaterial;

        private void Start()
        {
            OnChanges += Frustum_Changes;
            
            if (!Learner.Instance.IsVR)
                FindNonVRControls();
        }

        private void Frustum_Changes(object sender)
        {
            LrsController.Instance.SendStatement(
                verb: xAPI_Definitions.generic.verbs.changed,
                activity: xAPI_Definitions.virtualReality.activities.vrObject,
                extensions: new xAPI_Extensions
                {
                    xAPI_Definitions.virtualReality.extensions.activity.vrObjectName(this.name),
                    xAPI_Definitions.generic.extensions.result.value(ToString())
                }
            );
            OnFrustumChanged?.Invoke();
        }

        public void SetFarZ(float z)
        {
            farZ = -z;
            OnChanges?.Invoke(this);
        }
        public void SetFarZDiv10(float z) => SetFarZ(z / 10);
        public void SetNearZ(float z)
        {
            nearZ = -z;
            OnChanges?.Invoke(this);
        }
        public void SetNearZDiv10(float z) => SetNearZ(z / 10);

        public void SetFov(float fov)
        {
            this.fov = fov;
            OnChanges?.Invoke(this);
        }
        public void SetFovDiv10(float fov) => SetFov(fov / 10);

        public void SetAspect(float aspect)
        {
            this.aspect = aspect;
            OnChanges?.Invoke(this);
        }
        public void SetAspectDiv10(float aspect) => SetAspect(aspect / 10);

        private void FindNonVRControls()
        {
        }

        private void OnEnable()
        {
            ResetFrustumValues();

            // Find or create lines
            if (!lines)
                lines = gameObject.Find("Lines", true);

            // if still no lines GO
            if (!lines)
            {
                lines = new GameObject("Lines");
                lines.transform.SetParent(gameObject.transform);
            }

            if (lines.transform.childCount < 1)
                InsertLines(lines.transform);

            lines.SetActive(true);

            if (!appliedCamera)
                appliedCamera = gameObject.GetComponentInChildren<Camera>();

            var mr = GetComponent<MeshRenderer>();
            if (mr)
                mr.enabled = true;
        }

        private void OnDisable()
        {
            lines?.SetActive(false);

            var mr = GetComponent<MeshRenderer>();
            if (mr)
                mr.enabled = false;
        }


        private void ResetFrustumValues()
        {
            aspect = Aspect_Default;
            fov = Fov_Default;
            farZ = FarZ_Default;
            nearZ = NearZ_Default;
        }
        /// <summary>
        /// Set Wire mesh between two positions.
        /// </summary>
        private void SetLine(int index, Vector3 from, Vector3 to)
        {
            var linesTransform = lines.transform;
            if (index < 0 || index >= linesTransform.childCount) 
                return;

            var line = linesTransform.GetChild(index);
            Transform_Utils.PlaceLine(line, from, to);
        }

        public FrustumPlanes CreateFrustumPlanes()
        {
            var v = GetFrustumValues();
            return new FrustumPlanes()
            {
                zFar = Mathf.Abs(farZ),
                zNear = Mathf.Abs(nearZ),
                top = v.top,
                bottom = v.bottom,
                left = v.left,
                right = v.right
            };
        }

        private FrustumValues GetFrustumValues() => new FrustumValues(Mathf.Abs(nearZ), fov, aspect);
        /// <summary>
        /// Get 4x4 frustum matrix.
        /// </summary>
        public Matrix4x4 GetFrustumMatrix()
        {
            var v = GetFrustumValues();
            return Matrix4x4.Frustum(
                left: v.left, 
                right: v.right, 
                bottom: v.bottom, 
                top: v.top, 
                zNear: Mathf.Abs(nearZ), 
                zFar: Mathf.Abs(farZ)
            );
        }

        public void FireChange(object sender)
        {
            OnChanges?.Invoke(sender);
        }
        public static void InsertLines(Transform parent)
        {
            for (var i = 0; i < 12; i++)
            {
                var line = GameObject_Utils.CreateLine(parent);
                line.name += " (" + i + ")";
            }
        }

        private void Update()
        {
            UpdateAppliedCamera();
        }
        /// <summary>
        /// Provide frustum values to a camera.
        /// </summary>
        private void UpdateAppliedCamera()
        {
            if (!appliedCamera)
                return;

            appliedCamera.farClipPlane = -farZ;
            appliedCamera.nearClipPlane = -nearZ;
            appliedCamera.aspect = aspect;
            appliedCamera.fieldOfView = fov;
            appliedCamera.Render();
        }
        private float SnapFloat(float v) => ((int)(v * 10.0f)) / 10.0f;
        /// <summary>
        /// Hold values in an area.
        /// </summary>
        private void ClampValues()
        {
            fov = Mathf.Clamp(fov, Fov_Min, Fov_Max);
            aspect = Mathf.Clamp(aspect, Aspect_Min, Aspect_Max);

            var absN = Mathf.Abs(nearZ);
            var absF = Mathf.Abs(farZ);

            var clampedF = Mathf.Max(absF, absN + 0.01f);
            var clampedN = Mathf.Clamp(absN, 0.1f, clampedF - 0.01f);

            farZ = -clampedF;
            nearZ = -clampedN;

            farZ = SnapFloat(farZ);
            nearZ = SnapFloat(nearZ);
        }

        private void SetLinesForPlane(int offset, QuadCorners plane)
        {
            SetLine(offset + 0, from: plane.topLeft, to: plane.topRight);
            SetLine(offset + 1, from: plane.topRight, to: plane.bottomRight);
            SetLine(offset + 2, from: plane.bottomRight, to: plane.bottomLeft);
            SetLine(offset + 3, from: plane.bottomLeft, to: plane.topLeft);
        }

        private void SetLinesFromToPlane(int offset, Vector3 from, QuadCorners plane)
        {
            SetLine(offset + 0, from: from, to: plane.topLeft);
            SetLine(offset + 1, from: from, to: plane.topRight);
            SetLine(offset + 2, from: from, to: plane.bottomLeft);
            SetLine(offset + 3, from: from, to: plane.bottomRight);
        }
        /// <summary>
        /// Uses far z to get quad corners.
        /// </summary>
        /// <returns>Four vertices representing a far plane on frustum.</returns>
        public QuadCorners GetFarPlaneCorners() => GetPlane(farZ);
        /// <summary>
        /// Uses near z to get quad corners.
        /// </summary>
        /// <returns>Four vertices representing a near plane on frustum.</returns>
        public QuadCorners GetNearPlaneCorners() => GetPlane(nearZ);

        /// <summary>
        /// Get quad corners depending on z position.
        /// </summary>
        /// <param name="z">Position in frustum.</param>
        /// <returns>Four vertices representing a plane on frustum.</returns>
        public QuadCorners GetPlane(float z)
        {
            var values = new FrustumValues(z, fov, aspect);
            var left = values.left;
            var top = values.top;
            var bottom = values.bottom;
            var right = values.right;

            var tl = new Vector3(left, top, z);
            var tr = new Vector3(right, top, z);
            var br = new Vector3(right, bottom, z);
            var bl = new Vector3(left, bottom, z);

            return new QuadCorners()
            {
                topLeft = tl,
                topRight = tr,
                bottomLeft = bl,
                bottomRight = br
            };
        }
        /// <summary>
        /// Updates the frustum model.
        /// </summary>
        public void UpdateModel()
        {
            // pre calculate all needed parameters depending on properties
            ClampValues();

            // get near and far plane positions
            var near = GetNearPlaneCorners();
            var far = GetFarPlaneCorners();

            // get near and far plane vertices
            var nearVertices = near.GetVerticesClockwise();
            var farVertices = far.GetVerticesClockwise();

            // create all 8 vertices
            var corners = nearVertices.Concat(farVertices).ToArray();

            // merge left, right, top and bottom triangles
            var triangles = FrustumTriangles.sides;

            // Create sides mesh
            var mesh = Mesh_Ext.CreateFrom(corners, triangles);
            GetComponent<MeshFilter>().mesh = mesh;
            GetComponent<MeshRenderer>().material = frustumMaterial;

            // near plane lines
            SetLinesForPlane(0, near);
            // between near and far
            SetLinesFromToPlane(offset: 4, from: Vector3.zero, plane: far);
            // far plane lines
            SetLinesForPlane(8, far);
        }


        public override string ToString()
        {
            return $"far: {farZ}, near: {nearZ}, aspect: {aspect}, fov: {fov}";
        }

    }
}