﻿using OmiLAXR.Adapters.SteamVR;
using OmiLAXR.Extensions;
using OmiLAXR.Interaction;
using OmiLAXR.UI;
using RePiX_VR.GeometryStage;
using UnityEngine;
using UnityEngine.UI;

// This class is the same as the original class ("TriangleMenuPanel")
// It just works with a Ra_TriangleBuilder component instead of a TriangleBuilder component
namespace RePiX_VR
{
    [RequireComponent(typeof(PointableUI))]
    public class Ra_TriangleMenuPanel : MenuPanel
    {
        private Ra_TriangleBuilder _triangleBuilder;
        protected override void Start()
        {
            base.Start();

            var noneBtnGameObject = transform.Find("Button_None");
            var noneBtn = noneBtnGameObject.GetComponent<Button>();
            noneBtn.Select();

            _triangleBuilder = transform.GetComponentInParent<Ra_TriangleBuilder>();

            var pointableUI = GetComponent<PointableUI>();
            pointableUI.onHover.AddListener(OnPointerIn);
            pointableUI.onUnhover.AddListener(OnPointerOut);

            onEnterGroup.AddListener(() => _triangleBuilder.PauseInteraction());
            onLeaveGroup.AddListener(() => _triangleBuilder.ResumeInteraction());
        }

        private void OnPointerOut(LaserPointerEventArgs e)
        {
            if (_triangleBuilder.GetInteractionState() == TriangleInteractionState.None)
                return;

            // Limit laser interaction to panel and its children
            var laser = (SteamVR_ImprovedLaserPointer)e.Sender;
            laser.limitedOn.Add(gameObject);
            gameObject.ForEachChild(c => laser.limitedOn.Add(c));
        }

        private void OnPointerIn(LaserPointerEventArgs e)
        {
            // Clear laser limit list
            var laser = (SteamVR_ImprovedLaserPointer)e.Sender;
            laser.limitedOn.Clear();
        }
    }

}