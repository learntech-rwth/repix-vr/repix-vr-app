﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace RePiX_VR
{
    [RequireComponent(typeof(Image))]
    public class Ra_ImageController : MonoBehaviour
    {
        //public Image canvasImage;
        private Color[] whiteScreen;
        private int curResolution = 50;
        Texture2D texture;
        Color grey;
        Color white;

        // Start is called before the first frame update
        // Initializes a white screen and the "whiteScreen" array that consists of only white
        void Start()
        {
            grey = new Color(0.5f, 0.5f, 0.5f, 1f);
            white = new Color(1f, 1f, 1f, 1f);
            whiteScreen = new Color[250000];
            for (int i = 0; i < whiteScreen.Length; i++)
            {
                whiteScreen[i] = white;
            }
            texture = new Texture2D(curResolution, curResolution);

            GetComponent<Image>().material.mainTexture = texture;
            texture.SetPixels(whiteScreen);
            texture.Apply();
        }

        // Updates the screen accordingly to given image and resolution
        // Used in the fragments-calculation methods
        public void UpdateImage(Vector2Int[] image, int resolution)
        {
            if (resolution != curResolution)
            {
                texture.Reinitialize(resolution, resolution, TextureFormat.RGB24, false);
                curResolution = resolution;
            }

            texture.SetPixels(whiteScreen);

            for (int i = 0; i < image.Length; i++)
            {
                if(image[i].x < resolution && image[i].y < resolution && image[i].x >= 0 && image[i].y >= 0)
                {
                    texture.SetPixel(image[i].x, image[i].y, grey);
                }
            }

            texture.Apply();
        }

        // Scales the given image up by the given integer scale
        // The scale has to be a positive value greater than 1
        public Vector2Int[] ScaleImageUp(Vector2Int[] image, int scale)
        {
            if (scale < 2) return image;
            Vector2Int[] newImage = new Vector2Int[image.Length * scale * scale];
            int nICounter = 0;
            for(int i = 0; i < image.Length; i++)
            {
                if(nICounter < newImage.Length)
                {
                    for(int j = 0; j < scale; j++)
                    {
                        for(int k = 0; k < scale; k++)
                        {
                            newImage[nICounter] = new Vector2Int((image[i].x * scale) + j, (image[i].y * scale) + k);
                            nICounter++;
                        }
                    }
                }
            }
            return newImage;
        }

        // Updates the screen accordingly to given image and color
        // Used in the animation methods
        public void UpdateAnimation(Vector2Int[] image, Color color)
        {
            grey = color;
            for (int i = 0; i < image.Length; i++)
            {
                if (image[i].x < curResolution && image[i].y < curResolution)
                {
                    texture.SetPixel((curResolution - (image[i].x + 1)), image[i].y, grey);
                }
            }
            texture.Apply();
            grey = new Color(0.5f, 0.5f, 0.5f, 1f);
        }

        // Used to flip an animation from left to right (because the screen was flipped during animation development)
        public Vector2Int[] FlipAnimationX(Vector2Int[] image, int resolution)
        {
            for(int i = 0; i < image.Length; i++)
            {
                image[i].x = resolution - image[i].x;
            }
            return image;
        }

        // Resizes and clears the screen for the animations
        public void AnimationSetup(int resolution)
        {
            curResolution = resolution;
            texture.Reinitialize(resolution, resolution, TextureFormat.RGB24, false);
            texture.SetPixels(whiteScreen);
            UpdateAnimation(new Vector2Int[] { }, grey);
        }

        // Results in an empty screen
        public void Scene3DSetup(int resolution)
        {
            UpdateImage(new Vector2Int[] { }, resolution);
        }

        // Used for testing purposes to see if the component and screen works
        public void mockExampleImageUpdateBlue()
        {
            Vector2Int[] mockImage = new Vector2Int[]
            {
                new Vector2Int (-1,-1),
                new Vector2Int (0,0),
                new Vector2Int (1,1),
                new Vector2Int (2,2),
                new Vector2Int (3,3),
                new Vector2Int (4,4),
                new Vector2Int (5,5),
                new Vector2Int (6,6),
                new Vector2Int (7,7),
                new Vector2Int (8,8),
                new Vector2Int (9,9),
                new Vector2Int (10,10),
                new Vector2Int (11,11),
                new Vector2Int (12,12),
                new Vector2Int (1,10),
                new Vector2Int (1,11),
                new Vector2Int (1,12),
                new Vector2Int (1,13),
                new Vector2Int (1,14),
                new Vector2Int (1,15),
                new Vector2Int (1,16),
                new Vector2Int (1,17),
                new Vector2Int (1,18),
                new Vector2Int (1,19),
                new Vector2Int (10,1),
                new Vector2Int (11,1),
                new Vector2Int (12,1),
                new Vector2Int (13,1),
                new Vector2Int (14,1),
                new Vector2Int (15,1),
                new Vector2Int (16,1),
                new Vector2Int (17,1),
                new Vector2Int (18,1),
                new Vector2Int (19,1),
                new Vector2Int (3,3),
                new Vector2Int (31,3),
                new Vector2Int (32,3),
                new Vector2Int (33,3),
                new Vector2Int (34,3),
                new Vector2Int (35,3),
                new Vector2Int (36,3),
                new Vector2Int (37,3),
                new Vector2Int (38,3),
                new Vector2Int (39,3),
                new Vector2Int (30,3),
                new Vector2Int (3,30),
                new Vector2Int (3,31),
                new Vector2Int (3,32),
                new Vector2Int (3,33),
                new Vector2Int (3,34),
                new Vector2Int (3,35),
                new Vector2Int (3,36),
                new Vector2Int (3,37),
                new Vector2Int (3,38),
                new Vector2Int (3,39),
                new Vector2Int (30,30),
                new Vector2Int (31,31),
                new Vector2Int (32,32),
                new Vector2Int (33,33),
                new Vector2Int (34,34),
                new Vector2Int (35,35),
                new Vector2Int (36,36)
            };

            grey = new Color(0.2f, 0.2f, 1.0f, 1.0f);
            UpdateImage(mockImage, 50);
            grey = new Color(0.5f, 0.5f, 0.5f, 1f);
        }

        // Used for testing and resetting the screen
        public void mockExampleImageUpdate()
        {
            Vector2Int[] mockImage = new Vector2Int[]
            {
            };

            UpdateImage(mockImage, curResolution);
        }
    }
}
