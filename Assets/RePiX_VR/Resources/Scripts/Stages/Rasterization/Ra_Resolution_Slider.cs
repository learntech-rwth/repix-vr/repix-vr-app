﻿using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// This class is the same as the other slider classes ("AspectSlider" and "FovSlider")
// It just works with adjustments for the resolution and which component it invokes
namespace RePiX_VR
{
    [RequireComponent(typeof(Slider))]
    public class Ra_Resolution_Slider : MonoBehaviour
    {
        [Tooltip("Panel instance to apply on.")]
        public Ra_Resolution_FragmentsScreen panel;

        public float minResolution = Ra_Resolution_FragmentsScreen.min_Resolution;
        public float maxResolution = Ra_Resolution_FragmentsScreen.max_Resolution;

        public UnityEvent onValueChanged = new UnityEvent();

        private Slider slider;
        private TMP_Text valueText;
        private TMP_Text minText;
        private TMP_Text maxText;

        private void Awake()
        {
            slider = GetComponent<Slider>();
            slider.minValue = minResolution;
            slider.maxValue = maxResolution;
            slider.value = panel.resolution;
            slider.onValueChanged.AddListener(SliderChanged);

            var texts = GetComponentsInChildren<TMP_Text>();
            valueText = texts.FirstOrDefault(t => t.name.ToLower().Contains("value"));
            minText = texts.FirstOrDefault(t => t.name.ToLower().Contains("min"));
            maxText = texts.FirstOrDefault(t => t.name.ToLower().Contains("max"));

            if (minText)
                minText.text = minResolution.ToString(CultureInfo.CurrentCulture);
            if (maxText)
                maxText.text = maxResolution.ToString(CultureInfo.CurrentCulture);
            if (valueText)
                valueText.text = "Resolution: " + slider.value.ToString(CultureInfo.CurrentCulture);
        }

        private void SliderChanged(float resolution)
        {
            panel.resolution = resolution;
            if (valueText)
                valueText.text = "Resolution: " + resolution;
            panel.invokeUpdate();
            onValueChanged.Invoke();
        }
    }
}