﻿using UnityEngine;

namespace RePiX_VR

{
    public class Ra_TriangleCollection : MonoBehaviour
    {
        // This class exists in order to execute functions on all prebuild triangles
        public Ra_Triangle blue01;
        public Ra_Triangle blue02;
        public Ra_Triangle blue03;
        public Ra_Triangle blue04;
        public Ra_Triangle blue05;
        public Ra_Triangle blue06;
        public Ra_Triangle blue07;
        public Ra_Triangle blue08;
        public Ra_Triangle blue09;
        public Ra_Triangle blue10;
        public Ra_Triangle blue11;
        public Ra_Triangle blue12;
        public Ra_Triangle green01;
        public Ra_Triangle green02;
        public Ra_Triangle green03;
        public Ra_Triangle green04;
        public Ra_Triangle green05;
        public Ra_Triangle green06;
        public Ra_Triangle green07;
        public Ra_Triangle green08;
        public Ra_Triangle green09;
        public Ra_Triangle green10;
        public Ra_Triangle green11;
        public Ra_Triangle green12;
        public Ra_Triangle red01;
        public Ra_Triangle red02;
        public Ra_Triangle red03;
        public Ra_Triangle red04;
        public Ra_Triangle red05;
        public Ra_Triangle red06;
        public Ra_Triangle red07;
        public Ra_Triangle red08;
        public Ra_Triangle background01;
        public Ra_Triangle background02;

        private Ra_Triangle[] _allTriangles;

        private void Start()
        {
            _allTriangles = new[] {
                blue01, blue02, blue03, blue04, blue05, blue06, blue07, blue08, blue09, blue10, blue11, blue12,
                green01, green02, green03, green04, green05, green06, green07, green08, green09, green10, green11, green12,
                red01, red02, red03, red04, red05, red06, red07, red08,
                background01, background02
            };
        }

        // Resets the Material for all triangles in the collection
        public void SetMaterialFromParent_ResetAll()
        {
            foreach(var triangle in _allTriangles)
            {
                triangle.setMaterialFromParent_Reset();
            }
        }

        // Sets the Material for all triangles in the collection
        public void SetMaterialFromPointable_HoverAll()
        {
            foreach(var triangle in _allTriangles)
            {
                triangle.setMaterialFromPointable_Hover();
            }
        }

        // Enables the interaction for all triangles in the collection
        public void EnableInteractionAll()
        {
            foreach(var triangle in _allTriangles)
            {
                triangle.EnableInteraction();
            }
        }

        // Disables the interaction for all triangles in the collection
        public void DisableInteractionAll()
        {
            foreach(var triangle in _allTriangles)
            {
                triangle.DisableInteraction();
            }
        }
    }

}