﻿using System;
using System.Linq;
using OmiLAXR;
using OmiLAXR.Interaction;
using RePiX_VR.GeometryStage;
using UnityEngine;
using Interactable = Valve.VR.InteractionSystem.Interactable;

// This class represents the triangles
// It is used to form the meshes and make the triangles visible
// Note that the parent of the triangle should have the default material of the triangle on it
namespace RePiX_VR

{
    [RequireComponent(typeof(MeshFilter))]
    public class Ra_Triangle : MonoBehaviour
    {
        public VertexComponent vertex1;
        public VertexComponent vertex2;
        public VertexComponent vertex3;

        private Vector3 _vertex1Position;
        private Vector3 _vertex2Position;
        private Vector3 _vertex3Position;

        private Mesh _mesh;
        private MeshCollider _collider;
        private MeshFilter _meshFilter;

        [Tooltip("Without Backface Culling primitives are visible from both sides")]
        public bool useBackfaceCulling;
        public bool userTriangle;

        public Vector3[] vertices;
        public int[] triangles;

        // Start is called before the first frame update
        private void Start()
        {
            // If it is a user Triangle, the mesh is already defined and the whole method can be skipped.
            if (userTriangle)
            {
                _collider = GetComponent<MeshCollider>();
                if (!_collider)
                    _collider = gameObject.AddComponent<MeshCollider>();
                
                var mesh = _collider.sharedMesh;
                mesh.triangles = mesh.triangles.Concat(mesh.triangles.Reverse()).ToArray();
                
                SetupListeners();
                return;
            }
            var t = transform;
            
            // Determine position of Triangle
            t.localPosition = (vertex1.transform.localPosition + vertex2.transform.localPosition + vertex3.transform.localPosition) / 3.0F;

            // Create one regular triangle 
            _mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = _mesh;

            var position = t.position;
            vertices = new[]
            {
                vertex1.transform.position - position,
                vertex2.transform.position - position,
                vertex3.transform.position - position
            };

            triangles = new[]
            {
                0, 1, 2
            };
            if (!userTriangle)
            {
                
            }
            _mesh.Clear();
            _mesh.vertices = vertices;
            _mesh.triangles = triangles;

            _mesh.RecalculateNormals();

            // Without Backface Culling create a second triangle with the same vertices in opposite direction
            if (useBackfaceCulling) return;
            var normals = _mesh.normals;
            
            vertices = new[]
            {
                vertex1.transform.position - position,
                vertex2.transform.position - t.position,
                vertex3.transform.position - t.position,
                vertex1.transform.position - t.position,
                vertex2.transform.position - t.position,
                vertex3.transform.position - t.position
            };

            triangles = new int[]
            {
                0, 1, 2,
                5, 4, 3
            };

            _mesh.Clear();
            _mesh.vertices = vertices;
            _mesh.triangles = triangles;

            // Three of the normals have to point in opposite direction
            Vector3[] newNormals =
            {
                normals[0],
                normals[1],
                normals[2],
                -normals[0],
                -normals[1],
                -normals[2]
            };

            // Add a mesh collider with the correct mesh
            _mesh.normals = newNormals;
            var meshCollider = GetComponent<MeshCollider>();
            if (!meshCollider)
                meshCollider = gameObject.AddComponent<MeshCollider>();
            meshCollider.sharedMesh = _mesh;
            
            SetupListeners();
        }

        private void FixedUpdate()
        {
            if (!_collider) return;
            
            var vertexPosChanged = false;
            vertexPosChanged |= vertex1.transform.position != _vertex1Position;
            vertexPosChanged |= vertex2.transform.position != _vertex2Position;
            vertexPosChanged |= vertex3.transform.position != _vertex3Position;
            if (!vertexPosChanged) return;

            _meshFilter ??= GetComponent<MeshFilter>();
            var mesh = _meshFilter.sharedMesh;
            mesh.triangles = mesh.triangles.Concat(mesh.triangles.Reverse()).ToArray();

            _collider.sharedMesh = mesh;

            _vertex1Position = vertex1.transform.position;
            _vertex2Position = vertex2.transform.position;
            _vertex3Position = vertex3.transform.position;
        }

        private void SetupListeners()
        {
            // Add listeners for fragment calculation automatically
            var pointable = GetComponent<Pointable>();
            var mouse = GetComponent<MouseInteractionEvents>();
            var fragmentCalculation = FindObjectOfType<Ra_Fragment_Calculation>();
            
            if (fragmentCalculation && pointable)
                pointable.onClick.AddListener(_=> fragmentCalculation.TriangleInput(this));
            if (fragmentCalculation && mouse)
                mouse.onMouseClick.AddListener(_=> fragmentCalculation.TriangleInput(this));
        }

        // Resets the material to the material that is given as the material of the parent component
        public void setMaterialFromParent_Reset()
        {
            GetComponent<MeshRenderer>().material = transform.parent.GetComponent<MeshRenderer>().materials[0];
        }

        // Sets the material to the material that is given as the hover material of the pointable component
        public void setMaterialFromPointable_Hover()
        {
            GetComponent<MeshRenderer>().material = GetComponent<Pointable>().hoverMaterial;
        }

        // Enables the interaction for this triangle
        // The enabled components are different for VR and nonVR
        public void EnableInteraction()
        {
            if (Learner.Instance.IsVR)
            {
                // Enable Scripts:
                GetComponent<Pointable>().enabled = true;
                GetComponent<Interactable>().enabled = true;
                GetComponent<Collider>().enabled = true;
                GetComponent<MouseInteractionEvents>().enabled = false;
                return;
            }
            
            // Enable Scripts:
            GetComponent<Pointable>().enabled = true;
            GetComponent<MouseInteractionEvents>().enabled = true;
            GetComponent<Collider>().enabled = true;
            GetComponent<Interactable>().enabled = true;
        }

        // Disables the interaction for this triangle
        public void DisableInteraction()
        {
            // Disable Scripts:
            GetComponent<Pointable>().enabled = false;
            GetComponent<Interactable>().enabled = false;
            GetComponent<MouseInteractionEvents>().enabled = false;
            GetComponent<Collider>().enabled = false;
        }
    }

}