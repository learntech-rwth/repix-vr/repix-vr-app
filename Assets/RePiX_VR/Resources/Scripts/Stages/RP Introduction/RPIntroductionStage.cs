using System;
using OmiLAXR.UI;
using OmiLAXR.InstructionalDesign;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR
{
    public class RPIntroductionStage : RPStageManager
    {
        public UnityEvent onSelectNext = new UnityEvent();

        protected override void Start()
        {
            base.Start();
            StartStage();
        }
        
        public void SelectNext()
        {
            onSelectNext?.Invoke();
        }
    }
}
