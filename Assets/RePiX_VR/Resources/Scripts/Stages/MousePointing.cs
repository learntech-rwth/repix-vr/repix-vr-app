using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MousePointing :  MonoBehaviour, IPointerExitHandler, IPointerEnterHandler
{
    [HideInInspector] public bool isInsideTexture = false;
    
    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData) => isInsideTexture = true;
    
    void IPointerExitHandler.OnPointerExit(PointerEventData eventData) => isInsideTexture = false;
}
