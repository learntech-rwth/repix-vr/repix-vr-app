using System.Linq;
using OmiLAXR;
using UnityEngine;

namespace RePiX_VR
{
    public class DimmerSetup : MonoBehaviour
    {
        private void Start()
        {
            var lights = FindObjectsOfType<Light>();
            var sun = lights.FirstOrDefault(l => l.CompareTag("Sun"));
            var learner = lights.FirstOrDefault(l => l.GetComponent<Learner>());

            var dimmerAttacher = GetComponent<DimmerAttacher>();
            dimmerAttacher.AddTarget(sun);
            dimmerAttacher.AddTarget(learner);
        }
    }
}