﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Controls custom lit objects to sync up light sources etc.
/// </summary>
public class CustomLitController : MonoBehaviour
{
    public Material shaderMat;
    
    public Matrix4x4 diffuseMat = Matrix4x4.identity;
    public Matrix4x4 specularMat = Matrix4x4.identity;

    #region Cached Properties
    private static readonly int DiffuseMat = Shader.PropertyToID("_DiffuseMat");
    private static readonly int SpecularMat = Shader.PropertyToID("_SpecularMat");
    private static readonly int LightPositions = Shader.PropertyToID("_LightPositions");
    private static readonly int LightColors = Shader.PropertyToID("_LightColors");
    private static readonly int LightDirections = Shader.PropertyToID("_LightDirections");

    #endregion

    private const int MaxLights = 32;
    private readonly List<CustomLight> lights = new List<CustomLight>();

    public int CurrentLights => lights.Count;

    private void Start()
    {
        shaderMat.SetMatrix(DiffuseMat, diffuseMat);
        shaderMat.SetMatrix(SpecularMat, specularMat);
    }
    
    public void AddLight(CustomLight lightSource)
    {
        lights.Add(lightSource);
        if (CurrentLights <= MaxLights)
        {
            lights.Add(lightSource);
            UpdateLights();
        }
        else
        {
            lights.RemoveAt(0);
            AddLight(lightSource);
        }
    }

    public void RemoveLight(CustomLight lightSource)
    {
        if (lightSource == null) throw new NullReferenceException("Light can't be null");
        lights.Remove(lightSource);
        UpdateLights();
    }

    public void UpdateLights()
    {
        var lightPositions = new Vector4[MaxLights];
        var lightDirections = new Vector4[MaxLights];
        var lightColors = new Color[MaxLights];

        var i = 0;
        foreach (var l in lights)
        {
            lightPositions[i] = l.Position;
            lightDirections[i] = l.Direction;
            lightColors[i] = l.LightColor;
        }
        
        shaderMat.SetVectorArray(LightPositions, lightPositions);
        shaderMat.SetColorArray(LightColors, lightColors);
        shaderMat.SetVectorArray(LightDirections, lightDirections);
    }
}
