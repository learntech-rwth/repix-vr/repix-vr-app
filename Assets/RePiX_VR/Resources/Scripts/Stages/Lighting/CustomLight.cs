﻿using System;
using UnityEngine;

public class CustomLight : MonoBehaviour
{
    public event Action<Color> ColorChanged;
    public event Action<float> AngleChanged;
    
    public float intensity = 1f;
    
    [SerializeField] private Color color = Color.white;

    public Color LightColor
    {
        get => color;
        set
        {
            color = value;
            ColorChanged?.Invoke(color);
        }
    }

    [SerializeField] private float angle = 0f;

    public float Angle
    {
        get => angle;
        set
        {
            angle = value;
            AngleChanged?.Invoke(angle);
        }
    }

    public Vector3 Position => transform.position;
    public Vector3 Direction => transform.forward;

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(Position, Direction);
    }
}
