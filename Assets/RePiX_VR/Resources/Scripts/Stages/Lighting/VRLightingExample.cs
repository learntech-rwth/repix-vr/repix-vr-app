﻿using OmiLAXR.Helpers;
using UnityEngine;
using Valve.VR;

namespace RePiX_VR
{
    /// <summary>
    /// Controller for VR-specific interactions.
    /// </summary>
    public class VRLightingExample : MonoBehaviour
    {
        [SerializeField] private Placer placer;
        [SerializeField] private Attacher attacher;
        [SerializeField] private Vector3 offset;

        public SteamVR_Action_Boolean placeAction;

        private Transform hand;
        
        private void OnEnable()
        {
            hand = FindLaserPointer.Find();
            placer.SetParent(hand, offset);

            var source = hand.GetComponent<SteamVR_Behaviour_Pose>().inputSource;
            placeAction.AddOnStateDownListener(OnClick, source);
            
            var colorWheel = attacher.AttachTo(hand).GetComponent<TrackedColorPicker>();
            colorWheel.ColorPicked += (_, c) => placer.Color = c;
        }

        private void OnClick(SteamVR_Action_Boolean arg1, SteamVR_Input_Sources arg2)
        {
            placer.Place(new Ray(hand.transform.position, hand.forward));
        }
        
        private void OnDisable() => placer.Clear();
    }
}