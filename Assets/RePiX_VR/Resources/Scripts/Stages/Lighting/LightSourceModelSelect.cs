﻿using UnityEngine;

namespace RePiX_VR
{
    [RequireComponent(typeof(CustomLight))]
    public class LightSourceModelSelect : MonoBehaviour
    {
        public GameObject pointLight, coneLight;
        private CustomLight light;
    }

}