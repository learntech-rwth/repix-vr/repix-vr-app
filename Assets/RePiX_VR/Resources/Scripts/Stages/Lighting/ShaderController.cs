﻿using System;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR.Extensions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

public class ShaderController : MonoBehaviour
{
    public Material litMat;

    [SerializeField] private List<GameObject> litObjects = new List<GameObject>();
    
    #region PropertyIDs
    private static readonly int SpecularType1 = Shader.PropertyToID("_Model");
    private static readonly int Shininess = Shader.PropertyToID("_Shininess");
    private static readonly int AmbientTerm = Shader.PropertyToID("_AmbientTerm");
    private static readonly int DiffuseTerm = Shader.PropertyToID("_DiffuseTerm");
    private static readonly int SpecularTerm = Shader.PropertyToID("_SpecularTerm");
    private static readonly int LightColors = Shader.PropertyToID("_LightColors");
    private static readonly int LightPositions = Shader.PropertyToID("_LightPositions");
    private static readonly int LightDirections = Shader.PropertyToID("_LightDirections");
    private static readonly int StencilPosition = Shader.PropertyToID("_StencilPosition");
    private static readonly int StencilColor = Shader.PropertyToID("_StencilColor");
    private static readonly int StencilDirection = Shader.PropertyToID("_StencilDirection");
    #endregion
    
    #region Property Setters
    public void SetShaderType(Shader s)
    {
        litMat.shader = s;
        RefreshShader();
    }

    private int specularType = 0;
    public void SetSpecularType(int type)
    {
        specularType = type;
        litMat.SetInt(SpecularType1, type);
    }

    private float shininess = 10;
    public void SetShininess(float h)
    {
        shininess = h;
        litMat.SetFloat(Shininess, h);
    }

    private float ambient = 1f;
    public void SetAmbient(float a)
    {
        ambient = a;
        litMat.SetFloat(AmbientTerm, a);
    }

    private float diffuse = 1f;
    public void SetDiffuse(float d)
    {
        diffuse = d;
        litMat.SetFloat(DiffuseTerm, d);
    }

    private float specular = 1f;
    public void SetSpecular(float s)
    {
        specular = s;
        litMat.SetFloat(SpecularTerm, s);
    }
    #endregion

    public List<CustomLight> lights = new List<CustomLight>();
    private const int MaxLights = 16;

    public CustomLight stencil;

    private void OnEnable()
    {
        litMat = new Material(litMat);
        
        foreach (var mo in litObjects.Select(lo => lo.AddComponent<MaterialOverlay>()))
        {
            mo.SetMaterial(litMat);
        }
        
        UpdateLights();
    }

    private void OnDisable()
    {
        foreach (var mo in litObjects.Select(lo => lo.GetComponent<MaterialOverlay>()))
        {
            Destroy(mo);
        }
    }

    private void Update()
    {
        if (stencil != null) UpdateStencil();
    }

    private void RefreshShader()
    {
        // resubmit uniforms if shader is changed
        UpdateLights();
        SetSpecularType(specularType);
        SetShininess(shininess);
        SetAmbient(ambient);
        SetDiffuse(diffuse);
        SetSpecular(specular);
        UpdateStencil();
    }
    
    private void UpdateLights()
    {
        var colors = new Color[MaxLights];
        var directions = new Vector4[MaxLights];
        var positions = new Vector4[MaxLights];
        
        var i = 0;
        foreach (var l in lights)
        {
            colors[i] = l.LightColor;
            var dir = l.Direction;
            directions[i] = new Vector4(dir.x, dir.y, dir.z, l.Angle);

            var pos = l.Position;
            positions[i] = new Vector4(pos.x, pos.y, pos.z, l.intensity);
            i++;
        }

        litMat.SetColorArray(LightColors, colors);
        litMat.SetVectorArray(LightPositions, positions);
        litMat.SetVectorArray(LightDirections, directions);
    }
    
    private void UpdateStencil()
    {
        var active = stencil.gameObject.activeInHierarchy;
        
        litMat.SetColor(StencilColor, stencil.LightColor);

        var pos = new Vector4(stencil.Position.x, stencil.Position.y, stencil.Position.z, active ? stencil.intensity : 0f);
        litMat.SetVector(StencilPosition, pos);

        var dir = new Vector4(stencil.Direction.x, stencil.Direction.y, stencil.Direction.z, stencil.Angle);
        litMat.SetVector(StencilDirection, dir);
    }

    public void AddLight(CustomLight customLight)
    {
        // Remove oldest light if list is at capacity
        if (lights.Count >= MaxLights)
        {
            Destroy(lights[0].gameObject);
            lights.RemoveAt(0);
        }
        
        lights.Add(customLight);
        UpdateLights();
    }

    public void RemoveLight(CustomLight l)
    {
        lights.Remove(l);
        Destroy(l.gameObject);

        UpdateLights();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(ShaderController))]
public class ShaderControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var sc = target as ShaderController;
        
        if (GUILayout.Button("Add Light")) AddLight(sc);
    }

    private static void AddLight(ShaderController sc)
    {
        var light = new GameObject("Light");
        if (sc != null) sc.AddLight(light.AddComponent<CustomLight>());
    }
}
#endif
