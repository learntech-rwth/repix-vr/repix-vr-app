﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace RePiX_VR
{
    internal enum DimmerInterpolation
    {
        Lerp, SmoothStep
    }

    /// <summary>
    /// Automatically places and removes dimmer components at designated light sources.
    /// </summary>
    public class DimmerAttacher : MonoBehaviour
    {
        [SerializeField, Tooltip("Lights to attach dimmers to.")]
        private Light[] targets;

        [Tooltip("Time needed for the lighting to change."), Range(0.1f, 5f)]
        public float duration = 1f;

        [SerializeField, Tooltip("Interpolation function to apply to dimmer progression")]
        private DimmerInterpolation interpolation = DimmerInterpolation.SmoothStep;

        [SerializeField, Tooltip("Determines whether the lights will initially be toggled on or off when this script comes into action.")]
        private bool startState = true;

        private List<LightDimmer> dimmers = new List<LightDimmer>();

        public bool State { get; private set; } = false;

        private float _intensity = 1f;
        
        public float Intensity
        {
            get => _intensity;
            set
            {
                _intensity = Mathf.Clamp01(value);

                // update intensity in dimmers accordingly
                foreach (var d in dimmers) d.Intensity = _intensity;
            }
        }

        private void Awake()
        {
            State = startState;
            foreach (var t in targets) dimmers.Add(t.gameObject.AddComponent<LightDimmer>());
        }

        bool running = false;
        Coroutine intensityChange;

        public void SetTargetIntensity(float targetIntensity)
        {
            if (running && intensityChange != null) StopCoroutine(intensityChange);

            targetIntensity = Mathf.Clamp01(targetIntensity);
            intensityChange = StartCoroutine(ChangeIntensity(Intensity, targetIntensity));
        }

        private IEnumerator ChangeIntensity(float start, float target)
        {
            running = true;

            var rateChange = 1f / duration;
            var t = 0f;

            while (t < 1f)
            {
                t += rateChange * Time.deltaTime;

                switch (interpolation)
                {
                    case DimmerInterpolation.Lerp:
                        Intensity = Mathf.Lerp(start, target, t);
                        break;
                    case DimmerInterpolation.SmoothStep:
                        Intensity = Mathf.SmoothStep(start, target, t);
                        break;
                }

                yield return null;
            }

            running = false;
        }

        public void AddTarget(Light target)
        {
            targets = targets.Append(target).ToArray();
            
            var lightDimmer = target.GetComponent<LightDimmer>();
            if (!lightDimmer)
                lightDimmer = target.gameObject.AddComponent<LightDimmer>();
            
            dimmers.Add(lightDimmer);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(DimmerAttacher))]
    internal class DimmerAttacherEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var attacher = target as DimmerAttacher;

            if (Application.isPlaying)
            {
                attacher.Intensity = EditorGUILayout.Slider("Intensity", attacher.Intensity, 0f, 1f);
                
            }
        }
    }
#endif
}

