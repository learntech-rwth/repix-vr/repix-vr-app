﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace RePiX_VR
{
    [Serializable]
    public class PlacerStencil
    {
        public string id;
        public GameObject prefab;
        public GameObject stencil;
    }

    public enum PlacerMode
    {
        Place, Move, Delete
    }

    public class Placer : MonoBehaviour
    {
        [SerializeField]
        private Color color = Color.white;

        public PlacerMode mode;
        
        public Color Color
        {
            get => color;
            set
            {
                color = value;
                if (stencil) stencil.GetComponent<ColorLight>().Color = color;
            }
        }
        
        [SerializeField] private List<PlacerStencil> stencils = new List<PlacerStencil>();
        private Transform itemParent;
        
        private PlacerStencil selected;
        private GameObject stencil;

        /// <summary>
        /// Sets the parent transform for the spawn stencil.
        /// Items will be spawned into the game world at the same position.
        /// </summary>
        /// <param name="t">Transform to parent to</param>
        /// <param name="offset">Offset from the parent transform</param>
        public void SetParent(Transform t, Vector3 offset)
        {
            Clear();
            
            var parent = new GameObject("Placer Parent");
            itemParent = parent.transform;
            itemParent.parent = t;
            itemParent.localPosition = offset;
        }

        public void Place(Ray ray)
        {
            var isHit = Physics.Raycast(ray, out var hit);
            
            if (isHit && hit.collider.gameObject.CompareTag("PrioritizeOnHover")) return;
            
            if (CanPlace()) PlaceItem();
            else if (isHit) hit.collider.gameObject.GetComponent<PlacedObject>()?.Action(this);
        }
        
        public void Clear()
        {
            if (itemParent) DestroyImmediate(itemParent.gameObject);
        }

        private void Deselect()
        {
            Destroy(stencil);
            selected = null;
        }

        public void SetItem(string id)
        {
            if (selected != null && selected.id == id) return;
               
            DestroyImmediate(stencil);
            selected = stencils.Find(s => s.id == id);

            if (selected == null) return;

            mode = PlacerMode.Place;
            
            stencil = Instantiate(selected.stencil, itemParent);
            stencil.GetComponent<ColorLight>().Color = Color;
        }

        public void SetStencil(PlacerStencil s)
        {
            selected = s ?? throw new ArgumentNullException(nameof(s));
            
            stencil = Instantiate(selected.stencil, itemParent);
            stencil.GetComponent<ColorLight>().Color = Color;
        }
        
        private void PlaceItem()
        {
            if (selected == null) return; // TODO: Throw error here
            if (mode == PlacerMode.Delete) throw new ApplicationException("Should not place in Delete mode.");
            
            var item = Instantiate(selected.prefab, itemParent.transform);
            item.transform.parent = transform;

            var placedObject = item.AddComponent<PlacedObject>();
            placedObject.placerStencil = selected;

            placedObject.GetComponent<ColorLight>().Color = color;
            
            if (mode == PlacerMode.Move) Deselect();
        }

        public void DeleteMode()
        {
            Deselect();
            mode = PlacerMode.Delete;
        }

        public void MoveMode()
        {
            Deselect();
            mode = PlacerMode.Move;
        }

        public bool CanPlace() => mode == PlacerMode.Place || mode == PlacerMode.Move && stencil != null;
    }
}