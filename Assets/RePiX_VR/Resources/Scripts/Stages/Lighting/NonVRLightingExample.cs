﻿using System;
using OmiLAXR;
using OmiLAXR.Interaction;
using OmiLAXR.UI;
using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Controller for non-vr specific interactions.
    /// </summary>
    public class NonVRLightingExample : MonoBehaviour
    {
        [SerializeField] private Placer placer;
        [SerializeField] private Vector3 offset;
        [SerializeField] private ColorPicker2D colorPicker2D;

        private Camera main;
        
        private void OnEnable()
        {
            main = Camera.main;
            
            if (!main) throw new NullReferenceException("No main camera exists!");
            var hand = main.gameObject;

            placer.SetParent(hand.transform, offset);
            colorPicker2D.OnChanged += (_, c) => placer.Color = c;
        }

        private void OnDisable() => placer.Clear();

        private void Update()
        {
            if (Input.GetMouseButtonDown(0)) placer.Place(main.ScreenPointToRay(Input.mousePosition));
        }
    }
}