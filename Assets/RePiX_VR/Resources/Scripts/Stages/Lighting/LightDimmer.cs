﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Allows smoothly dimming an associated light component.
    /// </summary>
    [RequireComponent(typeof(Light))]
    public class LightDimmer : MonoBehaviour
    {
        private Light source;
        private float initialIntensity;

        private float _intensity = 0f;

        /// <summary>
        /// Current intensity value of the light normalized to be between 0 (off) and 1 (full intensity).
        /// </summary>
        public float Intensity { get => _intensity; set => SetIntensity(value); }

        private void SetIntensity(float v)
        {
            _intensity = Mathf.Clamp01(v);
            source.intensity = Mathf.Lerp(0f, initialIntensity, _intensity);
        }

        private void Awake()
        {
            source = GetComponent<Light>();
            initialIntensity = source.intensity;
        }

        private void OnDisable()
        {
            source.intensity = initialIntensity;
        }

        private void OnDestroy()
        {
            source.intensity = initialIntensity;
        }
    }
}