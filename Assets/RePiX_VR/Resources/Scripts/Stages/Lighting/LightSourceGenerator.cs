﻿using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

namespace RePiX_VR
{
    public class LightSourceGenerator : MonoBehaviour
    {
        [SerializeField, Tooltip("Color components.")]
        private LinearMapping red, green, blue;

        [SerializeField, Tooltip("Location at which to create light sources.")]
        private Transform origin;

        private GameObject instance;

        /// <summary>
        /// Called when the light source gets separated from the placer.
        /// </summary>
        public UnityEvent OnDecouple;

        private void Update()
        {
            if (instance)
            {
                instance.GetComponent<ColorLight>().Color = new Color(red.value, green.value, blue.value);
            }

        }

        /// <summary>
        /// Generates a new lightsource
        /// </summary>
        /// <param name="prefab">light source prefab</param>
        public void GenerateSource(GameObject prefab)
        {
            if (instance && instance.transform.parent == origin) Destroy(instance);

            instance = Instantiate(prefab, origin);
            instance.GetComponent<Throwable>().onPickUp.AddListener(DecoupleSource);
        }

        /// <summary>
        /// Decouples light source from hand.
        /// </summary>
        public void DecoupleSource()
        {
            OnDecouple?.Invoke();
            instance.GetComponent<Throwable>().onPickUp.RemoveListener(DecoupleSource);
            instance = null;
        }
    }
}