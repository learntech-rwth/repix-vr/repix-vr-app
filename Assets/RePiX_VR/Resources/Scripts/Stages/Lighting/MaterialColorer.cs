﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR
{

    /// <summary>
    /// Component that allows to programmatically change the color
    /// of a material associated with this object.
    /// </summary>
    public class MaterialColorer : MonoBehaviour
    {
        [Header("Color Settings")]
        [SerializeField, Tooltip("Color for the mesh to take.")]
        private Color color = Color.white;

        /// <summary>
        /// Color currently applied to a material.
        /// </summary>
        public Color Color => color;

        /// <summary>
        /// Determines whether the alpha value of the previous color should be kept.
        /// </summary>
        public bool inheritAlpha = false;

        [Header("Renderer Settings")]
        [SerializeField, Tooltip("Color properties to set (use multiple e.g. to change emission as well).")]
        private string[] colorProperties = { "_Color" };
        private int[] propId;

        [SerializeField, Tooltip("Index of the material that should be changed")]
        private int index = -1;

        // We make use of a material property block to update shader parameters
        // without causing unnecessary draw calls.
        // Refer: https://docs.unity3d.com/ScriptReference/MaterialPropertyBlock.html
        private MaterialPropertyBlock mpb;
        private Renderer rend;

        private void Awake()
        {
            rend = GetComponent<Renderer>();
            mpb = new MaterialPropertyBlock();
            
            propId = new int[colorProperties.Length];
            
            // Assignment via property id is faster, although it barely matters here
            for (var i = 0; i < colorProperties.Length; i++)
            {
                propId[i] = Shader.PropertyToID(colorProperties[i]);
            }
        }

        private void Start()
        {
            SetColor(color);
        }

        /// <summary>
        /// Updates the renderers with the given color.
        /// </summary>
        /// <param name="c">new material color</param>
        public void SetColor(Color c)
        {
            if (inheritAlpha) c.a = color.a;
            color = c;

            if (index != -1) rend.GetPropertyBlock(mpb, index);
            else rend.GetPropertyBlock(mpb);

            foreach (var p in propId) mpb.SetColor(p, c);

            if (index != -1) rend.SetPropertyBlock(mpb, index);
            else rend.SetPropertyBlock(mpb);
        }
    }
}