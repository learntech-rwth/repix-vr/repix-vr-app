﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class LightPlacementController : MonoBehaviour
{
    private enum Modes { Delete, Place, Normal }
    private Modes mode = Modes.Delete;
    
    private CustomLight stencilLight;
    private GameObject stencilInstance;
    
    public GameObject stencil;
    public ShaderController shader;
    public Transform lightsParent;
    public VrRangeSlider slider;
    
    public void SetColor(MonoBehaviour sender, Color c) => stencilLight.LightColor = c;

    public void Init(Transform spawnPoint, Vector3 offset)
    {
        stencilInstance = Instantiate(stencil, spawnPoint);
        stencilInstance.transform.localPosition += offset;
        
        stencilLight = stencilInstance.GetComponent<CustomLight>();
        shader.stencil = stencilLight;
        stencilInstance.SetActive(mode == Modes.Place);
    }

    private void Place()
    {
        var lamp = Instantiate(stencilInstance, lightsParent);
        lamp.transform.position = stencilInstance.transform.position;
        lamp.transform.rotation = stencilInstance.transform.rotation;
        lamp.GetComponent<LampModelSelector>().SetCollider(true);

        var customLight = lamp.GetComponent<CustomLight>();
        customLight.Angle = stencilLight.Angle;
        customLight.LightColor = stencilLight.LightColor;
        
        shader.AddLight(customLight);
    }

    public void MakePlacement(Ray ray)
    {
        var hasHit = Physics.Raycast(ray, out var rayHit);

        switch (mode)
        {
            case Modes.Normal:
                break;
            case Modes.Place:
                if (!hasHit || !rayHit.collider.CompareTag("PrioritizeOnHover")) Place();
                break;
            case Modes.Delete:
                if (!hasHit) return;

                var customLight = rayHit.collider.transform.GetComponentInParent<CustomLight>();
                if (customLight) shader.RemoveLight(customLight);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void Awake()
    {
        var obj = new GameObject("Lights")
        {
            transform =
            {
                position = Vector3.zero
            }
        };
        obj.SetActive(false);
        
        lightsParent = obj.transform;
        slider.OnValueChange += v => stencilLight.Angle = v;
    }

    private void OnEnable()
    {
        lightsParent.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        if (lightsParent) lightsParent.gameObject.SetActive(false);

        Destroy(stencilInstance);
        stencilLight = null;
        shader.stencil = null;
    }

    public void DeleteMode()
    {
        mode = Modes.Delete;

        if (stencilInstance)
        {
            stencilInstance.SetActive(false);
        }

        if (stencilLight)
        {
            stencilLight.intensity = 0f;    
        }
    }

    public void FlashLightPlacementMode() => PlacementMode(slider.Value);
    public void LightBulbPlacementMode() => PlacementMode();

    public void NormalMode()
    {
        mode = Modes.Normal;
        
        if (stencilInstance)
        {
            stencilInstance.SetActive(false);
        }
    }


    private void PlacementMode(float angle = 0f)
    {
        mode = Modes.Place;

        if (stencilInstance)
        {
            stencilInstance.SetActive(true);
        }

        if (!stencilLight) return;
        
        stencilLight.intensity = 1f;
        stencilLight.Angle = angle;
    }
}
