﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace RePiX_VR
{
    /// <summary>
    /// Sets light color and updates colorer components
    /// to reflect the selection.
    /// </summary>
    public class ColorLight : MonoBehaviour
    {
        [SerializeField, Tooltip("Color of the light source")]
        private Color color = Color.white;

        private Light lightSource;

        [SerializeField, Tooltip("Components to update with the chosen color")]
        private List<MaterialColorer> materialColorerList;

        public Color Color {
            get => color; 
            set
            {
                color = value;
                lightSource.color = value;
                foreach (var c in materialColorerList) c.SetColor(value);
            }
        }

        private void Awake()
        {
            lightSource = GetComponentInChildren<Light>();
        }
    }

    #if UNITY_EDITOR
    [CustomEditor(typeof(ColorLight))]
    public class ColorLightEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (Application.isPlaying && GUILayout.Button("Update"))
            {
                var cl = target as ColorLight;
                cl.Color = cl.Color;
            }
        }
    }
    #endif
}