using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightControls2D : MonoBehaviour
{
    public enum LightMode
    {
        NormalMode,
        LightBulb,
        Flashlight,
        RemoveLight
    };
    public LightPlacementController lightPlacementController;
    public LightMode lightMode = LightMode.NormalMode;
    private Button _button;
    void Start()
    {
        if (!lightPlacementController)
            lightPlacementController = GameObject.FindObjectOfType<LightPlacementController>(true)
                .GetComponent<LightPlacementController>();

        _button = gameObject.GetComponent<Button>();
        
        if (!_button)
            return;

        switch (lightMode)
        {
            case LightMode.NormalMode:
                _button.onClick.AddListener(() => lightPlacementController.NormalMode());
                break;
            case LightMode.LightBulb:
                _button.onClick.AddListener(() => lightPlacementController.LightBulbPlacementMode());
                break;
            case LightMode.Flashlight :
                _button.onClick.AddListener(() => lightPlacementController.FlashLightPlacementMode());
                break;
            case LightMode.RemoveLight :
                _button.onClick.AddListener(() => lightPlacementController.DeleteMode());
                break;
            default:
                Debug.LogWarning("Wrong Light Mode");
                break;
        }
    }
}
