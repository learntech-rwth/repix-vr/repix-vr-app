﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR
{
    /// <summary>
    /// Mere test-component that sets a random light color on instantiation.
    /// </summary>

    [RequireComponent(typeof(ColorLight))]
    public class RandomizeLight : MonoBehaviour
    {
        private ColorLight colorLight;

        private void Start()
        {
            colorLight = GetComponent<ColorLight>();
            colorLight.Color = Random.ColorHSV(0f, 1f, 1f, 1f, .8f, 1f);
        }
    }
}