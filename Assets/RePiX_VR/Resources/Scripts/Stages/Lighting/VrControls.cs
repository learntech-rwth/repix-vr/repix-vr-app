﻿using OmiLAXR.Helpers;
using RePiX_VR;
using UnityEngine;
using Valve.VR;

public class VrControls : MonoBehaviour
{
    public SteamVR_Action_Boolean placeAction;
    public Vector3 offset;
    public LightPlacementController placer;

    private void OnEnable()
    {
        var hand = FindLaserPointer.Find();
        
        var picker = GetComponent<Attacher>().AttachTo(hand);
        picker.GetComponent<TrackedColorPicker>().ColorPicked += placer.SetColor;
        
        placeAction.onStateDown += VrPlace;
        
        placer.Init(hand, offset);
    }

    private void OnDisable()
    {
        placeAction.onStateDown -= VrPlace;
        GetComponent<Attacher>().Detach();
    }

    private void VrPlace(SteamVR_Action_Boolean arg1, SteamVR_Input_Sources arg2)
    {
        placer.MakePlacement(FindLaserPointer.CastFrom());
    }
}
