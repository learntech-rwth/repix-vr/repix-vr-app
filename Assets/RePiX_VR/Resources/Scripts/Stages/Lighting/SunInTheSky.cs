using System.Collections;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class SunInTheSky : MonoBehaviour
{
    public float sunRadius = 1f;

    private Light _sun;
    private float _initialReflectionIntensity;
    private float _initialAmbientIntensity;

    private void Awake()
    {
        _sun = FindObjectsOfType<Light>(true).FirstOrDefault(s => s.CompareTag("Sun"));
        if (!_sun)
        {
            enabled = false;
            return;
        }

        _initialReflectionIntensity = RenderSettings.reflectionIntensity;
        _initialAmbientIntensity = RenderSettings.ambientIntensity;

        GetComponent<SphereCollider>().radius = sunRadius;
        transform.position = _sun.transform.position;
    }

    

    public void Off()
    {
        _sun.enabled = false;
        RenderSettings.reflectionIntensity = 0f;
        RenderSettings.ambientIntensity = 0f;
    }

    public void On()
    {
        _sun.enabled = true;
        RenderSettings.reflectionIntensity = _initialReflectionIntensity;
        RenderSettings.ambientIntensity = _initialAmbientIntensity;
    }
}