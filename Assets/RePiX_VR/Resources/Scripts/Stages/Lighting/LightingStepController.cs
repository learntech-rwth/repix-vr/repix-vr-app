﻿using OmiLAXR;
using OmiLAXR.Adapters.SteamVR;
using UnityEngine;

public class LightingStepController : MonoBehaviour
{
    private void Start()
    {
        if (Camera.main == null) throw new MissingReferenceException("No main camera in scene");

        var isVR = Learner.Instance.IsVR;
        
        GetComponent<VrControls>().enabled = isVR;
        GetComponent<MouseControls>().enabled = !isVR;
        transform.Find("NonVR").gameObject.SetActive(!isVR);
    }
}
