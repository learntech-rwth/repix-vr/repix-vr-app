﻿using System.Collections;
using UnityEngine;

namespace RePiX_VR
{
    [RequireComponent(typeof(LightSourceGenerator))]
    public class VendingMachineToggle : MonoBehaviour
    {
        private Animator anim;

        [SerializeField, Tooltip("Prefabs for the respective light sources.")]
        private GameObject pointLight, spotLight;

        private LightSourceGenerator lsg;

        private void Start()
        {
            anim = GetComponent<Animator>();
            lsg = GetComponent<LightSourceGenerator>();
        }

        public void TogglePointlight(bool state)
        {
            // TODO: add animation
            anim.SetFloat("Blend", state ? -1f : 1f);
            lsg.GenerateSource(state ? pointLight : spotLight);
        }

        public void ResetButtons()
        {
            anim.SetFloat("Blend", 0f);
        }
    }
}