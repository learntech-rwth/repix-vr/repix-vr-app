﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampModelSelector : MonoBehaviour
{
    private CustomLight _light;

    private GameObject _point, _cone;
    
    private void Awake()
    {
        _point = transform.Find("Lightbulb").gameObject;
        _cone = transform.Find("Flashlight").gameObject;

        _light = GetComponent<CustomLight>();
        _light.AngleChanged += angle =>
        {
            _point.SetActive(angle == 0f);
            _cone.SetActive(angle > 0f);
        };
    }

    public void SetCollider(bool b)
    {
        _point.GetComponent<Collider>().enabled = b;
        _cone.GetComponent<Collider>().enabled = b;
    }
}
