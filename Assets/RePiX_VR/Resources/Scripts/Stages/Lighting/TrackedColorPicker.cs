﻿using UnityEngine;
using Image = UnityEngine.UI.Image;
using System;
using OmiLAXR.UI;
using OmiLAXR.xAPI.Tracking;
using OmiLAXR.xAPI.Tracking.Interaction;
using RePiX_VR.xAPI;

namespace RePiX_VR
{
    /// <summary>
    /// Reworked VR color picker that can be combined with interaction tracking
    /// more easily.
    /// </summary>
    public class TrackedColorPicker : MonoBehaviour
    {
        /// <summary>
        /// Reference to the color pickers picker circle.
        /// </summary>
        public RectTransform picker;

        /// <summary>
        /// Outer threshold which the picker cannot overcome.
        /// </summary>
        public float boundary = 0.95f;
        
        /// <summary>
        /// Reference to the color circle object.
        /// </summary>
        public GameObject colorCircle;
        private RectTransform _rectTransform;
        private Image _image;
        private Trackable _trackable;

        /// <summary>
        /// Called if a new color has just been picked.
        /// </summary>
        public event OmiLAXRUiElementChangeHandler<Color> ColorPicked;

        private Texture2D Texture => _image.sprite.texture;

        // Start is called before the first frame update
        private void Start()
        {
            _rectTransform = colorCircle.GetComponent<RectTransform>();
            _image = colorCircle.GetComponent<Image>();
            _trackable = GetComponent<Trackable>();
        }

        private Color _color;
        
        /// <summary>
        /// Select a color from a position vector.
        /// </summary>
        /// <param name="v">position</param>
        /// <returns>color</returns>
        public void PickColor(Vector2 v)
        {
            // Get rect size, we assume that width = height.
            var size = _rectTransform.rect.width;

            // force v to be a valid position within the color circle
            if (v.magnitude >= boundary) v = v.normalized * boundary;
            
            picker.localPosition = (size / 2f) * (Vector2.one + v);

            var texPos = (v + Vector2.one) / 2f;

            _color = Texture.GetPixel(
                Mathf.RoundToInt(texPos.x * Texture.width),
                Mathf.RoundToInt(texPos.y * Texture.height));

            ColorPicked?.Invoke(_trackable, _color);
        }

        private bool _prevState;
        
        public void ColorOnRelease(bool isDown)
        {
            Debug.Log($"State: {_prevState} {isDown}");
            
            if (!isDown && _prevState) ColorPickerStatementsController.Instance.PickColor(_color);
            _prevState = isDown;
        }
    }

}