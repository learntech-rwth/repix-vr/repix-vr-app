using System;
using System.Collections;
using System.Collections.Generic;
using OmiLAXR.UI;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ShaderController2D : MonoBehaviour
{
    public enum ShaderMode
    {
        Shader, 
        AmbientLight, 
        DiffuseLight, 
        SpecularLight, 
        ObjectShinyness
    };

    public ShaderMode shaderMode;
    public ShaderController shaderController;
    private TextValueUpdater _textValueUpdater;
    void Start()
    {
        if (!shaderController)
            shaderController = GameObject.FindObjectOfType<ShaderController>(true).GetComponent<ShaderController>();

        switch (shaderMode)
        {
            case ShaderMode.Shader:
                gameObject.GetComponentInChildren<TMP_Dropdown>().onValueChanged.AddListener(value => ChangeShinyness(value));
                break;
            case ShaderMode.AmbientLight:
                gameObject.GetComponent<Toggle>().onValueChanged.AddListener(value => shaderController.SetAmbient((value ? 1 : 0)));
                break;
            case ShaderMode.DiffuseLight:
                gameObject.GetComponent<Toggle>().onValueChanged.AddListener(value => shaderController.SetDiffuse((value ? 1 : 0)));
                break;
            case ShaderMode.SpecularLight:
                gameObject.GetComponent<Toggle>().onValueChanged.AddListener(value => shaderController.SetSpecular((value ? 1 : 0)));
                gameObject.GetComponentInChildren<TMP_Dropdown>().onValueChanged.AddListener(value => shaderController.SetSpecularType(value));
                break;
            case ShaderMode.ObjectShinyness:
                _textValueUpdater = GetComponentInChildren<TextValueUpdater>();
                gameObject.GetComponent<Slider>().onValueChanged.AddListener(value => ChangeShinyness(value));
                break;
            default:
                Debug.LogWarning("Wrong Shader Mode");
                break;
        }
    }

    private void ChangeShinyness(float value)
    {
        shaderController.SetShininess(value);
        
        if (!_textValueUpdater)
            return;
        
        _textValueUpdater.UpdateText(value);
    }

    private void ChangeShaderMode(int value)
    {
        if (value == 0)
            shaderController.SetShaderType(Shader.Find("CustomVertexLit")); 
        
        if (value == 1)
            shaderController.SetShaderType(Shader.Find("CustomLit")); 

    }
}
