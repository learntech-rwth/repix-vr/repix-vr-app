﻿using UnityEngine;

public class MaterialOverlay : MonoBehaviour
{
    private Material original;
    private Renderer rend;
    
    private void Awake()
    {
        rend = GetComponent<Renderer>();
    }

    public void SetMaterial(Material litMat)
    {
        original = rend.material;
        rend.material = litMat;
    }

    public void OnDestroy()
    {
        rend.material = original;
    }
}
