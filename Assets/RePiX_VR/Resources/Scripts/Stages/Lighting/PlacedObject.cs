﻿using System;
using OmiLAXR.Interaction;
using RePiX_VR;
using UnityEngine;

[RequireComponent(typeof(Pointable))]
public class PlacedObject : MonoBehaviour
{
    [HideInInspector] public PlacerStencil placerStencil;

    public void Action(Placer p)
    {
        switch (p.mode)
        {
            case PlacerMode.Delete:
                DestroyImmediate(gameObject);
                break;
            
            case PlacerMode.Move:
                p.SetStencil(placerStencil);
                p.Color = GetComponent<ColorLight>().Color;
                DestroyImmediate(gameObject);
                break;
            case PlacerMode.Place:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
