using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;

namespace RePiX_VR
{
    public class LightingUIController : MonoBehaviour
    {
        #region Actions
        [Header("UI Actions")]
        public DimmerAttacher dimmer;

        [SerializeField]
        private bool initialState = true;
        #endregion

        private bool _lightState;
        public bool LightState
        {
            get => _lightState;
            set
            {
                _lightState = value;
                dimmer.SetTargetIntensity(_lightState ? 1f : 0f);
            }
        }

        private void OnEnable() => LightState = initialState;
    }
}