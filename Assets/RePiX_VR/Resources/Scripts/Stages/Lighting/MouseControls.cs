﻿using System;
using System.Collections;
using OmiLAXR;
using OmiLAXR.Interaction;
using OmiLAXR.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseControls : MonoBehaviour
{
    public Vector3 offset;
    public LightPlacementController placer;
    private Camera raycastFrom;
    public ColorPicker2D colorPicker;
    public MousePointing interactionMenu2D;
    public MousePointing shaderController2D;
    
    private event Action mouseClicked;
    private Coroutine mouseCheck;

    private void Awake()
    {
        if (Camera.main == null) throw new NullReferenceException();
        raycastFrom = Camera.main;
    }

    private void OnEnable()
    {
        mouseCheck = StartCoroutine(CheckMouse());
        mouseClicked += PlaceAction;
        colorPicker.OnChanged += placer.SetColor;
        
        placer.Init(raycastFrom.transform, offset);
    }

    private void OnDisable()
    {
        StopCoroutine(mouseCheck);
        mouseClicked -= PlaceAction;
    }

    private IEnumerator CheckMouse()
    {
        while (true)
        {
            if (!interactionMenu2D.isInsideTexture && !shaderController2D.isInsideTexture && !colorPicker.hoveredOver && Input.GetMouseButtonDown(0)) mouseClicked?.Invoke();
            yield return null;
        }
    }

    private void PlaceAction() => placer.MakePlacement(raycastFrom.ScreenPointToRay(Input.mousePosition));
}
