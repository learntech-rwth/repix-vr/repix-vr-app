﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR
{
    public class MultiColorer : MonoBehaviour
    {
        [SerializeField, Tooltip("Color of the light source")]
        private Color color = Color.white;

        [SerializeField, Tooltip("Components to update with the chosen color")]
        private List<MaterialColorer> materialColorerList;

        public Color Color
        {
            get => color;
            set
            {
                color = value;
                foreach (var c in materialColorerList) c.SetColor(value);
            }
        }
    }
}