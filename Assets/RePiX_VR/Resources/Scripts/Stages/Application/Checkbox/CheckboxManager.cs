﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace RePiX_VR.ApplicationStage
{
    [Obsolete("Use CheckboxPanel and MatrixManager instead.", true)]
    public class CheckboxManager : MonoBehaviour
    {
        public GameObject nothingSelectedPanel;
        public GameObject xAxisPanel;
        public GameObject yAxisPanel;
        public GameObject zAxisPanel;

        [FormerlySerializedAs("matrixFloat")] public TransformationMatrix transformationMatrix;
        [HideInInspector] public Axis axisActivated = Axis.None;

        public UnityEvent<Axis> onChosen = new UnityEvent<Axis>();

        public void UpdatePanels()
        {
            DeselectPanels();
            switch (axisActivated)
            {
                case Axis.None:
                    transformationMatrix.UnitMatrix();
                    nothingSelectedPanel.SetActive(true);
                    break;
                case Axis.X:
                    xAxisPanel.SetActive(true);
                    break;
                case Axis.Y:
                    yAxisPanel.SetActive(true);
                    break;
                case Axis.Z:
                    zAxisPanel.SetActive(true);
                    break;
                case Axis.All:
                default:
                    break;
            }
        }

        private void DeselectPanels()
        {
            xAxisPanel.SetActive(false);
            yAxisPanel.SetActive(false);
            zAxisPanel.SetActive(false);
            nothingSelectedPanel.SetActive(false);
        }
        
        public void Choose(Axis axis)
        {
            if (axisActivated == axis)
            {
                axisActivated = Axis.None;
                UpdatePanels();
                return;
            }

            axisActivated = axis;
            UpdatePanels();

            var axisValue = (int)axis;

            if (axisValue < 1)
                return;
            
            onChosen?.Invoke(axis);
        }

        public void ChooseNone()
            => Choose(Axis.None);
        
        public void ChooseX()
            => Choose(Axis.X);

        public void ChooseY()
            => Choose(Axis.Y);

        public void ChooseZ()
            => Choose(Axis.Z);
    }
}