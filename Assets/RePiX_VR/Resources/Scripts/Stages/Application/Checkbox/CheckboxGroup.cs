using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR.ApplicationStage {
    public class CheckboxGroup : MonoBehaviour
    {
        public Checkbox checkboxXAxis;
        public Checkbox checkboxYAxis;
        public Checkbox checkboxZAxis;


        public UnityEvent<Checkbox, Axis> onChangedAxis = new UnityEvent<Checkbox, Axis>();
        // Start is called before the first frame update
        private void Start()
        {
            BindCheckedEvent(checkboxXAxis, Axis.X);
            BindCheckedEvent(checkboxYAxis, Axis.Y);
            BindCheckedEvent(checkboxZAxis, Axis.Z);
        }

        private void BindCheckedEvent(Checkbox cb, Axis axis)
        {
            cb.onCheckedChanged.AddListener(isChecked =>
            {
                if (!isChecked)
                {
                    SetAxisState(cb, Axis.None);
                    return;
                }
                SetAxisState(cb, axis);
            });
        }

        public void SetAxisState(Checkbox sender, Axis axis)
        {
            var cbs = new Checkbox[] { checkboxXAxis, checkboxYAxis, checkboxZAxis };
            foreach (var cb in cbs)
            {
                if (cb == sender)
                    continue;
                cb.SetChecked(false);
            }
            onChangedAxis.Invoke(sender, axis);
        }
        
    }
}