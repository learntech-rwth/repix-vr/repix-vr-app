﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(Button), typeof(Image))]
    public class Checkbox : MonoBehaviour
    {
        public bool IsChecked => _checkedImage.gameObject.activeSelf;
        private bool _wasChecked;
        
        private Button _button;
        private Image _checkedImage;
        
        public UnityEvent<bool> onCheckedChanged = new UnityEvent<bool>();
        private void Awake()
        {
            _button = GetComponent<Button>();
            _checkedImage = GetComponentsInChildren<Image>(true)[1];
            
            _button.onClick.AddListener(ToggleCheck);
            _wasChecked = IsChecked;
        }

        public void SetChecked(bool isChecked) => _checkedImage.gameObject.SetActive(isChecked);

        public void ToggleCheck()
        {
            SetChecked(!IsChecked);
            UpdateCheckedState();
        }
        public void DoCheck(bool isChecked)
        {
            SetChecked(isChecked);
            UpdateCheckedState();
        }

        private void UpdateCheckedState()
        {
            if (_wasChecked != IsChecked)
            {
                onCheckedChanged.Invoke(IsChecked);
                _wasChecked = IsChecked;
            }
            ChangeCheckedColor();
        }

        private void ChangeCheckedColor()
        {
            var color = IsChecked ? _button.colors.selectedColor : _button.colors.normalColor;
            _checkedImage.color = color;
            _checkedImage.gameObject.SetActive(IsChecked);
        }
    }
}