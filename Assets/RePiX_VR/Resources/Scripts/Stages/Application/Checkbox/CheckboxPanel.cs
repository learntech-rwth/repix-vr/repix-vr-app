using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    public class CheckboxPanel : MonoBehaviour
    {
        public Axis selectedAxis = Axis.None;
        public Slider slider;
        public Image panelBackground;
        public Image panelBackgroundSmall;

        public UnityEvent<Axis> onChangedAxis = new UnityEvent<Axis>();
        private ResetMatrix _resetMatrix;
        private void Start()
        {
            var cbGroup = GetComponentInChildren<CheckboxGroup>();
            if (!cbGroup) // Don't initialize if Matrix is Scaling Matrix
                return;
            
            cbGroup.onChangedAxis.AddListener((cb, axis) => ChooseAxis(axis));
            cbGroup.SetAxisState(null, selectedAxis);

            _resetMatrix = GetComponentInChildren<ResetMatrix>();
        }

        public void ChooseAxis(Axis axis)
        {
            selectedAxis = axis;
            var isSmallPanel = axis == Axis.None;
            slider.gameObject.SetActive(!isSmallPanel);
            panelBackgroundSmall.gameObject.SetActive(isSmallPanel);
            panelBackground.gameObject.SetActive(!isSmallPanel);
            if (_resetMatrix)
                _resetMatrix.DoReset();
            onChangedAxis.Invoke(axis);
        }
    }

}