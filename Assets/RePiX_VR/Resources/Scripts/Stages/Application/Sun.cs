﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class Sun : Planet
    {
        public override Vector3 TRSMatrixPosition =>
            new Vector3(5 * Mathf.Cos(7 * 0.15f), -0.1f, -5 * Mathf.Sin(7 * 0.15f));
    }
}