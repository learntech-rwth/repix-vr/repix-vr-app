using OmiLAXR.Helpers.Togglers;
using OmiLAXR.Path;
using UnityEngine;
using UnityEngine.Playables;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(OnEnableDisableController))]
    public class CubeMatrixPreview : MonoBehaviour
    {
        public RotationMatrixHighlight rotationMatrixHighlight;
        public ScaleMatrixHighlight scaleMatrixHighlight;
        public PlayableDirector transformedCubeDirector;

        private void Awake()
        {
            var onEnableDisableController = GetComponent<OnEnableDisableController>();
            
            onEnableDisableController.onEnabled.AddListener(() =>
            {
                transformedCubeDirector.Play();
                transformedCubeDirector.GetComponent<LocalPositionPathDriver>().Play();
                rotationMatrixHighlight.ResetColors();
                scaleMatrixHighlight.ResetColors();
            });
            onEnableDisableController.onDisabled.AddListener(() =>
            {
                transformedCubeDirector.Stop();

                var pathDrivers = new PathDriver[]
                {
                    transformedCubeDirector.GetComponent<LocalPositionPathDriver>(),
                    transformedCubeDirector.GetComponent<LocalEulerAnglesPathDriver>(),
                    transformedCubeDirector.GetComponent<LocalScalePathDriver>()
                };

                foreach (var pd in pathDrivers)
                {
                    pd.SetLoop(false);
                    pd.SetCalledEventCalls(0);
                    pd.Stop();
                }
            });
        }
    }

}