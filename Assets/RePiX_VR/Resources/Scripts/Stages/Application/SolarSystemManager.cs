using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    public class SolarSystemManager : MonoBehaviour
    {
        // Interaction menu
        //[FormerlySerializedAs("interactionMenu")] public SolarInteractionMenu solarInteractionMenu;
        //public SolarInteractionMenu interactionMenu2D;
        public GameObject nonVRControls;
        public GameObject matrixPlaceholder;
        public SolarSystem solarSystem;
        //private GameObject InteractionMenuPanel => solarInteractionMenu.gameObject.Find("Panel", true);

        [Header("TRS Matrices")] 
        public TRSMatrix trsMatrixSun;
        public TRSMatrix trsMatrixEarth;
        public TRSMatrix trsMatrixMoon;

        [Header("Scenario Objects")]
        public TranslationMatrix translationMatrix;
        public RotationMatrix rotationMatrix;

        public Moon moon;
        
        public GameObject moonMatrix;
        
        [Header("Matrix Targets")]
        public GameObject sunMatrixTarget;
        public GameObject earthMatrixTarget;
        public GameObject moonMatrixTarget;
        public GameObject sunTarget;
        public GameObject moonTarget;
        public GameObject rotationTarget;
        public GameObject scalingTarget;
        public GameObject translationTarget;

        [Header("Sliders")]
        public Slider xSliderTranslation;
        public Slider ySliderTranslation;
        public Slider zSliderTranslation;
        public Slider xSliderRotation;
        public Slider ySliderRotation;
        public Slider zSliderRotation;
        public Slider sliderScaling;

        [Header("Dropdowns")] 
        public Dropdown dropDownRotation;
        public Dropdown dropDownTranslation;
        
        private bool _storedDefaultValues;
        
        public void EnableInteraction()
        {
            //InteractionMenuPanel.SetActive(true);
            matrixPlaceholder.SetActive(true);
            //interactionMenu2D.gameObject.SetActive(true);
            solarSystem.gameObject.SetActive(true);
        }

        public void AppearSunAndEarth()
        {
            trsMatrixSun.gameObject.SetActive(true);
            trsMatrixEarth.gameObject.SetActive(true);
        }

        private void Awake()
        {
            StoreDefaultValues();
        }

        private void StoreDefaultValues()
        {
            if (_storedDefaultValues)
                return;
            
            // Todo reset

            _storedDefaultValues = true;
        }

        public void SetEarthInitialized(bool value)
        {
        }
        
        private void InitializeScenario()
        {
            var solarSystemTransform = solarSystem.transform;
            
            /*var sunTransform = sun.transform;
            sunTransform.SetParent(solarSystemTransform);
            sunTransform.position = sun.TransformStateHolder.SavedState.Position + sun.appliedMatrices.transformationsAdded;
            sunTransform.localScale = sun.TransformStateHolder.SavedState.LocalScale;
            sunTransform.rotation = Quaternion.identity;

            var moonTransform = moon.transform;
            moonTransform.SetParent(solarSystemTransform);
            moonTransform.position = moon.TransformStateHolder.SavedState.Position + moon.appliedMatrices.transformationsAdded;
            moonTransform.localScale = moon.TransformStateHolder.SavedState.LocalScale;
            moonTransform.rotation = Quaternion.identity;
            
            var earthTransform = earth.transform;
            earthTransform.SetParent(solarSystemTransform);
            earthTransform.position = earth.TransformStateHolder.SavedState.Position + earth.appliedMatrices.transformationsAdded;
            earthTransform.localScale = earth.TransformStateHolder.SavedState.LocalScale;
            earthTransform.rotation = Quaternion.identity;*/

            //translationMatrix.transform.localScale = translationMatrix.TransformStateHolder.SavedState.LocalScale;
            //rotationMatrix.transform.localScale = rotationMatrix.TransformStateHolder.SavedState.LocalScale;
            //scalingMatrix.transform.localScale = scalingMatrix.TransformStateHolder.SavedState.LocalScale;

            //sunRotationCenter.transform.rotation = Quaternion.identity;
            //moonRotationCenter.transform.rotation = Quaternion.identity;
            //earthRotationCenter.transform.rotation = Quaternion.identity;

            //sun.appliedMatrices.Clear();
            //moon.appliedMatrices.Clear();
            //earth.appliedMatrices.Clear();
        }

        public void ReloadScenario()
        {
            translationMatrix.GetComponentInParent<MatrixManager>(true).checkboxPanel.ChooseAxis(Axis.None);
            rotationMatrix.GetComponentInParent<MatrixManager>(true).checkboxPanel.ChooseAxis(Axis.None);
            
            sunMatrixTarget.SetActive(false);
            earthMatrixTarget.SetActive(false);
            moonMatrixTarget.SetActive(false);
            sunTarget.SetActive(false);
            moonTarget.SetActive(false);
            rotationTarget.SetActive(false);
            scalingTarget.SetActive(false);
            translationTarget.SetActive(false);

            // Sliders in 2D
            xSliderTranslation.value = 0;
            ySliderTranslation.value = 0;
            zSliderTranslation.value = 0;
            xSliderRotation.value = 0;
            ySliderRotation.value = 0;
            zSliderRotation.value = 0;

            sliderScaling.value = 1;

            // Dropdowns in 2D
            dropDownRotation.SetValueWithoutNotify(0);
            dropDownTranslation.SetValueWithoutNotify(0);

            // Disable moon
            moon.gameObject.SetActive(false);
            trsMatrixMoon.gameObject.SetActive(false);
            moonMatrix.SetActive(false);

            InitializeScenario();
        }
    }

}