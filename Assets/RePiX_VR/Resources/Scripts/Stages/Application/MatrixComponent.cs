﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(TransformationMatrix))]
    [DefaultExecutionOrder(0)]
    public class MatrixComponent : MonoBehaviour
    {
        public const float UpdateFps = 1 / 10.0f;
        // 0th Row
        public TextMeshProUGUI m00 { get; private set; }
        public TextMeshProUGUI m01 { get; private set; }
        public TextMeshProUGUI m02 { get; private set; }
        public TextMeshProUGUI m03 { get; private set; }
        // 1st Row
        public TextMeshProUGUI m10 { get; private set; }
        public TextMeshProUGUI m11 { get; private set; }
        public TextMeshProUGUI m12 { get; private set; }
        public TextMeshProUGUI m13 { get; private set; }
        // 2nd Row
        public TextMeshProUGUI m20 { get; private set; }
        public TextMeshProUGUI m21 { get; private set; }
        public TextMeshProUGUI m22 { get; private set; }
        public TextMeshProUGUI m23 { get; private set; }
        // 3rd Row
        public TextMeshProUGUI m30 { get; private set; }
        public TextMeshProUGUI m31 { get; private set; }
        public TextMeshProUGUI m32 { get; private set; }
        public TextMeshProUGUI m33 { get; private set; }

        public List<TextMeshProUGUI> values { get; private set; }

        private float _elapsedTime;

        public TransformationMatrix transformationMatrix;

        /// <summary>
        /// Get TextMesh Object by name.
        /// </summary>
        /// <param name="objectName">Valid name "m[0-3][0-3]".</param>
        /// <returns>TextMesh Object</returns>
        public TextMeshProUGUI GetTextObject(string objectName)
        {
            // If has not correct format
            if (objectName.Length != 3 || objectName[0] != 'm')
                throw new FormatException(objectName + " has not correct format.");
            // Convert to row and column
            var row = objectName[1] - '0';
            var column = objectName[2] - '0';
            // Return correct object
            return GetTextObject(row, column);
        }

        public TextMeshProUGUI GetTextObject(int row, int column) => values[column + row * 4];

        private bool _isDirty = true;
        
        private void Start()
        {
            // 0th Row
            m00 = transform.Find("m00").transform.GetComponent<TextMeshProUGUI>();
            m01 = transform.Find("m01").transform.GetComponent<TextMeshProUGUI>();
            m02 = transform.Find("m02").transform.GetComponent<TextMeshProUGUI>();
            m03 = transform.Find("m03").transform.GetComponent<TextMeshProUGUI>();
            // 1st Row
            m10 = transform.Find("m10").transform.GetComponent<TextMeshProUGUI>();
            m11 = transform.Find("m11").transform.GetComponent<TextMeshProUGUI>();
            m12 = transform.Find("m12").transform.GetComponent<TextMeshProUGUI>();
            m13 = transform.Find("m13").transform.GetComponent<TextMeshProUGUI>();
            // 2nd Row
            m20 = transform.Find("m20").transform.GetComponent<TextMeshProUGUI>();
            m21 = transform.Find("m21").transform.GetComponent<TextMeshProUGUI>();
            m22 = transform.Find("m22").transform.GetComponent<TextMeshProUGUI>();
            m23 = transform.Find("m23").transform.GetComponent<TextMeshProUGUI>();
            // 3rd Row
            m30 = transform.Find("m30").transform.GetComponent<TextMeshProUGUI>();
            m31 = transform.Find("m31").transform.GetComponent<TextMeshProUGUI>();
            m32 = transform.Find("m32").transform.GetComponent<TextMeshProUGUI>();
            m33 = transform.Find("m33").transform.GetComponent<TextMeshProUGUI>();
            
            if (!transformationMatrix)
                transformationMatrix = GetComponent<TransformationMatrix>();

            transformationMatrix.OnChanged += _ =>
            {
                _isDirty = true;
            };
            
            values = new List<TextMeshProUGUI>
            {
                m00, m01, m02, m03,
                m10, m11, m12, m13,
                m20, m21, m22, m23,
                m30, m31, m32, m33,
            };

        }

        private void FixedUpdate()
        {
            _elapsedTime += Time.deltaTime;

            if (!_isDirty || _elapsedTime < UpdateFps) 
                return;
            
            RefreshLabels();
            _elapsedTime = 0f;
            _isDirty = false;
        }

        private void RefreshLabels()
        {
            var m = transformationMatrix.matrix;
            // 0th row
            m00.text = m.m00.ToString("0.0");
            m01.text = m.m01.ToString("0.0");
            m02.text = m.m02.ToString("0.0");
            m03.text = m.m03.ToString("0.0");
            // 1st row
            m10.text = m.m10.ToString("0.0");
            m11.text = m.m11.ToString("0.0");
            m12.text = m.m12.ToString("0.0");
            m13.text = m.m13.ToString("0.0");
            // 2nd row
            m20.text = m.m20.ToString("0.0");
            m21.text = m.m21.ToString("0.0");
            m22.text = m.m22.ToString("0.0");
            m23.text = m.m23.ToString("0.0");
            // 3rd row
            m30.text = m.m30.ToString("0.0");
            m31.text = m.m31.ToString("0.0");
            m32.text = m.m32.ToString("0.0");
            m33.text = m.m33.ToString("0.0");
        }
    }

}