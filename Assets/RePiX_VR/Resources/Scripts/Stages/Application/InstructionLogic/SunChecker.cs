﻿using System;
using UnityEngine;

namespace RePiX_VR.ApplicationStage.InstructionLogic
{
    [RequireComponent(typeof(Sun))]
    public class SunChecker : PlanetChecker
    {
        private void FixedUpdate()
        {
            OnCheckRotation(null);
        }
    }
}