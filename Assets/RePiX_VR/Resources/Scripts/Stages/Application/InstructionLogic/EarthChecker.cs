﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR.ApplicationStage.InstructionLogic
{
    [RequireComponent(typeof(Earth))]
    public class EarthChecker : PlanetChecker
    {
        public UnityEvent onCorrectSunEarthRatio = new UnityEvent();
        public UnityEvent onIncorrectSunEarthRatio = new UnityEvent();
        private bool _hadCorrectScale;

        public UnityEvent onCorrectEarthRotationAndTranslation = new UnityEvent();
        public UnityEvent onIncorrectEarthRotationAndTranslation = new UnityEvent();
        private bool _wasCorrectTranslation;

        public UnityEvent onCorrectEarthFirstRotation = new UnityEvent();
        public UnityEvent onIncorrectEarthFirstRotation = new UnityEvent();
        private bool _hadCorrectRotation1;

        public UnityEvent onCorrectEarthSecondRotation = new UnityEvent();
        public UnityEvent onIncorrectEarthSecondRotation = new UnityEvent();
        private bool _hadCorrectRotation2;

        public UnityEvent onCorrectEarthTranslation = new UnityEvent();
        public UnityEvent onIncorrectEarthTranslation = new UnityEvent();
        private bool _hadCorrectTranslation;


        private Sun _sun;
        private Earth _earth;

        private bool _wasCorrectlyRotatedAndTranslated = false;

        protected override void Start()
        {
            base.Start();

            _earth = (Earth) planet;
            _sun = FindObjectOfType<Sun>();

            _earth.trsMatrix.onAppliedMatrix.AddListener(_ => CheckEarthForRotationsAndTranslation());
            _earth.trsMatrix.onRemovedMatrix.AddListener(_ => CheckEarthForRotationsAndTranslation());


            _sun.onChangedScaling.AddListener(_ => CheckSunEarthRatio());
            _earth.onChangedScaling.AddListener(_ => CheckSunEarthRatio());

            _earth.onChangedRotation.AddListener(_ => CheckEarthForRotationsAndTranslation());
            _earth.onChangedTranslation.AddListener(_ => CheckEarthForRotationsAndTranslation());
        }

        protected void FixedUpdate()
        {
            OnCheckRotation(null);
            CheckEarthForRotationsAndTranslation();
            CheckSunEarthRatio();
        }

        private void CheckSunEarthRatio()
        {
            var sunScale = _sun.trsMatrix.ComputedGlobalMatrix.GetScale();
            var earthScale = _earth.trsMatrix.ComputedGlobalMatrix.GetScale();

            if (earthScale.x <= 0.0f)
                return;

            var ratio = sunScale.x / earthScale.x;

            CompareToOldAndFire(ratio >= 4.0f, ref _hadCorrectScale,
                onCorrectSunEarthRatio, onIncorrectSunEarthRatio);
        }

        private bool RotationChecker(TransformationMatrix matrix)
        {
            var rotMatrix = matrix as RotationMatrix;
            if (!rotMatrix)
                return false;
            var rotation = Mathf.Abs(rotMatrix.RotationSpeedX)
                           + Mathf.Abs(rotMatrix.RotationSpeedY)
                           + Mathf.Abs(rotMatrix.RotationSpeedZ);
            return rotation > 0;
        }

        private bool TranslationChecker(TransformationMatrix matrix)
        {
            var correct = _earth.trsMatrix.ComputedLocalMatrix.GetPosition().magnitude >= 1f - 0.05f;
            return correct;
        }

        private void CheckEarthForRotationsAndTranslation()
        {
            var matrices = new Queue<TransformationMatrix>(TransformationsMatrices);

            var flags = new bool[3];

            flags[0] = CheckNextMatricesFor<RotationMatrix>(ref matrices, RotationChecker);
            flags[1] = CheckNextMatricesFor<TranslationMatrix>(ref matrices, TranslationChecker);
            flags[2] = CheckNextMatricesFor<RotationMatrix>(ref matrices, RotationChecker);


            CompareToOldAndFire(flags[0], ref _hadCorrectRotation1,
                onCorrectEarthFirstRotation, onIncorrectEarthFirstRotation);
            CompareToOldAndFire(flags[1], ref _hadCorrectTranslation,
                onCorrectEarthTranslation, onIncorrectEarthTranslation);
            CompareToOldAndFire(flags[2], ref _hadCorrectRotation2,
                onCorrectEarthSecondRotation, onIncorrectEarthSecondRotation);
            CompareToOldAndFire(flags.All(b => b), ref _wasCorrectlyRotatedAndTranslated,
                onCorrectEarthRotationAndTranslation, onIncorrectEarthRotationAndTranslation);
        }
    }
}