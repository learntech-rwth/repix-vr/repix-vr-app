﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR.ApplicationStage.InstructionLogic
{
    [RequireComponent(typeof(Moon))]
    public class MoonChecker : PlanetChecker
    {
        public UnityEvent onCorrectMoon = new UnityEvent();
        public UnityEvent onIncorrectMoon = new UnityEvent();

        public UnityEvent onCorrectMoonRotation = new UnityEvent();
        public UnityEvent onIncorrectMoonRotation = new UnityEvent();
        public UnityEvent onCorrectMoonTranslation = new UnityEvent();
        public UnityEvent onIncorrectMoonTranslation = new UnityEvent();
        public UnityEvent onCorrectMoonRatio = new UnityEvent();
        public UnityEvent onIncorrectMoonRatio = new UnityEvent();


        private bool _hadCorrectRotation;
        private bool _hadCorrectTranslation;
        private bool _hadCorrectRatio;
        private bool _wasCorrectMoon;


        private Moon _moon;
        private Earth _earth;

        protected override void Start()
        {
            base.Start();
            _moon = GetComponent<Moon>();
            _earth = FindObjectOfType<Earth>();
        }

        protected void FixedUpdate()
        {
            CheckCorrectMoon();
        }

        private bool CheckRatio()
        {
            var earthScale = _earth.trsMatrix.ComputedGlobalMatrix.GetScale();
            var moonScale = _moon.trsMatrix.ComputedGlobalMatrix.GetScale();
            var ratio = earthScale.x / moonScale.x;

            return ratio >= 2.0f;
        }


        private bool RotationChecker(TransformationMatrix matrix)
        {
            var rotMatrix = matrix as RotationMatrix;
            if (!rotMatrix)
                return false;
            var rotation = Mathf.Abs(rotMatrix.RotationSpeedX)
                           + Mathf.Abs(rotMatrix.RotationSpeedY)
                           + Mathf.Abs(rotMatrix.RotationSpeedZ);
            return rotation > 0;
        }

        private bool TranslationChecker(TransformationMatrix matrix)
        {
            return _moon.trsMatrix.ComputedLocalMatrix.GetPosition().magnitude >= 0.9f - 0.05f;
        }


        private void CheckCorrectMoon()
        {
            var matrices = new Queue<TransformationMatrix>(TransformationsMatrices);
            var flags = new bool[3];

            flags[0] = CheckNextMatricesFor<RotationMatrix>(ref matrices, RotationChecker);
            flags[1] = CheckNextMatricesFor<TranslationMatrix>(ref matrices, TranslationChecker);
            flags[2] = CheckRatio();


            CompareToOldAndFire(flags[0], ref _hadCorrectRotation,
                onCorrectMoonRotation, onIncorrectMoonRotation);
            CompareToOldAndFire(flags[1], ref _hadCorrectTranslation,
                onCorrectMoonTranslation, onCorrectMoonTranslation);
            CompareToOldAndFire(flags[2], ref _hadCorrectRatio,
                onCorrectMoonRatio, onIncorrectMoonRatio);
            CompareToOldAndFire(flags.All(b => b), ref _wasCorrectMoon,
                onCorrectMoon, onIncorrectMoon);
        }
    }
}