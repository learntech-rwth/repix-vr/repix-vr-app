﻿using System;
using System.Collections.Generic;
using OmiLAXR.Interaction.DragAndDrop;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR.ApplicationStage.InstructionLogic
{
    [RequireComponent(typeof(Planet))]
    public abstract class PlanetChecker : MonoBehaviour
    {
        public Planet planet;

        public UnityEvent onPlacedInCoordinateSystem = new UnityEvent();
        public UnityEvent onRemovedFromCoordinateSystem = new UnityEvent();

        public UnityEvent onHasRotation = new UnityEvent();
        public UnityEvent onHasNoRotation = new UnityEvent();
        protected bool HadRotation;

        private bool _startChecking;

        protected readonly List<TransformationMatrix> TransformationsMatrices = new List<TransformationMatrix>();

        protected virtual void Start()
        {
            if (!planet)
                planet = GetComponent<Planet>();

            planet.onEndedDrag.AddListener(Dropped);
            planet.onAppliedRotation.AddListener(_ => OnAddedRotation());

            planet.trsMatrix.onAppliedMatrix.AddListener(matrix => TransformationsMatrices.Add(matrix));
            planet.trsMatrix.onRemovedMatrix.AddListener(matrix => TransformationsMatrices.Remove(matrix));
        }

        private void OnAddedRotation()
        {
            _startChecking = true;
        }

        protected void OnCheckRotation(TransformationMatrix arg0)
        {
            if (!_startChecking)
                return;

            var planetLocalMatrix = planet.trsMatrix.ComputedLocalMatrix;
            var rotation = planetLocalMatrix.rotation.eulerAngles;

            var rot = Mathf.Abs(rotation.x) + Mathf.Abs(rotation.y) + Mathf.Abs(rotation.z);
            var hasRot = rot > 0;

            if (hasRot && !HadRotation)
            {
                onHasRotation.Invoke();
            }
            else if (!hasRot && HadRotation)
            {
                onHasNoRotation.Invoke();
            }

            HadRotation = hasRot;
        }

        public List<TransformationMatrix> GetTransformationMatrices() =>
            new List<TransformationMatrix>(TransformationsMatrices);

        private void Dropped(Draggable arg0, DropTarget arg1)
        {
            if (planet.IsPlacedInCoordinateSystem)
                onPlacedInCoordinateSystem.Invoke();
            else onRemovedFromCoordinateSystem.Invoke();
        }

        protected bool CheckNextMatricesFor<T>(ref Queue<TransformationMatrix> matrices,
            Func<TransformationMatrix, bool> checker) where T : TransformationMatrix
        {
            var flag = false;
            while (matrices.Count > 0)
            {
                var matrix = matrices.Dequeue();
                if (matrix as ScalingMatrix)
                    continue;
                var typeMatrix = matrix as T;
                if (!typeMatrix)
                {
                    matrices.Clear();
                    break;
                }

                if (checker(typeMatrix))
                {
                    flag = true;
                    break;
                }
            }

            while (matrices.Count > 0)
            {
                var matrix = matrices.Peek();
                if (matrix as ScalingMatrix || matrix as T)
                {
                    matrices.Dequeue();
                }
                else
                {
                    break;
                }
            }

            return flag;
        }


        protected void CompareToOldAndFire(bool value, ref bool oldValue, UnityEvent onCorrect, UnityEvent onIncorrect)
        {
            if (value && !oldValue)
                onCorrect.Invoke();
            if (!value && oldValue)
                onIncorrect.Invoke();
            oldValue = value;
        }
    }
}