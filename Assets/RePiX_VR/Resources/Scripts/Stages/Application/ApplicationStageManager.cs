﻿

using Valve.VR.InteractionSystem;

namespace RePiX_VR.ApplicationStage
{
    public class ApplicationStageManager : RPStageManager
    {
        public MatrixManager translationMatrixManager;
        public MatrixManager rotationMatrixManager;
        public MatrixManager scalingMatrixManager;

        public SolarSystem solarSystem;
        public SolarSystemManager solarSystemManager;

        public TRSMatrix trsMatrixSun;
        public TRSMatrix trsMatrixEarth;
        public TRSMatrix trsMatrixMoon;

        public AppliedMatricesHolder appliedMatricesHolder;

        //public SolarInteractionMenu interactionMenuNonVR;
        //public SolarInteractionMenu interactionMenu;

        public CubeMatrixPreview cubeMatrixPreview;
        
        protected override void Start()
        {
            onReset.AddListener(ResetSliders);
            onReset.AddListener(ResetCheckboxPanels);
            
            translationMatrixManager.onChangedAxis.AddListener(_ => 
                playPauseManager.playPauseDirector.DoneAction("learned_translation_dropdown"));
            
            base.Start();
        }

        public void ReloadScenario()
        {
            solarSystemManager.gameObject.SetActive(true);
            solarSystemManager.EnableInteraction();
            //solarSystemManager.ReloadScenario();
            //solarSystemManager.AppearSunAndEarth();
        }

        public void KillMiniScene()
        {
            solarSystem.gameObject.SetActive(false);
            trsMatrixSun.gameObject.SetActive(false);
            trsMatrixEarth.gameObject.SetActive(false);
            appliedMatricesHolder.gameObject.SetActive(false);
            //interactionMenuNonVR.gameObject.SetActive(false);
            //interactionMenu.gameObject.GetChild("Panel").SetActive(false);
            cubeMatrixPreview.gameObject.SetActive(false);
        }
        
        private void ResetSliders()
        {
            //var sliders = GetComponentsInChildren<SliderInteraction>(includeInactive: true);
            //sliders.ForEach(s => s.ResetValue());
            var resetMatrix = GetComponentsInChildren<ResetMatrix>();
            resetMatrix.ForEach(rm => rm.DoReset());
        }

        private void ResetCheckboxPanels()
        {
            var cb = GetComponentsInChildren<Checkbox>(includeInactive: false);
            cb.ForEach(cbp => cbp.SetChecked(false));
            /*var initialPanels = GetComponentsInChildren<InitialCheckboxPanel>(includeInactive: true);
            initialPanels.ForEach(icp => icp.gameObject.SetActive(true));*/
        }
    }
}