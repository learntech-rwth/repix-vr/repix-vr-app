﻿using OmiLAXR.Interaction;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(PointableUI), typeof(Slider))]
    public class MatrixSlider : MonoBehaviour
    {
        public Slider slider;

        protected TransformationMatrix TransformationMatrix => matrixManager.transformationMatrix;
        public MatrixManager matrixManager;

        public Axis changingAxis = Axis.All;

        public TextMeshProUGUI minText;
        public TextMeshProUGUI maxText;
        public TextMeshProUGUI valueText;

        public Slider.SliderEvent onValueChanged = new Slider.SliderEvent();

        public CheckboxPanel checkboxPanel;
        
        protected void Awake()
        {
            if (!matrixManager)
                matrixManager = GetComponentInParent<MatrixManager>();

            if (!slider)
                slider = GetComponent<Slider>();

            if (!checkboxPanel)
                checkboxPanel = GetComponentInParent<CheckboxPanel>();

            SetupLabels();
            slider.onValueChanged.AddListener(OnChangedValue);
            checkboxPanel.onChangedAxis.AddListener(OnChangedAxis);
            
            
        }

        public void SetValue(float value, bool withoutNotify = false)
        {
            if (withoutNotify)
                slider.SetValueWithoutNotify(value);
            else
                slider.value = value;
        }

        private void SetupLabels()
        {
            if (!slider)
                return;
            if (minText)
                minText.text = $"{slider.minValue}";
            if (maxText)
                maxText.text = $"{slider.maxValue}";

            if (!valueText)
                return;

            var floatPrecisionSlider = GetComponent<PointableUI>().sliderPrecision;
            
            var precision = "0.0";
            for (var i = 0; i < floatPrecisionSlider - 1; i++)
                precision += "0";

            valueText.text = slider.value.ToString(precision);
            slider.onValueChanged.AddListener(value => valueText.text = value.ToString(precision));
        }

        private void OnChangedValue(float value)
        {
            TransformationMatrix.ApplyValue(changingAxis, value);
            TransformationMatrix.UpdateValues();
            onValueChanged?.Invoke(value);
        }

        private void OnChangedAxis(Axis axis)
        {
            changingAxis = axis;
        }
    }
}