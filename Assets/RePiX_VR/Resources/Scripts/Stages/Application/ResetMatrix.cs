using System;
using System.Collections.Generic;
using OmiLAXR.Interaction;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.UI.Slider;

namespace RePiX_VR.ApplicationStage
{
    public class ResetMatrix : MonoBehaviour
    {
        private Dictionary<Slider, float> _startValues = new Dictionary<Slider, float>();
        protected virtual IEnumerable<Slider> GetSliders() => _matrixManager.gameObject.GetComponentsInChildren<Slider>(true);
        private MatrixManager _matrixManager;
        protected virtual void Start()
        {
            GetComponent<Button>().onClick.AddListener(DoReset);
            _matrixManager = GetComponentInParent<MatrixManager>();
            SaveStartValues();
        }

        private void SaveStartValues()
        {
            foreach (var slider in GetSliders())
            {
                _startValues.Add(slider, slider.value);
            }
        }

        [Obsolete("Todo: Make protected and not accessable from outside?")]
        public virtual void DoReset()
        {
            if (!_matrixManager)
                return;
            
            _matrixManager.ResetValues();
            foreach (var slider in GetSliders())
            {
                slider.value = _startValues[slider];
            }
        }
    }
}