﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class CenterInCoordinateSystem : MonoBehaviour
    {
        public CoordinateSystem coordinateSystem;
        private void Start()
        {
            coordinateSystem = GetComponentInParent<CoordinateSystem>();

            var coneYPosition = coordinateSystem.coneY.transform.position;
            var coneXPosition = coordinateSystem.coneX.transform.position;
            
            transform.position = new Vector3(coneYPosition.x, coneXPosition.y, coneYPosition.z);
        }
    }
}
