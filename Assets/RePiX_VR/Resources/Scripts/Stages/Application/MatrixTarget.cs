using OmiLAXR.Interaction;
using OmiLAXR.Interaction.DragAndDrop;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(Pointable))]
    public abstract class MatrixTarget : MonoBehaviour
    {
        public UnityEvent<MatrixTarget> onSelected = new UnityEvent<MatrixTarget>();
        public TRSMatrix trsMatrix;

        private bool _onClickAllowed = false; //used for stopping adding matrices to planets if they are in their initial position

        public void setOnClickedAllowed(bool var1)
        {
            _onClickAllowed = var1;
        }

        private void Start()
        {
            var pointable = GetComponent<Pointable>();
            pointable.onClick.AddListener(_ => OnClickedMatrixTarget());
            //_onClickAllowed = false;
        }

        private void OnClickedMatrixTarget()
        {
            if (DropTarget.DraggingObject != null)
                return;
            if (!DraggableMatrix.DraggingMatrix || !_onClickAllowed)
                return;

            var matrixHolder = DraggableMatrix.DraggingMatrix.matrixManager;
            var copy = trsMatrix.CloneMatrixHolder(matrixHolder);
            trsMatrix.Append(copy);
            DraggableMatrix.DraggingMatrix.DropThis();
            trsMatrix.gameObject.SetActive(true);
        }
    }
}