﻿using OmiLAXR.Interaction.DragAndDrop;

namespace RePiX_VR.ApplicationStage
{
    public class MatrixPlaceholder : Placeholder
    {
        public override void DropHere()
        {
            if (!DraggableMatrix.DraggingMatrix || DraggableMatrix.DraggingMatrix.placeholder != this)
                return;
            
            DraggableMatrix.DraggingMatrix.DropThis();
        }

        protected override void DropHere(Draggable draggable)
        {
            
        }

        protected override void DragFromHere(Draggable draggable)
        {
        }
    }
}