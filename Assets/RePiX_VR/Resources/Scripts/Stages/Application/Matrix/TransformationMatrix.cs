﻿using System;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    [Serializable]
    public class TransformationMatrix : MonoBehaviour
    {
        private Matrix4x4 _prevMatrix;
        public Matrix4x4 matrix = Matrix4x4.identity;
        public event Action<TransformationMatrix> OnChanged;

        private TransformStateHolder _transformStateHolder;
        public TransformStateHolder TransformStateHolder => _transformStateHolder ??= new TransformStateHolder(transform);
        
        public void SaveState() => TransformStateHolder.SaveState();
        public void RestoreState() => TransformStateHolder.RestoreState();

        public static Matrix4x4 operator *(TransformationMatrix a, TransformationMatrix b)
            => a.matrix * b.matrix;
        public static Matrix4x4 operator *(Matrix4x4 a, TransformationMatrix b)
            => a * b.matrix;
        public static Matrix4x4 operator *(TransformationMatrix a, Matrix4x4 b)
            => a.matrix * b;
        public static Matrix4x4 Product(IEnumerable<TransformationMatrix> matrices)
            => matrices.Aggregate(Matrix4x4.identity, (current, m) => current * m);

        public virtual void ApplyValue(Axis axis, float value)
        {
        }
        
        public void UnitMatrix()
        {
            matrix = Matrix4x4.identity;
            UpdateValues();
        }

        public void UpdateValues()
        {
            if (_prevMatrix == matrix)
                return;
            
            OnChanged?.Invoke(this);
            _prevMatrix = matrix;
        }

        public void FireOnChanged() => OnChanged?.Invoke(this);

        private void Update()
            => UpdateValues();

        public virtual void ResetValues() => matrix = Matrix4x4.identity;

        public virtual Matrix4x4 ApplyingMatrix => matrix;

        public virtual void ApplyTm(TransformationMatrix tm)
        {
            matrix = tm.matrix;
        }
    }
}