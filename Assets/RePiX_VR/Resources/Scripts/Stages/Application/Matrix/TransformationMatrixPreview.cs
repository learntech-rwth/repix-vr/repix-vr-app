﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(MatrixComponent))]
    public class TransformationMatrixPreview : MonoBehaviour
    {
        [Tooltip("Transform those transformation matrix has to be reflected on panel.")]
        public Transform transformReference;

        private MatrixComponent _matrixComponent;

        private void Start()
        {
            _matrixComponent = GetComponent<MatrixComponent>();
        }

        private void Update()
        {
            if (!transformReference)
                return;

            _matrixComponent.transformationMatrix.matrix = Matrix4x4.TRS(transformReference.localPosition, transformReference.localRotation,
                transformReference.localScale);
        }
    }
}