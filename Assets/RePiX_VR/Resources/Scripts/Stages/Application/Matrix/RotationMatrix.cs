﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class RotationMatrix : TransformationMatrix
    {
        public float RotationSpeedX { get; private set; }
        public float RotationSpeedY { get; private set; }
        public float RotationSpeedZ { get; private set; }
        public float RotationAngle { get; private set; }

        public override void ApplyValue(Axis axis, float value)
        {
            switch (axis)
            {
                case Axis.All:
                    RotationSpeedX = 
                        RotationSpeedY = 
                            RotationSpeedZ = 
                                value;
                    break;
                case Axis.X:
                    RotationSpeedX = value;
                    RotationSpeedY = RotationSpeedZ = 0;
                    break;
                case Axis.Y:
                    RotationSpeedY = value;
                    RotationSpeedX = RotationSpeedZ = 0;
                    break;
                case Axis.Z:
                    RotationSpeedZ = value;
                    RotationSpeedX = RotationSpeedY = 0;
                    break;
                case Axis.None:
                    UnitMatrix();
                    break;
                default:
                    break;
            }
            UpdateValues();
        }

        private static Matrix4x4 GetRotXMatrix(float rad)
        {
            var rx = Matrix4x4.identity;
            rx.m11 = Mathf.Cos(rad);
            rx.m12 = -Mathf.Sin(rad);
            rx.m21 = Mathf.Sin(rad);
            rx.m22 = Mathf.Cos(rad);
            return rx;
        }

        private static Matrix4x4 GetRotYMatrix(float rad)
        {
            var ry = Matrix4x4.identity;
            ry.m00 = Mathf.Cos(rad);
            ry.m02 = Mathf.Sin(rad);
            ry.m20 = -Mathf.Sin(rad);
            ry.m22 = Mathf.Cos(rad);

            return ry;
        }

        private static Matrix4x4 GetRotZMatrix(float rad)
        {
            var rz = Matrix4x4.identity;
            rz.m00 = Mathf.Cos(rad);
            rz.m01 = -Mathf.Sin(rad);
            rz.m10 = Mathf.Sin(rad);
            rz.m11 = Mathf.Cos(rad);

            return rz;
        }
        
        private void Update()
        {
            RotationAngle += 5f * Time.deltaTime;

            if (RotationAngle >= 360.0f)
                RotationAngle = 0.0f;
            
            matrix = Matrix4x4.Rotate(
                Quaternion.Euler(
                    x: RotationSpeedX * RotationAngle, 
                    y: RotationSpeedY * RotationAngle,
                    z: RotationSpeedZ * RotationAngle
                ));
            
            FireOnChanged();
        }

        public override void ApplyTm(TransformationMatrix tm)
        {
            base.ApplyTm(tm);
            var rot = (RotationMatrix)tm;
            RotationSpeedX = rot.RotationSpeedX;
            RotationSpeedY = rot.RotationSpeedY;
            RotationSpeedZ = rot.RotationSpeedZ;
        }
    }
}