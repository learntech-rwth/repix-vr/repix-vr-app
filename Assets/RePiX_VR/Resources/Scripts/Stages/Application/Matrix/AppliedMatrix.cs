using OmiLAXR.Interaction;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class AppliedMatrix : MonoBehaviour
    {
        private MatrixManager _matrixManager;
        public TRSMatrix trsMatrix;
        private void Start()
        {
            GetComponent<Pointable>().onClick.AddListener(_ => OnClick());
            _matrixManager = GetComponentInParent<MatrixManager>();
        }

        private void OnClick()
        {
            //if (DropTarget.DraggingObject != null)
            //    return;
            //if (!SolarInteractionMenu.IsDeletingMatrix)
            //    return;
            trsMatrix.Remove(_matrixManager);
            Destroy(_matrixManager.gameObject);
        }
    }
}