﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR.ApplicationStage
{
    public class TRSMatrix : MonoBehaviour
    {
        private const float FPS = 1 / 30.0f;
        private const float MatrixScale = 0.8f;
        private readonly List<MatrixManager> _appliedMatrices = new List<MatrixManager>();

        private TransformationMatrix _transformationMatrix;
        public event Action<Matrix4x4> OnUpdatedMatrix;
        public UnityEvent<TransformationMatrix> onAppliedMatrix = new UnityEvent<TransformationMatrix>();
        public UnityEvent<TransformationMatrix> onRemovedMatrix = new UnityEvent<TransformationMatrix>();
        public UnityEvent<TransformationMatrix> onChangedMatrix = new UnityEvent<TransformationMatrix>();

        public TRSMatrix parentMatrix;
        public Matrix4x4 ComputedGlobalMatrix { get; private set; }
        public Matrix4x4 ComputedLocalMatrix { get; private set; }
        
        private float _elapsedSeconds;

        private void Start()
        {
            _transformationMatrix = GetComponentInChildren<TransformationMatrix>();
            _transformationMatrix.OnChanged += matrix => onChangedMatrix.Invoke(matrix);
            ComputedGlobalMatrix = Matrix4x4.identity;
            ComputedLocalMatrix = Matrix4x4.identity;
        }

        public void ClearMatrices()
        {
            foreach (var am in _appliedMatrices)
            {
                Destroy(am);
            }
            _appliedMatrices.Clear();
            UpdateComputedMatrix();
        }

        public T GetFirstMatrix<T>() where T : TransformationMatrix
            => GetMatrices<T>().FirstOrDefault();

        public T[] GetMatrices<T>() where T : TransformationMatrix
            => _appliedMatrices
                .Select(m => m.transformationMatrix)
                .Where(m => m.GetType() == typeof(T))
                .Select(m => (T)m)
                .ToArray();

        public MatrixManager CloneMatrixHolder(MatrixManager matrixManager)
        {
            var copy = Instantiate(matrixManager, null);

            var draggable = copy.GetComponentInChildren<DraggableMatrix>();

            // @todo: what happens if we remove this, can the matrices be adjusted afterwards?
            // var cbm = copy.GetComponentInChildren<CheckboxManager>();
            // if (cbm) 
            //     Destroy(cbm.gameObject);

            if (draggable)
            {
                draggable.gameObject.transform.GetChild(0).gameObject.SetActive(true);
                var amp = draggable.gameObject.transform.GetChild(0).gameObject.AddComponent<AppliedMatrix>();
                amp.trsMatrix = this;
                Destroy(draggable);    
            }

            copy.transformationMatrix.ApplyTm(matrixManager.transformationMatrix);
            copy.transformationMatrix.UpdateValues();
            return copy;
        }

        public void Append(MatrixManager matrixManager)
        {
            _appliedMatrices.Add(matrixManager);

            var t = matrixManager.transform;
            t.SetParent(transform);
            t.localEulerAngles = Vector3.zero;
            t.localScale = Vector3.one * MatrixScale;
            matrixManager.gameObject.SetActive(true);

            UpdateComputedMatrix();
            onAppliedMatrix.Invoke(matrixManager.transformationMatrix);
            
            RefreshMatricesPositions();
        }

        private void RefreshMatricesPositions()
        {
            _appliedMatrices.Aggregate(Vector3.zero, 
                (current, am) => 
                    am.transform.localPosition = current + new Vector3(0.6f, 0, 0));
        }

        public void Remove(MatrixManager matrixManager)
        {
            var index = _appliedMatrices.IndexOf(matrixManager);

            if (index < 0) 
                return;
            
            _appliedMatrices.RemoveAt(index);
            onRemovedMatrix.Invoke(matrixManager.transformationMatrix);
            UpdateComputedMatrix();
            RefreshMatricesPositions();
        }


        private void UpdateComputedMatrix()
        {
            var localMatrix = Matrix4x4.identity;
            for (var i = 0; i < _appliedMatrices.Count; i++)
            {
                var am = _appliedMatrices[i];
                var tm = am.transformationMatrix;
                localMatrix *= tm.ApplyingMatrix;
            }

            ComputedLocalMatrix = localMatrix;
            
            var globalParentMatrix = parentMatrix != null ? parentMatrix._transformationMatrix.ApplyingMatrix : Matrix4x4.identity;
            var m = _transformationMatrix.matrix = globalParentMatrix * localMatrix;
            
            ComputedGlobalMatrix = m;
            
            OnUpdatedMatrix?.Invoke(m);
        }

        private void FixedUpdate()
        {
            _elapsedSeconds += Time.fixedDeltaTime;

            if (_elapsedSeconds < FPS)
                return;

            UpdateComputedMatrix();
            
            _elapsedSeconds = 0.0f;
        }
    }
}