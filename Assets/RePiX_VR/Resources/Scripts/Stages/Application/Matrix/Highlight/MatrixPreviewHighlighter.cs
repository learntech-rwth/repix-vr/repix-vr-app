﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(MatrixComponent))]
    [DefaultExecutionOrder(1)]
    public class MatrixPreviewHighlighter : MonoBehaviour
    {
        public bool applyColorsOnStart;
        public Color defaultColor = Color.white;
        public Color highlightColor = Color.white;

        protected MatrixComponent MatrixComponent;

        private void Start()
        {
            MatrixComponent = GetComponent<MatrixComponent>();
            ResetColors();

            if (applyColorsOnStart)
                ApplyColors();
        }

        public virtual void ResetColors()
        {
            // Reset color of all m-values
            if (MatrixComponent && MatrixComponent.values != null)
                MatrixComponent.values.ForEach(m =>
                {
                    if (!m)
                        return;
                    m.color = defaultColor;
                });
        }

        public void ChangeColor(Color color, params string[] names)
        {
            foreach (var name in names)
            {
                var m = MatrixComponent.GetTextObject(name);
                m.color = color;
            }
        }

        public virtual void ApplyColors()
        {
            MatrixComponent.values.ForEach(m => m.color = highlightColor);
        }
    }

}