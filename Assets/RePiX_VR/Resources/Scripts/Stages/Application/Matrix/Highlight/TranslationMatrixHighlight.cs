﻿
namespace RePiX_VR.ApplicationStage
{
    public class TranslationMatrixHighlight : MatrixPreviewHighlighter
    {
        public override void ApplyColors()
        {
            var mc = MatrixComponent;
            mc.m03.color = mc.m13.color = mc.m23.color = highlightColor;
        }

        public override void ResetColors()
        {
            var mc = MatrixComponent;
            mc.m03.color = mc.m13.color = mc.m23.color = defaultColor;
        }

        public void TintX()
        {
            if (!MatrixComponent || !MatrixComponent.m03)
                return;
            MatrixComponent.m03.color = highlightColor;
        }
        public void TintY()
        {
            if (!MatrixComponent || !MatrixComponent.m13)
                return;
            MatrixComponent.m13.color = highlightColor;
        }
        public void TintZ()
        {
            if (!MatrixComponent || !MatrixComponent.m23)
                return;

            MatrixComponent.m23.color = highlightColor;
        }
    }
}
