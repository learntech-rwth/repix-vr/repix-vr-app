﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class RotationMatrixHighlight : MatrixPreviewHighlighter
    {
        public override void ApplyColors()
        {
            TintX();
            TintY();
            TintZ();
        }

        public void TintX()
        {
            var mc = MatrixComponent;
            mc.m11.color = mc.m12.color = mc.m21.color = mc.m22.color = highlightColor;
            mc.m00.color = Color.black;
        }

        public void TintY()
        {
            var mc = MatrixComponent;
            mc.m00.color = mc.m02.color = mc.m20.color = mc.m22.color = highlightColor;
            mc.m11.color = Color.black;
        }

        public void TintZ()
        {
            var mc = MatrixComponent;
            mc.m00.color = mc.m01.color = mc.m10.color = mc.m11.color = highlightColor;
            mc.m22.color = Color.black;
        }
    }

}