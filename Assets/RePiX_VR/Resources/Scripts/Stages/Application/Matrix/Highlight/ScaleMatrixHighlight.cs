﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class ScaleMatrixHighlight : MatrixPreviewHighlighter
    {
        public override void ApplyColors()
        {
            var mc = MatrixComponent;
            mc.m00.color = mc.m11.color = mc.m22.color = highlightColor;
        }

        public override void ResetColors()
        {
            if (!MatrixComponent)
                return;
            var mc = MatrixComponent;
            mc.m00.color = mc.m11.color = mc.m22.color = defaultColor;
        }

        public void TintX() => MatrixComponent.m00.color = highlightColor;
        public void TintY() => MatrixComponent.m11.color = highlightColor;
        public void TintZ() => MatrixComponent.m22.color = highlightColor;
    }

}