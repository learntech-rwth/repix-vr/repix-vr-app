﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class RotationMatrixPreview : MonoBehaviour
    {
        [Tooltip("Transform those rotation has to be reflected on panel.")]
        public Transform transformReference;

        private MatrixComponent _matrixComponent;

        private void Start()
        {
            _matrixComponent = GetComponent<MatrixComponent>();
        }

        private void Update()
        {
            if (!transformReference)
                return;

            _matrixComponent.transformationMatrix.matrix = Matrix4x4.Rotate(transformReference.localRotation);
        }
    }
}
