﻿using UnityEngine;
using Matrix4x4 = UnityEngine.Matrix4x4;

namespace RePiX_VR.ApplicationStage
{
    public class TranslationMatrix : TransformationMatrix
    {
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        public override void ResetValues()
        {
            matrix = Matrix4x4.identity;
        }
        
        public override void ApplyValue(Axis axis, float value)
        {
            matrix = Matrix4x4.identity;
            switch (axis)
            {
                case Axis.All:
                    X = Y = Z = value;
                    break;
                case Axis.X:
                    X = value;
                    break;
                case Axis.Y:
                    Y = value;
                    break;
                case Axis.Z:
                    Z = value;
                    break;
                case Axis.None:
                default:
                    break;
            }

            matrix = Matrix4x4.Translate(new Vector3(X, Y, Z));
            UpdateValues();
        }
    }
}