﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class ScalingMatrix : TransformationMatrix
    {
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        
        public override void ResetValues()
        {
            matrix = Matrix4x4.identity;
        }

        public override void ApplyValue(Axis axis, float value)
        {
            switch (axis)
            {
                case Axis.All:
                    X = Y = Z = value;
                    break;
                case Axis.X:
                    X = value;
                    break;
                case Axis.Y:
                    Y = value;
                    break;
                case Axis.Z:
                    Z = value;
                    break;
                case Axis.None:
                default:
                    break;
            }

            matrix = Matrix4x4.Scale(new Vector3(X, Y, Z));
            UpdateValues();
        }
        
      
    }
}