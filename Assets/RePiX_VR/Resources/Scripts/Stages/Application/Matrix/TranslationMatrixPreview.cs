﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class TranslationMatrixPreview : MonoBehaviour
    {
        [Tooltip("Transform those transition matrix has to be reflected on panel.")]
        public Transform transformReference;

        private MatrixComponent _matrixComponent;

        private void Start()
        {
            _matrixComponent = GetComponent<MatrixComponent>();
        }

        private void Update()
        {
            if (!transformReference)
                return;

            _matrixComponent.transformationMatrix.matrix = Matrix4x4.Translate(transformReference.localPosition);
        }
    }

}