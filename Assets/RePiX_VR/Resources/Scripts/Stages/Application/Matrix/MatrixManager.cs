using System;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR.ApplicationStage
{
    [Serializable]
    public class MatrixManager : MonoBehaviour
    {
        public TransformationMatrix transformationMatrix;
        public CheckboxPanel checkboxPanel;

        [InspectorName("Allow only one axis to change")]
        public bool onlyOneAxis;

        public UnityEvent<Axis> onChangedAxis = new UnityEvent<Axis>();
        public UnityEvent<Matrix4x4> onChangedMatrix = new UnityEvent<Matrix4x4>();

        public void FireOnChangedAxis(Axis axis) => onChangedAxis.Invoke(axis);

        private void Awake()
        {
            if (!transformationMatrix)
                transformationMatrix = GetComponentInChildren<TransformationMatrix>();
            
            transformationMatrix.OnChanged += TransformationMatrixChanged;
            
            if (!checkboxPanel)
                checkboxPanel = GetComponentInChildren<CheckboxPanel>();

            checkboxPanel.onChangedAxis.AddListener(axis =>
            {
                if (axis == Axis.None)
                    transformationMatrix.UnitMatrix();
            });
           
        }

        private void TransformationMatrixChanged(TransformationMatrix m)
            => onChangedMatrix?.Invoke(m.matrix);

        public void ChangeValue(Axis axis, float value)
        {
            transformationMatrix.ApplyValue(axis, value);
            transformationMatrix.UpdateValues();
        }
        
        public void ResetValues()
        {
            transformationMatrix.ResetValues();
            transformationMatrix.UpdateValues();
        }

    }
}
