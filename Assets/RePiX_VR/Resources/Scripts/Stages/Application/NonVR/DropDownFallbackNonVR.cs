﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR;
using OmiLAXR.UI;
using RePiX_VR.NonVrCanvas;
using Valve.VR.InteractionSystem;

namespace RePiX_VR.ApplicationStage
{
    public class DropDownFallbackNonVR : MonoBehaviour
    {
        public MatrixManager matrixManager;
        public Slider xSlider;
        public Slider ySlider;
        public Slider zSlider;

        public Toggle[] toggle;

        private Axis _currentAxis;
        //public Dropdown dropdown;
        //public TextValueUpdater textValueUpdater;

        public Dictionary<Axis, Slider> Sliders => new Dictionary<Axis, Slider>()
        {
            [Axis.All] = null,
            [Axis.None] = null,
            [Axis.X] = xSlider,
            [Axis.Y] = ySlider,
            [Axis.Z] = zSlider
        };

        private void Start()
        {
            //dropdown = gameObject.GetComponentInChildren<Dropdown>();
            //dropdown.onValueChanged.AddListener(_ => HandleInputData());
            if (toggle == null)
                toggle = gameObject.GetComponentsInChildren<Toggle>();
            toggle[0].onValueChanged.AddListener(_ => HandleInputData(Axis.X));
            toggle[1].onValueChanged.AddListener(_ => HandleInputData(Axis.Y));
            toggle[2].onValueChanged.AddListener(_ => HandleInputData(Axis.Z));
            //textValueUpdater = GetComponent<TextValueUpdater>();
            //textValueUpdater = GetComponent<TextValueUpdater>();

            xSlider.onValueChanged.AddListener(value => matrixManager.ChangeValue(Axis.X, value));
            ySlider.onValueChanged.AddListener(value => matrixManager.ChangeValue(Axis.Y, value));
            zSlider.onValueChanged.AddListener(value => matrixManager.ChangeValue(Axis.Z, value));
        }

        public void HandleInputData(Axis axis)
        {
            //var axis = (Axis)dropdown.value;
            DeselectSlider();
            if (_currentAxis == axis)
                _currentAxis = Axis.None;
            else 
                _currentAxis = axis;
            
            var sliders = Sliders;
            
            sliders.Where(s => s.Value).ForEach(entry => entry.Value.value = 0);
            
            var slider = sliders[axis];
            if (_currentAxis != Axis.None)
                ActivateSlider(slider);
            matrixManager.checkboxPanel.ChooseAxis(axis);
            matrixManager.FireOnChangedAxis(axis);
        }

        public void DeselectSlider()
        {
            xSlider.gameObject.SetActive(false);
            ySlider.gameObject.SetActive(false);
            zSlider.gameObject.SetActive(false);
        }

        public void ActivateSlider(Slider slider)
        {
            if (slider == null)
                return;
            slider.gameObject.SetActive(true);
            //textValueUpdater.UpdateText(slider.value);
        }

        public void Reset()
        {
            toggle[0].isOn = false;
            toggle[1].isOn = false;
            toggle[2].isOn = false;
            HandleInputData(_currentAxis);
        }
    }
}