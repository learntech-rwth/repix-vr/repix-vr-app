﻿using System.Net.Mime;
using OmiLAXR.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(Slider))]
    public class SliderFallbackNonVR : MonoBehaviour
    {
        [HideInInspector] public Slider slider;

        private TextValueUpdater _textValueUpdater;
        private TextMeshProUGUI min;
        private TextMeshProUGUI max;

        private void Start()
        {
            
            
            slider = GetComponent<Slider>();
            _textValueUpdater = GetComponentInChildren<TextValueUpdater>();

            slider.onValueChanged.AddListener(ValueChanged);

            if (Mathf.Abs(slider.maxValue - slider.minValue) < 0.0001f)
            {
                slider.maxValue += 1.0f;
            }

            min = GetComponentsInChildren<TextMeshProUGUI>()[1];
            max = GetComponentsInChildren<TextMeshProUGUI>()[2];
            min.text = slider.minValue.ToString();
            max.text = slider.maxValue.ToString();
        }

        private void ValueChanged(float v)
        {
            _textValueUpdater.UpdateText(v);
        }
    }
}