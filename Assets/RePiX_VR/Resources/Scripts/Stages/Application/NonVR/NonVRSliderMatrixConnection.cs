using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    public class NonVRSliderMatrixConnection : MonoBehaviour
    {
        public MatrixManager matrixManager;
        private Slider slider;

        private void Start()
        {
            slider = this.GetComponent<Slider>();
            slider.onValueChanged.AddListener(value => matrixManager.ChangeValue(Axis.All, value));
        }
    }
}
