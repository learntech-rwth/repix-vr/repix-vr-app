﻿using System.Collections;
using UnityEngine;

namespace RePiX_VR.ApplicationStage.old
{
    [RequireComponent(typeof(MatrixTransformation))]
    public class RotationMatrix : MonoBehaviour
    {
        [Tooltip("Axis of rotation")]
        public TransformationAxis axis;

        [Tooltip("Whether the rotation is a function of time.")]
        public bool animated = true;

        [Tooltip("Rotation speed around the given axis.")]
        public float speed = 90f;

        [Tooltip("Rotation angle represented by the matrix.")]
        public float angle = 0f;

        private MatrixTransformation matTrans;

        private bool started = false;

        private void Start()
        {
            matTrans = GetComponent<MatrixTransformation>();

            if (animated)
            {
                matTrans.Label = $"Rot<sub>{axis}</sub>( {speed}° · t )";
                StartCoroutine(Animate());
                started = true;
            }
            else
            {
                matTrans.Label = $"Rot<sub>{axis}</sub>( {angle}° )";
                matTrans.Rotate(axis, angle);
            }
        }

        private void OnEnable()
        {
            if (started) StartCoroutine(Animate());
        }

        private IEnumerator Animate()
        {
            while (true)
            {
                angle += Time.deltaTime * speed;
                matTrans.Rotate(axis, angle);
                yield return null;
            }
        }
    }
}