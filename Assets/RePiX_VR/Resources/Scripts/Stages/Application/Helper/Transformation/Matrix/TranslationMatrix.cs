﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR.ApplicationStage.old
{
    public class TranslationMatrix : MonoBehaviour
    {
        [SerializeField, Tooltip("Translation vector, maximum translation if animated")]
        public Vector3 amplitude;

        public bool animated = false;

        public float speed = 1f;

        private MatrixTransformation matTrans;

        private bool started = false;

        private void Start()
        {
            matTrans = GetComponent<MatrixTransformation>();

            var t = amplitude;
            var anim = animated ? $"( sin( {speed} * t ) )" : "";
            matTrans.Label = $"Pos<sub>( {t.x}, {t.y}, {t.z} )</sub>{anim}";

            if (animated)
            {
                started = true;
                StartCoroutine(Animate());
            }
            else
            {
                matTrans.Translate(amplitude);
            }
        }

        private void OnEnable()
        {
            if (started) StartCoroutine(Animate());
        }

        float t = 0f;

        private IEnumerator Animate()
        {
            while (true)
            {
                t += speed * Time.deltaTime;
                matTrans.Translate(Vector3.LerpUnclamped(Vector3.zero, amplitude, Mathf.Sin(t)));
            }
        }
    }
}