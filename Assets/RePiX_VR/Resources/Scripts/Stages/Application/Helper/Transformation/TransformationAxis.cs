﻿
namespace RePiX_VR.ApplicationStage
{
    public enum TransformationAxis
    {
        X, Y, Z
    }
}