﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR.ApplicationStage.old
{
    public class ScalingMatrix : MonoBehaviour
    {
        [Tooltip("Per-Axis scale")]
        public Vector3 scale = Vector3.one;

        public bool animated = false;

        public float speed = 1f;

        private MatrixTransformation matTrans;
        private bool started = false;

        private void Start()
        {
            matTrans = GetComponent<MatrixTransformation>();
            matTrans.Label = $"Scale<sub>( {scale.x}, {scale.y}, {scale.z} )</sub>";
            
            if (animated)
            {
                matTrans.Label += $"( sin( {speed} * t ) )";

                started = true;
                StartCoroutine(Animate());
            }
            else
            {
                matTrans.Scale(scale);
            }
        }

        private void OnEnable()
        {
            if (started) StartCoroutine(Animate());
        }

        float t = 0f;

        private IEnumerator Animate()
        {
            while (true)
            {
                t += Time.deltaTime * speed;
                matTrans.Scale(Vector3.Lerp(Vector3.one, scale, (1f + Mathf.Sin(t)) / 2));
            }
        }
    }
}