﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class MatrixConnectionHandler : MonoBehaviour
    {
        private static MatrixConnectionHandler _instance;

        private MatrixHandle _start, _end;

        /// <summary>
        /// Singleton reference to this class instance.
        /// </summary>
        public static MatrixConnectionHandler Instance
        {
            get => _instance;
            private set => _instance = value;
        }

        private void Awake()
        {
            if (_instance != null) Destroy(this);
            else _instance = this;
        }

        public void StartConnecting(MatrixHandle mh)
        {
            _start = mh;
        }

        public void HoverConnect(MatrixHandle mh)
        {
            _end = mh;
        }

        public void Unhover()
        {
            _end = null;
        }

        public void EndConnect()
        {
            // connect a left hand side node with a right hand side one
            if (_end != null && _start.IsLeft != _end.IsLeft)
            {
                if (_start.IsLeft) _start.Connect(_end);
                else _end.Connect(_start);
            }
            else
            {
                if (_start && _start.IsLeft) _start.Disconnect();

                // reset if connection is invalid
                _start = null; _end = null;
            }
        }
    }
}