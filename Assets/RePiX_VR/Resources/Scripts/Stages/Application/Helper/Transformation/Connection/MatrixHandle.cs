﻿using UnityEngine;
using System;
using OmiLAXR.Interaction;

namespace RePiX_VR.ApplicationStage
{
    public class MatrixHandle : MonoBehaviour
    {
        public Transform handlePos;
        private Transform otherPos;

        public bool IsLeft { get => lr != null; }

        private LineRenderer lr;

        [SerializeField, Tooltip("Reference to the parent matrix transformation.")]
        private MatrixTransformation mat;

        private void Awake()
        {
            lr = GetComponent<LineRenderer>();
            mat = transform.parent.GetComponent<MatrixTransformation>();
        }

        private void Start()
        {
            var mch = MatrixConnectionHandler.Instance;
            var p = GetComponent<Pointable>();

            p.onSelect.AddListener(_ => mch.StartConnecting(this));
            p.onUnselect.AddListener(_ => mch.EndConnect());
            p.onHover.AddListener(_ => mch.HoverConnect(this));
            p.onUnhover.AddListener(_ => mch.Unhover());
        }

        private void LateUpdate()
        {
            transform.position = handlePos.position;

            if (otherPos) UpdateLineRenderer();
        }

        /// <summary>
        /// Connect a left hand side to a right side handle.
        /// </summary>
        /// <param name="other">right side handle</param>
        public void Connect(MatrixHandle other)
        {
            if (!IsLeft) throw new ArgumentException("Can only connect left to right!");

            mat.predecessor = other.mat;
            lr.enabled = true;
            otherPos = other.transform;
            UpdateLineRenderer();
        }

        /// <summary>
        /// Disconnects a (left hand side) handle.
        /// </summary>
        public void Disconnect()
        {
            mat.predecessor = null;
            lr.enabled = false;
            otherPos = null;
        }

        private void UpdateLineRenderer()
        {
            lr.SetPosition(1, transform.position);
            lr.SetPosition(0, otherPos.position);
        }
    }
}