﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    /// <summary>
    /// Provides a method that returns the gameobject to it's parent in
    /// the hierarchy.
    /// </summary>
    
    public class Reparent : MonoBehaviour
    {
        private Transform trackedParent;

        private void Awake()
        {
            trackedParent = transform.parent;
        }

        public void ResetParent()
        {
            transform.parent = trackedParent;
        }
    }
}