﻿using System.Collections.Generic;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class ModelMatrix : MonoBehaviour
    {
        private MatrixTransformation matTrans;

        [SerializeField, Tooltip("Transform of the affected object.")]
        private Transform model;

        private void Start()
        {
            matTrans = GetComponent<MatrixTransformation>();
            matTrans.Label = $"M<sub>{model.gameObject.name}</sub>";
        }

        private void LateUpdate()
        {
            Matrix4x4 res = Matrix4x4.identity;
            MatrixTransformation next = matTrans.predecessor;

            // List of matrices, to ensure that references are not circular
            var matrices = new HashSet<MatrixTransformation>();
            matrices.Add(matTrans);

            while (next != null)
            {
                if (matrices.Contains(next))
                {
                    next.BreakConnection(); // TODO: Better notification of a loop?
                    return;
                }
                matrices.Add(next);
                res = next.Matrix * res;
                next = next.predecessor;
            }

            matTrans.Matrix = res;

            Vector3 t = res.GetColumn(3);
            Vector3 s = new Vector3(res.GetColumn(0).magnitude, res.GetColumn(1).magnitude, res.GetColumn(2).magnitude);
            
            res.SetColumn(3, new Vector4(0f, 0f, 0f, 1f));
            res.SetColumn(0, res.GetColumn(0) / s.x);
            res.SetColumn(1, res.GetColumn(1) / s.y);
            res.SetColumn(2, res.GetColumn(2) / s.z);
            
            model.transform.localPosition = t;
            model.transform.localScale = s;
            model.transform.localRotation = res.GetRotation();
        }
    }
}