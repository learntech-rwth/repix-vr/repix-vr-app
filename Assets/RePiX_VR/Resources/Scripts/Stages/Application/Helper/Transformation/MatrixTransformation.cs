﻿using UnityEngine;
using TMPro;

namespace RePiX_VR.ApplicationStage
{
    public class MatrixTransformation : MonoBehaviour
    {
        public GameObject matrixSource;
        public MatrixTransformation predecessor;

        private MatrixHandle _leftHandle, _rightHandle;

        [SerializeField, Tooltip("Text mesh to use as label for the matrix.")]
        private TextMeshProUGUI _label;

        private TransformationMatrix _transformationMatrix;

        /// <summary>
        /// Reference to whatever script decides the final matrix.
        /// </summary>
        public TransformationMatrix MatrixProvider
        {
            get
            {
                if (_transformationMatrix == null) 
                    _transformationMatrix = matrixSource.GetComponent<TransformationMatrix>();
                return _transformationMatrix;
            }
        }

        /// <summary>
        /// Reference to the underlying matrix object.
        /// </summary>
        public Matrix4x4 Matrix { get => _transformationMatrix.matrix; set => _transformationMatrix.matrix = value; }

        /// <summary>
        /// Reference to the matrix's label text.
        /// </summary>
        public string Label { get => _label.text; set => _label.text = value; }

        private void Awake()
        {
            _leftHandle = transform.Find("LeftHandSide").GetComponent<MatrixHandle>();
            _rightHandle = transform.Find("RightHandSide").GetComponent<MatrixHandle>();
        }

        private void Start()
        {
            if (predecessor) Connect(predecessor);
        }

        /// <summary>
        /// Connects left handle of this matrix to another matrixes right handle.
        /// </summary>
        /// <param name="other"></param>
        public void Connect(MatrixTransformation other) => _leftHandle.Connect(other._rightHandle);

        /// <summary>
        /// Disconnects the matrix from its predecessor.
        /// </summary>
        public void BreakConnection() => _leftHandle.Disconnect();

        /// <summary>
        /// Sets the current Matrix to be a rotation matrix.
        /// </summary>
        /// <param name="around">axis to rotate around</param>
        /// <param name="angle">amount of rotation in degrees</param>
        public void Rotate(TransformationAxis around, float angle)
        {
            var axis = Vector3.zero;

            switch (around)
            {
                case TransformationAxis.X:
                    axis = Vector3.right;
                    break;
                case TransformationAxis.Y:
                    axis = Vector3.up;
                    break;
                case TransformationAxis.Z:
                    axis = Vector3.forward;
                    break;
            }

            Matrix = Matrix4x4.Rotate(Quaternion.AngleAxis(angle, axis));
        }

        /// <summary>
        /// Sets the current Matrix to be a translaion matrix.
        /// </summary>
        /// <param name="v">offset vector</param>
        public void Translate(Vector3 v)
        {
            Matrix = Matrix4x4.Translate(v);
        }

        /// <summary>
        /// Sets the current matrix to be a scaling matrix.
        /// </summary>
        /// <param name="dims">scaling factor along dimensions</param>
        public void Scale(Vector3 dims)
        {
            Matrix = Matrix4x4.Scale(dims);
        }
    }
}