﻿using OmiLAXR;
using TMPro;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class MatrixRenderer : MonoBehaviour
    {
        public Matrix4x4 Matrix { get; set; } = Matrix4x4.identity;

        private TextMeshProUGUI tmp;

        private void Start()
        {
            tmp = GetComponent<TextMeshProUGUI>();
        }

        private string matToString => 
            "\tx\ty\tz\tw\n"
            + $"x\t{Matrix.m00:F2}\t{Matrix.m01:F2}\t{Matrix.m02:F2}\t{Matrix.m03:F2}\n"
            + $"y\t{Matrix.m10:F2}\t{Matrix.m11:F2}\t{Matrix.m12:F2}\t{Matrix.m13:F2}\n"
            + $"z\t{Matrix.m20:F2}\t{Matrix.m21:F2}\t{Matrix.m22:F2}\t{Matrix.m23:F2}\n"
            + $"w\t{Matrix.m30:F2}\t{Matrix.m31:F2}\t{Matrix.m32:F2}\t{Matrix.m33:F2}";

        private void Update()
        {
            tmp.text = matToString;
        }
    }
}