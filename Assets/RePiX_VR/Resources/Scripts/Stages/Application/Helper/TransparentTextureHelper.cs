﻿using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class TransparentTextureHelper : MonoBehaviour
    {
        public float opacity = 1.0f;
        private Material _material;
        private static readonly int Opacity = Shader.PropertyToID("_Opacity");

        private void Start()
        {
            _material = GetComponent<MeshRenderer>().material;
            SetOpacity(opacity);
        }

        public void SetOpacity(float opc) => _material.SetFloat(Opacity, opc);
    }
}
