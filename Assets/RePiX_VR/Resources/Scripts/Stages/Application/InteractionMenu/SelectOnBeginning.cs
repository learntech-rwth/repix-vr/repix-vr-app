﻿using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    public class SelectOnBeginning : MonoBehaviour
    {
        private Button _button;

        private void Start()
        {
            _button = gameObject.GetComponent<Button>();
            _button.Select();
        }
    }

}