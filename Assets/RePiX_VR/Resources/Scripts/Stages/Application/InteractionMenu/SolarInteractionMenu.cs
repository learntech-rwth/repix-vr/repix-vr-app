﻿using System;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR;
using OmiLAXR.xAPI.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    public class SolarInteractionMenu : MonoBehaviour
    {
        public enum InteractionMenuMode
        {
            None,
            Normal,
            MoveMatrix,
            DeleteMatrix,
            MovePlanets
        }
        
        public static InteractionMenuMode Mode = InteractionMenuMode.Normal;

        public static bool IsMovingMatrix => Mode == InteractionMenuMode.MoveMatrix;
        public static bool IsMovingPlanets => Mode == InteractionMenuMode.MovePlanets;
        public static bool IsNormalMode => Mode == InteractionMenuMode.Normal;
        public static bool IsDeletingMatrix => Mode == InteractionMenuMode.DeleteMatrix;

        public Button normalModeButton;
        public Button moveMatrixButton;
        public Button deleteMatrixButton;
        public Button movePlanetsButton;

        public static event Action<InteractionMenuMode> OnChangedMode;

        private void Start()
        {
            normalModeButton.onClick.AddListener(ActivateNormalMode);
            moveMatrixButton.onClick.AddListener(ActivateMoveMatrix);
            deleteMatrixButton.onClick.AddListener(ActivateDeleteMatrix);
            movePlanetsButton.onClick.AddListener(ActivateMovePlanets);
        }
        
        private Dictionary<InteractionMenuMode, Button> Buttons => new Dictionary<InteractionMenuMode, Button>() {
            [InteractionMenuMode.None] = null,
            [InteractionMenuMode.Normal] = normalModeButton,
            [InteractionMenuMode.MoveMatrix] = moveMatrixButton,
            [InteractionMenuMode.DeleteMatrix] = deleteMatrixButton,
            [InteractionMenuMode.MovePlanets] = movePlanetsButton
        };

        private void Update()
        {
            if (Learner.Instance.IsVR) 
                return;
            
            if (Input.GetKeyDown("n"))
            {
                ActivateNormalMode();
            }
            else if (Input.GetKeyDown("m"))
            {
                ActivateMoveMatrix();
            }
            else if (Input.GetKeyDown("r"))
            {
                ActivateDeleteMatrix();
            }
            else if (Input.GetKeyDown("p"))
            {
                ActivateMovePlanets();
            }
        }

        private void ChangeMode(InteractionMenuMode interactionMenuMode)
        {
            Mode = interactionMenuMode;
            var buttons = Buttons;

            foreach (var btn in buttons.Values.Where(btn => btn))
            {
                btn.SetDisabled(false);
            }
            
            var button = buttons[Mode];
            button.Select();
            button.SetDisabled(true);

            OnChangedMode?.Invoke(interactionMenuMode);
        }

        public void ActivateNormalMode()
            => ChangeMode(InteractionMenuMode.Normal);

        public void ActivateMoveMatrix()
            => ChangeMode(InteractionMenuMode.MoveMatrix);

        public void ActivateDeleteMatrix()
            => ChangeMode(InteractionMenuMode.DeleteMatrix);

        public void ActivateMovePlanets()
            => ChangeMode(InteractionMenuMode.MovePlanets);

        public void DeactivateNormalMode()
            => ChangeMode(InteractionMenuMode.None);
    }
}