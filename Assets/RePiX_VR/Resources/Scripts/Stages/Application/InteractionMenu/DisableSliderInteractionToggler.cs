﻿using System;
using OmiLAXR.UI;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(SliderInteraction))]
    public class DisableSliderInteractionToggler : MonoBehaviour
    {
        public SolarInteractionMenu.InteractionMenuMode enableOnMode = SolarInteractionMenu.InteractionMenuMode.Normal;
        
        private void Start()
        {
            SolarInteractionMenu.OnChangedMode += Toggler;
        }

        private void Toggler(SolarInteractionMenu.InteractionMenuMode mode)
        {
            var sliderInteraction = gameObject.GetComponent<SliderInteraction>();
            if (sliderInteraction)
                sliderInteraction.enabled = mode == enableOnMode;
        }

        private void OnDestroy()
        {
            SolarInteractionMenu.OnChangedMode -= Toggler;
        }
    }

}