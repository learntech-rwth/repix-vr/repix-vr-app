﻿using System;

namespace RePiX_VR.ApplicationStage
{
    [Serializable]
    public enum Axis
    {
        All = -1,
        None = 0,
        X = 1,
        Y = 2,
        Z = 3
    };
}