﻿using System;
using OmiLAXR.Interaction;
using OmiLAXR.Interaction.DragAndDrop;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Object = UnityEngine.Object;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(Pointable))]
    public abstract class Planet : Draggable
    {
        public DropTarget placeHolder;
        public CenterInCoordinateSystem rotationCenter;
        public CoordinateSystem coordinateSystem;
        [FormerlySerializedAs("dndMatrix")] public MatrixTarget matrixTarget;
        
        private Transform _initialHolder;
        public TRSMatrix trsMatrix;

        [Obsolete("Still needed?")]
        private float _maxTranslation;

        public abstract Vector3 TRSMatrixPosition { get; }

        public UnityEvent<TransformationMatrix> onAppliedTranslation = new UnityEvent<TransformationMatrix>();
        public UnityEvent<TransformationMatrix> onChangedTranslation = new UnityEvent<TransformationMatrix>();
        public UnityEvent<TransformationMatrix> onAppliedRotation = new UnityEvent<TransformationMatrix>();
        public UnityEvent<TransformationMatrix> onChangedRotation = new UnityEvent<TransformationMatrix>();
        public UnityEvent<TransformationMatrix> onAppliedScaling = new UnityEvent<TransformationMatrix>();
        public UnityEvent<TransformationMatrix> onChangedScaling = new UnityEvent<TransformationMatrix>();

        public bool IsPlacedInCoordinateSystem { get; private set; }

        private GameObject coodinateSystemDragAndDropTarget;

        protected virtual void Start()
        {
            _initialHolder = transform.parent;

            onStartedDrag.AddListener(OnStartedDrag);
            onEndedDrag.AddListener(OnStoppedDrag);

            trsMatrix.OnUpdatedMatrix += OnUpdatedMatrix;
            trsMatrix.onAppliedMatrix.AddListener(OnAppliedMatrix);
            trsMatrix.onChangedMatrix.AddListener(OnChangedMatrix);
            
            PreparePosition();
            
            if(GameObject.FindObjectOfType<CoordinateSystemDropTarget>() != null)
                coodinateSystemDragAndDropTarget = Object.FindObjectOfType<CoordinateSystemDropTarget>().gameObject;
        }

        protected virtual void OnAppliedMatrix(TransformationMatrix matrix)
        {
            switch (matrix)
            {
                case TranslationMatrix:
                    onAppliedTranslation.Invoke(matrix);
                    break;
                case RotationMatrix:
                    onAppliedRotation.Invoke(matrix);
                    break;
                case ScalingMatrix:
                    onAppliedScaling.Invoke(matrix);
                    break;
            }
        }
        
        protected virtual void OnChangedMatrix(TransformationMatrix matrix)
        {
            switch (matrix)
            {
                case TranslationMatrix:
                    onChangedTranslation.Invoke(matrix);
                    break;
                case RotationMatrix:
                    onChangedRotation.Invoke(matrix);
                    break;
                case ScalingMatrix:
                    onChangedScaling.Invoke(matrix);
                    break;
            }
        }

        protected virtual void PreparePosition()
        {
            var coneY = coordinateSystem.coneY.transform.position;
            var coneNegY = coordinateSystem.coneMinusY.transform.position;
            
            _maxTranslation = (coneY.y - coneNegY.y) / 2 + 0.4f;

            var trsMatrixTransform = trsMatrix.transform;
            trsMatrixTransform.position = TRSMatrixPosition;
            trsMatrixTransform.localScale = new Vector3(0.769f, 0.769f, 0.769f);
        }

        protected virtual void OnUpdatedMatrix(Matrix4x4 m)
        {
            var t = transform;
            t.localPosition = m.GetPosition();
            t.rotation = m.rotation;
            t.localScale = m.lossyScale;
        }

        private void OnStartedDrag(Draggable d)
        {
            if (placeHolder)
                placeHolder.gameObject.SetActive(true);
            
            matrixTarget.gameObject.SetActive(false);

            if (!coodinateSystemDragAndDropTarget)
                return;
            coodinateSystemDragAndDropTarget.SetActive(true);
        }

        private void OnStoppedDrag(Draggable draggable, DropTarget target)
        {
            var t = transform;

            var isPlacedOnPlaceholder = target == placeHolder;
            
            if (isPlacedOnPlaceholder)
            {
                // placed on placeholder
                t.position = InitialPosition;
                t.SetParent(_initialHolder);
            }
            else
            {
                // placed in coordinate system
                var coneX = coordinateSystem.coneX.transform.position;
                var coneY = coordinateSystem.coneY.transform.position;
                
                t.position = new Vector3(coneY.x, coneX.y, coneY.z);
                t.SetParent(rotationCenter.transform);
                
            }
            IsPlacedInCoordinateSystem = !isPlacedOnPlaceholder;
            
            matrixTarget.gameObject.SetActive(!isPlacedOnPlaceholder);
            trsMatrix.gameObject.SetActive(!isPlacedOnPlaceholder);
            
            if (DropTarget)
                DropTarget.enabled = !isPlacedOnPlaceholder;
            
            placeHolder.gameObject.SetActive(false);
            t.localScale = InitialScale;
            
            if (!coodinateSystemDragAndDropTarget)
                return;
            coodinateSystemDragAndDropTarget.SetActive(false);
        }

        protected override void DragThis()
        {
            if (DraggableMatrix.DraggingMatrix != null)
                return;
            //if (!SolarInteractionMenu.IsMovingPlanets)
                //return;
                
            base.DragThis();
        }
    }
}