using System.Collections.Generic;
using UnityEngine.UI;

namespace RePiX_VR.ApplicationStage
{
    public class ResetMatrix2D : ResetMatrix
    {
        private Button _button;

        private DropDownFallbackNonVR _dropDownFallbackNonVR;

        private Slider _slider;

        public void Start()
        {
            _button = gameObject.GetComponent<Button>();
            _dropDownFallbackNonVR = gameObject.GetComponentInParent<DropDownFallbackNonVR>();
            if (_dropDownFallbackNonVR != null)
                _button.onClick.AddListener(() => _dropDownFallbackNonVR.Reset());
            else
                _button.onClick.AddListener(() => ResetScalingMatrix());

        }

        private void ResetScalingMatrix()
        {
            _slider = gameObject.transform.parent.GetComponentInChildren<Slider>();
            _slider.value = 1;
        }
        /*private Dropdown _dropdown;

        protected override IEnumerable<Slider> GetSliders()
            => transform.parent.GetComponentsInChildren<Slider>();
        
        protected override void Start()
        {
            base.Start();
            var parent = transform.parent;
            _dropdown = parent.GetComponentInChildren<Dropdown>();
        }

        public override void DoReset()
        {
            base.DoReset();
            
            if (!_dropdown)
                return;

            _dropdown.value = 0;
        }*/
    }
}
