﻿using OmiLAXR.Interaction.DragAndDrop;
using UnityEngine;
using UnityEngine.Serialization;

namespace RePiX_VR.ApplicationStage
{
    public class Placeholder : DropTarget
    {
        [FormerlySerializedAs("go")]
        [Tooltip("For which game object the placeholder is used.")]
        public GameObject forGameObject;

        private void Awake()
        {
            gameObject.transform.position = forGameObject.transform.position;
            gameObject.SetActive(false);
            
            onlyAllowedDraggables.Clear();
            onlyAllowedDraggables.Add(forGameObject.GetComponent<Draggable>());
        }
    }
}