using OmiLAXR;
using UnityEngine;
using OmiLAXR.Interaction;
using OmiLAXR.Interaction.DragAndDrop;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

namespace RePiX_VR.ApplicationStage
{
    [RequireComponent(typeof(Pointable))]
    public class DraggableMatrix : Draggable
    {
        public static DraggableMatrix DraggingMatrix;
        [FormerlySerializedAs("matrixHolder")] public MatrixManager matrixManager;
        public Placeholder placeholder;
        
        private readonly Vector3 _smallerScale = new Vector3(0.1f, 0.1f, 0.1f);
        
        protected override void DragThis()
        {
            if (DropTarget.DraggingObject != null)
                return;
            if (DraggingMatrix != null)
                return;
            
            if (Learner.Instance.IsVR && showMiniature)
                CreateMatrixMiniature();

            matrixManager.gameObject.SetActive(false);
            placeholder.gameObject.SetActive(true);
            DraggingMatrix = this;
        }

        public void DropThis()
        {
            if (DraggingMatrix != this)
                return;
            
            if (Miniature)
                Destroy(Miniature);
            
            matrixManager.gameObject.SetActive(true);
            placeholder.gameObject.SetActive(false);
            
            DraggingMatrix = null;
        }

        protected void CreateMatrixMiniature()
        {
            CreateMiniature(matrixManager.gameObject);
            var checkboxPanel = Miniature.GetComponentInChildren<CheckboxPanel>();
            if (checkboxPanel)
            {
                Destroy(checkboxPanel.transform.parent.gameObject);
            }
        }
    }
}