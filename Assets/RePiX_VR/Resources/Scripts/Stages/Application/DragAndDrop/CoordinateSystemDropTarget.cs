﻿using OmiLAXR.Interaction.DragAndDrop;

namespace RePiX_VR.ApplicationStage
{
    public class CoordinateSystemDropTarget : DropTarget
    {
        protected override void DropHere(Draggable draggable)
        {
            if (DraggingObject != null && DraggingObject != draggable)
                return;
            base.DropHere(draggable);
            gameObject.SetActive(HoldingObjects.Count < 1);
        }

        protected override void DragFromHere(Draggable draggable)
        {
            if (DraggableMatrix.DraggingMatrix != null)
                return;
            base.DragFromHere(draggable);
        }
    }
}