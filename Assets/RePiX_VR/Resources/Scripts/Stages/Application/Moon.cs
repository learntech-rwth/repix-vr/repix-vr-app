﻿using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace RePiX_VR.ApplicationStage
{
    public class Moon : Planet
    {
        public override Vector3 TRSMatrixPosition =>
            new Vector3(5 * Mathf.Cos(7 * 0.15f), 1.9f, -5 * Mathf.Sin(7 * 0.15f));
    }
}