using System.Linq;
using UnityEngine;

namespace RePiX_VR.ApplicationStage
{
    public class CoordinateSystem : MonoBehaviour
    {
        [Header("Coordinate Cones")]
        public GameObject coneX;
        public GameObject coneMinusX;
        public GameObject coneY;
        public GameObject coneMinusY;
        public GameObject coneZ;
        public GameObject coneMinusZ;
        
        private void Start()
        {
            var transforms = transform.GetComponentsInChildren<Transform>();
            var cones = transforms.Where(t => t.name.ToLower().Contains("cone")).Select(t => t.gameObject);
            foreach (var cone in cones)
            {
                switch (cone.name)
                {
                    case "ConeX":
                        coneX = cone;
                        break;
                    case "ConeY":
                        coneY = cone;
                        break;
                    case "ConeZ":
                        coneZ = cone;
                        break;
                    case "Cone-X":
                        coneMinusX = cone;
                        break;
                    case "Cone-Y":
                        coneMinusY = cone;
                        break;
                    case "Cone-Z":
                        coneMinusZ = cone;
                        break;
                }
            }
        }
    }

}