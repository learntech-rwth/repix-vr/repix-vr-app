using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using OmiLAXR.UI;
using RePiX_VR;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR
{
    public class Frustum2D : MonoBehaviour
    {
        [HideInInspector]
        public enum FrustumValue
        {
            NearPlane,
            FarPlane,
            Aspect,
            FOV
        };

        private Frustum _frustum;

        private UnitFrustum _unitFrustum;

        private FrustumValueChecker _valueChecker;
        private TextValueUpdater _text;
        private Slider _slider;

        public FrustumValue frustumValue = FrustumValue.NearPlane;

        void Start()
        {
            _slider = this.GetComponent<Slider>();
            _slider.onValueChanged.AddListener(value => SliderPlaneChanged(value));
            _frustum = FindObjectOfType<Frustum>().GetComponent<Frustum>();
            _unitFrustum = FindObjectOfType<UnitFrustum>().GetComponent<UnitFrustum>();
            _text = GetComponentInChildren<TextValueUpdater>();
            if (FindObjectOfType<FrustumValueChecker>())
                _valueChecker = FindObjectOfType<FrustumValueChecker>().GetComponent<FrustumValueChecker>();
        }

        private void SliderPlaneChanged(float value)
        {
            switch (frustumValue)
            {
                case FrustumValue.Aspect:
                    _frustum.SetAspectDiv10(value);
                    break;
                case FrustumValue.FarPlane:
                    _frustum.SetFarZDiv10(value);
                    break;
                case FrustumValue.NearPlane:
                    _frustum.SetNearZDiv10(value);
                    break;
                case FrustumValue.FOV:
                    _frustum.SetFov(value);
                    break;
            }
            _unitFrustum.MakeDirty();
            _text.UpdateText(value);
            if (_valueChecker)
                _valueChecker.CheckMatch();
        }
        
    }
}
