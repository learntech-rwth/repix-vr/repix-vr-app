using OmiLAXR;
using OmiLAXR.InstructionalDesign;
using UnityEngine;
using UnityEngine.Events;

namespace RePiX_VR
{
    public class LanguageSelectionStage : MonoBehaviour
    {
        
        public LearningUnit learningUnit;
        public LanguageSelectionPanel selectionPanel;

        public UnityEvent onNext = new UnityEvent();
        
        // Start is called before the first frame update
        private void Start()
        {
            selectionPanel.onSelectedLanguage.AddListener(() =>
            {
                learningUnit.Complete();
                onNext.Invoke();
                gameObject.SetActive(false);
            });
        }
    }

}