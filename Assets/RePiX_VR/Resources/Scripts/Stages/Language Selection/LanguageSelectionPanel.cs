using OmiLAXR;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace RePiX_VR
{
    public class LanguageSelectionPanel : MonoBehaviour
    {
        public Button germanButton;
        public Button englishButton;
        
        public UnityEvent onSelectedLanguage = new UnityEvent();

        private void Start()
        {
            BindEvents(germanButton);
            BindEvents(englishButton);
        }
        
        public void SelectLanguage(string language)
        {
            LearningEnvironment.Instance.SetLanguage(language);
            onSelectedLanguage?.Invoke();
        }
        
        private void BindEvents(Button button)
        {
            button.onClick.AddListener(() =>
            {
                SelectLanguage(button == englishButton ? "English" : "German");
                germanButton.gameObject.SetActive(false);
                englishButton.gameObject.SetActive(false);
            });
        }
    }
}
