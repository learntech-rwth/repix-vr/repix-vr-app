﻿using OmiLAXR.Interaction;
using QuickOutline;
using UnityEngine;

namespace RePiX_VR.GeometryStage
{
    [RequireComponent(typeof(Outline), typeof(Pointable), typeof(InteractableEvents))]
    public class TriangleBuilderObject : MonoBehaviour
    {
        public Outline outline;
        public InteractableEvents interactableEvents;
        public MouseInteractionEvents mouseInteractionEvents;
        public Pointable pointable;
        protected virtual void Start()
        {
            if (!outline)
                outline = GetComponent<Outline>();
            
            if (!interactableEvents)
                interactableEvents = GetComponent<InteractableEvents>();

            if (!mouseInteractionEvents)
                mouseInteractionEvents = GetComponent<MouseInteractionEvents>();

            if (!pointable)
                pointable = GetComponent<Pointable>();
        }
    }
}