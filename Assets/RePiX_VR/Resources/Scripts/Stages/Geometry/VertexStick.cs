﻿using UnityEngine;

namespace RePiX_VR.GeometryStage
{
    public class VertexStick : MonoBehaviour
    {
        public GameObject stick;

        public GameObject ball;

        private MeshRenderer _mrStick;

        private MeshRenderer _mrBall;
        // Start is called before the first frame update
        private void Start()
        {
            _mrBall = ball.gameObject.GetComponent<MeshRenderer>();
            _mrStick = stick.GetComponent<MeshRenderer>();
        }

        public void SetColor(Color color)
        {
            _mrBall.material.SetColor("_Color", color);
            _mrStick.material.SetColor("_Color", color);
        }

        public void PlaceOnHand(GameObject hand)
        {
            var t = transform;
            t.parent = hand.transform;
            t.localPosition = Vector3.zero;
        }
    }

}