using UnityEngine;

namespace RePiX_VR.GeometryStage
{
    public class GeometryStageInstructions : MonoBehaviour
    {
        public RotatingBunny rotatingBunny;
        public ExampleTriangle exampleTriangle;
        public TourVertexManager tourVertexManager;
        
        public void SpawnInteractable()
        {
            gameObject.SetActive(true);
            tourVertexManager.PlaceTriangleVerticesPlaceholder();
        }

        public void EnableInteraction()
        {
            exampleTriangle.gameObject.SetActive(false);
        }

        public void SpawnExampleMesh()
        {
            tourVertexManager.StartExploration();
        }

        public void SpawnBunny()
        {
            rotatingBunny.gameObject.SetActive(true);
            rotatingBunny.HideWire();
            rotatingBunny.ShowSurface();
        }

        public void SpawnBunnyWire()
        {
            rotatingBunny.gameObject.SetActive(true);
            rotatingBunny.HideSurface();
            rotatingBunny.ShowWire();
        }

        public void SpawnBunnyAndWire()
        {
            rotatingBunny.gameObject.SetActive(true);
            rotatingBunny.ShowSurface();
            rotatingBunny.ShowWire();
        }

        public void UnspawnBunny()
        {
            rotatingBunny.HideSurface();
            rotatingBunny.HideWire();
            rotatingBunny.gameObject.SetActive(false);
        }
    }

}