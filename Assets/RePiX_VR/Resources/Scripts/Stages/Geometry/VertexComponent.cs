﻿using TMPro;
using UnityEngine;

namespace RePiX_VR.GeometryStage
{
    [RequireComponent(typeof(VertexHelper))]
    public class VertexComponent : TriangleBuilderObject
    {
        public VertexHelper vertexHelper;
        public SphereCollider vertexCollider;
        private Color _highlightColor;

        public TextMeshProUGUI textMesh;
        public int Index { get; private set; }
        
        protected override void Start()
        {
            base.Start();

            if (!vertexCollider)
                vertexCollider = GetComponent<SphereCollider>();

            if (!vertexHelper)
                vertexHelper = GetComponent<VertexHelper>();

            if (!textMesh)
                textMesh = GetComponentInChildren<TextMeshProUGUI>();
        }

        public void SetIndex(int index)
        {
            Index = index;
            textMesh.text = index.ToString();
        }

        public void Unselect()
        {
            vertexHelper.CleanupWires();
            pointable.KeepOutline(false);
            outline.enabled = false;
            outline.OutlineColor = _highlightColor;
        }


        public void Select(Color outlineColor)
        {
            _highlightColor = outline.OutlineColor;
            pointable.KeepOutline(true);
            outline.OutlineColor = outlineColor;
            outline.enabled = true;
        }
    }
}