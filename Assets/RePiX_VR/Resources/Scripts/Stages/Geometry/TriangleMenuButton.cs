﻿using OmiLAXR;
using OmiLAXR.Adapters.SteamVR;
using UnityEngine;
using UnityEngine.UI;

using OmiLAXR.xAPI.Extensions;

namespace RePiX_VR.GeometryStage
{
    [RequireComponent(typeof(Button))]
    public class TriangleMenuButton : MonoBehaviour
    {
        public TriangleInteractionState interactionState = TriangleInteractionState.None;

        private void Start()
        {
            // Bind click event
            var button = GetComponent<Button>();
            button.onClick.AddListener(UpdateInteractionState);
            if (Learner.Instance.IsVR) return;
            var triangleBuilder = transform.GetComponentInParent<TriangleBuilder>();
            triangleBuilder.OnChangedState += TriangleBuilder_OnChangedState;
            button.SetDisabled(interactionState == triangleBuilder.GetInteractionState());

        }

        private void TriangleBuilder_OnChangedState(TriangleInteractionState from, TriangleInteractionState to)
        {
            var button = GetComponent<Button>();
            // disable if selected
            button.SetDisabled(to == interactionState);

            //change color of button when selected
            var image = GetComponent<Image>();
            if (to == interactionState)
                image.color = Color.gray;
            else
                image.color = Color.white;
        }

        private void UpdateInteractionState()
        {
            var triangleBuilder = transform.GetComponentInParent<TriangleBuilder>();
            if (Learner.Instance.IsVR)
                triangleBuilder.SetNextInteractionState(interactionState);
            else
                triangleBuilder.SetInteractionState(interactionState);
        }
    }

}