﻿using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace RePiX_VR.GeometryStage
{
    /// <summary>
    /// This class manages the creation of vertex game objects.
    /// </summary>
    public class VertexManager : MonoBehaviour
    {
        [Tooltip("The scale of the vertices.")]
        public float vertexScale = 0.01f;
        [Tooltip("Outline color of vertices for collision.")]
        public Color vertexHighlightColor = Color.yellow;
        [Tooltip("Shader, which will be applied on vertices.")]
        public Shader vertexShader;

        public VertexComponent vertexPrefab;
        private static readonly int Color1 = Shader.PropertyToID("_Color");

        private void Start()
        {
            // Apply standard shader if no shader is provided
            if (!vertexShader)
                vertexShader = Shader.Find("Standard");
        }
        public VertexComponent CreateVertexObject(Vector3 position, Color color)
        {
            var vertex = Instantiate(vertexPrefab);

            var vertexTransform = vertex.transform;
            // Make Vertex Small
            vertexTransform.localScale = Vector3.one * vertexScale;
            // Place to attached object position
            vertexTransform.position = position;

            // prepare shaders
            var meshRenderer = vertex.GetComponent<MeshRenderer>();
            meshRenderer.material.shader = vertexShader;
            meshRenderer.material.SetColor(Color1, color);

            // Add Outline
            var outline = vertex.outline;
            outline.OutlineColor = vertexHighlightColor;
            outline.OutlineWidth = 5.0f;
            outline.enabled = false;

            // prepare collider
            var col = vertex.GetComponent<SphereCollider>();
            col.isTrigger = true;
            col.enabled = false;
            
            return vertex;
        }
    }
}