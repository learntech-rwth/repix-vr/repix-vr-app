﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR.Extensions;
using OmiLAXR.Interaction;

namespace RePiX_VR.GeometryStage
{
    /// <summary>
    /// This component should be assigned to a triangle game object to hold 
    /// the information which vertices and wires are related to this triangle.
    /// </summary>
    public class TriangleHelper : MonoBehaviour
    {
        /// <summary>
        /// Collection of vertices game objects, which are related to this triangle.
        /// </summary>
        public List<VertexComponent> vertices = new List<VertexComponent>();

        /// <summary>
        /// Collection of wire game objects, which are related to this triangle.
        /// </summary>
        public List<GameObject> wires = new List<GameObject>();

        public TriangleComponent triangleComponent;

        private void Start()
        {
            triangleComponent = GetComponent<TriangleComponent>();
        }

        /// <summary>
        /// Assign a collection of vertices to this component
        /// </summary>
        /// <param name="vs">Collection of vertices game objects.</param>
        public void AssignVertices(IEnumerable<VertexComponent> vs)
            => vertices.AddRange(vs);

        /// <summary>
        /// Assign a collection of wires to this component.
        /// </summary>
        /// <param name="ws">Collection of wires game objects.</param>
        public void AssignWires(IEnumerable<GameObject> ws)
            => wires.AddRange(ws);

        public void DeassignVertices()
        {
            foreach (var v in vertices)
            {
                var vh = v.GetComponent<VertexHelper>();
                vh.RemoveTriangle(gameObject);
            }
        }

        public void HighlightWires()
        {
            foreach (var wire in wires)
            {
                wire.transform.GetChild(0).GetComponent<MeshRenderer>().material.SetColor("_Color", Color.yellow);
            }
        }

        public void UnhighlightWires()
        {
            foreach (var wire in wires)
            {
                wire.transform.GetChild(0).GetComponent<MeshRenderer>().material.SetColor("_Color", Color.white);
            }
        }

        public void ReleaseTriangleAsMesh()
        {
            // attach vertices to triangle
            // and remove their interaction
            foreach (var v in vertices)
            {
                v.transform.SetParent(transform);
                Destroy(v.GetComponent<InteractableEvents>());
            }

            // Attach wires to triangle
            foreach (var w in wires)
            {
                w.transform.SetParent(transform);
            }

            transform.SetParent(null, true);
        }
        /// <summary>
        /// Get center of gravity.
        /// </summary>
        public static Vector3 GetCog(params Vector3[] vertices)
        {
            if (vertices.Length < 1)
                return Vector3.zero;

            var cog = Vector3.zero;
            foreach (var v in vertices)
                cog += v;

            return cog / vertices.Length;
        }
        /// <summary>
        /// Get center of gravity.
        /// </summary>
        public Vector3 GetCog() => GetCog(vertices.Select(v => v.transform.position).ToArray());
        /// <summary>
        /// Destroy all wires.
        /// </summary>
        public void DestroyWires()
        {
            var wiresArray = wires.ToArray();
            vertices.ForEach(v => v.GetComponent<VertexHelper>().RemoveWires(wiresArray));
            wires.ForEach(w => w.Destroy());
            wires.Clear();
        }
    }
}