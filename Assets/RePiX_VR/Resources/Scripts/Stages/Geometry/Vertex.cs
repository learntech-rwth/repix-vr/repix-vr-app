﻿using UnityEngine;

namespace RePiX_VR.GeometryStage
{
    public struct Vertex
    {
        /// <summary>
        /// Position of the vertex.
        /// </summary>
        public readonly Vector3 position;
        /// <summary>
        /// Color of the vertex.
        /// </summary>
        public readonly Color color;

        private static readonly int Color1 = Shader.PropertyToID("_Color");

        public Vertex(float x, float y, float z, Color c)
        {
            position = new Vector3(x, y, z);
            color = c;
        }
        public Vertex(Vector3 pos, Color col)
        {
            position = pos;
            color = col;
        }
        /// <summary>
        /// Create Vertex from a game object. It takes _Color from MeshRenderer for the color.
        /// </summary>
        public static Vertex FromGameObject(GameObject go)
        {
            var color = go.GetComponent<MeshRenderer>().material.GetColor(Color1);
            var position = go.transform.position;
            return new Vertex(position, color);
        }

        public override bool Equals(object obj)
        {
            var o = (Vertex)obj;
            return position == o.position && color == o.color;
        }

        public static bool operator ==(Vertex a, Vertex b) => a.Equals(b);
        public static bool operator !=(Vertex a, Vertex b) => !a.Equals(b);
        public override int GetHashCode() => base.GetHashCode();
    }

}