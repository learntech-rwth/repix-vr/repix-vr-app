﻿using UnityEngine;
using System.Collections;

namespace RePiX_VR.GeometryStage
{
    public delegate void VertexHandler(VertexComponent vertexGameObject);
    public delegate void TriangleHandler(TriangleComponent triangle, VertexComponent[] vertices);
    public delegate void TriangleInteractionStateHandler(TriangleInteractionState from, TriangleInteractionState to);
}