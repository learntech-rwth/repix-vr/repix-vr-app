﻿using System.Collections.Generic;
using OmiLAXR;
using OmiLAXR.Adapters.SteamVR;
using OmiLAXR.Extensions;
using OmiLAXR.Interaction;
using OmiLAXR.UI;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

namespace RePiX_VR.GeometryStage
{
    [RequireComponent(typeof(PointableUI))]
    public class TriangleMenuPanel : MenuPanel
    {
        public Button noneButton;
        public Button placeVerticesButton;
        public Button moveVerticesButton;
        public Button buildTriangleButton;
        public Button removeVerticesButton;

        private IEnumerable<Button> AllButtons => new[]
            { noneButton, placeVerticesButton, moveVerticesButton, buildTriangleButton, removeVerticesButton };
        
        private TriangleBuilder _triangleBuilder;
        protected override void Start()
        {
            base.Start();

            var noneBtnGameObject = transform.Find("Button_None");
            var noneBtn = noneBtnGameObject.GetComponent<Button>();
            noneBtn.Select();

            _triangleBuilder = transform.GetComponentInParent<TriangleBuilder>();

            var pointableUI = GetComponent<PointableUI>();
            pointableUI.onHover.AddListener(OnPointerIn);
            pointableUI.onUnhover.AddListener(OnPointerOut);

            onEnterGroup.AddListener(() => _triangleBuilder.PauseInteraction());
            onLeaveGroup.AddListener(() => _triangleBuilder.ResumeInteraction());
        }

        public void SetActiveAllButtons(bool active)
            => AllButtons.ForEach(b => b.gameObject.SetActive(active));

        private void OnPointerOut(LaserPointerEventArgs e)
        {
            if (_triangleBuilder.GetInteractionState() == TriangleInteractionState.None)
                return;

            // Limit laser interaction to panel and its children
            var laser = (SteamVR_ImprovedLaserPointer)e.Sender;
            laser.limitedOn.Add(gameObject);
            gameObject.ForEachChild(c => laser.limitedOn.Add(c));
        }

        private static void OnPointerIn(LaserPointerEventArgs e)
        {
            var laser = (SteamVR_ImprovedLaserPointer)e.Sender;
            // Clear laser limit list
            laser.limitedOn.Clear();
        }
    }

}