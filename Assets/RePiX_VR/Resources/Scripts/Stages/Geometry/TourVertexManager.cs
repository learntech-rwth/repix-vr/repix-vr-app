﻿using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Serialization;

using OmiLAXR;
using OmiLAXR.Extensions;
using OmiLAXR.Helpers.Sticky;
using OmiLAXR.xAPI.Extensions;

namespace RePiX_VR.GeometryStage
{
    public class TourVertexManager : MonoBehaviour
    {
        private enum TourVertexManagerStep
        {
            FirstTriangle,
            Exploration
        }

        public TriangleBuilder triangleBuilder;

        private GameObject _triangleMenuGameObject;
        private GameObject _triangleMenuPanelGo;

        private StickyManager _stickyManager;

        public ExampleMesh exampleMeshPrefab;

        private IEnumerable<TriangleMenuButton> Buttons => _triangleMenuPanelGo.GetComponentsInChildren<TriangleMenuButton>();

        private TourVertexManagerStep _step = TourVertexManagerStep.FirstTriangle;

        private ExampleMesh _exampleMesh;
        public ExampleTriangle exampleTriangle;

        private int _exampleMeshTriangles;

        [FormerlySerializedAs("OnFinishFirstTriangle")] public UnityEvent onFinishFirstTriangle = new UnityEvent();
        [FormerlySerializedAs("OnFinishExampleMesh")] public UnityEvent onFinishExampleMesh = new UnityEvent();
        public UnityEvent onPlacedVertex = new UnityEvent();

        public UnityEvent onConnectedVertices = new UnityEvent();
        
        // Start is called before the first frame update
        private void Start()
        {
            _stickyManager = GetComponent<StickyManager>();
            
            triangleBuilder.OnCreatedVertex += OnCreatedVertex;
            triangleBuilder.OnRemovedVertex += OnRemovedVertex;
            triangleBuilder.OnMovedVertex += OnMovedVertex;
            triangleBuilder.OnChangedState += UnstickVertex;
            triangleBuilder.OnCreatedTriangle += OnCreatedTriangle;

            // Find references
            _triangleMenuGameObject = FindObjectOfType<TriangleBuilderMenu>(true).gameObject;
            _triangleMenuPanelGo = _triangleMenuGameObject.GetComponentInChildren<TriangleMenuPanel>(true).gameObject;
        }

        private void OnRemovedVertex(VertexComponent vertex)
        {
            OnFirstThreeVertices();
            _stickyManager.Unstick(vertex.gameObject);
        }

        private void OnCreatedVertex(VertexComponent vertex)
        {
            _stickyManager.TryToStick(vertex.gameObject);
            onPlacedVertex.Invoke();
            OnFirstThreeVertices();
        }
        private void OnMovedVertex(VertexComponent vertex) => _stickyManager.TryToStick(vertex.gameObject);

        private void OnFirstThreeVertices()
        {
            if (!triangleBuilder)
                return;

            // Let Build Triangle blink
            var triangleButtons = GetButtonByInteractionState(TriangleInteractionState.BuildTriangles);
            
            if (triangleButtons == null) 
                return;

            var isDisabled = triangleBuilder.VertexCount < 3;
            foreach (var tb in triangleButtons)
            {
                tb.SetDisabled(isDisabled);
            }
        }

        private void OnFirstTriangle(TriangleComponent triangle, VertexComponent[] vertices)
        {
            // remove vertices from sticky manager
            _stickyManager.Clear();
            onFinishFirstTriangle?.Invoke();
        }

        private void OnAddedTriangle(TriangleComponent triangle, VertexComponent[] vertices)
        {
            _exampleMeshTriangles++;
            if (_exampleMeshTriangles >= 8)
            {
                onFinishExampleMesh.Invoke();
            }
        }

        public void HideElements()
        {
            triangleBuilder.Clear();
            triangleBuilder.gameObject.SetActive(false);

            Destroy(_exampleMesh);
        }

        private void OnCreatedTriangle(TriangleComponent triangle, VertexComponent[] vertices)
        {
            if (_step == TourVertexManagerStep.FirstTriangle)
                OnFirstTriangle(triangle, vertices);
            else OnAddedTriangle(triangle, vertices);
        }

        private Button[] GetButtonByInteractionState(TriangleInteractionState interactionState)
            => Buttons.Where(btn => btn != null && btn.interactionState == interactionState)
                .Select(btn => btn.GetComponent<Button>())
                .ToArray();
        
        private void UnstickVertex(TriangleInteractionState from, TriangleInteractionState to) => _stickyManager.Unstick();
        public void PlaceTriangleVerticesPlaceholder()
        {
            _step = TourVertexManagerStep.FirstTriangle;
            if (!Learner.Instance.IsVR)
            {
                exampleTriangle.transform.position = new Vector3(-0.1f, 0.227f, 0.18f);
            }
            _stickyManager.Clear();

            // Enable triangle builder
            triangleBuilder.gameObject.SetActive(true);
        }

        public void StartExploration()
        {
            //triangleBuilder.Clear();
            _step = TourVertexManagerStep.Exploration;

            foreach (var btn in Buttons)
                btn.GetComponent<Button>().SetDisabled(false);

            // Prepare sticky manager for new object
            //stickyManager.Clear();
            _exampleMesh = CreateExampleMesh(new Vector3(0, 0.448f, 0.275f));
            _exampleMeshTriangles = 0;
            _exampleMesh.gameObject.SetActive(true);
            _exampleMesh.gameObject.ForEachChild(c =>
            {
                c.SetActive(true);
                _stickyManager.MakeStickable(c);
            });
        }

        public ExampleMesh CreateExampleMesh(Vector3 position)
        {
            var exampleMesh = Instantiate(exampleMeshPrefab, transform, true);
            exampleMesh.transform.position = position;
            exampleMesh.name = "ExampleMesh";
            return exampleMesh;
        }
    }
}
