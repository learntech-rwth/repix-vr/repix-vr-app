﻿using UnityEngine;

namespace RePiX_VR.GeometryStage
{
    [DisallowMultipleComponent]
    public class VertexPair : MonoBehaviour
    {
        public VertexComponent fromVertex;
        public VertexComponent toVertex;

        public void AddWireReference(VertexComponent from, VertexComponent to)
        {
            fromVertex = from;
            toVertex = to;

            // Create outgoing wire reference
            var fromWires = from.GetComponent<VertexHelper>();

            // Create incoming wire reference
            var toWires = to.GetComponent<VertexHelper>();

            // Provide reference
            fromWires.AddVertexPair(this);
            toWires.AddVertexPair(this);
        }
    }
}