﻿using OmiLAXR.Helpers;
using UnityEngine;

namespace RePiX_VR.GeometryStage
{
    [RequireComponent(typeof(RotationAround))]
    public class RotatingBunny : MonoBehaviour
    {
        public GameObject surfaceGameObject;
        public GameObject wireGameObject;

        public void ShowSurface()
            => surfaceGameObject.SetActive(true);

        public void HideSurface()
            => surfaceGameObject.SetActive(false);

        public void ShowWire()
            => wireGameObject.SetActive(true);

        public void HideWire()
            => wireGameObject.SetActive(false);
    }
}