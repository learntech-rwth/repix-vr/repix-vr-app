﻿using System.Linq;
using UnityEngine;

namespace RePiX_VR.GeometryStage
{
    public class ExampleMesh : MonoBehaviour
    {
        public bool createOnStart = true;
        // Start is called before the first frame update
        private void Start()
        {
            if (createOnStart)
                Create();
        }

        public void Create()
        {
            var triangleBuilder = TriangleBuilder.Instance;
            const float s = 0.1f;
            var localVertices = new[]
            {
                new Vertex(0, s, 0, Color.red), // UP
                new Vertex(-s, 0, s, Color.green), // TL
                new Vertex(s, 0, s, Color.blue), // TR
                new Vertex(s, 0, -s, Color.magenta), // BR
                new Vertex(-s, 0, -s, Color.yellow), // BL
                new Vertex(0, -s, 0, Color.cyan), // DOWN
            };
            var indices = new[]
            {
                // TOP
                0, 1, 2,
                0, 2, 3,
                0, 3, 4,
                0, 4, 1,

                // BOTTOM
                5, 2, 1,
                5, 3, 2,
                5, 4, 3,
                5, 1, 4,
            };

            // World vertices position
            var position = new Vector3(0.5f, 0.5f, 0.5f);
            var vertices = localVertices.Select(v => new Vertex(v.position + position, v.color)).ToArray();

            // Create example mesh
            var exampleObject = triangleBuilder.CreateMeshGameObject(vertices, indices);
            exampleObject.transform.SetParent(transform);
            exampleObject.name = "ExampleMesh Mesh";
        }
    }

}