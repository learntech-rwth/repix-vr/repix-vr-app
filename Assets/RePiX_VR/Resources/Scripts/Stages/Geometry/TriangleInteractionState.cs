﻿using System;
using UnityEngine;
using System.Collections;

namespace RePiX_VR.GeometryStage
{
    [Flags]
    public enum TriangleInteractionState
    {
        None,
        PlaceVertices,
        MoveVertices,
        BuildTriangles,
        RemoveVertices,
        DestroyTriangles
    }
}