﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

using OmiLAXR.Extensions;
using OmiLAXR.Utils;

namespace RePiX_VR.GeometryStage
{
    [DisallowMultipleComponent]
    public class VertexHelper : MonoBehaviour
    {
        public List<MeshFilter> meshFilters = new List<MeshFilter>();

        public IEnumerable<TriangleComponent> Triangles => meshFilters.Select(mr => mr.GetComponent<TriangleComponent>()).ToArray();

        public List<GameObject> incomingWires = new List<GameObject>();
        public List<GameObject> outgoingWires = new List<GameObject>();

        public List<VertexComponent> connectedVertices = new List<VertexComponent>();

        public void AddVertexPair(VertexPair vertexPair)
        {
            var lineObj = vertexPair.gameObject;

            incomingWires.Add(lineObj);
            outgoingWires.Add(lineObj);

            CollectVertex(vertexPair.fromVertex);
            CollectVertex(vertexPair.toVertex);
        }

        public void CollectVertex(VertexComponent vertex)
        {
            if (!connectedVertices.Contains(vertex))
                connectedVertices.Add(vertex);
        }

        /// <summary>
        /// Remove missing wires.
        /// </summary>
        public void CleanupWires()
        {
            incomingWires.RemoveAll(o => o == null);
            outgoingWires.RemoveAll(o => o == null);
        }

        public void RemoveWires(GameObject[] wires)
        {
            incomingWires.RemoveAll(w => wires.Contains(w));
            outgoingWires.RemoveAll(w => wires.Contains(w));
        }

        public void RemoveConnectedWires()
        {
            incomingWires.ForEach(w => w.Destroy());
            outgoingWires.ForEach(w => w.Destroy());

            incomingWires.Clear();
            outgoingWires.Clear();
        }

        public void AddTriangle(TriangleComponent triangle)
        {
            var mr = triangle.GetComponent<MeshFilter>();
            meshFilters.Add(mr);
        }

        public void RemoveTriangle(GameObject triangle)
        {
            var mr = triangle.GetComponent<MeshFilter>();
            meshFilters.Remove(mr);
        }

        public void DestroyTriangles()
        {
            for (var i = 0; i < meshFilters.Count; i++)
            {
                var mr = meshFilters[i];
                var triangle = mr.gameObject;

                var triangleHelper = triangle.GetComponent<TriangleHelper>();
                triangleHelper.DestroyWires();

                mr.Destroy();
                triangle.Destroy();
            }
            meshFilters.Clear();
        }

        public void UpdateTriangleMeshes()
        {
            // iterate via "for" because of performance reasons
            foreach (var mf in meshFilters)
            {
                var triangle = mf.gameObject;

                var vRef = triangle.GetComponent<TriangleHelper>();

                var vertices = vRef.vertices.Select(v => Vertex.FromGameObject(v.gameObject)).ToArray();
                var points = vertices.Select(v => v.position).ToArray();
                var colors = vertices.Select(v => v.color).ToArray();

                var mesh = Mesh_Ext.CreateTriangle(points, colors);
                mf.mesh = mesh;

                var col = triangle.GetComponent<SphereCollider>();

                var cog = Vector3_Utils.CenterOfGravity(points);
                col.center = cog;

                var minMidDistance = Vector3_Utils.SmallestMid(cog, points);
                col.radius = minMidDistance;
            }
        }
        /// <summary>
        /// Update orientation and position of incoming and outgoing wire game objects.
        /// </summary>
        public void UpdateWiresOrientations()
        {
            UpdateWireOrientation(incomingWires);
            UpdateWireOrientation(outgoingWires);
        }
        /// <summary>
        /// Shift wire in correct direction and position according to it's connected vertices.
        /// </summary>
        /// <param name="wires"></param>
        private static void UpdateWireOrientation(IList<GameObject> wires)
        {
            for (var i = 0; i < wires.Count; i++)
            {
                // Get wire
                var wire = wires[i];

                if (!wire)
                {
                    // Remove wire if it is invalid (null)
                    wires.RemoveAt(i);
                    i--;
                    continue;
                }

                // Get helper class
                var vertexPair = wire.GetComponent<VertexPair>();

                // Get from and to positions
                var from = vertexPair.fromVertex.transform.position;
                var to = vertexPair.toVertex.transform.position;

                // Calculate their distance to each other
                var distance = Vector3.Distance(from, to);

                // Set wire position and update it's orientation [from]--->[to]
                wire.transform.position = from;
                wire.transform.LookAt(to);

                // Adjust the scale of the wire.
                var s = wire.transform.localScale;
                s.z = distance;
                wire.transform.localScale = s;
            }
        }
    }
}