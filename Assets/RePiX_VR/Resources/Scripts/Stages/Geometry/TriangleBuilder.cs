﻿using UnityEngine;

using Valve.VR;
using QuickOutline;

using System;
using System.Linq;
using System.Collections.Generic;
using OmiLAXR;
using OmiLAXR.Adapters.SteamVR;
using OmiLAXR.Extensions;
using OmiLAXR.UI;
using OmiLAXR.Utils;


namespace RePiX_VR.GeometryStage
{
    /// <summary>
    /// Component to realize 3D Geometry generation with vertices and triangles.
    /// It regulates all interaction states (place vertices, remove vertices, move vertices, build triangles)
    /// and creates their visualizations.
    /// </summary>
    [RequireComponent(typeof(VertexManager))]
    public class TriangleBuilder : MonoBehaviour
    {
        #region Public Fields

        private static TriangleBuilder _instance;
        public static TriangleBuilder Instance
            => _instance == null ? _instance = FindObjectOfType<TriangleBuilder>() : null;
        /// Number of vertex objects
        public int VertexCount => _verticesObject == null ? 0 : _verticesObject.transform.childCount;
        /// Number of triangle objects
        public int TriangleCount => _trianglesObject == null ? 0 : _trianglesObject.transform.childCount;
        /// Reference to interactive hand's laser pointer.
        private SteamVR_ImprovedLaserPointer PlaceHandLaser => placeHand != null ? placeHand.GetComponent<SteamVR_ImprovedLaserPointer>() : null;
        #endregion

        #region Public Properties   
        [Tooltip("Thickness scale of visualized wires.")]
        public float wireThickness = 0.0025f;
        [Tooltip("Selection outline color of vertices.")]
        public Color vertexSelectionColor = Color.green;
        [Tooltip("Shader, that has to be used on triangles.")]
        public Shader triangleShader;

        /// Event which get fired, if Edit state changes
        public event TriangleInteractionStateHandler OnChangedState;
        // SteamVR Actions
        [Tooltip("Gradient selection action. (E.g. trackpad)")]
        public SteamVR_Action_Boolean touchAction = SteamVR_Input.GetBooleanAction("Touch");
        [Tooltip("Gradient selection action. (E.g. trackpad touch)")]
        public SteamVR_Action_Vector2 touchPositionAction = SteamVR_Input.GetVector2Action("Move");
        [Tooltip("Trigger which has to be used to place vertices and selection them.")]
        public SteamVR_Action_Boolean interactAction = SteamVR_Input.GetBooleanAction("InteractUI");
        [Tooltip("SteamVR pose reference.")]
        public SteamVR_Behaviour_Pose pose;

        // Game Objects
        [Tooltip("Reference to placing hand.")]
        public GameObject placeHand;
        [Tooltip("Reference to color picker.")]
        public ColorPicker colorPicker;
        [Tooltip("Reference to 2d color picker")]
        public ColorPicker2D colorPicker2D;
        [Tooltip("Prefab, which label has to be used.")]
        public GameObject labelPrefab;
        [Tooltip("GameObject will be placed on controller for placing vertices.")]
        public VertexStick vertexStick;
        #endregion

        #region Events
        public event VertexHandler OnMovedVertex;
        public event VertexHandler OnCreatedVertex;
        public event VertexHandler OnRemovedVertex;
        public event TriangleHandler OnCreatedTriangle;
        public event TriangleHandler OnCreatingTriangle;
        public event TriangleHandler OnRemovedTriangle;
        #endregion

        #region Helper Variables

        /// Distance of vertex to controller
        private const float PlaceOffsetScale = 0.1f;

        /// Helper variables to pause the interaction 
        private TriangleInteractionState _pausedState = TriangleInteractionState.None;
        private bool _isPaused;

        /// Reference to vertex manager
        private VertexManager _vertexManager;

        // Active Object
        private TriangleBuilderObject _activeObject;
        private MeshRenderer _activeObjectMr;

        // Active Vertices Variables
        private TriangleInteractionState _interactionState = TriangleInteractionState.None;
        private Color _activeVertexColor = Color.red;
        private TriangleBuilderObject _collidedGameObject;
        private VertexHelper _activeVertexHelper;

        // Mesh Object Holder Objects
        private GameObject _triangleBuilderHolder;
        private GameObject _verticesObject;
        private GameObject _trianglesObject;
        private GameObject _wiresObject;

        // Triangle Variables
        private readonly List<VertexComponent> _selectedVertices = new List<VertexComponent>();
        private Material _triangleMaterial;
        private readonly List<GameObject> _collectedWires = new List<GameObject>();

        // Controller Variables
        private bool _isTouchDown;
        private Vector2 _touchPosition;
        private bool _isTriggerDown;

        private float _handleCooldown = 1.0f;
        private static readonly int Color1 = Shader.PropertyToID("_Color");

        private Learner _learner;

        //private TriangleMenuPanel _triangleMenuPanel;

        // Active Vertex Placing position
        private Vector3 PlacePosition 
        {
            get
            {
                if (Learner.Instance.IsVR)
                {
                    var pt = pose.transform;
                    if (pt)
                        return pt.position + pt.forward * PlaceOffsetScale;
                }
                // if is not VR, then don't use controller pose. Use non VR camera instead.
                var head = _learner.Player.GetHead();
                return head.position + head.forward * PlaceOffsetScale;
            }
        }
        #endregion

        #region Initialization
        protected virtual void Start()
        {
            // Find color picker
            colorPicker = GetComponentInChildren<ColorPicker>();

            if (colorPicker && colorPicker.gradientObject)
                colorPicker.gradientObject.SetActive(false);

            _learner = Learner.Instance;
            
            if (_learner.IsVR)
            {
                // Make Primary hand as placer hand
                if (!placeHand && Learner.Instance)
                    placeHand = Learner.Instance.GetPrimaryHand().GameObject;

                if (!pose && placeHand)
                    pose = placeHand.GetComponent<SteamVR_Behaviour_Pose>();
            }

            _vertexManager = GetComponent<VertexManager>();

            if (!triangleShader)
                triangleShader = Shader.Find("RenderingPipeline/TriangleShader");

            _triangleMaterial = new Material(triangleShader);

            InitEvents();
            InitBuilderHolder();

            if (!_learner.IsVR)
            {
                gameObject.Find("NonVR_Controls", true).SetActive(true);
                colorPicker2D.OnChanged += (_, c) => SetVertexColor(c);
                return;
            }

            if (!placeHand) return;

            if (vertexStick)
                vertexStick.PlaceOnHand(placeHand);

            //_triangleMenuPanel = GetComponentInChildren<TriangleMenuPanel>();
        }

        private void MenuClear(SteamVR_Action_Boolean action, SteamVR_Input_Sources inputSource) => Clear();

        /// <summary>
        /// Init. Game Object, where the whole created mesh will be contained.
        /// </summary>
        private void InitBuilderHolder()
        {
            // Create holders
            _triangleBuilderHolder = new GameObject("TriangleBuilderHolder");
            _verticesObject = new GameObject("Vertices");
            _trianglesObject = new GameObject("Triangles");
            _wiresObject = new GameObject("Wires");

            // Attach them this object.
            _triangleBuilderHolder.transform.SetParent(transform.parent);
            _verticesObject.transform.SetParent(_triangleBuilderHolder.transform);
            _trianglesObject.transform.SetParent(_triangleBuilderHolder.transform);
            _wiresObject.transform.SetParent(_triangleBuilderHolder.transform);
        }

        /// <summary>
        /// Init TouchDown, TouchChanged and StateChanged events.
        /// </summary>
        private void InitEvents()
        {
            OnChangedState += TriangleBuilder_OnChangedState;
            if (!pose)
                return;
            touchAction?.AddOnChangeListener(OnTouchDownChanged, pose.inputSource);
            touchPositionAction?.AddOnChangeListener(OnTouchChanged, pose.inputSource);
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Event where the game objects and interactions functionality will get turned off and on, depending on states "from" and "to".
        /// </summary>
        /// <param name="from">Old state</param>
        /// <param name="to">New state</param>
        private void TriangleBuilder_OnChangedState(TriangleInteractionState from, TriangleInteractionState to)
        {
            switch (from)
            {
                case TriangleInteractionState.None: break;
                case TriangleInteractionState.PlaceVertices:
                    RemoveColorPicker();
                    // Remove current controller attached vertex
                    DestroyActiveVertex();
                    break;
                case TriangleInteractionState.MoveVertices:
                case TriangleInteractionState.BuildTriangles:
                case TriangleInteractionState.RemoveVertices:
                case TriangleInteractionState.DestroyTriangles:
                    // Drop selection
                    UnselectActiveObject();
                    // Do not react on vertex collisions
                    SetVerticesCollidersState(colliderEnabled: false);
                    break;
                default: break;
            }

            switch (to)
            {
                case TriangleInteractionState.None: break;
                case TriangleInteractionState.PlaceVertices:
                    PlaceColorPicker();
                    // Create a new attached vertex game object
                    NewActiveVertex();
                    break;
                case TriangleInteractionState.MoveVertices:
                case TriangleInteractionState.BuildTriangles:
                case TriangleInteractionState.RemoveVertices:
                case TriangleInteractionState.DestroyTriangles:
                    // Enable vertex collision
                    SetVerticesCollidersState(colliderEnabled: true);
                    break;
                default: break;
            }
        }

        private void OnTouchDownChanged(SteamVR_Action_Boolean fromAction, SteamVR_Input_Sources fromSource, bool isDown)
            => _isTouchDown = isDown;

        private void OnTouchChanged(SteamVR_Action_Vector2 fromAction, SteamVR_Input_Sources fromSource, Vector2 axis, Vector2 delta)
            => _touchPosition = axis;

        #endregion

        #region Color Picker

        private void PlaceColorPicker()
        {
            var gradient = colorPicker.GetComponent<ControllerGradient>();
            if (Learner.Instance.IsVR)
                // Set Gradient to primary hand
                gradient.PlaceOnController(Learner.Instance.GetPrimaryHand().GameObject);
            else if (colorPicker2D)
                colorPicker2D.gameObject.SetActive(true);
        }

        private void RemoveColorPicker()
        {
            if (!colorPicker)
                return;

            var gradient = colorPicker.gradientObject;
            // Disable color picker
            if (Learner.Instance.IsVR)
                gradient.SetActive(false);
            else if (colorPicker2D)
                colorPicker2D.gameObject.SetActive(false);
        }

        #endregion

        /// <summary>
        /// Mark "obj" as active vertex.
        /// </summary>
        private void SelectObject(TriangleBuilderObject obj)
        {
            _activeObject = obj;
            _activeObjectMr = obj.GetComponent<MeshRenderer>();
        }
        /// <summary>
        /// Deselect active object.
        /// </summary>
        private void UnselectActiveObject()
        {
            // Drop all references
            _activeObject = null;
            _activeObjectMr = null;
            _activeVertexHelper = null;
        }
        /// <summary>
        /// Update active vertex color dependend from trackpad position.
        /// </summary>
        private void UpdateVertexColor(Vector2 trackpadPosition)
        {
            var color = colorPicker.PickColor(trackpadPosition);
            SetVertexColor(color);
            //if (vertexStick)
            //    vertexStick.SetColor(color);
        }
        private void SetVertexColor(Color color)
        {
            _activeVertexColor = color;

            if (_activeObjectMr)
                _activeObjectMr.material.SetColor(Color1, color);
        }

        public void Clear()
        {
            UnselectActiveObject();
            SetInteractionState(TriangleInteractionState.None);
            DestroyActiveVertex();

            _trianglesObject.ForEachChild(child => child.Destroy());
            _wiresObject.ForEachChild(child => child.Destroy());
            _verticesObject.ForEachChild(child => child.Destroy());
        }

        public TriangleInteractionState GetInteractionState() => _interactionState;
        public void SetInteractionState(TriangleInteractionState state)
        {
            var oldState = _interactionState;
            _interactionState = state;
            OnChangedState?.Invoke(oldState, state);
            _handleCooldown = 1.0f;

            if (PlaceHandLaser == null)
                return;
            // Handle laser visibility
            PlaceHandLaser.showsAlways = state == TriangleInteractionState.None;

            if (!vertexStick) return;
            vertexStick.gameObject.SetActive(state != TriangleInteractionState.None);
            vertexStick.ball.SetActive(state != TriangleInteractionState.PlaceVertices);
        }

        public void ResumeInteraction()
        {
            // Recover remembered mode
            SetInteractionState(_pausedState);
            _isPaused = false;
        }

        public void PauseInteraction()
        {
            // Remember mode
            _pausedState = _interactionState;
            SetInteractionState(TriangleInteractionState.None);
            _isPaused = true;
        }

        public void SetNextInteractionState(TriangleInteractionState state) => _pausedState = state;

        private void UpdateActiveVertexPlacePosition()
        {
            // Let vertex follow controller relative point
            _activeObject.transform.position = PlacePosition;
            if (vertexStick)
            {
                vertexStick.transform.position = PlacePosition;
            }
        }
        /// <summary>
        /// Assign a new active vertex.
        /// </summary>
        private void NewActiveVertex()
        {
            var v = _vertexManager.CreateVertexObject(PlacePosition, _activeVertexColor);
            SelectObject(v);
            
            // set non VR interaction events
            if (!Learner.Instance.IsVR)
            {
                var mouseEvents = v.mouseInteractionEvents;
                mouseEvents.onMouseClick.AddListener(_ => HandleVertexOnNonVR(v));
            }
            
            v.transform.SetParent(_verticesObject.transform);
            var nextIndex = _verticesObject.transform.childCount - 1;
            v.SetIndex(nextIndex);

            if (placeHand)
                v.transform.SetParent(placeHand.transform);
            
            v.transform.position = PlacePosition;
        }

        private void HandleVertexOnNonVR(VertexComponent vertex)
        {
            switch (_interactionState)
            {
                case TriangleInteractionState.BuildTriangles:
                    SelectVertex(vertex);
                    break;
                case TriangleInteractionState.MoveVertices:
                    // release active vertex
                    if (_activeObject)
                        ReleaseActiveVertex();

                    var playerTransform = _learner.Player.GetTransform();
                    var playerPosition = playerTransform.position;
                    // get direction from vertex to player
                    var dir = vertex.transform.position - playerPosition;
                    var normalizedDir = dir.normalized;
                    // place player in front of vertex
                    playerPosition += (dir - normalizedDir * PlaceOffsetScale);

                    // update player position
                    playerTransform.position = playerPosition;
                    
                    // make vertex active to follow the camera
                    _activeObject = vertex;
                    _activeVertexHelper = _activeObject.GetComponent<VertexHelper>();

                    break;
                case TriangleInteractionState.RemoveVertices:
                    // fire event
                    OnRemovedVertex?.Invoke(vertex);
                    // kill vertex
                    DestroyVertex(vertex);
                    UpdateVerticesLabels();
                    break;
                case TriangleInteractionState.None:
                    break;
                case TriangleInteractionState.PlaceVertices:
                    break;
                case TriangleInteractionState.DestroyTriangles:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetVerticesCollidersState(bool colliderEnabled)
        {
            foreach (var vertex in _verticesObject.GetComponentsInChildren<VertexComponent>())
            {
                vertex.vertexCollider.enabled = colliderEnabled;
            }
        }

        private void Interactable_Triangle_TriggerExit(TriangleComponent triangle, Collider col)
        {
            triangle.GetComponent<TriangleHelper>().UnhighlightWires();
            UnselectActiveObject();
        }

        private void Interactable_Triangle_TriggerEnter(TriangleComponent triangle, Collider col)
        {
            triangle.GetComponent<TriangleHelper>().HighlightWires();
            SelectObject(triangle);
        }

        private void InitVertexInteractionEvents(VertexComponent vertexObject)
        {
            var interactable = vertexObject.interactableEvents;
            interactable.TriggerEnter += Interactable_Vertex_TriggerEnter;
            interactable.TriggerExit += Interactable_Vertex_TriggerExit;
        }

        private void Interactable_Vertex_TriggerExit(GameObject vertexGo, Collider col)
        {
            if (_isTriggerDown)
                return;

            var vc = vertexGo.GetComponent<VertexComponent>();
            vertexGo.GetComponent<Outline>().enabled = _selectedVertices.Contains(vc);

            if (vc == _collidedGameObject)
                _collidedGameObject = null;
        }

        private void Interactable_Vertex_TriggerEnter(GameObject vertex, Collider col)
        {
            if (_isTriggerDown)
                return;
            
            foreach (var v in _verticesObject.GetComponentsInChildren<VertexComponent>())
            {
                v.GetComponent<Outline>().enabled = _selectedVertices.Contains(v);
            }
            vertex.GetComponent<Outline>().enabled = true;
            _collidedGameObject = vertex.GetComponent<VertexComponent>();
        }


        private void DestroyActiveVertex()
        {
            if (!_activeObject)
                return;
            // Unset all active vertex values
            _activeVertexHelper = null;
            _activeObjectMr = null;
            Destroy(_activeObject.gameObject);
        }
        private void ReleaseActiveVertex()
        {
            if (!_activeObject)
                return;

            var activeVertex = (VertexComponent)_activeObject;
            
            _activeObject.transform.SetParent(_verticesObject.transform);
            _activeObject.transform.localEulerAngles = Vector3.zero;
            InitVertexInteractionEvents(activeVertex);

            switch (_interactionState)
            {
                case TriangleInteractionState.PlaceVertices:
                    OnCreatedVertex?.Invoke(activeVertex);
                    break;
                case TriangleInteractionState.MoveVertices:
                    OnMovedVertex?.Invoke(activeVertex);
                    break;
                case TriangleInteractionState.None:
                    break;
                case TriangleInteractionState.BuildTriangles:
                    break;
                case TriangleInteractionState.RemoveVertices:
                    break;
                case TriangleInteractionState.DestroyTriangles:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }


            UnselectActiveObject();
        }

        private void UpdateVerticesLabels()
        {
            var vertices = _verticesObject.GetComponentsInChildren<VertexComponent>();
            for (var i = 0; i < vertices.Length; i++)
            {
                var vertex = vertices[i];
                vertex.SetIndex(i);
            }
        }
        /// <summary>
        /// Handle pressing state.
        /// </summary>
        private void StartPressingTrigger()
        {
            if (_interactionState != TriangleInteractionState.None)
                PlaceHandLaser.enabled = false;

            _isTriggerDown = true;

            if (_interactionState != TriangleInteractionState.MoveVertices || !_collidedGameObject) return;
            _activeObject = _collidedGameObject;
            _activeObject.transform.SetParent(pose.transform);
            _activeVertexHelper = _activeObject.GetComponent<VertexHelper>();
        }
        private void UpdateActiveVertexConnections()
        {
            _activeVertexHelper.UpdateWiresOrientations();
            _activeVertexHelper.UpdateTriangleMeshes();
        }
        private void IsPressingTrigger()
        {
            if (_interactionState == TriangleInteractionState.MoveVertices && _activeObject && _activeVertexHelper)
            {
                UpdateActiveVertexConnections();
            } 
        }
        /// <summary>
        /// Handle pressing state.
        /// </summary>
        private void EndPressingTrigger()
        {
            _isTriggerDown = false;

            PlaceHandLaser.enabled = true;

            switch (_interactionState)
            {
                case TriangleInteractionState.None:
                    break;
                case TriangleInteractionState.BuildTriangles:
                    SelectVertex((VertexComponent)_collidedGameObject);
                    break;
                case TriangleInteractionState.DestroyTriangles:
                    DestroyTriangle((TriangleComponent)_activeObject);
                    UnselectActiveObject();
                    break;
                case TriangleInteractionState.PlaceVertices:
                    ReleaseActiveVertex();
                    NewActiveVertex();
                    break;
                case TriangleInteractionState.MoveVertices:
                    ReleaseActiveVertex();
                    break;
                case TriangleInteractionState.RemoveVertices:
                    OnRemovedVertex?.Invoke((VertexComponent)_collidedGameObject);
                    DestroyVertex((VertexComponent)_collidedGameObject);
                    UpdateVerticesLabels();
                    break;
                default: 
                    break;
            }
        }

        private void DestroyTriangle(TriangleComponent triangle)
        {
            if (!triangle)
                return;
            var triangleHelper = triangle.GetComponent<TriangleHelper>();
            
            OnRemovedTriangle?.Invoke(triangle, triangleHelper.vertices.ToArray());

            triangleHelper.DestroyWires();
            triangleHelper.DeassignVertices();
            triangle.gameObject.Destroy();
            UnselectActiveObject();
        }

        private void DestroyVertex(VertexComponent vertex)
        {
            if (!vertex)
                return;

            var vh = vertex.vertexHelper;
            foreach (var t in vh.Triangles)
                DestroyTriangle(t);
            vertex.transform.SetParent(null);
            vertex.gameObject.Destroy();
            UnselectActiveObject();
        }
        /// <summary>
        /// Select a vertex for connection to a triangle.
        /// </summary>
        private void SelectVertex(VertexComponent vertex)
        {
            if (!vertex)
                return;

            // Catch if the vertex was already selected
            if (_selectedVertices.Contains(vertex))
            {
                // Unselect vertex
                _selectedVertices.Remove(vertex);
                // Destroy it's active wires
                _collectedWires.ForEach(Destroy);
                _collectedWires.Clear();
                vertex.Unselect();
                // Stop here
                return;
            }
            // Collect the vertex
            _selectedVertices.Add(vertex);
            
            vertex.Select(vertexSelectionColor);

            // Collect existing wires
            var wires = ConnectVertices();
            if (wires == null)
                return;

            _collectedWires.AddRange(wires);
            // Skip if not valid vertices count
            if (_selectedVertices.Count != 3)
                return;
            // Translate GameObjects into Vertex structs
            var vertices = _selectedVertices.Select(v => Vertex.FromGameObject(v.gameObject)).ToArray();
            // Create triangle
            var triangle = CreateTriangle(vertices);
            // Assign vertices to triangle
            var triangleVertRef = triangle.GetComponent<TriangleHelper>();
            triangleVertRef.AssignVertices(_selectedVertices);
            triangleVertRef.AssignWires(_collectedWires);
            // Add triangle reference to vertices
            _selectedVertices.ForEach(v => v.GetComponent<VertexHelper>().AddTriangle(triangle));
            // Send triangle to event
            OnCreatedTriangle?.Invoke(triangle, vertices: _selectedVertices.ToArray());
            // Deselect the vertices again
            UnselectVertices();
        }

        private GameObject CreateWire(VertexComponent from, VertexComponent to)
        {
            if (!from || !to)
                return null;

            // Create wire mesh
            var fromPos = from.transform.position;
            var toPos = to.transform.position;
            var lineObj = GameObject_Utils.CreateLine(fromPos, toPos, thickness: wireThickness, holder: _wiresObject);

            // Create vertex references
            var vertexPair = lineObj.AddComponent<VertexPair>();
            vertexPair.AddWireReference(from, to);

            return lineObj;
        }

        /// <summary>
        /// Connect two vertices with a wire object. if three vertices are selected then connect also the first and the last one to complete a triangle.
        /// </summary>
        /// <returns>Array with generated wire objects. This array can have 0-2 wire objects.</returns>
        private GameObject[] ConnectVertices()
        {
            // do only connect if minimum 2 vertices are selected
            if (_selectedVertices.Count < 2)
                return new GameObject[] { };

            // get previous selected vertex and the current selected (last)
            var last = _selectedVertices[_selectedVertices.Count - 1];
            var prev = _selectedVertices[_selectedVertices.Count - 2];

            // create a wire between both
            var w1 = CreateWire(@from: prev, to: last);
            if (!w1)
                return null;

            // if already selected three vertices, 
            // then create also a wire between the first and the last selected vertex
            if (_selectedVertices.Count != 3) return new[] { w1 };
            var first = _selectedVertices[0];
            var w2 = CreateWire(from: last, to: first);
            return new [] { w1, w2 };

        }

        private void UnselectVertices()
        {
            _selectedVertices.ForEach(vo => vo.Unselect());
            _selectedVertices.Clear();
            _collectedWires.Clear();
        }

        /// <summary>
        /// Create a mesh object out of provided vertices and indices.
        /// </summary>
        /// <param name="vertices">Vertices of object.</param>
        /// <param name="indices">Indices of object</param>
        /// <returns>GameObject with a mesh (triangle) in it.</returns>
        public GameObject CreateMeshGameObject(Vertex[] vertices, int[] indices)
        {
            // Create a mesh object
            var meshObject = new GameObject("Mesh Object");
            // Vertices game objects list
            var goVertices = vertices
                .Select(v => _vertexManager.CreateVertexObject(v.position, v.color)).ToArray();
            // Connect vertices
            for (var i = 0; i < indices.Length; i += 3)
            {
                // Get vertices
                var u = vertices[indices[i]];
                var v = vertices[indices[i + 1]];
                var w = vertices[indices[i + 2]];
                // Create triangle
                var triangle = CreateTriangle(new[] { u, v, w });
                // Assign vertices and wires game objects
                var th = triangle.GetComponent<TriangleHelper>();
                th.AssignVertices(goVertices);
                // Release triangle
                th.ReleaseTriangleAsMesh();
                // Append triangle to mesh objects
                triangle.transform.SetParent(meshObject.transform);
            }

            return meshObject;
        }

        public TriangleComponent CreateTriangle(Vertex[] vertices)
        {
            if (vertices.Length != 3)
                throw new Exception("A triangle should have 3 vertices. You provided " + vertices.Length + ".");

            var triangle = GameObject_Utils.CreateTriangle(vertices, _trianglesObject);
            triangle.GetComponent<MeshRenderer>().material = _triangleMaterial;

            // Add triangle helper
            var tc = triangle.AddComponent<TriangleComponent>();
            triangle.AddComponent<TriangleHelper>();

            OnCreatingTriangle?.Invoke(tc, null);

            return tc;
        }

        private void HandleTriggerPress()
        {
            if (!pose)
                return;

            var inputSource = pose.inputSource;
            if (interactAction.GetState(inputSource))
            {
                if (!interactAction.GetLastState(inputSource))
                    StartPressingTrigger();
                IsPressingTrigger();
            }
            else if (interactAction.GetLastState(inputSource))
                EndPressingTrigger();
        }

        // Update is called once per frame
        private void Update()
        {
            if (_isPaused)
                return;

            if (_activeObject && _interactionState == TriangleInteractionState.PlaceVertices)
                UpdateActiveVertexPlacePosition();

            if (!Learner.Instance.IsVR && _activeObject && _interactionState == TriangleInteractionState.MoveVertices)
            {
                UpdateActiveVertexPlacePosition();
                UpdateActiveVertexConnections();
            }

            if (_isTouchDown)
                UpdateVertexColor(_touchPosition);

            HandleTriggerPress();

            // Select mode by Space, N, M, P, B and R keys
            HandleKeyboard();
        }

        private void HandleKeyboard()
        {
            if (_handleCooldown > 0)
                _handleCooldown -= Time.deltaTime;

            if (Input.GetKeyUp(KeyCode.N))
                SetInteractionState(TriangleInteractionState.None);
            else if (Input.GetKeyUp(KeyCode.M))
                SetInteractionState(TriangleInteractionState.MoveVertices);
            else if (Input.GetKeyUp(KeyCode.P))
            {
                SetInteractionState(TriangleInteractionState.PlaceVertices);
            }
            else if (Input.GetKeyUp(KeyCode.B))
                SetInteractionState(TriangleInteractionState.BuildTriangles);
            else if (Input.GetKeyUp(KeyCode.R))
                SetInteractionState(TriangleInteractionState.RemoveVertices);
            else if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0) && _handleCooldown <= 0 && (colorPicker2D == null || !colorPicker2D.isPointerInteractingWith))
            {
                ReleaseActiveVertex();
            
                if (_interactionState == TriangleInteractionState.PlaceVertices)
                    NewActiveVertex();
            }
        }
    }
}