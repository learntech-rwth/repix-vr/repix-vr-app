using System.Collections;
using System.Collections.Generic;
using System.Linq;
using OmiLAXR.SceneManagement.Runtime;
using UnityEngine;
using UnityEngine.UI;


public class StageManager2D : MonoBehaviour
{
    private Button _button;
    private GameObject _sceneManager;
    private GameObject _2dCanvas;
    private bool _settingsFirstOpening = false;

    public Button closeStageSelection;

    void Start()
    {
        
        
        _button = gameObject.GetComponent<Button>();
        _button.onClick.AddListener(() => OpenSceneManager());
        
        _sceneManager = FindObjectOfType<SceneQueue>(true).gameObject;
        _sceneManager.transform.localScale = Vector3.zero;
        _2dCanvas = gameObject.transform.parent.gameObject;

        if (closeStageSelection)
            closeStageSelection.onClick.AddListener(() => DeactivateSceneSelection());

    }

    private void OpenSceneManager()
    {
        
        _2dCanvas.SetActive(false);
        
        if (_sceneManager)
            _sceneManager.SetActive(true);
        
        if (closeStageSelection)
            closeStageSelection.gameObject.SetActive(true);

        if (!_settingsFirstOpening)
        {
            _settingsFirstOpening = true;
            Changingto2DCanvas();
        }
        

    }

    private void Changingto2DCanvas()
    {
        _sceneManager.transform.localScale = Vector3.one;
        
        //change canvas settings 
        _sceneManager.GetComponentInChildren<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
        _sceneManager.GetComponentInChildren<StageOverviewBody>().gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(150, 150);
        _sceneManager.GetComponentInChildren<VerticalLayoutGroup>().childControlWidth = false;
        
        CanvasScaler _canvasScaler = _sceneManager.GetComponentInChildren<CanvasScaler>();
        _canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        _canvasScaler.referenceResolution = new Vector2(100f, 600f);
        _canvasScaler.matchWidthOrHeight = 0.5f;

        //deactivate stageSelection when button pressed
        StageButtons[] _stageButtons = _sceneManager.GetComponentsInChildren<StageButtons>();
        foreach (var _stageButton in _stageButtons)
        {
            Button[] _buttons = _stageButton.gameObject.GetComponentsInChildren<Button>();
            foreach (var _button in _buttons)
            {
                _button.onClick.AddListener(() => DeactivateSceneSelection());
            }
        }
        
        
    }
    

    private void DeactivateSceneSelection()
    {
        _2dCanvas.SetActive(true);
        
        if (_sceneManager)
            _sceneManager.SetActive(false);
        
        if (closeStageSelection)
            closeStageSelection.gameObject.SetActive(false);
    }
}
