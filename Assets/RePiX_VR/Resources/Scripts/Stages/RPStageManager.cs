﻿using UnityEngine.Events;

namespace RePiX_VR
{
    public abstract class RPStageManager : StageManager
    {
        public UnityEvent onEnabledNextStage = new UnityEvent();
        public void EnableNextStage() => onEnabledNextStage?.Invoke();
    }
}