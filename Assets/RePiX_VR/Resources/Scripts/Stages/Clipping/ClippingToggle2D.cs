using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR
{
    public class ClippingToggle2D : MonoBehaviour
    {
        private UnitFrustum _unitFrustum;
        private Toggle _toggle;

        void Start()
        {
            _unitFrustum = FindObjectOfType<UnitFrustum>().GetComponent<UnitFrustum>();
            _toggle = this.GetComponent<Toggle>();
            _toggle.onValueChanged.AddListener(_ => _unitFrustum.FireToggleClipping());
        }
    }
}
