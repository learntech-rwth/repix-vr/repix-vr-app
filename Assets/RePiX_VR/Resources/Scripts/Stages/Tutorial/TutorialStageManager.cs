using OmiLAXR.Helpers;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

namespace RePiX_VR
{
    public class TutorialStageManager : StageManager
    {
        [Header("Inner Tutorial")]
        public CounterBasedEvent teleportCounter;
        public Button repeatButton;
        public Button skipButton;
        public Button nextStageButton;

        [Header("Outer Tutorial")]
        public TeleportArea teleportArea;

        protected override void Start()
        {
            base.Start();

            if (nextStageButton)
            {
                nextStageButton.onClick.AddListener(NextStage);
            }
        }

        protected override void RunStage()
        {
            base.RunStage();
            
            if (skipButton)
                skipButton.gameObject.SetActive(false);
        }

        public override void RepeatStage()
        {
            base.RepeatStage();
            
            if (skipButton)
                skipButton.gameObject.SetActive(true);
        }

        public override void ResetStage()
        {
            teleportCounter.ResetCounter();
            base.ResetStage();
        }

        public void SetActiveNextStageButton(bool active)
        {
            if (nextStageButton)
                nextStageButton.gameObject.SetActive(active);
        }

        public void SelectingNextStage()
        {
            if (skipButton)
                skipButton.gameObject.SetActive(false);
            
            SetActiveNextStageButton(true);
            
            if (repeatButton)
                repeatButton.gameObject.SetActive(true);
        }
        
        public void SetActiveTeleportArea(bool active)
        {
            if (teleportArea)
                teleportArea.gameObject.SetActive(active);
        }
    }

}