﻿using System;
using OmiLAXR;
using OmiLAXR.UI;
using OmiLAXR.InstructionalDesign;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace RePiX_VR
{
    public abstract class StageManager : MonoBehaviour
    {
        [Header("Stage Manager Fields")] 
        public bool autoStart = true;
        public LearningUnit learningUnit;
        public PlayPauseManager playPauseManager;
        public PlayableDirector director;
        
        [FormerlySerializedAs("OnReset")] 
        public UnityEvent onReset = new UnityEvent();
        public UnityEvent onNext = new UnityEvent();
        
        protected virtual void Start()
        {
            if (autoStart)
                StartStage();
        }
        
        public void StartStage()
        {
            ResetStage();

            if (learningUnit)
                learningUnit.Begin();
            
            RunStage();
        }
        
        public virtual void RepeatStage()
        {
            ResetStage();
            RunStage();
        }

        protected virtual void RunStage()
        {
            playPauseManager.Run(director);
        }

        public virtual void ResetStage()
        {
            onReset?.Invoke();
            playPauseManager.Stop();
        }

        public virtual void SkipStage()
        {
            
        }

        protected virtual void NextStage()
        {
            onNext.Invoke();
            gameObject.SetActive(false);
        }

        public void FireNextStage()
            => NextStage();
    }
}