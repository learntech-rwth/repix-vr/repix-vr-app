using OmiLAXR.Interaction;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RePiX_VR
{
    [RequireComponent(typeof(PointableUI), typeof(Slider))]
    public class FrustrumSlider : MonoBehaviour
    {
        public enum AspectFOV
        {
            Aspect = 0,
            FOV = 1,
        };
        
        public Slider slider;

        [Tooltip("Frustum instance to apply on.")]
        public Frustum frustum;
        [HideInInspector] public float maxValue;
        [HideInInspector] public float minValue;

        public AspectFOV aspectFOV;

        public TextMeshProUGUI minText;
        public TextMeshProUGUI maxText;
        public TextMeshProUGUI valueText;

        public Slider.SliderEvent onValueChanged = new Slider.SliderEvent();
        
        protected void Awake()
        {
            if (!frustum)
                frustum = FindObjectOfType<Frustum>();

            if (!slider)
                slider = GetComponent<Slider>();

            if (aspectFOV == AspectFOV.Aspect) {
                maxValue = Frustum.Aspect_Max;
                minValue = Frustum.Aspect_Min;
            } 
            
            if (aspectFOV == AspectFOV.FOV) {
                maxValue = Frustum.Fov_Max;
                minValue = Frustum.Fov_Min;
            }

            SetupLabels();
            slider.onValueChanged.AddListener(OnChangedValue);
        }

        public void SetValue(float value, bool withoutNotify = false)
        {
            if (withoutNotify)
                slider.SetValueWithoutNotify(value);
            else
                slider.value = value;
        }

        private void SetupLabels()
        {
            if (!slider)
                return;
            if (minText)
                minText.text = $"{slider.minValue}";
            if (maxText)
                maxText.text = $"{slider.maxValue}";

            if (!valueText)
                return;

            //var floatPrecisionSlider = GetComponent<PointableUI>().sliderPrecision;
            
            var precision = "0.0";
            //for (var i = 0; i < floatPrecisionSlider - 1; i++)
            //    precision += "0";

            valueText.text = slider.value.ToString(precision);
            slider.onValueChanged.AddListener(value => valueText.text = value.ToString(precision));
        }

        public void OnChangedValue(float value)
        {
            if (aspectFOV == AspectFOV.Aspect)
                frustum.aspect = value;

            if (aspectFOV == AspectFOV.FOV)
                frustum.fov = value;
            
            frustum.FireChange(this);
            onValueChanged?.Invoke(value);
        }
    }
}
