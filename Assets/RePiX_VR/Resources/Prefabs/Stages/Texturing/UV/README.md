## How to set up a mesh with Texture Paint

In order to use Texture Paint, currently there are some objects required in the scene:

* A instance of either `NonVR_Controls` or `VR_Controls` (or both, but they can't be active at the
  same time; e.g. use the `VREnabler` script component to toggle instances)
* A instance of the `Pen` prefab, which has to at least have the `TexturePen script`
* A game object with the `TexturePaint` script, this can but doesn't need to be the mesh to be
  painted on.  
  The script must be set up as follows:
  * **Base Texture:** the target materials default color, which is also used to clear the material
  * **Mesh Material:** the main material used by the mesh that we want to paint on
  * **Mesh GameObject:** the game object we want to paint on
  * **UV Shader:** the `Texture Painter/TexturePaint` shader
  * **Mesh To Draw:** the mesh object of the game object we want to draw on
  * **Island Marker Shader:** the `Texture Painter/MarkIslands` shader
  * **Fix Ilsand Edges Shader:** the `Texture Painter/FixIslandEdges` shader
  * **Uv Material:** keep empty

The material set under `MeshMaterial` will be updated continuously by the script, thus any other
object making use of this material will also show these updates. This can be used with a simple
quad to get a representation of the current texture map of the material.  

By attaching the `UVPainter` script to the quad mesh and registering the `TexturePaint` shader from
above and the `TexturePaint` instance of the desired mesh this quad allows painting directly onto
the meshes texture map (respecting the UV islands).

Please note that all mesh instances on which we wish to draw must be marked with the `PaintObject`
label and possess a valid collider (e.g. mesh collider).
