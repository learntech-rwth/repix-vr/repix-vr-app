using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage;
using UnityEngine;

public class RotationMatrixHasRotationCondition : CorrectStateCondition
{
    private bool _hadRotation;

    protected override void SetupCheckListeners()
    {
        var rotMatrix = FindObjectOfType<RotationMatrix>(true);
        if (!rotMatrix) return;
        rotMatrix.OnChanged += OnChanged;
    }

    private void OnChanged(TransformationMatrix matrix)
    {
        var rotMatrix = matrix as RotationMatrix;
        if (!rotMatrix) return;

        var rotation = Mathf.Abs(rotMatrix.RotationSpeedX) + Mathf.Abs(rotMatrix.RotationSpeedX) +
                       Mathf.Abs(rotMatrix.RotationSpeedX);
        var hasRotation = rotation > 0;

        if (hasRotation && !_hadRotation)
            SetCorrect(true);
        if (!hasRotation && _hadRotation)
            SetCorrect(false);
        _hadRotation = hasRotation;
    }
}