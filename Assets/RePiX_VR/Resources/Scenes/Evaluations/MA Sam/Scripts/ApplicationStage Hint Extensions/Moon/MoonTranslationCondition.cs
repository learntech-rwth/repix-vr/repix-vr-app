﻿using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;

public class MoonTranslationCondition : CorrectStateCondition
{
    protected override void SetupCheckListeners()
    {
        var moonChecker = FindObjectOfType<MoonChecker>(true);
        if (!moonChecker) return;
        moonChecker.onCorrectMoonTranslation.AddListener(() => SetCorrect(true));
        moonChecker.onIncorrectMoonTranslation.AddListener(() => SetCorrect(false));
    }
}