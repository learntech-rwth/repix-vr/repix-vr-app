﻿using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;

namespace RePiX_VR.Resources.Scenes.Evaluations.MA_Sam.ApplicationStageConditions.Moon
{
    public class MoonScaleCondition : CorrectStateCondition
    {
        protected override void SetupCheckListeners()
        {
            var moonChecker = FindObjectOfType<MoonChecker>(true);
            if (!moonChecker) return;
            moonChecker.onCorrectMoonRatio.AddListener(() => SetCorrect(true));
            moonChecker.onIncorrectMoonRatio.AddListener(() =>SetCorrect(false));
        }
    }
}