﻿using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;

public class SunRotationCondition : CorrectStateCondition
{
    protected override void SetupCheckListeners()
    {
        var sunChecker = FindObjectOfType<SunChecker>(true);
        if (!sunChecker) return;
        sunChecker.onHasRotation.AddListener(() => SetCorrect(true));
        sunChecker.onHasNoRotation.AddListener(() => SetCorrect(false));
    }
}