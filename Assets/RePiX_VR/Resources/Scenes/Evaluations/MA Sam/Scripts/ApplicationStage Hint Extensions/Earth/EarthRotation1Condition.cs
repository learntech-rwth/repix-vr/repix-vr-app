﻿using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;

public class EarthRotation1Condition : CorrectStateCondition
{
    protected override void SetupCheckListeners()
    {
        var earthChecker = FindObjectOfType<EarthChecker>(true);
        if (!earthChecker) return;
        earthChecker.onCorrectEarthFirstRotation.AddListener(() => SetCorrect(true));
        earthChecker.onIncorrectEarthFirstRotation.AddListener(() => SetCorrect(false));
    }
}