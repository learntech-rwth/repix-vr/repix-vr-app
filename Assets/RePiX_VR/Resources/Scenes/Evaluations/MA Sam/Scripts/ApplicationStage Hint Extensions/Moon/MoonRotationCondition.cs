﻿using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;

namespace RePiX_VR.Resources.Scenes.Evaluations.MA_Sam.ApplicationStageConditions.Moon
{
    public class MoonRotationCondition : CorrectStateCondition
    {
        protected override void SetupCheckListeners()
        {
            var moonChecker = FindObjectOfType<MoonChecker>(true);
            if (!moonChecker) return;
            moonChecker.onCorrectMoonRotation.AddListener(() => SetCorrect(true));
            moonChecker.onIncorrectMoonRotation.AddListener(() => SetCorrect(false));
        }
    }
}