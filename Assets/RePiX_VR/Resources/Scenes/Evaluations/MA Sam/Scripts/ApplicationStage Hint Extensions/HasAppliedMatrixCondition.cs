﻿using System.Collections.Generic;
using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage;
using RePiX_VR.ApplicationStage.InstructionLogic;

public class HasAppliedMatrixCondition : CorrectStateCondition
{
    public PlanetChecker planetChecker;

    private List<TransformationMatrix> _transformations = new List<TransformationMatrix>();

    protected override void SetupCheckListeners()
    {
        planetChecker.planet.trsMatrix.onAppliedMatrix.AddListener(OnAppliedMatrix);
        planetChecker.planet.trsMatrix.onRemovedMatrix.AddListener(OnRemovedMatrix);
    }

    private void OnAppliedMatrix(TransformationMatrix matrix)
    {
        _transformations.Add(matrix);
        if (_transformations.Count > 0)
        {
            SetCorrect(true);
        }
    }

    private void OnRemovedMatrix(TransformationMatrix matrix)
    {
        _transformations.Remove(matrix);
        if (_transformations.Count <= 0)
        {
            SetCorrect(false);
        }
    }
}