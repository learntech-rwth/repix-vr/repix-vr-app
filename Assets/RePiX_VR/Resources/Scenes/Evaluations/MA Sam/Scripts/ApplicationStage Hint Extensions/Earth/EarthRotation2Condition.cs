﻿using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;

public class EarthRotation2Condition : CorrectStateCondition
{
    protected override void SetupCheckListeners()
    {
        var earthChecker = FindObjectOfType<EarthChecker>(true);
        if (!earthChecker) return;
        earthChecker.onCorrectEarthSecondRotation.AddListener(() => SetCorrect(true));
        earthChecker.onIncorrectEarthSecondRotation.AddListener(() => SetCorrect(false));
    }
}