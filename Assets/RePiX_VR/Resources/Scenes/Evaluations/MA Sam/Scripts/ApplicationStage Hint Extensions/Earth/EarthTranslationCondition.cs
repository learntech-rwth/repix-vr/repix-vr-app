﻿using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;


public class EarthTranslationCondition : CorrectStateCondition
{
    protected override void SetupCheckListeners()
    {
        var earthChecker = FindObjectOfType<EarthChecker>(true);
        if (!earthChecker) return;
        earthChecker.onCorrectEarthTranslation.AddListener(() => SetCorrect(true));
        earthChecker.onIncorrectEarthTranslation.AddListener(() => SetCorrect(false));
    }
}