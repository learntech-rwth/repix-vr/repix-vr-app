﻿using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;

public class EarthScaleCondition : CorrectStateCondition
{
    protected override void SetupCheckListeners()
    {
        var earthChecker = FindObjectOfType<EarthChecker>(true);
        if (!earthChecker) return;
        earthChecker.onCorrectSunEarthRatio.AddListener(() => SetCorrect(true));
        earthChecker.onIncorrectSunEarthRatio.AddListener(() => SetCorrect(false));
    }
}