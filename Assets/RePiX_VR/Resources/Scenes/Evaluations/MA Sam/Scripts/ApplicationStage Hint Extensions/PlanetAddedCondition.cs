using EduXR.Assistance.HintSystem;
using RePiX_VR.ApplicationStage.InstructionLogic;

public abstract class PlanetAddedCondition<T> : CorrectStateCondition where T : PlanetChecker
{ 
    protected override void SetupCheckListeners()
    {
        var checker = FindObjectOfType<T>(true);
        // if (!checker) return;
        checker.onPlacedInCoordinateSystem.AddListener(() => SetCorrect(true));
        checker.onRemovedFromCoordinateSystem.AddListener(() => SetCorrect(false));
    }
}