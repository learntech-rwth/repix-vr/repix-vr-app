﻿using OmiLAXR;
using EduXR.Assistance;
using OmiLAXR.Timeline.MultiLanguage;
using UnityEngine;

namespace RePiX_VR.Resources.Scenes.Evaluations.MA_Sam.Scripts
{
    [RequireComponent(typeof(AgentSpeakSimple))]
    public class AgentSpeakSimpleLanguageSelector : MonoBehaviour
    {
        [SerializeField]
        private AudioClip englishAudio;

        [SerializeField]
        private AudioClip germanAudio;

        private AgentSpeakSimple _agentSpeakSimple;

        private void Awake()
        {
            _agentSpeakSimple = GetComponent<AgentSpeakSimple>();
            LearningEnvironment.Instance.OnChangedLanguage += SetLanguage;
            SetLanguage(LearningEnvironment.Instance.language);
        }

        private void SetLanguage(MultiLanguageLanguages language)
        {
            _agentSpeakSimple.speechAudioClip = language switch
            {
                MultiLanguageLanguages.English => englishAudio,
                MultiLanguageLanguages.German => germanAudio,
                _ => englishAudio
            };
        }
    }
}