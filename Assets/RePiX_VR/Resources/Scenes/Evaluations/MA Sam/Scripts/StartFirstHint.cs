using OmiLAXR.Adapters.ReCoPa;
using EduXR.Assistance.HintSystem;
using OmiLAXR.xAPI.Tracking;
using UnityEngine;

public class StartFirstHint : MonoBehaviour
{
    [SerializeField] private HintTimeBased hint;

    private void Start()
    {
        if (!hint) return;

        hint.StartHintTimer();
        //RecopaConnector.Instance.onConnected.AddListener(() => hint.StopHintTimer());
        //RecopaConnector.Instance.onConnected.AddListener(() =>
        //    Debug.LogWarning("First Hint will start when tracking starts."));
        //MainTrackingSystem.Instance.OnStartedTracking += (_, _) => hint.StartHintTimer();
    }
}