﻿Shader "RenderingPipeline/FrustumObject"
{
	Properties { 
		_Color ("Tint", Color) = (0, 0, 0, 1)
		_MainTex ("Texture", 2D) = "white" {}
		_Smoothness ("Smoothness", Range(0, 1)) = 0
		_Metallic ("Metalness", Range(0, 1)) = 0
		[HDR]_Emission ("Emission", color) = (0,0,0)
		[HDR]_CutoffColor("Cutoff Color", Color) = (1,0,0,0)
	}
    SubShader
    {
		Tags { "Queue" = "Transparent" "RenderType" = "Opaque" }

		Cull Off

		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows vertex:vert
		#pragma target 3.0

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
			float3 localPos;
			float facing : VFACE;
		};

		struct Output {
			float4 pos : SV_POSITION;
			float4 posInObjectCoords : TEXCOORD0;
		};

		sampler2D _MainTex;
		fixed4 _Color;
		float3 _Center;
		float3 _Scale;
		float _DoClipping;

		half _Smoothness;
		half _Metallic;
		half3 _Emission;

		float4 _CutoffColor;

		void vert(inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.localPos = v.vertex.xyz;
		}

		void surf(Input i, inout SurfaceOutputStandard o) {
			float3 pos = i.worldPos - _Center;

			// Do frustum clipping
			if (pos.y > 1 || pos.y < -1 || pos.z < -1 || pos.z > 1 || pos.x < -1 || pos.x > 1) {
				_Color = _CutoffColor;

				if (_DoClipping > 0)
					clip(-1);
			}

			float facing = i.facing * 0.5 + 0.5;

			// set material color
			fixed4 col = tex2D(_MainTex, i.uv_MainTex);
			col *= _Color;
			o.Albedo = col.rgb * facing;
			o.Metallic = _Metallic * facing;
			o.Smoothness = _Smoothness * facing;
			o.Emission = lerp(_CutoffColor, _Emission, facing);
		}

		ENDCG
    }
	FallBack "Standard"
}
