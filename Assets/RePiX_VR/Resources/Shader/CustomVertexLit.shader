﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/CustomVertexLit"
{
    Properties
    {
        _Shininess ("Shininess", Float) = 10
        _AmbientColor ("Ambient Color", Color) = (0.0, 0.0, 0.0, 1.0)
        _AmbientTerm ("Ambient Term", Float) = 1.0
        _DiffuseTerm ("Diffuse Term", Float) = 1.0
        _SpecularTerm ("Specular Term", Float) = 1.0
        _Attenuation ("Attenuation Fall-off", Float) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            #define NUM_LIGHTS 16

            // Lighting model parameters
            fixed4 _AmbientColor;
            float _AmbientTerm;

            fixed4x4 _DiffuseMat;
            float _DiffuseTerm;
            
            fixed4x4 _SpecularMat;
            float _SpecularTerm;

            float _Shininess;
            int _Model = 0;

            float _Attenuation;
            
            // Light data
            float4 _LightPositions[NUM_LIGHTS];
            fixed4 _LightColors[NUM_LIGHTS];
            float4 _LightDirections[NUM_LIGHTS];

            float4 _StencilPosition;
            fixed4 _StencilColor;
            float4 _StencilDirection;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 col : TEXCOORD0;
            };

            float3 light_color(float4 pos, const float3 color, float4 dir, float4 w_pos, const float3 normal, const float3 view)
            {
                const float intensity = pos.w;
                if (intensity == 0.0) return float3(0.0, 0.0, 0.0);

                const float3 vert_to_light = pos.xyz - w_pos.xyz;
                const float distance = length(vert_to_light);
                const float3 to_light_dir = normalize(vert_to_light);

                float attenuation = intensity / distance;
                attenuation = attenuation < 1.0 ? pow(attenuation, _Attenuation) : attenuation;

                const float3 from_light_dir = normalize(dir.xyz);
                const float angle = dir.w;
                
                if (angle > 0.0)
                {
                    attenuation = acos(dot(from_light_dir, -to_light_dir)) < angle / 90.0 ? attenuation : 0.0;
                }

                const float3 diffuse = attenuation * mul(_DiffuseMat, color) * max(0.0, dot(normal, to_light_dir)); 
                
                float shininess;
                
                if (dot(normal, to_light_dir) < 0) shininess = 0;
                else
                {
                    if (_Model == 1)
                    {
                        const float3 halfway = view + to_light_dir;
                        shininess = dot(halfway, normal) / (length(halfway) * length(normal));
                    }
                    else
                    {
                        shininess = max(0.0, dot(reflect(-to_light_dir, normal), view));
                    }
                }

                const float3 specular = attenuation * mul(_SpecularMat, color) * pow(shininess, _Shininess);
                return _DiffuseTerm * diffuse + _SpecularTerm * specular;
            }
            
            v2f vert (appdata v)
            {
                v2f o;
                
                const float4 w_pos = mul(unity_ObjectToWorld, v.vertex); // world position
                const float3 normal = normalize(mul(float4(v.normal, 0), unity_WorldToObject).xyz); // world coord normal
                o.pos = UnityObjectToClipPos(v.vertex); // model position

                const float3 normal_dir = normalize(normal);
                const float3 view_dir = normalize(_WorldSpaceCameraPos - w_pos);

                const float3 ambient = _AmbientColor;
                
                float3 color = light_color(_StencilPosition, _StencilColor, _StencilDirection, w_pos, normal_dir, view_dir);
                
                for (int j = 0; j < NUM_LIGHTS; j++)
                {
                    color += light_color(_LightPositions[j], _LightColors[j], _LightDirections[j], w_pos, normal_dir, view_dir);
                }
                
                o.col = float4(_AmbientTerm * ambient + color, 0.0);
                return o;
            }
            
            float4 frag (v2f o) : SV_Target
            {
                return o.col;
            }
            ENDCG
        }
    }
}
