﻿
// By Shahriar Shahrabi (2019)
// See: https://github.com/IRCSS/TexturePaint/blob/master/Assets/Scripts/MarkIlsands.shader

Shader "Texture Painter/MarkIslands"
{
	SubShader
	{
		// =====================================================================================================================
		// TAGS AND SETUP ------------------------------------------
		Tags{ "RenderType" = "Opaque" }
		LOD	   100
		ZTest  Off
		ZWrite Off
		Cull   Off

		Pass
	{
		CGPROGRAM
		// =====================================================================================================================
		// DEFINE AND INCLUDE ----------------------------------
		#pragma vertex   vert
		#pragma fragment frag


		#include "UnityCG.cginc"

		// =====================================================================================================================
		// DECLERANTIONS ----------------------------------
		struct appdata
	{
		float4 vertex   : POSITION;
		float2 uv	    : TEXCOORD0;
	};

	struct v2f
	{
		float4 vertex   : SV_POSITION;
	};



	// =====================================================================================================================
	// VERTEX FRAGMENT ----------------------------------

	v2f vert(appdata v)
	{
		v2f o;

		float2 uvRemapped = v.uv.xy;

		#if UNITY_UV_STARTS_AT_TOP
			uvRemapped.y = 1. - uvRemapped.y;
		#endif

		uvRemapped = uvRemapped * 2. - 1.;

			   o.vertex = float4(uvRemapped.xy, 0., 1.);

		return o;
	}

	fixed4 frag(v2f i) : SV_Target
	{
		return float4(1.,0.,0.,1.);
	}
		ENDCG
	}

	}
}
