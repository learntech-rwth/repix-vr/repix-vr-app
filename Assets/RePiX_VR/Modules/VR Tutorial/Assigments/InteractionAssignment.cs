﻿using OmiLAXR;
using OmiLAXR.Interaction;
using OmiLAXR.InstructionalDesign;
using OmiLAXR.Timeline;
using UnityEngine;

namespace VR_Tutorial
{
    public class InteractionAssignment : MonoBehaviour
    {
        public Assignment assignment;

        public Pointable hoverableCube;
        public Pointable clickableCube;

        public PlayPauseDirector playPauseDirector;

        private void Awake()
        {
            hoverableCube.onHover.AddListener(_ =>
            {
                assignment.DoneStep("hover");
                playPauseDirector.DoneAction("hover");
            });
            clickableCube.onClick.AddListener(_ =>
            {
                assignment.DoneStep("click");
                playPauseDirector.DoneAction("click");
            });
        }
    }

}