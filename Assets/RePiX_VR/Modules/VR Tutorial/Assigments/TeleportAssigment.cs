﻿using OmiLAXR;
using OmiLAXR.Adapters.SteamVR;
using OmiLAXR.Helpers;
using OmiLAXR.Interaction;
using OmiLAXR.InstructionalDesign;
using OmiLAXR.Timeline;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace VR_Tutorial
{
    public class TeleportAssigment : MonoBehaviour
    {
        [Header("Inner TeleportAssignment")]
        public TeleportPoint teleportPoint1;
        public TeleportPoint teleportPoint2;
        public TeleportPoint teleportPoint3;
        public TeleportPoint teleportPointFinal;
        public CounterBasedEvent counterBasedEvent;

        [Header("Outer TeleportAssignment")]
        public Assignment assignment;
        public PlayPauseDirector playPauseDirector;
        
        private void Awake()
        {
            assignment.onBegin.AddListener(Begin);
            assignment.onEnd.AddListener(End);
            
            BindTeleportPointEvents(teleportPoint1);
            BindTeleportPointEvents(teleportPoint2);
            BindTeleportPointEvents(teleportPoint3);
            BindTeleportPointEvents(teleportPointFinal, isFinal: true);
        }

        public void Begin()
        {
            ResetAssignment();
            teleportPoint1.gameObject.SetActive(true);
            teleportPoint2.gameObject.SetActive(true);
            teleportPoint3.gameObject.SetActive(true);
            assignment.Restart();
        }

        public void End()
        {
            ResetAssignment();
        }

        private void ResetAssignment()
        {
            teleportPoint1.gameObject.SetActive(false);
            teleportPoint2.gameObject.SetActive(false);
            teleportPoint3.gameObject.SetActive(false);
            teleportPointFinal.gameObject.SetActive(false);
            counterBasedEvent.ResetCounter();
        }

        private void BindTeleportPointEvents(TeleportPoint teleportPoint, bool isFinal = false)
        {
            
            // bind teleport events
            var teleportEvents = teleportPoint.GetComponent<SteamVR_TeleportEvents>();
            teleportEvents.OnVisit.AddListener(() =>
            {
                if (isFinal)
                {
                    playPauseDirector.DoneAction("teleport");
                    counterBasedEvent.ResetCounter();
                }
                else
                {
                    counterBasedEvent.IncreaseCounter();
                }
                
                teleportPoint.Highlight(false);
                teleportPoint.gameObject.SetActive(false);
                
                assignment.DoneStep();
            });
        }
    }

}