using System.Collections.Generic;

namespace xAPI.Registry {
    /// <summary>
    /// Provides all extensions of the context virtualReality of type result as public properties.
    /// </summary>
    public sealed class xAPI_Extensions_Result_VirtualReality : xAPI_Extensions_Result {

        public xAPI_Extensions_Result_VirtualReality() 
            : base("virtualReality") {
        }

        /// <summary>
        /// Position in scene.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/virtualReality/extensions/result/position
        /// </summary>
        public xAPI_Extensions_Result_VirtualReality position(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "position",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "position",
                        ["de-DE"] = "Position" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Position in scene.",
                        ["de-DE"] = "Position in der Szene." }),
                 value);
            return this;
        }

        /// <summary>
        /// Rotation in scene.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/virtualReality/extensions/result/rotation
        /// </summary>
        public xAPI_Extensions_Result_VirtualReality rotation(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "rotation",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "rotation",
                        ["de-DE"] = "Rotation" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Rotation in scene.",
                        ["de-DE"] = "Rotation in der Szene." }),
                 value);
            return this;
        }

        /// <summary>
        /// Scale in scene.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/virtualReality/extensions/result/scale
        /// </summary>
        public xAPI_Extensions_Result_VirtualReality scale(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "scale",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "scale",
                        ["de-DE"] = "Skalierung" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Scale in scene.",
                        ["de-DE"] = "Skalierung in der Szene." }),
                 value);
            return this;
        }
    }
}