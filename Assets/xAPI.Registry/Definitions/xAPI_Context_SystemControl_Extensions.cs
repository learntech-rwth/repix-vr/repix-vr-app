
namespace xAPI.Registry {
    /// <summary>
    /// Provides the extensions of the context systemControl as public properties.
    /// </summary>
    public sealed class xAPI_Context_SystemControl_Extensions {

        public xAPI_Context_SystemControl_Extensions() {
        }

        /// <summary>
        /// 4 extensions of 'systemControl': 3 in activity, 1 in result.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/systemControl/extensions/activity
        /// </summary>
        public xAPI_Extensions_Activity_SystemControl activity {
            get {
                return new xAPI_Extensions_Activity_SystemControl();
            }
        }

        /// <summary>
        /// 4 extensions of 'systemControl': 3 in activity, 1 in result.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/systemControl/extensions/result
        /// </summary>
        public xAPI_Extensions_Result_SystemControl result {
            get {
                return new xAPI_Extensions_Result_SystemControl();
            }
        }
    }
}