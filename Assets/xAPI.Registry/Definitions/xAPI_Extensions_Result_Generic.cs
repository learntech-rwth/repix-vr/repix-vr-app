using System.Collections.Generic;

namespace xAPI.Registry {
    /// <summary>
    /// Provides all extensions of the context generic of type result as public properties.
    /// </summary>
    public sealed class xAPI_Extensions_Result_Generic : xAPI_Extensions_Result {

        public xAPI_Extensions_Result_Generic() 
            : base("generic") {
        }

        /// <summary>
        /// Representation of a object directly after the statement occurred. 'End Value' should be used with the 'Start Value' extension to provide information about the actual change of the object. Can be used with 'edited' or 'changed' verbs.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/generic/extensions/result/endValue
        /// </summary>
        public xAPI_Extensions_Result_Generic endValue(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "endValue",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "End Value",
                        ["de-DE"] = "Endwert" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Representation of a object directly after the statement occurred. 'End Value' should be used with the 'Start Value' extension to provide information about the actual change of the object. Can be used with 'edited' or 'changed' verbs.",
                        ["de-DE"] = "Repräsentation von einem Objekt direkt nachdem das Statement auftrat. 'Endwert' sollte mit der Extension 'Startwert' benutzt werden um Information über die Änderung eines Objektes darzulegen. Kann zum Beispiel mit den Verben 'editierte' und 'veränderte' benutzt werden." }),
                 value);
            return this;
        }

        /// <summary>
        /// A number to index something. Can only be an integer.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/generic/extensions/result/index
        /// </summary>
        public xAPI_Extensions_Result_Generic index(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "index",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "index",
                        ["de-DE"] = "Index" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "A number to index something. Can only be an integer.",
                        ["de-DE"] = "Eine Zahl zur Indexierung von etwas. Muss ganzzahlig sein." }),
                 value);
            return this;
        }

        /// <summary>
        /// Representation of a object directly before the statement occurred. 'Start Value' should be used with the 'End Value' extension to provide information about the actual change of the object. Can be used with 'edited' or 'changed' verbs.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/generic/extensions/result/startValue
        /// </summary>
        public xAPI_Extensions_Result_Generic startValue(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "startValue",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "Start Value",
                        ["de-DE"] = "Startwert" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "Representation of a object directly before the statement occurred. 'Start Value' should be used with the 'End Value' extension to provide information about the actual change of the object. Can be used with 'edited' or 'changed' verbs.",
                        ["de-DE"] = "Repräsentation von einem Objekt bevor das Statement auftrat. 'Startwert' sollte mit der Extension 'Endwert' benutzt werden um Information über die Änderung eines Objektes darzulegen. Kann zum Beispiel mit den Verben 'editierte' und 'veränderte' benutzt werden." }),
                 value);
            return this;
        }

        /// <summary>
        /// A time span depending on context.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/generic/extensions/result/timeSpan
        /// </summary>
        public xAPI_Extensions_Result_Generic timeSpan(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "timeSpan",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "Time Span",
                        ["de-DE"] = "Zeitspanne" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "A time span depending on context.",
                        ["de-DE"] = "Eine bestimmte Zeitspanne abhängig vom Kontext." }),
                 value);
            return this;
        }

        /// <summary>
        /// The value of an element.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/generic/extensions/result/value
        /// </summary>
        public xAPI_Extensions_Result_Generic value(object value) {
            Add(new xAPI_Extension(
                    context: Context,
                    extensionType: ExtensionType,
                    key: "value",
                    names: new Dictionary<string, string> {
                        ["en-US"] = "value",
                        ["de-DE"] = "Wert" },
                    descriptions: new Dictionary<string, string> {
                        ["en-US"] = "The value of an element.",
                        ["de-DE"] = "Der Wert eines Elements." }),
                 value);
            return this;
        }
    }
}