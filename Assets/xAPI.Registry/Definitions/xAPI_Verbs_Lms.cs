using System.Collections.Generic;

namespace xAPI.Registry {
    /// <summary>
    /// Provides 44 items of the xAPI_Verbs of the context lms as public properties.
    /// </summary>
    public sealed class xAPI_Verbs_Lms : xAPI_Verbs {
        /// <summary>
        /// Actor accessed/viewed a page, file, video, ...
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/accessed
        /// </summary>
        public readonly xAPI_Verb accessed = new xAPI_Verb(
            context: "lms",
            key: "accessed",
            names: new Dictionary<string, string> {
                ["en-US"] = "accessed",
                ["de-DE"] = "zugegriffen" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor accessed/viewed a page, file, video, ...",
                ["de-DE"] = "Akteur hat (auf) eine Seite, eine Datei, ein Video, ... zugegriffen/angesehen" });

        /// <summary>
        /// Actor added an object/person to a collection
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/added
        /// </summary>
        public readonly xAPI_Verb added = new xAPI_Verb(
            context: "lms",
            key: "added",
            names: new Dictionary<string, string> {
                ["en-US"] = "added",
                ["de-DE"] = "hinzugefügt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor added an object/person to a collection",
                ["de-DE"] = "Akteur hat eine/n Person/Gegenstand zu einer Sammlung hinzugefügt" });

        /// <summary>
        /// An Actor gave an answer to a question.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/answered
        /// </summary>
        public readonly xAPI_Verb answered = new xAPI_Verb(
            context: "lms",
            key: "answered",
            names: new Dictionary<string, string> {
                ["en-US"] = "answered",
                ["de-DE"] = "beantwortete" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An Actor gave an answer to a question.",
                ["de-DE"] = "Ein Akteur hat eine Antwort auf eine Frage gegeben." });

        /// <summary>
        /// Actor approved an object (in Moodle "learning plan")
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/approved
        /// </summary>
        public readonly xAPI_Verb approved = new xAPI_Verb(
            context: "lms",
            key: "approved",
            names: new Dictionary<string, string> {
                ["en-US"] = "approved",
                ["de-DE"] = "genehmigt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor approved an object (in Moodle \"learning plan\")",
                ["de-DE"] = "Akteur genehmigte ein Objekt (in Moodle \"Lernplan\")" });

        /// <summary>
        /// Actor assigned an object/task/activity to someone
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/assigned
        /// </summary>
        public readonly xAPI_Verb assigned = new xAPI_Verb(
            context: "lms",
            key: "assigned",
            names: new Dictionary<string, string> {
                ["en-US"] = "assigned",
                ["de-DE"] = "zugeteilt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor assigned an object/task/activity to someone",
                ["de-DE"] = "Akteur wurde ein/e Objekt/Aufgabe/Aktivität zugeteilt" });

        /// <summary>
        /// Actor blocked a person
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/blocked
        /// </summary>
        public readonly xAPI_Verb blocked = new xAPI_Verb(
            context: "lms",
            key: "blocked",
            names: new Dictionary<string, string> {
                ["en-US"] = "blocked",
                ["de-DE"] = "gesperrt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor blocked a person",
                ["de-DE"] = "Akteur hat eine Person gesperrt" });

        /// <summary>
        /// Actor cancelled an activity, a meetin
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/cancelled
        /// </summary>
        public readonly xAPI_Verb cancelled = new xAPI_Verb(
            context: "lms",
            key: "cancelled",
            names: new Dictionary<string, string> {
                ["en-US"] = "cancelled",
                ["de-DE"] = "abgesagt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor cancelled an activity, a meetin",
                ["de-DE"] = "Akteur hat eine Aktivität, ein Meeting abgesagt" });

        /// <summary>
        /// Actor closed an object (forum post)
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/closed
        /// </summary>
        public readonly xAPI_Verb closed = new xAPI_Verb(
            context: "lms",
            key: "closed",
            names: new Dictionary<string, string> {
                ["en-US"] = "closed",
                ["de-DE"] = "geschlossen" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor closed an object (forum post)",
                ["de-DE"] = "Akteur hat ein Objekt (Forumeintrag) geschlossen" });

        /// <summary>
        /// Actor completed an activity/task
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/completed
        /// </summary>
        public readonly xAPI_Verb completed = new xAPI_Verb(
            context: "lms",
            key: "completed",
            names: new Dictionary<string, string> {
                ["en-US"] = "completed",
                ["de-DE"] = "fertiggestellt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor completed an activity/task",
                ["de-DE"] = "Akteur hat eine Aktivität/Aufgabe fertiggestellt" });

        /// <summary>
        /// Actor created an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/created
        /// </summary>
        public readonly xAPI_Verb created = new xAPI_Verb(
            context: "lms",
            key: "created",
            names: new Dictionary<string, string> {
                ["en-US"] = "created",
                ["de-DE"] = "erstellt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor created an object",
                ["de-DE"] = "Akteur hat ein Objekt erstellt" });

        /// <summary>
        /// Actor deleted an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/deleted
        /// </summary>
        public readonly xAPI_Verb deleted = new xAPI_Verb(
            context: "lms",
            key: "deleted",
            names: new Dictionary<string, string> {
                ["en-US"] = "deleted",
                ["de-DE"] = "gelöscht" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor deleted an object",
                ["de-DE"] = "Akteur hat ein Objekt gelöscht" });

        /// <summary>
        /// Actor downloaded an object (e. g. a PDF file)
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/downloaded
        /// </summary>
        public readonly xAPI_Verb downloaded = new xAPI_Verb(
            context: "lms",
            key: "downloaded",
            names: new Dictionary<string, string> {
                ["en-US"] = "downloaded",
                ["de-DE"] = "heruntergeladen" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor downloaded an object (e. g. a PDF file)",
                ["de-DE"] = "Akteur hat ein Objekt (z. B. eine PDF Datei) heruntergeladen" });

        /// <summary>
        /// Actor exported an object/data
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/exported
        /// </summary>
        public readonly xAPI_Verb exported = new xAPI_Verb(
            context: "lms",
            key: "exported",
            names: new Dictionary<string, string> {
                ["en-US"] = "exported",
                ["de-DE"] = "exportiert" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor exported an object/data",
                ["de-DE"] = "Akteur hat ein Objekt/Daten exportiert" });

        /// <summary>
        /// Actor failed an activity (quiz attempt, upload, log in)
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/failed
        /// </summary>
        public readonly xAPI_Verb failed = new xAPI_Verb(
            context: "lms",
            key: "failed",
            names: new Dictionary<string, string> {
                ["en-US"] = "failed",
                ["de-DE"] = "scheiterte" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor failed an activity (quiz attempt, upload, log in)",
                ["de-DE"] = "Akteur scheiterte an einer Aktivität (Quizversuch, Upload, Einloggen)" });

        /// <summary>
        /// Actor rated an object/person
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/graded
        /// </summary>
        public readonly xAPI_Verb graded = new xAPI_Verb(
            context: "lms",
            key: "graded",
            names: new Dictionary<string, string> {
                ["en-US"] = "graded",
                ["de-DE"] = "benotet" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor rated an object/person",
                ["de-DE"] = "Akteur hat ein/e Objekt/Person benotet" });

        /// <summary>
        /// Actor imported an object/data
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/imported
        /// </summary>
        public readonly xAPI_Verb imported = new xAPI_Verb(
            context: "lms",
            key: "imported",
            names: new Dictionary<string, string> {
                ["en-US"] = "imported",
                ["de-DE"] = "importiert" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor imported an object/data",
                ["de-DE"] = "Akteur hat ein Objekt/Daten importiert" });

        /// <summary>
        /// An Actor did not gave an answer to a question. Might occur in quizzes if a question is left open.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/leftUnanswered
        /// </summary>
        public readonly xAPI_Verb leftUnanswered = new xAPI_Verb(
            context: "lms",
            key: "leftUnanswered",
            names: new Dictionary<string, string> {
                ["en-US"] = "left unanswered",
                ["de-DE"] = "ließ unbeantwortet" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "An Actor did not gave an answer to a question. Might occur in quizzes if a question is left open.",
                ["de-DE"] = "Ein Akteur hat keine Antwort auf eine Frage gegeben. Kann z. B. in Tests auftreten, wenn das Antwortfeld einer Frage leer ist." });

        /// <summary>
        /// Actor linked an object/person to an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/linked
        /// </summary>
        public readonly xAPI_Verb linked = new xAPI_Verb(
            context: "lms",
            key: "linked",
            names: new Dictionary<string, string> {
                ["en-US"] = "linked",
                ["de-DE"] = "verknüpft" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor linked an object/person to an object",
                ["de-DE"] = "Akteur hat ein/e Objekt/Person mit einem Objekt verknüpft" });

        /// <summary>
        /// Actor locked an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/locked
        /// </summary>
        public readonly xAPI_Verb locked = new xAPI_Verb(
            context: "lms",
            key: "locked",
            names: new Dictionary<string, string> {
                ["en-US"] = "locked",
                ["de-DE"] = "gesperrt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor locked an object",
                ["de-DE"] = "Akteur hat ein Objekt gesperrt" });

        /// <summary>
        /// Actor logged in (as him-/herself)
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/loggedIn
        /// </summary>
        public readonly xAPI_Verb loggedIn = new xAPI_Verb(
            context: "lms",
            key: "loggedIn",
            names: new Dictionary<string, string> {
                ["en-US"] = "logged in",
                ["de-DE"] = "meldet sich an" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor logged in (as him-/herself)",
                ["de-DE"] = "Akteur hat sich angemeldet (als er/sie selbst)" });

        /// <summary>
        /// Actor logged in as another person
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/loggedInAs
        /// </summary>
        public readonly xAPI_Verb loggedInAs = new xAPI_Verb(
            context: "lms",
            key: "loggedInAs",
            names: new Dictionary<string, string> {
                ["en-US"] = "logged in as",
                ["de-DE"] = "angemeldet als" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor logged in as another person",
                ["de-DE"] = "Akteur hat sich als eine andere Person angemeldet" });

        /// <summary>
        /// Actor logged out
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/loggedOut
        /// </summary>
        public readonly xAPI_Verb loggedOut = new xAPI_Verb(
            context: "lms",
            key: "loggedOut",
            names: new Dictionary<string, string> {
                ["en-US"] = "logged out",
                ["de-DE"] = "abgemeldet" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor logged out",
                ["de-DE"] = "Akteur hat sich abgemeldet" });

        /// <summary>
        /// Actor moved an object/person from one group/area to another
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/moved
        /// </summary>
        public readonly xAPI_Verb moved = new xAPI_Verb(
            context: "lms",
            key: "moved",
            names: new Dictionary<string, string> {
                ["en-US"] = "moved",
                ["de-DE"] = "bewegt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor moved an object/person from one group/area to another",
                ["de-DE"] = "Akteuer hat ein/e Objekt/Person von einer Gruppe/Region in eine andere bewegt" });

        /// <summary>
        /// Actor changed the grading of an object/person.
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/regraded
        /// </summary>
        public readonly xAPI_Verb regraded = new xAPI_Verb(
            context: "lms",
            key: "regraded",
            names: new Dictionary<string, string> {
                ["en-US"] = "regraded",
                ["de-DE"] = "neu benotet" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor changed the grading of an object/person.",
                ["de-DE"] = "Akteur hat ein/e Objekt/Person neu benotet. Eine bestehende Bewertung wurde geändert." });

        /// <summary>
        /// Actor removed an object/person from a collection
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/removed
        /// </summary>
        public readonly xAPI_Verb removed = new xAPI_Verb(
            context: "lms",
            key: "removed",
            names: new Dictionary<string, string> {
                ["en-US"] = "removed",
                ["de-DE"] = "entfernt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor removed an object/person from a collection",
                ["de-DE"] = "Akteur hat ein/e Objekt/Person von einer Sammlung entfernt" });

        /// <summary>
        /// Actor reopened an activity (assignment submission, forum)
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/reopened
        /// </summary>
        public readonly xAPI_Verb reopened = new xAPI_Verb(
            context: "lms",
            key: "reopened",
            names: new Dictionary<string, string> {
                ["en-US"] = "reopened",
                ["de-DE"] = "wiedereröffnet" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor reopened an activity (assignment submission, forum)",
                ["de-DE"] = "Akteuer hat eine Aktivität (Aufgabenabgabe, Forum) wiedereröffnet" });

        /// <summary>
        /// Actor replaced an object/person
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/replaced
        /// </summary>
        public readonly xAPI_Verb replaced = new xAPI_Verb(
            context: "lms",
            key: "replaced",
            names: new Dictionary<string, string> {
                ["en-US"] = "replaced",
                ["de-DE"] = "ersetzt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor replaced an object/person",
                ["de-DE"] = "Akteur hat ein/e Objekt/Person ersetzt" });

        /// <summary>
        /// Actor requested an object/access/information
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/requested
        /// </summary>
        public readonly xAPI_Verb requested = new xAPI_Verb(
            context: "lms",
            key: "requested",
            names: new Dictionary<string, string> {
                ["en-US"] = "requested",
                ["de-DE"] = "angefordert" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor requested an object/access/information",
                ["de-DE"] = "Akteur hat ein/en Objekt/Zugang/Informationen angefordert" });

        /// <summary>
        /// Actor resetted an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/resetted
        /// </summary>
        public readonly xAPI_Verb resetted = new xAPI_Verb(
            context: "lms",
            key: "resetted",
            names: new Dictionary<string, string> {
                ["en-US"] = "resetted",
                ["de-DE"] = "zurückgesetzt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor resetted an object",
                ["de-DE"] = "Akteur hat ein Objekt zurückgesetzt" });

        /// <summary>
        /// Actor restored a previous version of an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/restored
        /// </summary>
        public readonly xAPI_Verb restored = new xAPI_Verb(
            context: "lms",
            key: "restored",
            names: new Dictionary<string, string> {
                ["en-US"] = "restored",
                ["de-DE"] = "wiederhergestellt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor restored a previous version of an object",
                ["de-DE"] = "Akteur hat eine vorherige Version eines Objektes wiederhergestellt" });

        /// <summary>
        /// Actor reviewed an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/reviewed
        /// </summary>
        public readonly xAPI_Verb reviewed = new xAPI_Verb(
            context: "lms",
            key: "reviewed",
            names: new Dictionary<string, string> {
                ["en-US"] = "reviewed",
                ["de-DE"] = "geprüft" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor reviewed an object",
                ["de-DE"] = "Akteur hat ein Objekt geprüft" });

        /// <summary>
        /// Actor searched for something
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/searched
        /// </summary>
        public readonly xAPI_Verb searched = new xAPI_Verb(
            context: "lms",
            key: "searched",
            names: new Dictionary<string, string> {
                ["en-US"] = "searched",
                ["de-DE"] = "gesucht" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor searched for something",
                ["de-DE"] = "Akteur hat nach etwas gesucht" });

        /// <summary>
        /// Actor sent a message to a person/group
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/sent
        /// </summary>
        public readonly xAPI_Verb sent = new xAPI_Verb(
            context: "lms",
            key: "sent",
            names: new Dictionary<string, string> {
                ["en-US"] = "sent",
                ["de-DE"] = "gesendet" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor sent a message to a person/group",
                ["de-DE"] = "Akteur hat eine Nachricht zu einer Person/Gruppe gesendet" });

        /// <summary>
        /// Actor started an activity
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/started
        /// </summary>
        public readonly xAPI_Verb started = new xAPI_Verb(
            context: "lms",
            key: "started",
            names: new Dictionary<string, string> {
                ["en-US"] = "started",
                ["de-DE"] = "gestartet" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor started an activity",
                ["de-DE"] = "Akteur hat eine Aktivität gestartet" });

        /// <summary>
        /// Actor stopped an activity/process
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/stopped
        /// </summary>
        public readonly xAPI_Verb stopped = new xAPI_Verb(
            context: "lms",
            key: "stopped",
            names: new Dictionary<string, string> {
                ["en-US"] = "stopped",
                ["de-DE"] = "gestoppt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor stopped an activity/process",
                ["de-DE"] = "Akteur hat eine/n Aktivität/Prozess gestoppt" });

        /// <summary>
        /// Actor submitted a quiz, assignment, ...
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/submitted
        /// </summary>
        public readonly xAPI_Verb submitted = new xAPI_Verb(
            context: "lms",
            key: "submitted",
            names: new Dictionary<string, string> {
                ["en-US"] = "submitted",
                ["de-DE"] = "gab ab" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor submitted a quiz, assignment, ...",
                ["de-DE"] = "Akteur hat ein Quiz, eine Abgabe, ... abgegeben." });

        /// <summary>
        /// Actor succeeded an activity
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/succeeded
        /// </summary>
        public readonly xAPI_Verb succeeded = new xAPI_Verb(
            context: "lms",
            key: "succeeded",
            names: new Dictionary<string, string> {
                ["en-US"] = "succeeded",
                ["de-DE"] = "gelang" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor succeeded an activity",
                ["de-DE"] = "Akteur ist eine Aktivität gelungen" });

        /// <summary>
        /// Actor triggered a process
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/triggered
        /// </summary>
        public readonly xAPI_Verb triggered = new xAPI_Verb(
            context: "lms",
            key: "triggered",
            names: new Dictionary<string, string> {
                ["en-US"] = "triggered",
                ["de-DE"] = "ausgelöst" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor triggered a process",
                ["de-DE"] = "Akteur hat einen Prozess ausgelöst" });

        /// <summary>
        /// Actor unapproved an object (in Moodle "learning plan")
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unapproved
        /// </summary>
        public readonly xAPI_Verb unapproved = new xAPI_Verb(
            context: "lms",
            key: "unapproved",
            names: new Dictionary<string, string> {
                ["en-US"] = "unapproved",
                ["de-DE"] = "nicht genehmigt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor unapproved an object (in Moodle \"learning plan\")",
                ["de-DE"] = "Akteur hat ein Objekt (in Moodle \"learning plan\") nicht genehmigt" });

        /// <summary>
        /// Actor unblocked a blocked person
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unblocked
        /// </summary>
        public readonly xAPI_Verb unblocked = new xAPI_Verb(
            context: "lms",
            key: "unblocked",
            names: new Dictionary<string, string> {
                ["en-US"] = "unblocked",
                ["de-DE"] = "entblockt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor unblocked a blocked person",
                ["de-DE"] = "Akteur hat eine blockierte Person entblockt" });

        /// <summary>
        /// Actor unlinked an object/person from an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlinked
        /// </summary>
        public readonly xAPI_Verb unlinked = new xAPI_Verb(
            context: "lms",
            key: "unlinked",
            names: new Dictionary<string, string> {
                ["en-US"] = "unlinked",
                ["de-DE"] = "getrennt" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor unlinked an object/person from an object",
                ["de-DE"] = "Akteur hat ein/e Objekt/Person von einem Objekt getrennt" });

        /// <summary>
        /// Actor unlocked an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/unlocked
        /// </summary>
        public readonly xAPI_Verb unlocked = new xAPI_Verb(
            context: "lms",
            key: "unlocked",
            names: new Dictionary<string, string> {
                ["en-US"] = "unlocked",
                ["de-DE"] = "freigegeben" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor unlocked an object",
                ["de-DE"] = "Akteur hat ein Objekt freigegeben" });

        /// <summary>
        /// Actor updated the content of an object
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/updated
        /// </summary>
        public readonly xAPI_Verb updated = new xAPI_Verb(
            context: "lms",
            key: "updated",
            names: new Dictionary<string, string> {
                ["en-US"] = "updated",
                ["de-DE"] = "aktualisiert" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor updated the content of an object",
                ["de-DE"] = "Akteur aktualisierte den Inhalt eines Objekts." });

        /// <summary>
        /// Actor removed the assignment of an object/task/activity to someone
        /// URI: https://xapi.elearn.rwth-aachen.de/definitions/lms/verbs/withdrew
        /// </summary>
        public readonly xAPI_Verb withdrew = new xAPI_Verb(
            context: "lms",
            key: "withdrew",
            names: new Dictionary<string, string> {
                ["en-US"] = "withdrew",
                ["de-DE"] = "zog zurück" },
            descriptions: new Dictionary<string, string> {
                ["en-US"] = "Actor removed the assignment of an object/task/activity to someone",
                ["de-DE"] = "Akteur hat die Zuteilung eines/r Objektes/Aufgabe/Aktivität zurückgezogen" });

        public xAPI_Verbs_Lms() 
            : base("lms") {
        }
    }
}