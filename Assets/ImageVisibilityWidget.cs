﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ImageVisibilityWidget : MonoBehaviour
{
    [Tooltip("Length of hiding animation in seconds.")]
    public float animLenght = .1f;

    [SerializeField] private bool visibility = false;

    [SerializeField, Range(0f, 1f)] private float offOpacity = 0f, onOpacity = 1f;
    
    private Image img;

    public bool Visibility { get => visibility; set => visibility = value; } 

    private void Awake()
    {
        img = GetComponent<Image>();
    }

    private float t;

    private void Update()
    {
        t = Mathf.Clamp01(t + (visibility ? 1f : -1f) * Time.deltaTime * (1f / animLenght));
        img.color = new Color(
            img.color.r, img.color.g, img.color.b,
            Mathf.SmoothStep(onOpacity, offOpacity, t)
            );
    }
}
