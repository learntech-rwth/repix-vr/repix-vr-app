## Steps to reproduce the bug

## Resulting and expected behaviour

## Screen recording or screenshot

## System information

- Operating system: Windows 10
- VR Mode: [ ]
- VR Equipment: HTC Vive Pro

## Impact [If known]